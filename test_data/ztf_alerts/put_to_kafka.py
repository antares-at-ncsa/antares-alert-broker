import os

from antares.config import config
from antares.services import kafka


def main():
    topic = config.INPUT_KAFKA_TOPICS[0]
    kafka_producer = kafka.KafkaProducer()
    for blob in get_avro_blobs():
        kafka_producer.produce(topic, blob)
    kafka_producer.flush()


def get_avro_blobs():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    for fname in os.listdir(dir_path):
        if fname.endswith(".avro"):
            yield get_avro_blob(fname)


def get_avro_blob(filename):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(dir_path, filename), "rb") as f:
        return f.read()


if __name__ == "__main__":
    main()
