Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

v1.7.0 (03/29/2021)
-------------------

Added
~~~~~

- (AN-769) Add details about watch objects to Slack notifications

Fixed
~~~~~

- (AN-819) Clicking on active search filter text removes the filter
- (AN-775) Updated paper link
- (AN-822) Dependency issues with image builds

v1.6.2 (02/03/2021)
-------------------

Fixed
~~~~~

- Remove `pip debug` call

v1.6.1 (02/03/2021)
-------------------

Added
~~~~~

- Build script to deploy to datalab also deploys to gp12

v1.6.0 (02/03/2021)
-------------------

Added
~~~~~

- API: Expose `/catalogs` and `/loci/<locus_id>/catalog-matches` endpoints
- FRONTEND: Notes about citing ANTARES
- FRONTEND: Catalog cross-matches to locus pages

Changed
~~~~~~~

- Remove unnecessary configuration of DataDog API key

Fixed
~~~~~

- FRONTEND: Filter out negative magnitudes from plots

v1.5.0 (01/19/2021)
-------------------

Changed
~~~~~~~

- Deploys against `v2.2.0` of the ANTARES Helm chart.

- FRONTEND: Limit number of significant digits displayed in the alert table on the
  frontend's locus detail page.

Added
~~~~~

- FRONTEND: Runtime configuration settings for the frontend exposed in
  `frontend/public/config.json`.

Fixed
~~~~~

- FRONTEND: Issue where long tag names broke layout on locus detail page.
