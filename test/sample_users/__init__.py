import os

from antares.sql import engine
from antares.sql.schema import SUser


def bootstrap():
    with engine.session() as session:
        for user in USERS:
            session.add(SUser(**user))
        session.commit()


USERS = [
    {
        "username": "user",
        "password": "password",
        "name": "Sample User",
        "email": "user@noao.edu",
        "staff": False,
        "admin": False,
    },
    {
        "username": "staff",
        "password": "password",
        "name": "Sample Staff Member",
        "email": "staff@noao.edu",
        "staff": True,
        "admin": False,
    },
    {
        "username": "admin",
        "password": "password",
        "name": "Sample Administrator",
        "email": "admin@noao.edu",
        "staff": True,
        "admin": True,
    },
]
