import antares.devkit as dk


class Require2Mags(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def run(self, locus):
        """
        Require 2+ ztf_magpsf measurements in the lightcurve.
        This excludes upper-limits from the count, which don't have magpsf.
        """
        n = sum(1 for a in locus.alerts if a.properties.get("ztf_magpsf"))
        if n < 2:
            raise locus.halt
