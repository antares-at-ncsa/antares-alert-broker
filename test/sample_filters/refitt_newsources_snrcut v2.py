def refitt_newsources_snrcut(locus):
    # check if this source is nuclear, SSO, or known variable star
    streams = set(locus.get_locus_streams())
    bad_cats = {
        "nuclear_transient",
        "high_amplitude_variable_stars",
        #'ztf_sso_candidates',
    }
    if streams & bad_cats:
        return

    snr_threshold = {1: 5.0, 2: 5.0}
    p = locus.get_properties()
    alert_snr = 1.0 / p["ztf_sigmapsf"]
    if (
        p["ztf_fid"] in snr_threshold.keys()
        and alert_snr > snr_threshold[p["ztf_fid"]]
        and p["ztf_distnr"] > 1.0
        and p["ztf_distpsnr1"] > 2.0
    ):
        locus.send_to_stream("refitt_newsources_snrcut")
