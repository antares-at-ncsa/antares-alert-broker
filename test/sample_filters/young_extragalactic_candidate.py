# Imports
import astropy
import numpy as np

ERROR_SLACK_CHANNEL = "UMWERJYM8"


def young_extragalactic_candidate(locus_data, verbose=False):

    """
    A filter for young extragalactic transient candidates.
    Filters out stars and alerts near the Galactic plane. This
    filter casts a very wide net with NO catalog cross matching
    (as opposed to the `extragalactic` stream), thus downstream
    filtering will be required.
    Contact: michael.stroh@northwestern.edu
    """

    # Alert is rejected if the absolute value of the galactic latitude (bii) is less than this value (degrees)
    gal_latitude_cut_degrees = 15.0

    # Alerts are rejected if a previous alert for this object was found more than this number of days previous to the sampled alert.
    previous_alert_cut_days = 14.0

    # Star criteria based on conversations with Adam Miller
    #     Must be > ztf_sgscore_cut and proximity <= ztf_distpsnr_cut
    ztf_sgscore_cut = (
        0.5  # > 0.5 likely star; = 0.5 unknown due to Pan-STARRS data quality issues
    )
    ztf_distpsnr_cut = 1.0  # arcseconds from closest Pan-STARRS source.

    if verbose:
        print("`young_extragalactic_candidate` is running...")

    ##################################################
    #
    # Get a dict of all properties on the new alert.
    #

    p = locus_data.get_properties()

    #
    ##################################################

    ##################################################
    #
    # Is this source near the Galactic Plane?
    #

    p_coordinate_object = astropy.coordinates.SkyCoord(
        ra=p["ra"],
        dec=p["dec"],
        unit="deg",
        frame=astropy.coordinates.FK5,
        equinox="J2000.0",
    )

    #
    # Stop if this is near the Galactic plane
    #
    if abs(p_coordinate_object.galactic.b.deg) <= gal_latitude_cut_degrees:
        if verbose:
            print(
                "ZTF alert within {0:.1f} degrees of the Galactic plane: {1:.1f} degrees".format(
                    gal_latitude_cut_degrees, p_coordinate_object.galactic.b.deg
                )
            )
        return

    #
    ##################################################

    ##################################################
    #
    # Ignore if a ZTF alert has been made more
    # than 14 days prior to this alert.
    #

    history_check_ld = locus_data.get_time_series("ztf_magpsf")

    #
    # Previous listings could be due to non-detections.
    # Remove these from consideration.
    #

    historical_detection_indices = np.arange(history_check_ld[2].size)[
        ~np.isnan(history_check_ld[2].astype(np.float64))
    ]

    #
    # Ensure that this is a relatively new transient
    #
    if (historical_detection_indices.size > 0) and (
        p["mjd"] - np.min(history_check_ld[1][historical_detection_indices])
        > previous_alert_cut_days
    ):
        if verbose:
            print(
                "Previous ZTF alerts found more than {0:.1f} days prior to this alert: {1:.1f} days".format(
                    previous_alert_cut_days, p["mjd"] - np.min(history_check_ld[1])
                )
            )
        return

    #
    ##################################################

    ##################################################
    #
    # Use Tachibana & Miller (2018) to filter out stars
    #

    #
    # It is possible to have multiple PS DR1 associations
    # within 1", so loop through all to be on the safe
    # side. Many of these are screened out in PS DR2 (and
    # have sgscore=0.5 which isn't particularly useful), so
    # this may be removed if compared against that release.
    #
    for ps_id in range(1, 4):
        if (
            "ztf_sgscore{0}".format(ps_id) in p.keys()
            and p["ztf_sgscore{0}".format(ps_id)] > ztf_sgscore_cut
            and "ztf_distpsnr{0}".format(ps_id) in p.keys()
            and p["ztf_distpsnr{0}".format(ps_id)] <= ztf_distpsnr_cut
        ):
            if verbose:
                print(
                    "Satisfies star classification criteria from Tachibana & Miller (2018): ztf_sgscore{0}={1:.2f}, ztf_distpsnr{0}={2:.2f}".format(
                        ps_id,
                        p["ztf_sgscore{0}".format(ps_id)],
                        p["ztf_distpsnr{0}".format(ps_id)],
                    )
                )
            return

    #
    ##################################################

    if verbose:
        print("Candidate found: {0}".format(p["alert_id"]))

    ##################################################
    #
    # Record useful information and send to the stream
    # - None for now
    #

    #
    # Send to stream
    #
    locus_data.send_to_stream("young_extragalactic_candidate")

    #
    ##################################################
