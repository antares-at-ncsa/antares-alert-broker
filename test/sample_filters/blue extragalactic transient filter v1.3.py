# Imports
import astropy
import numpy as np

ERROR_SLACK_CHANNEL = "UMWERJYM8"


def blue_extragalactic_transient_filter_v1p3(locus_data):
    """
    A preliminary filter for blue extragalactic transients
    """
    # Alert is rejected if the absolute value of the galactic latitude (bii) is less than this value (degrees)
    gal_latitude_cut_degrees = 15.0

    # Alerts are rejected if a previous alert for this object was found more than this number of days previous to the sampled alert.
    previous_alert_cut_days = 14.0

    # Star criteria based on conversation with Adam Miller on October 2nd, 2019
    #     Must be > ztf_sgscore_cut and proximity <= ztf_distpsnr_cut
    ztf_sgscore_cut = (
        0.5  # > 0.5 likely star; = 0.5 unknown due to Pan-STARRS data quality issues
    )
    ztf_distpsnr_cut = 1.0  # arcseconds from closest Pan-STARRS source.

    # Screen out g-R comparisons when alerts are from more than this number of days apart
    delta_days_in_corresponding_filter = 4.0

    # Color selection criteria for correlated alerts.
    #     Rejecting if color >= ztf_g_R_cut
    ztf_g_R_cut = 0.0

    print("`blue_extragalactic_transient_filter_v1p3` is running...")

    ##################################################
    #
    # Get a dict of all properties on the new alert.
    #

    p = locus_data.get_properties()

    #
    ##################################################

    ##################################################
    #
    # Is this source near the Galactic Plane?
    #

    p_coordinate_object = astropy.coordinates.SkyCoord(
        ra=p["ra"],
        dec=p["dec"],
        unit="deg",
        frame=astropy.coordinates.FK5,
        equinox="J2000.0",
    )

    # Stop if this is a Galactic target
    if abs(p_coordinate_object.galactic.b.deg) <= gal_latitude_cut_degrees:
        # print( 'ZTF alert within {0:.1f} degrees of the Galactic plane: {1:.1f} degrees'.format( gal_latitude_cut_degrees, p_coordinate_object.galactic.b.deg ) )
        return

    #
    ##################################################

    ##################################################
    #
    # Ignore if a ZTF alert has been made more than 14 days prior to this alert.
    #

    history_check_ld = locus_data.get_time_series("ztf_magpsf")

    # There is no other alert related to this ld
    #     Cannot:
    #             Track rise / decay
    #             Create a color based classification

    if history_check_ld[0].size == 1:
        # print( 'This is the only ZTF alert for this locus.' )
        return

    # Previous listings could be due to non-detections. Remove these from consideration.
    historical_detection_indices = np.arange(history_check_ld[2].size)[
        ~np.isnan(history_check_ld[2].astype(np.float64))
    ]
    if historical_detection_indices.size == 0:
        # print( 'No other detections exist for this locus.' )
        return

    elif (historical_detection_indices.size > 0) and (
        p["mjd"] - np.min(history_check_ld[1][historical_detection_indices])
        > previous_alert_cut_days
    ):
        # print( 'Previous ZTF alerts found more than {0:.1f} days prior to this alert: {1:.1f} days'.format( previous_alert_cut_days, p['mjd'] - np.min( history_check_ld[1] ) ) )
        return

    #
    ##################################################

    ##################################################
    #
    # Use Tachibana & Miller (2018) to filter out stars
    #

    if (
        "ztf_sgscore1" in p.keys()
        and p["ztf_sgscore1"] > ztf_sgscore_cut
        and "ztf_distpsnr1" in p.keys()
        and p["ztf_distpsnr1"] <= ztf_distpsnr_cut
    ):
        # print('Satisfies star classification criteria from Tachibana & Miller (2018): ztf_sgscore1={0:.2f}, ztf_distpsnr1={1:.2f}'.format( p['ztf_sgscore1'], p['ztf_distpsnr1'] ))
        return

    #
    ##################################################

    ##################################################
    #
    # Are there ZTF alerts in the other filter?
    #

    # If g check for r
    if p["ztf_fid"] == 1:
        check_fid = 2

    # If r check for g
    else:
        check_fid = 1

    #
    # Query associations in the complementary filter
    #     check_ld.size will be a multiple of 2 + the number of fields requested
    #     Indexing is required on this array since it does not have keys, unlike the locus_data.get_properties()
    #
    check_ld = locus_data.get_time_series(
        "ra", "dec", "ztf_magpsf", "ztf_sigmapsf", filters={"ztf_fid": check_fid}
    )

    #
    # Ignore nan entries
    #
    check_ld_good_indices = np.arange(check_ld[4].size)[
        ~np.isnan(check_ld[4].astype(np.float64))
    ]

    #
    # No corresponding detections in the alternative filter
    #
    if check_ld[0][check_ld_good_indices].size == 0:
        # print('No detections in the other filter ({0}).'.format(check_passband))
        return

    else:

        ##############################################
        #
        # Find index of temporally closest observation in the other filter
        #     This is needed for the following steps
        #
        temporally_closest_previous_alert_index = check_ld_good_indices[
            np.argmin(p["mjd"] - check_ld[1][check_ld_good_indices])
        ]

        #
        # Ensure that we are comparing [z]-[R] when taken with a relatively close temporal proximity
        #
        if (
            np.abs(p["mjd"] - check_ld[1][temporally_closest_previous_alert_index])
            > delta_days_in_corresponding_filter
        ):
            # print('Closest z and R alerts were taken too far apart: {0:.1f} > {1:.1f}'.format( np.abs( p['mjd'] - check_ld[1][ temporally_closest_previous_alert_index ] ), delta_days_in_corresponding_filter))
            return

        #
        # Confirmed detections in this alternative filter previous to this alert and taken within a resonable time period.
        #
        else:

            # print( 'Alerts {0} days apart.'.format( p['mjd'] - check_ld[1][ temporally_closest_previous_alert_index ] ) )

            ##########################################
            #
            # Look for cases where g and R were taken within 4 days of each other
            #
            # Create ZTF g-R:
            #
            # print( 'Check filter mag: {0:f}'.format( check_ld[4][ temporally_closest_previous_alert_index ] ) )
            if p["ztf_fid"] == 1:
                ztf_g_R = (
                    p["ztf_magpsf"]
                    - check_ld[4][temporally_closest_previous_alert_index]
                )
            else:
                ztf_g_R = (
                    check_ld[4][temporally_closest_previous_alert_index]
                    - p["ztf_magpsf"]
                )

            #
            # Always keep track of error propogation
            #
            ztf_g_R_error = np.sqrt(
                p["ztf_sigmapsf"] ** 2
                + check_ld[5][temporally_closest_previous_alert_index] ** 2
            )

            #
            # This is a blue source
            #
            if ztf_g_R >= ztf_g_R_cut:

                return

            #
            ##########################################

        #
        ##############################################

    #
    ##################################################

    print("Potential match {0}".format(p["alert_id"]))

    ##################################################
    #
    # Record useful information and send to the stream
    #

    #
    # Add color properties
    #
    locus_data.set_property("g_minus_r", ztf_g_R)
    locus_data.set_property("g_minus_r_err", ztf_g_R_error)

    #
    # Send to stream
    #
    locus_data.send_to_stream("blue_extragalactic_transient_filter")

    #
    ##################################################
