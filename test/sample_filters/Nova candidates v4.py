import numpy as np
import pandas as pd


def nov_filter(LD):
    TS_map = {
        "id": str,
        "mjd": float,
        "ra": float,
        "dec": float,
        "ztf_fid": int,
        "ztf_magpsf": float,
        "ztf_sigmapsf": float,
        "ztf_isdiffpos": str,
        "ztf_distnr": float,
        "ztf_magzpsci": float,
    }

    TS_rows = [
        "id",
        "mjd",
        "ztf_fid",
        "ztf_magpsf",
        "ztf_sigmapsf",
        "ztf_isdiffpos",
        "ztf_distnr",
        "ztf_magzpsci",
    ]  # allow nan for distnr, but nan not allowed for any other property

    TS = LD.get_time_series(*TS_rows[2:], sep=",")
    PD = pd.DataFrame(TS, index=TS_rows)

    ### mask away any alerts whose property other than distnr is nan
    col_masks = []
    for i in range(len(TS_rows)):
        temp = ~(pd.isnull(PD.loc[TS_rows[i]]).values)
        if TS_rows[i] == "ztf_distnr":
            temp = np.ones(len(TS[i])).astype(bool)
        col_masks.append(temp)

    col_masks = np.array(col_masks)
    mask = []
    for i in range(col_masks.shape[1]):
        mask.append(np.all(col_masks[:, i]))

    mask = np.array(mask)
    TS_new = PD.T[mask].T.values

    ###mask away old alerts without magzpsci
    # zp = TS[7].astype(float)
    # mm_zp = np.isfinite(zp)
    # TS_new = TS[:, mm_zp]

    DT = {}
    for i in range(len(TS_rows)):
        DT[TS_rows[i]] = TS_new[i].astype(TS_map[TS_rows[i]])

    ###convert to flux
    # flux = 10.0**((DT['ztf_magzpsci']-DT['ztf_magpsf'])/2.5)
    # mm = np.ones(len(flux))
    # mm[DT['ztf_isdiffpos']=='f'] = -1.0
    # flux = flux*mm
    # flux_err = DT['ztf_sigmapsf']*(np.log(10)/2.5)*np.abs(flux)

    x = DT["mjd"]
    y = DT["ztf_magpsf"]
    # yerr = DT['ztf_sigmapsf']
    pb = DT["ztf_fid"]

    astro_objects = LD.get_astro_object_matches()
    two_mass_check = 0
    if "2mass_psc" in astro_objects.keys():
        two_mass_check = 1  # want 0

    positive_check = False  # want True, most alerts are brightening
    if np.sum(DT["ztf_isdiffpos"] == "t") >= 0.75 * len(DT["ztf_isdiffpos"]):
        positive_check = True

    no_nearby_refsrc_check = False  # want True
    if (
        np.all(np.isnan(DT["ztf_distnr"])) == True
        or DT["ztf_distnr"][np.isfinite(DT["ztf_distnr"])].mean() > 2.0
    ):
        no_nearby_refsrc_check = True

    color_check = False  # want True
    if np.sum(pb == 1) > 1 and np.sum(pb == 2) > 0:

        g_x_i = x[pb == 1]
        g_y_i = y[pb == 1]
        R_x_i = x[pb == 2]
        R_y_i = y[pb == 2]

        idx = np.argsort(g_x_i)
        g_x = g_x_i[idx]
        g_y = g_y_i[idx]
        idx = np.argsort(R_x_i)
        R_x = R_x_i[idx]
        R_y = R_y_i[idx]

        dt = []
        dc = []  ###make dc as flux ratio rather than sum
        for a, b in zip(g_x, g_y):
            dt_i = np.abs(a - R_x)
            dc_i = b - R_y
            if min(dt_i) <= 1.0:  ##within one day
                dt.append(min(dt_i))
                dc.append(dc_i[np.argmin(dt_i)])

        if len(dc) > 1:
            col_evol = np.argsort(
                dc
            )  ###want dc decreasing chronologically, most of the time if not at all times
            ori_idx = np.arange(len(dc))
            idx_diff = col_evol[::-1] - ori_idx
            if np.sum(idx_diff == 0) >= 0.75 * len(dc):
                color_check = True

    if (
        two_mass_check == 0
        and positive_check == True
        and color_check == True
        and no_nearby_refsrc_check == True
    ):
        # print ("NOVA")
        LD.send_to_stream("nova_test")
