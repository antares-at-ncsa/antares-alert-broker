import antares.devkit as dk


class BadSeeing(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def run(self, locus):
        a = locus.alert.properties
        try:
            if a.get("ztf_fwhm") is not None:
                assert a["ztf_fwhm"] <= 5.0
            if a.get("ztf_elong") is not None:
                assert a["ztf_elong"] <= 1.2
        except AssertionError:
            raise locus.halt
