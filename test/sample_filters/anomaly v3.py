import io
import itertools
import json

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from antares import utils

DATAFILE_VERSION = "v2"
ANTARES_STORAGE_PREFIX = f"monika/anomaly/{DATAFILE_VERSION}/"

RUN_RATE = 10  # Run on only every 10th Alert

fontsize = 14
plt.rcParams["font.size"] = fontsize
plt.rcParams["axes.labelsize"] = fontsize
plt.rcParams["axes.titlesize"] = fontsize
plt.rcParams["legend.fontsize"] = fontsize - 2
plt.rcParams["xtick.labelsize"] = fontsize
plt.rcParams["ytick.labelsize"] = fontsize
plt.rcParams["figure.figsize"] = (7, 5.8)

zero_dm = 1.0e-30
zero_log_dt = -7.0


class dmt:
    def __init__(self, obj, date, mag, mag_err, passband, color_code):  # alert_id=None
        self.objname = obj  # locus_id as string
        # self.alert_id=alert_id.astype(int)
        self.date = date.astype(float)
        self.mag = mag.astype(float)
        self.mag_err = mag_err.astype(float)
        self.passband = passband.astype(str)

        self.filters = (np.unique(passband)).astype(str)
        self.color_code = color_code

    def nocorr_bimod_structure_funct(self):
        F = list(self.filters)  # unique passbands
        out = {}
        F_series = {}
        for i in range(len(F)):
            if np.sum(self.passband == F[i]) >= 1:
                temp = {}
                date_per_F = self.date[self.passband == F[i]]
                mag_per_F = self.mag[self.passband == F[i]]
                magerr_per_F = self.mag_err[self.passband == F[i]]

                indices = np.argsort(date_per_F)
                dates = date_per_F[indices]
                mags = mag_per_F[indices]
                magerr = magerr_per_F[indices]

                ## need to remove entries that have same date: they are artifacts of Difference Imaging
                mask = np.ones(len(dates), dtype=bool)
                for k in range(len(dates)):
                    if list(dates).count(dates[k]) > 1:
                        mask[k] = False

                dates = dates[mask]
                mags = mags[mask]
                magerr = magerr[mask]

                if len(dates) >= 2:

                    log_delta_t = np.log10(np.diff(dates))
                    if np.sum(np.isinf(log_delta_t)) > 0:
                        print("CAUTION repeated obsmjds")

                    # RIGHT way
                    delta_mag = np.diff(mags)
                    log_dt = np.array(log_delta_t)
                    dmag = np.array(delta_mag)
                    mask = ~np.isinf(log_dt)
                    log_dt = log_dt[mask]
                    dmag = dmag[mask]
                    indices = np.argsort(log_dt)
                    log_dt = log_dt[indices]
                    dmag = dmag[indices]
                    if len(log_dt) > 0:
                        temp["dmag"] = list(dmag)
                        temp["log_dt"] = list(log_dt)
                        out[F[i]] = temp
                    else:
                        print("len(log_dt) is 0 for ", F[i])

                F_series[F[i]] = {"t": dates, "m": mags, "e_m": magerr}

        for val in self.color_code.values():
            avail_pbs = list(F_series.keys())
            if val[0] not in avail_pbs or val[1] not in avail_pbs:
                continue
            temp = {}

            idxes = pd.DataFrame(
                list(
                    itertools.product(
                        np.arange(len(F_series[val[0]]["t"])),
                        np.arange(len(F_series[val[1]]["t"])),
                    )
                ),
                columns=["l1", "l2"],
            )
            temp_log_dt = np.log10(
                np.abs(
                    F_series[val[0]]["t"][idxes["l1"][:]]
                    - F_series[val[1]]["t"][idxes["l2"][:]]
                )
            )
            temp_dc = (
                F_series[val[0]]["m"][idxes["l1"][:]]
                - F_series[val[1]]["m"][idxes["l2"][:]]
            )
            idx = np.argsort(temp_log_dt)
            c_log_dt = temp_log_dt[idx]
            dc = temp_dc[idx]
            mask = np.isinf(c_log_dt)
            c_log_dt[mask] = zero_log_dt

            temp["dmag"] = list(dc)
            temp["log_dt"] = list(c_log_dt)
            out[val[0] + "-" + val[1]] = temp

        return out


# function to compute the score
def get_score_normalized(test_class, pdf_models, expected_D):
    score = {}
    for K, V in test_class.items():
        Sum = 0.0
        for key, val in pdf_models.items():
            X = np.array(V[key]["x"])
            Y = np.array(V[key]["y"])

            mask = Y == -9999  # missing information as opposed to out of range
            X = X[~mask]
            Y = Y[~mask]

            if len(Y) < 1:
                # print ("SAISE", key, V[key]["y"])
                continue

            idx_i = np.digitize(X, val["log_dt_bin_edges"])

            if 0 in idx_i or len(val["log_dt_bin_edges"]) in idx_i:
                # if time bins are outside, we can't do anything, it's a property of survey rather than source
                mask = (idx_i == len(val["log_dt_bin_edges"])) + (idx_i == 0)
                idx = idx_i[
                    np.where((idx_i != 0) * (idx_i != len(val["log_dt_bin_edges"])))
                ]
                Y = Y[np.where((idx_i != 0) * (idx_i != len(val["log_dt_bin_edges"])))]

            if 0 not in idx_i and len(val["log_dt_bin_edges"]) not in idx_i:
                idx = idx_i

            uniq_idx = np.unique(idx)

            for k in range(len(uniq_idx)):
                mask = idx == uniq_idx[k]
                dm = Y[mask]
                indices = np.digitize(dm, val[str(uniq_idx[k])]["dm_bin_edges"])
                expectation = expected_D[key][str(uniq_idx[k])]
                if (0 in indices) or (
                    len(val[str(uniq_idx[k])]["dm_bin_edges"]) in indices
                ):
                    mask = (indices == 0) + (
                        indices == len(val[str(uniq_idx[k])]["dm_bin_edges"])
                    )
                    MSK = indices[
                        np.where(
                            (indices != 0)
                            * (indices != len(val[str(uniq_idx[k])]["dm_bin_edges"]))
                        )
                    ]
                    Sum = Sum + (np.sum(mask)) * (
                        np.log10(zero_dm) - np.log10(expectation)
                    )

                    if len(MSK) == 0:
                        continue

                if (
                    0 not in indices
                    and len(val[str(uniq_idx[k])]["dm_bin_edges"]) not in indices
                ):
                    MSK = indices

                Sum = Sum + np.sum(
                    np.log10(val[str(uniq_idx[k])]["dm_hist"][MSK - 1])
                    - np.log10(expectation)
                )

        score[K] = Sum  # Score for each lightcurve id

    return score


PASSBAND = ["g", "R"]
CC = {1: ["g", "R"]}
GRAND_PB = PASSBAND + ["-".join(v) for v in CC.values()]


# Load PDF Model from files
def _make_pdf_model():
    model = {}
    for K in GRAND_PB:
        model[K] = {}
        with engine.alert_db_session() as s:
            temp = json.loads(
                Storage.get(s, ANTARES_STORAGE_PREFIX + f"anmly_{K}_logdt_dm.json")
            )
        for key, Val in temp.items():
            if key == "log_dt_bin_edges":
                model[K][key] = np.array(Val)
            else:
                model[K][key] = {}
                model[K][key]["dm_hist"] = np.array(Val["dm_hist"])
                model[K][key]["dm_bin_edges"] = np.array(Val["dm_bin_edges"])
    return model


PDF_MODEL = _make_pdf_model()


# Load training sample file
def _get_trained_locus():
    with engine.alert_db_session() as s:
        trained_variables = np.load(
            file=io.BytesIO(
                Storage.get(s, ANTARES_STORAGE_PREFIX + "anmly_training_sample.npy")
            )
        )
        _trained_locus = []
        for A in trained_variables:
            _trained_locus.append(int(A.split(".csv")[0]))
        return np.array(_trained_locus)


TRAINED_LOCUS_IDS = _get_trained_locus()


def anomaly(LD):
    THRES = -2000
    alert_id_hash = utils.md5_int(str(LD.get_properties()["alert_id"]))
    if alert_id_hash % RUN_RATE != 0:
        # Skip this alert
        return

    # compute the normalization factor to be used in computing score
    expected_density = {}
    for key, val in PDF_MODEL.items():
        expected_density[key] = {}
        for K, V in val.items():
            if K == "log_dt_bin_edges":
                continue
            dm_hist = np.array(V["dm_hist"])
            dm_bin_edges = np.array(V["dm_bin_edges"])
            expectation = np.sum(
                dm_hist ** 2.0 * (dm_bin_edges[1:] - dm_bin_edges[0:-1])
            )
            expected_density[key][K] = expectation

    # Never run this filter on a locus used in constructing the PDF model
    if LD.get_properties()["locus_id"] in TRAINED_LOCUS_IDS:
        return

    MAG = []
    DMAG = []
    MJD = []
    passband = []

    mag, sigmag, mjd, _, _ = LD.get_varstar_corrected_mags(fid=1)
    MAG += list(mag)
    DMAG += list(sigmag)
    MJD += list(mjd)
    passband += len(mjd) * ["g"]
    mag, sigmag, mjd, _, _ = LD.get_varstar_corrected_mags(fid=2)
    MAG += list(mag)
    DMAG += list(sigmag)
    MJD += list(mjd)
    passband += len(mjd) * ["R"]

    MAG = np.array(MAG)
    DMAG = np.array(DMAG)
    MJD = np.array(MJD)
    passband = np.array(passband)
    mask = (MAG < 22.0) * (DMAG < 1.0)

    if np.sum(mask) == 0:
        return

    thisobj = dmt(
        LD.get_properties()["locus_id"],
        MJD[mask],
        MAG[mask],
        DMAG[mask],
        passband[mask],
        CC,
    )

    b = thisobj.nocorr_bimod_structure_funct()

    out = {}
    out[thisobj.objname] = {}

    for i in range(len(GRAND_PB)):
        out[thisobj.objname][GRAND_PB[i]] = {"x": [-9999], "y": [-9999]}
    for key, val in b.items():
        if key in GRAND_PB:
            out[thisobj.objname][key]["x"] = (
                out[thisobj.objname][key]["x"] + val["log_dt"]
            )
            out[thisobj.objname][key]["y"] = (
                out[thisobj.objname][key]["y"] + val["dmag"]
            )

    SCORE = get_score_normalized(out, PDF_MODEL, expected_density)
    score = list(SCORE.values())[0]
    LD.set_property("anomaly_dmdt_score", score)
    if score < THRES:
        LD.send_to_stream("anomaly_test")
        return
