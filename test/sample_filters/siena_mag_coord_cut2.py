def siena_mag_coord_cut2(ld):
    """
    Send alerts to stream 'Siena_mag_coord_cut2' if Alert is less than
    17 apparent magnitude in r, and if RA/Dec limits are met.
    """
    mag_max = 17
    dec_min = 0
    dec_max = 90
    ra_min = 75
    ra_max = 180
    p = ld.get_properties()
    ra = p["ra"]
    dec = p["dec"]

    # Get arrays of corrected mag and sigmag, if possible.
    # This function only works on variable stars, otherwise it returns
    # ZTF's `magpsf` and `sigmagpsf` without modification.
    mag, sigmag, mjd, is_var_star, corrected = ld.get_varstar_corrected_mags()
    alert_mag = mag[-1]

    if alert_mag < mag_max and dec_max > dec > dec_min and ra_max > ra > ra_min:
        ld.send_to_stream("siena_mag_coord_cut2")
