ERROR_SLACK_CHANNEL = "UP414JK1D"  # my slack id, instead of name

# wyrzykowski_bright_microlensing_v9
# uses g and r light curves, requires 10 points in each band
# excludes known variable stars from ASASSN and high amplitude variables from other stream
# fits microlensing curve

from scipy.stats import skew
import numpy as np
from scipy.optimize import leastsq


def ulens_fixedbl(t, t0, te, u0, I0, dummy):
    fs = 1.0
    tau = (t - t0) / te
    x = tau
    y = u0
    u = np.sqrt(x ** 2 + y ** 2)
    ampl = (u ** 2 + 2) / (u * np.sqrt(u ** 2 + 4))
    F = ampl * fs + (1 - fs)
    I = I0 - 2.5 * np.log10(F)
    return I


def fit_ulensfixedbl(epoch, avmag, err):
    # catching short light curves:
    if len(epoch) < 10:
        return [999, 999, 999, 999], 1999999.0
    t0 = epoch[np.argmin(avmag)]
    te = 50.0
    u0 = 0.1
    I0 = np.amax(avmag)
    x = epoch
    y = avmag
    ulensparam = [t0, te, u0, I0]
    fp = lambda v, x: ulens_fixedbl(x, v[0], v[1], v[2], v[3])
    e = lambda v, x, y, err: ((fp(v, x) - y) / err)
    v, success = leastsq(e, ulensparam, args=(x, y, err), maxfev=1000000)
    # no solution:
    if success == 0:
        return ulensparam, 999999.0
    chi2 = sum(e(v, x, y, err) ** 2)
    chi2dof = sum(e(v, x, y, err) ** 2) / (len(x) - len(v))
    out = []
    for t in v:
        out.append(t)
    out.append(chi2)
    out.append(chi2dof)
    return out, chi2dof


# computes merged chi^2 for two functions
def residtwo(avmag1, err1, y1, avmag2, err2, y2):
    diff1 = (y1 - avmag1) / err1
    diff2 = (y2 - avmag2) / err2
    return np.concatenate((diff1, diff2))


# fits two curves from two bands with one model
def fit_ulensfixedbltwo(epoch1, avmag1, err1, epoch2, avmag2, err2):
    # catching short light curves:
    if len(epoch1) < 10 or len(epoch2) < 10:
        return [999, 999, 999, 999, 999], 1999999.0
    t0 = epoch2[np.argmin(avmag2)]
    te = 50.0
    u0 = 0.1
    G0 = np.amax(avmag1)
    R0 = np.amax(avmag2)
    x1 = epoch1
    y1 = avmag1
    x2 = epoch2
    y2 = avmag2
    ulensparam = [t0, te, u0, G0, R0]
    fp1 = lambda v, x: ulens_fixedbl(x, v[0], v[1], v[2], v[3], v[4])
    fp2 = lambda v, x: ulens_fixedbl(x, v[0], v[1], v[2], v[4], v[3])
    e = lambda v, x1, x2, y1, y2, err1, err2: residtwo(
        avmag1, err1, fp1(v, epoch1), avmag2, err2, fp2(v, epoch2)
    )
    v, success = leastsq(
        e, ulensparam, args=(x1, x2, y1, y2, err1, err2), maxfev=1000000
    )
    # no solution:
    if success == 0:
        return ulensparam, 999999.0
    chi2 = sum(e(v, x1, x2, y1, y2, err1, err2) ** 2)
    chi2dof = sum(e(v, x1, x2, y1, y2, err1, err2) ** 2) / (len(x1) + len(x2) - len(v))
    out = []
    for t in v:
        out.append(t)
    out.append(chi2)
    out.append(chi2dof)
    return out, chi2dof


def bright_microlensing(ld):
    props = ld.get_properties()
    alert_id = props["alert_id"]

    skewThreshold = 0
    etaThreshold = 0.3
    chi2Threshold = 10.0
    u0Threshold = 0.3
    teThreshold = 1000.0

    # r-band only check
    _, mjd = ld.get_time_series(filters={"ztf_fid": 2})
    # these are all measurements above the baseline from difference imaging, so we can ask only for 4
    if len(mjd) < 4:
        print("less than 4 alerts in r-band")
        return

    # need to make sure this filter is run at stage 3 after we've associated solar system sources
    streams = ld.get_locus_streams()
    streams = set(streams)
    bad_streams = set(["high_amplitude_variable_stars", "ztf_known_solar_system"])
    # if this locus is associated with a var star or a known solar system catalog
    # we ignore it
    if not streams.isdisjoint(bad_streams):
        print("Locus associated with stream of var stars or solar system objects")
        return
    # if we get to here, we want to make sure the source is actually in the post-starburst catalog
    ld_cats = ld.get_astro_object_matches().keys()
    ld_cats = set(ld_cats)

    # check that this locus is NOT associated with ANY variable (not just high amp)
    bad_cats = set(["asassn_variable_catalog"])
    if ld_cats & bad_cats:
        print("Not microlensing, associated with variable source")
        return

    stream_name = "bright_microlensing"

    # g-band data
    mags1, errs1, times1, is_var_star1, corrected1 = ld.get_varstar_corrected_mags(
        fid=1
    )
    # r-band data
    mags2, errs2, times2, is_var_star2, corrected2 = ld.get_varstar_corrected_mags(
        fid=2
    )

    # these measurements will also include baseline so we require at least 10
    if len(times1) < 10 or len(times2) < 10:
        print(
            "MICROLENSING FILTER: Requiring at least 10 points with the baseline in g- and r-band"
        )
        return

    # computing stats
    nrtrid1 = len(times1)
    eta1 = (
        1.0
        / (nrtrid1 - 1.0)
        / np.var(mags1)
        * np.sum((mags1[1:] - mags1[:-1]) * (mags1[1:] - mags1[:-1]))
    )
    # skewness
    skewness1 = skew(mags1)
    nrtrid2 = len(times2)
    eta2 = (
        1.0
        / (nrtrid2 - 1.0)
        / np.var(mags2)
        * np.sum((mags2[1:] - mags2[:-1]) * (mags2[1:] - mags2[:-1]))
    )
    # skewness
    skewness2 = skew(mags2)
    rmax = np.amax(mags2)
    gmax = np.amax(mags1)

    #    print(times1, mags1)
    #    print(times2, mags2)

    # brightness cut only in r
    if (
        rmax > 17.0
        or skewness1 > skewThreshold
        or eta1 > etaThreshold
        or skewness2 > skewThreshold
        or eta2 > etaThreshold
    ):
        print(
            alert_id,
            " MICROLENSING FILTER: alert not bright enough or not skewed enough, ",
            rmax,
            skewness1,
            eta1,
            gmax,
            skewness2,
            eta2,
        )
        return

    # skew+vonN criteria ok, now fitting both bands

    fitparams, chi2dof = fit_ulensfixedbltwo(times1, mags1, errs1, times2, mags2, errs2)
    t0 = fitparams[0]
    te = np.abs(fitparams[1])
    u0 = np.abs(fitparams[2])
    G0 = np.abs(fitparams[3])
    R0 = np.abs(fitparams[4])
    print(alert_id, " MICROLENSING FILTER: fitting ", fitparams, chi2dof)
    if np.abs(u0) < u0Threshold and chi2dof < chi2Threshold and te < teThreshold:
        ld.set_property("t0", t0)
        ld.set_property("te", te)
        ld.set_property("u0", u0)
        ld.set_property("R0", R0)
        ld.set_property("G0", G0)
        ld.set_property("chi2", chi2dof)
        ld.send_to_stream(stream_name)
