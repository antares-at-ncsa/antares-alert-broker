from statsmodels.stats.weightstats import DescrStatsW

import antares.devkit as dk


class HighAmp(dk.Filter):
    ERROR_SLACK_CHANNEL = None
    REQUIRED_ALERT_PROPERTIES = ["ztf_fid"]
    OUTPUT_TAGS = [
        {
            "name": "high_amplitude",
            "description": "todo",
        },
        {
            "name": "high_amplitude_variable_stars",
            "description": "todo",
        },
    ]

    def run(self, locus):
        threshold = 1.0  # in magnitude unit, and same for all filters
        fid = locus.alert.properties["ztf_fid"]

        df = locus.timeseries.to_pandas()
        df = df[df.ztf_fid == fid]  # Filter dataframe to rows matching `fid`
        if len(df) < 2:
            # Locus has < 2 measurements with fid matching the current alert fid.
            return

        mag = df["ant_mag"][-1]  # Should always exist
        magerr = df["ant_magerr"][-1]  # Should always exist
        mag_corrected = df["ant_mag_corrected"][-1]  # Might be None
        magerr_corrected = df["ant_magerr_corrected"][-1]  # Might be None

        if mag_corrected and magerr_corrected:
            tag = "high_amplitude_variable_stars"
            mag = mag_corrected
            magerr = magerr_corrected
        else:
            tag = "high_amplitude"

        W = 1.0 / magerr ** 2.0
        des = DescrStatsW(mag, weights=W)
        if des.std > threshold:
            locus.tag(tag)
