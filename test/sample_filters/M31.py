import antares.devkit as dk


class InM31(dk.Filter):
    ERROR_SLACK_CHANNEL = None
    OUTPUT_TAGS = [
        {
            "name": "in_m31",
            "description": "Loci within a box around M31",
        }
    ]

    def run(self, locus):
        ra_min = 9.934793
        ra_max = 11.434793
        dec_min = 40.269065
        dec_max = 42.269065

        ra = locus.alert.properties["ant_ra"]
        dec = locus.alert.properties["ant_dec"]
        if ra_max > ra > ra_min and dec_max > dec > dec_min:
            locus.tag("in_m31")
