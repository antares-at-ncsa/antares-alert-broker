def nuclear_transient(locus):
    """
    Send alert to stream 'Nuclear Transient' if it is within 0.6 arcseconds of a
    source in the ZTF reference frame. It is also required that a match within
    1" of a known Pan-STARRS galaxy (ztf_distpsnr1 < 1. and ztf_sgscore1<0.3).
    To further remove small flux fluctuaion, we also require magpsf (alert PSF
    photometry) - magnr (PSF photometry of the nearby source in the reference
    image) > 1.5. The selection criteria are from Sjoert van Velzen et al.
    (2018, arXiv:1809.02608), section 2.1.
    """
    try:
        alert_props = locus.get_properties()
        sgscore = alert_props["ztf_sgscore1"]
        distpsnr = alert_props["ztf_distpsnr1"]
        magpsf = alert_props["ztf_magpsf"]
        magnr = alert_props["ztf_magnr"]
        distnr = alert_props["ztf_distnr"]
    except KeyError:
        return  # Skip
    if None in (distnr, distpsnr, sgscore, magpsf, magnr):
        return

    if distnr < 0.6 and distpsnr < 1.0 and sgscore < 0.3 and magpsf - magnr < 1.5:
        locus.send_to_stream("nuclear_transient")
