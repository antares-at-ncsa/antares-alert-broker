def post_starburst_xmatch(ld):
    """
    Send alert to stream 'tde_candidate' if it matches French and Zabludoff (2018) catalog.
    - Gautham Narayan (github: gnarayan)
    20190911
    """

    # need to make sure this filter is run at stage 3 after we've associated solar system sources
    streams = ld.get_locus_streams()
    streams = set(streams)
    bad_streams = set(["high_amplitude_variable_stars", "ztf_known_solar_system"])

    # if this locus is associated with a var star or a known solar system catalog
    # then even if it is associated with a potential post-starburst, we should ignore
    # TDE are long-lived enough that a single alert associated with a known solar system object
    # will take care of itself on the next observation
    # and if there is a high-amplitude variable at the same xmatch tolerance
    # then assume we're seeing something related to the common variable star acting up
    # rather than rare TDE behavior
    if not streams.isdisjoint(bad_streams):
        print("Locus associated with stream of var stars or solar system objects")
        return

    # if we get to here, we want to make sure the source is actually in the post-starburst catalog
    ld_cats = ld.get_astro_object_matches().keys()
    ld_cats = set(ld_cats)

    # check that this locus is associated with a post-starburst galaxy
    # and is NOT associated with ANY variable (not just high amp) or known QSO
    good_cats = set(["french_post_starburst_gals"])
    bad_cats = set(["veron_agn_qso", "asassn_variable_catalog"])

    if (ld_cats & good_cats) and not (ld_cats & bad_cats):
        # if yes, then flag this is a possible TDE candidate
        ld.send_to_stream("tde_candidate")
    else:
        print("Not in TDE cat, or associated with variable source")
        return
