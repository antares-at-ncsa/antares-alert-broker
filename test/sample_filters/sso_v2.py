import numpy as np
from astroquery.jplhorizons import Horizons
from requests.exceptions import ConnectionError, SSLError


def translate_identifier(ztf_name):
    targetname = ztf_name
    # comet designation
    if "/" in ztf_name:
        targetname = ztf_name[:6] + " " + ztf_name[6:]
    elif "-" in ztf_name:
        targetname = ztf_name
    # designation
    elif len(ztf_name) > 4 and ztf_name[4].isalpha():
        if float(ztf_name[6:]) == 0:
            targetname = ztf_name[:4] + " " + ztf_name[4:6]
        else:
            targetname = (
                ztf_name[:4] + " " + ztf_name[4:6] + str(int(float(ztf_name[6:])))
            )
    return targetname


def ztf_known_solar_system(ld):
    p = ld.get_properties()

    # Check for required parameters
    if p.get("ztf_magzpsci") is None or p.get("ztf_nmatches") is None:
        return
    if (
        p.get("ztf_isdiffpos") is None
        or p.get("ztf_nbad") is None
        or p.get("ztf_rb") is None
        or p.get("ztf_ssdistnr") is None
    ):
        return
    if p.get("ztf_ssnamenr") is None or p.get("ztf_jd") is None:
        return

    # reject unreliable detections
    if (
        p["ztf_isdiffpos"] == "f"
        or p["ztf_isdiffpos"] == 0
        or p["ztf_nbad"] > 0
        or p["ztf_rb"] < 0.65
        or p["ztf_ssdistnr"] == -999
    ):
        return  # Skip this Alert

    # Send to ztf_sso_candidates
    ld.send_to_stream("ztf_sso_candidates")

    # check positional agreement and positional uncertainties
    targetname = translate_identifier(p["ztf_ssnamenr"])
    epoch = p["ztf_jd"]
    hor = Horizons(id=targetname, location="I41", epochs=[epoch + 30 / 2 / 86400])
    try:
        eph = hor.ephemerides()
    except ValueError:
        return  # Skip this Alert
    except (ConnectionError, SSLError, Exception):
        return  # Failed to connect to JPL

    # Save Horizons properties in ANTARES
    ld.set_property("horizons_targetname", targetname)
    new_properties = [
        ("horizons_absolutemag", "H"),
        ("horizons_slopeparameter", "G"),
        ("horizons_predictedmagnitude", "V"),
        ("horizons_heliodist_au", "r"),
        ("horizons_observerdist_au", "delta"),
        ("horizons_solarphaseangle", "alpha"),
        ("horizons_solarelongation", "elong"),
        ("horizons_ra_posunc_3sig_arcsec", "RA_3sigma"),
        ("horizons_dec_posunc_3sig_arcsec", "DEC_3sigma"),
        ("horizons_true_anomaly", "true_anom"),
    ]
    for prop, var in new_properties:
        try:
            ld.set_property(prop, float(eph[var][0]))
        except (IndexError, ValueError, KeyError):
            pass

    if (
        np.sqrt((eph["RA"][0] - p["ra"]) ** 2 + (eph["DEC"][0] - p["dec"]) ** 2) * 3600
        > 1
    ):
        return  # Skip this Alert

    if np.sqrt(eph["RA_3sigma"][0] ** 2 + eph["DEC_3sigma"][0] ** 2) > 1:
        return  # Skip this Alert

    # Send to ztf_sso_confirmed
    ld.send_to_stream("ztf_sso_confirmed")
