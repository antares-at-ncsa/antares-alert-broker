import antares.devkit as dk


# Import Filter to test that load.py correctly identifies the filter subclass below
from antares.devkit import Filter
from antares.pipeline.ingestion import is_ztf_candidate

print(Filter)


class Foo(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "foo",
            "type": "int",
            "description": "blah",
        },
        {
            "name": "a_keyword_property",
            "type": "str",
            "description": "blah",
            "es_mapping": {
                "type": "keyword",
                "eager_global_ordinals": True,
            },
        },
    ]
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "foo",
            "type": "int",
            "description": "blah",
        },
        {
            "name": "bar",
            "type": "int",
            "description": "blah",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "hello",
            "description": "hi!",
        },
    ]
    REQUIRES_FILES = [
        "cstubens/foo_File.txt",
    ]

    def setup(self):
        data = self.files["cstubens/foo_File.txt"]
        assert data == b"testing\ntesting\n", data

    def run(self, locus):
        locus.properties["foo"] = 12345.6
        assert locus.properties["foo"] == 12345

        locus.properties["a_keyword_property"] = "X"
        assert locus.properties["a_keyword_property"] == "X"

        # Test that the Timeseries object gets updated as properties are set.
        ts = locus.timeseries
        locus.alert.properties["foo"] = 678
        assert locus.alert.properties["foo"] == 678
        assert ts["foo"][-1] == 678
        locus.alert.properties["bar"] = 10
        assert locus.alert.properties["bar"] == 10
        assert ts["bar"][-1] == 10

        locus.tag("hello")
        assert "hello" in locus.tags
