import astropy.time as atime
import datetime
import numpy as np
import astropy.coordinates as coords
from collections import Counter

ERROR_SLACK_CHANNEL = "UES8764SY"  # Gautham


def check_if_possible_young_sn(locus_data):

    alertid, mjd, fid, mag, magerr, survey = locus_data.get_time_series(
        "ztf_fid", "ztf_magpsf", "ztf_sigmapsf", "survey"
    )
    alertid = np.array(alertid)
    mjd = np.array(mjd)
    fid = np.array(fid)
    mag = np.array(mag)
    magerr = np.array(magerr)
    survey = np.array(survey)

    idet = survey == 1
    inondet = ~idet
    nobs = len(alertid)
    mjd_nondet = mjd[inondet]
    if len(mjd_nondet) > 0:
        mjd_last_nondet = max(mjd_nondet)
    else:
        mjd_last_nondet = -99

    locus_data.set_property("last_nondetection", mjd_last_nondet)

    alertid = alertid[idet]
    mjd = mjd[idet]
    fid = fid[idet]
    mag = mag[idet]
    magerr = magerr[idet]
    survey = survey[idet]
    ndet = len(survey)

    # lots of stages might want number of detections on locus up to this point - store it
    locus_data.set_property("ndet", ndet)

    # Alert is rejected if the absolute value of the galactic latitude (bii) is less than this value (degrees)
    gallat_cut = 15.0

    # Star criteria based on conversations with Adam Miller
    #     Must be > ztf_sgscore_cut and proximity <= ztf_distpsnr_cut
    ztf_sgscore_cut = (
        0.5  # > 0.5 likely star; = 0.5 unknown due to Pan-STARRS data quality issues
    )
    ztf_distpsnr_cut = 1.0  # arcseconds from closest Pan-STARRS source.

    p = locus_data.get_properties()
    pos = coords.SkyCoord(
        ra=p["ra"], dec=p["dec"], unit="deg", frame=coords.FK5, equinox="J2000.0"
    )
    if abs(pos.galactic.b.deg) <= gallat_cut:
        # don't bother with things near the plane
        locus_data.set_property(
            "snfilter_last_proc_status", "Too close to galactic plane"
        )
        return

    # this is a cut from Michael Stroh (Northwestern) that uses Tachibana and Miller PS1 criterion
    # to separate out stars - it's eminently sensible, so including it here
    for ps_id in range(1, 4):
        if (
            "ztf_sgscore{0}".format(ps_id) in p.keys()
            and p["ztf_sgscore{0}".format(ps_id)] > ztf_sgscore_cut
            and "ztf_distpsnr{0}".format(ps_id) in p.keys()
            and p["ztf_distpsnr{0}".format(ps_id)] <= ztf_distpsnr_cut
        ):
            locus_data.set_property(
                "snfilter_last_proc_status", "Alert probably from a star"
            )
            return

    # filter out "OLD" alerts
    # in case we are re-ingesting or doing testing, we don't want the output stream to have stale alerts
    # get the current UTC time
    mjd = np.array(mjd)
    tnow = atime.Time(datetime.datetime.utcnow()).mjd
    if np.abs(mjd.max() - tnow) > 7:
        locus_data.set_property(
            "snfilter_last_proc_status",
            "Most recent alert at this position is more than a week old. Ignoring.",
        )
        return

    # look for any previous variability at the astro object catalog level
    matching_catalog_names = locus_data.get_astro_object_matches().keys()

    var_cats = [
        "veron_agn_qso",
        "asassn_variable_catalog",
        "asassn_variable_catalog_v2_20190802",
        "sdss_stars",
        "bright_guide_star_cat",
    ]
    if set(matching_catalog_names) & set(var_cats):
        # this locus is associated with a previously variable source, even if we don't have history
        locus_data.set_property(
            "snfilter_last_proc_status",
            "Locus associated with source in catalog of known variables (Veron/ASASSN)",
        )
        return

    # check if this source is nuclear
    streams = locus_data.get_locus_streams()
    streams = set(streams)
    bad_cats = set(
        ["nuclear_transient", "high_amplitude_variable_stars", "ztf_known_solar_system"]
    )
    if not streams.isdisjoint(bad_cats):
        locus_data.set_property(
            "snfilter_last_proc_status",
            "Locus associated with stream of nuclear sources, var stars or solar system objects",
        )
        return

    # These are the catalogs (ANTARES-based names) with extended sources
    xsc_cats = [
        "2mass_xsc",
        "ned",
        "nyu_valueadded_gals",
        "sdss_gals",
        "french_post_starburst_gals",
    ]
    # check if there is a plausible host, but we shouldn't demand this because the catalogs themselves are incomplete
    exgal = 0
    if set(matching_catalog_names) & set(xsc_cats):
        exgal = 1
    # we don't filter on if exgal, but we can set the property so people can filter on this downstream
    locus_data.set_property("snfilter_known_exgal", exgal)

    # don't use alerts with not much history
    # this implies images, not detections - i.e.
    if nobs < 3:
        locus_data.set_property(
            "snfilter_last_proc_status",
            "Locus has fewer than 3 observations total. Ignoring.",
        )
        return

    # now we bifurcate
    # DESI is going off only ZTF, so we demand at least two detections - any filter
    # YSE has both ZTF and YSE detections, which they can check themselves
    # so they don't demand at least two detections at the cost of having to sort out more stuff

    if ndet >= 2:
        # if there are 2 detections any filters in the past 7 days
        # let's check if their brightness is about 20.5 for DESI
        if mag[-1] <= 20.5:
            locus_data.set_property(
                "snfilter_last_proc_status",
                "Locus has two or more detections and is in DESI brightness range. Triggering.",
            )
            locus_data.send_to_stream("desi_candidate_test")
        else:
            locus_data.set_property(
                "snfilter_last_proc_status",
                "Locus has two or more detections but not brighter than 20.5",
            )

    # even if YSE is NOT interested in having multiple detections
    # if there are two or more detections in each filter, we can be helpful and compute rise rate
    # also while they don't mind a minimum of 1 detection, they may not want a source with too many detections
    # We want to make sure we don't cut on time difference of detections because that'd remove things like 2009ip
    # but we can put a limit on a total number of detections to get ride of var stars/quasars
    if ndet >= 10:
        locus_data.set_property(
            "snfilter_last_proc_status",
            "Locus has several detections. It should have previously triggered. Ignoring.",
        )
        return

    fid = np.array(fid)
    filt_cts = Counter(fid).most_common()
    sn_rising = -99
    for filt, nfilt in filt_cts:
        snfilter_rising = -99
        if nfilt < 2:
            # cannot compute a rate with only one observation
            locus_data.set_property(f"snfilter_fid{filt}_rising", snfilter_rising)
            continue

        # get the measurements in this filter
        ind = fid == filt
        fmag = mag[ind]
        fmjd = mjd[ind]

        dm = np.ediff1d(fmag)
        dt = np.ediff1d(fmjd)
        ind_sep = dt > 0.0
        dm = dm[ind_sep]
        dt = dt[ind_sep]
        if len(dm) == 0:
            continue

        dmdt = dm / dt
        most_recent_rise = dmdt[-1]
        median_rise_rate = np.median(dmdt)
        best_rise_guess = min(most_recent_rise, median_rise_rate)

        if best_rise_guess >= 0.04:
            # source is fading
            snfilter_rising = -1
        elif best_rise_guess <= 0.04:
            # source is rising fast
            snfilter_rising = 1
        else:
            # source might be near max - not account for 2nd maximum here
            snfilter_rising = 0

        # we need to make a choice of if source is rising overall - just take the max to be conservative
        # better to have something that is fading that is accidentally flagged
        # than miss something that is rising that was not
        sn_rising = max(sn_rising, snfilter_rising)
        locus_data.set_property(f"snfilter_fid{filt}_rising", snfilter_rising)

    if ndet >= 2:
        if sn_rising >= 0:
            if exgal == 1:
                locus_data.set_property(
                    "snfilter_last_proc_status", "Locus rising and known exgal"
                )
            else:
                locus_data.set_property(
                    "snfilter_last_proc_status", "Locus rising but no catalog match"
                )
            locus_data.send_to_stream("yse_candidate_test")
        else:
            # source fading - do nothing
            pass
    else:
        # only one detection but the YSE folks don't mind since they have their own images to validate against
        locus_data.send_to_stream("yse_candidate_test")

    # potentially add color cut here
    return
