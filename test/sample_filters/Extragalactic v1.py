"""
First version of Extragalactic.
"""

import antares.devkit as dk


EXTENDED_SOURCE_CATALOGS = [
    "2mass_xsc",
    "ned",
    "nyu_valueadded_gals",
    "sdss_gals",
    "veron_agn_qso",
    "RC3",
]


class Extragalactic(dk.Filter):
    ERROR_SLACK_CHANNEL = None
    OUTPUT_TAGS = [
        {
            "name": "extragalactic",
            "description": "Loci which match any of our extended source catalogs.",
        }
    ]

    def run(self, ld):
        """
        Tag Locus 'extragalactic' if it matched any extended source catalogs.
        """
        if set(ld.catalog_objects.keys()) & set(EXTENDED_SOURCE_CATALOGS):
            ld.tag("extragalactic")
