ERROR_SLACK_CHANNEL = "UERMCJQ1W"


def dwarf_nova_outburst(ld):
    """
    Send alert to stream 'dwarf_nova_outburst' if it matches dwarf nova outburst criteria prepared by Paula Szkody.
    """
    p = ld.get_properties()
    filteron = False
    annotations = {}
    calccount = 10000
    is_star = False
    is_asteroid = True
    real = False
    hostgr = -99
    blue_host = False
    bright = False
    brightstar = False
    hostless = False
    rapid = False
    dt = -99
    dmag = -99
    dt_lim = 5.0
    # prevcandidates = current_observation['prv_candidates']
    # replace this one with get_time_series. Just want to make sure it's more than 2 detection
    pc_id, pc_mjd, pc_fid, pc_magpsf, pc_isdiffpos, pc_diffmaglim = ld.get_time_series(
        "ztf_fid", "ztf_magpsf", "ztf_isdiffpos", "ztf_diffmaglim"
    )
    m_now = p["ztf_magpsf"]
    m_app = p["ztf_magap"]
    t_now = p["ztf_jd"] - 2400000.5
    fid_now = p["ztf_fid"]
    sgscore = p["ztf_sgscore1"]
    sgscore2 = p["ztf_sgscore2"]
    sgscore3 = p["ztf_sgscore3"]
    srmag = p["ztf_srmag1"]
    srmag2 = p["ztf_srmag2"]
    srmag3 = p["ztf_srmag3"]
    sgmag = p["ztf_sgmag1"]
    rbscore = p["ztf_rb"]
    magnr = p["ztf_magnr"]
    distnr = p["ztf_distnr"]
    distpsnr1 = p["ztf_distpsnr1"]
    distpsnr2 = p["ztf_distpsnr2"]
    distpsnr3 = p["ztf_distpsnr3"]
    ssname = p["ztf_ssnamenr"]
    scorr = p["ztf_scorr"]
    fwhm = p["ztf_fwhm"]
    elong = p["ztf_elong"]
    nbad = p["ztf_nbad"]
    chipsf = p["ztf_chipsf"]
    psfminap = m_now - m_app
    if p["ztf_isdiffpos"] and p["ztf_isdiffpos"] == "t":
        if rbscore and rbscore > 0.1 and fwhm > 0.5 and nbad < 5:
            if not (
                (
                    distpsnr1
                    and srmag
                    and distpsnr1 < 20
                    and srmag < 15.0
                    and srmag > 0
                    and sgscore > 0.49
                )
                or (
                    distpsnr2
                    and srmag2
                    and distpsnr2 < 20
                    and srmag2 < 15.0
                    and srmag2 > 0
                    and sgscore2 > 0.49
                )
                or (
                    distpsnr3
                    and srmag3
                    and distpsnr3 < 20
                    and srmag3 < 15.0
                    and srmag3 > 0
                    and sgscore3 > 0.49
                )
            ):
                real = True
                calccount -= 2
            calccount -= 2
        calccount -= 2
    if m_now and m_now <= 19:
        bright = True
        calccount -= 2
    if real and bright:
        if sgscore and distpsnr1 and sgscore > 0.49 and distpsnr1 < 1:
            if sgmag > 0 and srmag > 0:
                hostgr = sgmag - srmag
                if hostgr < 0.6:
                    is_star = True
                    calccount -= 2
                calccount -= 3
            calccount -= 2
        if distpsnr1 and distpsnr1 > 3:
            hostless = True
            calccount -= 2
        if is_star or hostless:
            if len(pc_mjd) > 1:
                for j in range(len(pc_mjd) - 1):
                    calccount -= 4
                    dt = t_now - pc_mjd[j]
                    if dt > 0.02 and ssname == "null":
                        is_asteroid = False
                        calccount -= 2
                    #                    if (pc_mjd[j] and pc_magpsf[j] and pc_fid[j] and pc_isdiffpos[j] and pc_isdiffpos[j] == 't'):
                    #                    if (pc_magpsf[j] != 'nan' and pc_isdiffpos[j] == 't'):
                    if pc_magpsf[j] != "nan":
                        if pc_isdiffpos[j] == "t":
                            if pc_fid[j] == fid_now:
                                dmag = pow(pow((m_now - pc_magpsf[j]), 2), 0.5)
                                if dmag >= 2.0 and dt <= dt_lim:
                                    rapid = True
                                    calccount -= 2
                                calccount -= 3
                            calccount -= 3
                    else:
                        # This should be an upper limit
                        assert pc_diffmaglim[j] != None
                        if is_star:
                            if fid_now == 1:
                                dmag = sgmag - m_now
                                calccount -= 2
                            if fid_now == 2:
                                dmag = srmag - m_now
                                calccount -= 2
                            # comment out the following part as we do not have i-band in the public survey.
                            #                            if (fid_now == 3):
                            #                                dmag = simag - m_now
                            #                                calccount -= 2
                            if dmag >= 2.0 and dt <= dt_lim:
                                rapid = True
                                calccount -= 2
                            calccount -= 5
                        if hostless:
                            dmag = pc_diffmaglim[j] - m_now
                            if dmag >= 2.0 and dt <= dt_lim:
                                rapid = True
                                calccount -= 2
                            calccount -= 3
                        calccount -= 3
                    if calccount < 0:
                        break
                calccount -= 2
            calccount -= 2
        calccount -= 4
    # below are useful annotations that can be outputed to the stream, but we skip it for now.
    #    annotations['FWHM'] = fwhm
    #    annotations['host g-r'] = hostgr
    #    annotations['time difference'] = dt
    #    annotations['mag difference'] = dmag
    #    annotations['host ZTF ref PSF r-mag'] = magnr
    #    annotations['PS1 psf r-mag'] = srmag
    #    annotations['PS1 psf g-mag'] = sgmag
    #    annotations['rb score'] = rbscore
    #    annotations['sgscore1'] = sgscore
    #    annotations['ZOGI scorr'] = scorr
    #    annotations['distpsnr1'] = distpsnr1
    #    annotations['magpsf'] = m_now
    #    annotations['elongation'] = elong
    #    annotations['magap_min_magpsf'] = psfminap
    #    annotations['is_asteroid'] = is_asteroid
    #    filteron = rapid and ((not is_asteroid))
    #    return filteron,annotations
    if rapid and ((not is_asteroid)):
        ld.send_to_stream("dwarf_nova_outburst")
