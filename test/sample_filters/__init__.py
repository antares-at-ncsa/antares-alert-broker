import os

from antares.sql import engine
from antares.sql.schema import SFilter, SFilterVersion


def bootstrap():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    for dic in filters:
        with engine.session() as s:
            s.add(SFilter(**dic))
            s.commit()

    for dic in versions_manifest:
        dic = dic.copy()
        with open(os.path.join(dir_path, dic.pop("_file")), "r") as f:
            code = f.read()
        with engine.session() as s:
            fv = SFilterVersion()
            fv.code = code
            for k in dic:
                setattr(fv, k, dic[k])
            s.add(fv)
            s.commit()
            SFilterVersion.enable(fv.filter_version_id)


filters = [
    {
        "filter_id": 1,
        "name": "Foo",
        "description": "Description of Foo filter",
        "level": 2,
    },
    {
        "filter_id": 2,
        "name": "Bad Detection v2",
        "description": "Discard alerts that are likely bogus, have bad pixels or a large difference between aperture and PSF magnitudes.",
        "level": 1,
        "priority": 1,
    },
    {
        "filter_id": 3,
        "name": "Bad Seeing",
        "description": "Discard alerts with poor seeing or elongated sources.",
        "level": 1,
        "priority": 3,
    },
    {
        "filter_id": 4,
        "name": "Require 2+ Measurements",
        "description": "Halt the pipeline now if this is the first Alert on this Locus.",
        "level": 2,
        "priority": 1,
    },
    {
        "filter_id": 5,
        "name": "M31",
        "description": "Detect alerts within a square box around M31.",
        "level": 2,
    },
    {
        "filter_id": 6,
        "name": "Extragalactic v2",
        "description": "Send to stream `extragalactic` if the Alert is associated with a known galaxy from a catalog.",
        "level": 2,
    },
    {
        "filter_id": 7,
        "name": "High Amp v2",
        "level": 2,
    },
]


versions_manifest = [
    {
        "filter_id": 1,
        "user_id": 1,
        "comment": "First version of Foo",
        "_file": "Foo.py",
    },
    {
        "filter_id": 1,
        "user_id": 1,
        "comment": "2nd version of Foo",
        "_file": "Foo.py",
    },
    {
        "filter_id": 2,
        "_file": "Bad Detection v2.py",
    },
    {
        "filter_id": 3,
        "_file": "Bad Seeing.py",
    },
    {
        "filter_id": 4,
        "_file": "Require 2+ Measurements.py",
    },
    {
        "filter_id": 5,
        "_file": "M31.py",
    },
    {
        "filter_id": 6,
        "comment": "Initial version",
        "_file": "Extragalactic v1.py",
    },
    {
        "filter_id": 6,
        "comment": (
            "Removed docstring and, yes, this is a very long comment because I "
            "need a very long comment for testing purposes, I don't think anyone "
            "will leave a comment this long but we should be able to handle it "
            "because it's the max size our DB supports"
        ),
        "_file": "Extragalactic v2.py",
    },
    {
        "filter_id": 7,
        "_file": "High Amp v2.py",
    },
    # These will need to have companion filters created and the level/priority will
    # need to be moved there.
    # {
    #     "name": "Nuclear Transient",
    #     "handler": "nuclear_transient",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "High Flux",
    #     "handler": "high_flux_ratio_stream_v2",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "siena_mag_coord_cut2",
    #     "handler": "siena_mag_coord_cut2",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "Nova candidates v4",
    #     "handler": "nov_filter",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "anomaly v3",
    #     "handler": "anomaly",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "refitt_newsources_snrcut v2",
    #     "handler": "refitt_newsources_snrcut",
    #     "level": 2,
    #     "priority": 10
    # },
    # {
    #     "name": "Potential TDE v1",
    #     "handler": "post_starburst_xmatch",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "astrorapid v9",
    #     "handler": "rapid_stage",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "wyrzykowski_bright_microlensing_v9",
    #     "handler": "bright_microlensing",
    #     "level": 2,
    #     "priority": None
    # },
    # {
    #     "name": "High SNR v2",
    #     "handler": "high_snr",
    #     "level": 2,
    #     "priority": 5
    # },
    # {
    #     "name": "blue extragalactic transient filter v1.3",
    #     "handler": "blue_extragalactic_transient_filter_v1p3",
    #     "level": 2,
    #     "priority": None
    # },
    # {
    #     "name": "young_extragalactic_candidate",
    #     "handler": "young_extragalactic_candidate",
    #     "level": 2,
    #     "priority": None
    # },
    # {
    #     "name": "sso_v2",
    #     "handler": "ztf_known_solar_system",
    #     "level": 1,
    #     "priority": 2
    # },
    # {
    #     "name": "young_transient_candidate_v0.5",
    #     "handler": "check_if_possible_young_sn",
    #     "level": 2,
    #     "priority": None
    # },
    # {
    #     "name": "dwarf_nova_outburst_v6",
    #     "handler": "dwarf_nova_outburst",
    #     "level": 2,
    #     "priority": None
    # }
]
