import antares.devkit as dk


class BadDetection(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def run(self, locus):
        a = locus.alert.properties
        try:
            if a.get("ztf_rb") is not None:
                assert a["ztf_rb"] >= 0.55
            if a.get("ztf_nbad") is not None:
                assert a["ztf_nbad"] == 0
            if a.get("ztf_magdiff") is not None:
                assert -1.0 <= a["ztf_magdiff"] <= 1.0
        except AssertionError:
            raise locus.halt
