import base64
import pickle
import traceback
import time
import numpy as np
from astrorapid.classify import Classify
from astroquery.irsa_dust import IrsaDust
import astropy.coordinates as coord
import astropy.units as u
import dustmaps.sfd


# Send error notifications to Slack
# ERROR_SLACK_CHANNEL = '#astrorapid'

# Run this filter only once at a time within each ANTARES process
# RUN_IN_MUTEX = True

# Classifiers indexed by bool value of `known_redshift`
classifiers = {}


def setup():
    # Reset Tensorflow
    from keras import backend as K

    K.clear_session()

    # Load models from disk
    print(classifiers)
    classifiers[True] = Classify(known_redshift=True)
    classifiers[True].model._make_predict_function()
    classifiers[False] = Classify(known_redshift=False)
    classifiers[False].model._make_predict_function()
    print(classifiers)

    # Load dustmaps
    from dustmaps.config import config

    config["data_dir"] = "/static_files/"  # production
    # config['data_dir'] = '/tmp/'  # datalab
    dustmaps.sfd.fetch()


def delete_indexes(deleteindexes, *args):
    newarrs = []
    for arr in args:
        newarr = np.delete(arr, deleteindexes)
        newarrs.append(newarr)

    return newarrs


def rapid_stage(locus_data):
    locus_properties = locus_data.get_properties()
    objid = locus_properties["alert_id"]
    ra = locus_properties["ra"]
    dec = locus_properties["dec"]

    # Don't classify variable objects. Look for any previous variability at the astro object catalog level
    matching_catalog_names = locus_data.get_astro_object_matches().keys()
    var_cats = ["veron_agn_qso", "asassn_variable_catalog"]
    if set(matching_catalog_names) & set(var_cats):
        # this locus is associated with a previously variable source, even if we don't have history
        locus_data.set_property("astrorapid_skipped", "obj is known variable")
        return

    # Don't classify objects within the galactic plane
    coo = coord.SkyCoord(ra * u.deg, dec * u.deg, frame="icrs")
    b = coo.galactic.b.value
    if abs(b) < 15:
        locus_data.set_property("astrorapid_skipped", "ignore galactic plane")
        return

    # Get Milkyway extinction
    coo = coord.SkyCoord(ra * u.deg, dec * u.deg, frame="icrs")
    sfd = dustmaps.sfd.SFDQuery()
    mwebv = sfd(coo)

    # Get redshift from SDSS_gals
    catalog_matches = locus_data.get_astro_object_matches()
    if "sdss_gals" in catalog_matches:
        redshift = catalog_matches["sdss_gals"][0]["z"]
        known_redshift = True
    elif "ned" in catalog_matches:
        redshift = catalog_matches["ned"][0]["Redshift_1"]
        if redshift is None:
            redshift = catalog_matches["ned"][0]["redshift_2"]
        if redshift is None:
            redshift = catalog_matches["ned"][0]["redshift_corrected"]
        if redshift is None:
            known_redshift = False
        else:
            known_redshift = True
    else:
        known_redshift = False
        redshift = None

    # Get lightcurve data
    alert_id, mjd, passband, mag, magerr, zeropoint = locus_data.get_time_series(
        "ztf_fid",
        "ztf_magpsf",
        "ztf_sigmapsf",
        "ztf_magzpsci",
        require=["ztf_fid", "ztf_magpsf", "ztf_sigmapsf"],
    )

    # Require 2 unique passbands
    if len(np.unique(passband)) < 2:
        print("less than 2 bands")
        locus_data.set_property("astrorapid_skipped", "< 2 bands")
        return

    # Ignore lightcurves shorter than 3
    if len(mjd) < 3:
        print("less than 3 points")
        locus_data.set_property("astrorapid_skipped", "< 3 points")
        return

    # Fill in missing zeropoint values
    zeropoint = np.asarray(zeropoint, dtype=float)
    zpt_median = np.median(zeropoint[(zeropoint != None) & (~np.isnan(zeropoint))])
    zeropoint[zeropoint == None] = zpt_median
    zeropoint[np.isnan(zeropoint)] = zpt_median
    zeropoint = np.asarray(zeropoint, dtype=np.float64)
    if np.any(np.isnan(zeropoint)):
        log_id = locus_data.report_error(
            tag="astrorapid_zeropoint_contains_nan",
            data={
                "alert_id": objid,
            },
        )
        locus_data.set_property("astrorapid_error_log_id", log_id)
        locus_data.set_property("astrorapid_error", "zeropoint_contains_nan")
        return

    # Compute flux
    mag = np.asarray(mag, dtype=np.float64)
    flux = 10.0 ** (-0.4 * (mag - zeropoint))
    fluxerr = np.abs(flux * magerr * (np.log(10.0) / 2.5))

    # Set photflag detections when S/N > 5
    photflag = np.zeros(len(flux))
    photflag[flux / fluxerr > 5] = 4096
    photflag[np.where(mjd == min(mjd[photflag == 4096]))] = 6144

    # Filter out unwanted bands and convert ztf_fid to strings 'g', 'r'
    passband = np.where((passband == 1) | (passband == "1.0"), "g", passband)
    passband = np.where((passband == 2) | (passband == "2.0"), "r", passband)
    mjd, passband, flux, fluxerr, zeropoint, photflag = delete_indexes(
        np.where((passband == 3) | (passband == "3.0") | (np.isnan(mag))),
        mjd,
        passband,
        flux,
        fluxerr,
        zeropoint,
        photflag,
    )

    # Do classification
    light_curves = [
        (mjd, flux, fluxerr, passband, photflag, ra, dec, objid, redshift, mwebv)
    ]
    classifier = classifiers[known_redshift]
    try:
        predictions, time_steps = classifier.get_predictions(
            light_curves, return_predictions_at_obstime=True
        )
    except ValueError:
        log_id = locus_data.report_error(
            tag="astrorapid_get_predictions_valueerror",
            data={
                "alert_id": objid,
                "traceback": traceback.format_exc(),
                "light_curves_pickle_b64": base64.b64encode(
                    pickle.dumps(light_curves)
                ).decode("ascii"),
                "known_redshift": known_redshift,
            },
        )
        locus_data.set_property("astrorapid_error_log_id", log_id)
        locus_data.set_property("astrorapid_error", "get_predictions_valueerror")
        return
    locus_data.set_property("astrorapid_success", 1)

    # Output
    if predictions:
        for i, name in enumerate(classifier.class_names):
            # Store properties
            # p = predictions[0][-1][i]  # The probability at the last time-step
            p = max(
                predictions[0][:, i]
            )  # The max probability at any point in the light curve
            locus_data.set_property("rapid_class_probability_{}".format(name), p)

            # Send to output streams
            if name == "Pre-explosion":
                continue
            if p > 0.6:
                stream = "astrorapid_{}".format(name.lower().replace("-", "_"))
                locus_data.send_to_stream(stream)
