def high_flux_ratio_stream_v2(locus):
    ref_zps = {1: 26.325, 2: 26.275, 3: 25.660}
    T_pos_neg = {
        "pos": {1: 10.5, 2: 10.3},
        "neg": {1: 0.15, 2: 0.14},
    }  # this threshold expected to flag ~3% of the total alerts in respective fid's. No i-band filter alerts found
    alert_base_props = locus.get_properties()
    product_sign = 2.0 * (alert_base_props["ztf_isdiffpos"] == "t") - 1

    if product_sign > 0.0:
        threshold = T_pos_neg["pos"]
    if product_sign < 0.0:
        threshold = T_pos_neg["neg"]

    if (
        (
            alert_base_props["ztf_distnr"] <= 1.0
            and alert_base_props["ztf_distnr"] >= 0.0
        )
        and ("ztf_magzpsci" in alert_base_props.keys())
        and (alert_base_props["ztf_fid"] in threshold.keys())
    ):
        ref_flux = 10 ** (
            0.4 * (ref_zps[alert_base_props["ztf_fid"]] - alert_base_props["ztf_magnr"])
        )
        difference_flux = 10 ** (
            0.4 * (alert_base_props["ztf_magzpsci"] - alert_base_props["ztf_magpsf"])
        )
        sci_flux = ref_flux + product_sign * difference_flux
        flux_ratio = sci_flux / ref_flux

        if (
            product_sign < 0.0 and flux_ratio < threshold[alert_base_props["ztf_fid"]]
        ) or (
            product_sign > 0.0 and flux_ratio > threshold[alert_base_props["ztf_fid"]]
        ):
            locus.send_to_stream("high_flux_ratio_wrt_nn")
