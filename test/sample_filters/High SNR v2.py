def high_snr(locus):
    # should flag ~2-3% of alerts. Again no i-filter alerts found
    snr_threshold = {1: 50.0, 2: 55.0}

    p = locus.get_properties()
    try:
        sigmapsf = p["ztf_sigmapsf"]
        fid = p["ztf_fid"]
        threshold = snr_threshold[fid]
    except KeyError:
        return  # Skip

    alert_snr = 1.0 / sigmapsf
    if alert_snr > threshold:
        locus.send_to_stream("high_snr")
