import unittest

from antares.config import reset as reset_config, config
from antares import redis
from antares.api.main import create_app
from antares.elasticsearch import ingest
from antares.elasticsearch.ingest import delete_index, MappingCache
from antares.rtdb import bootstrap as bootstrap_rtdb, antcassandra
from antares.services.kafka import KafkaAdmin
from antares.sql import bootstrap as bootstrap_sql, engine
from antares.utils import cache


app = create_app()


def sql_init():
    engine.wait_for_online()
    bootstrap_sql.drop_main_db()
    bootstrap_sql.create_main_db()
    bootstrap_sql.drop_catalog_db()
    bootstrap_sql.create_catalog_db(mocks=True)


def sql_reset():
    bootstrap_sql.truncate_main_db(mocks=True)


def cassandra_init():
    antcassandra.init(do_bootstrap=True)
    bootstrap_rtdb.create_or_sync()


def cassandra_reset():
    bootstrap_rtdb.truncate(catalogs=True)


def elasticserch_init():
    ingest.wait_for_online()


def elasticsearch_reset():
    delete_index(config.ARCHIVE_INDEX_NAME)
    MappingCache.drop_all()


def kafka_init():
    pass


def kafka_reset():
    KafkaAdmin().delete_all_topics()


class Test(unittest.TestCase):
    def setUp(self):
        super().setUp()
        reset_config()
        cache.clear()
        redis.clear()


class SQLTest(Test):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        sql_init()

    def setUp(self):
        super().setUp()
        sql_reset()
        self.s = engine.get_session()

    def tearDown(self):
        super().tearDown()
        self.s.close_all()

    def newSession(self):
        self.s.close_all()
        self.s = engine.get_session()


class APITest(SQLTest):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cassandra_init()

    def login_user(self, username, password):
        response = self.client.post(
            "/v1/auth/login",
            json={
                "username": username,
                "password": password,
            },
            query_string={"type": "cookie"},
        )
        if response.status_code == 401:
            raise Exception(f"Unable to login as {username} with password {password}")
        elif response.status_code != 200:
            raise Exception(response.json)
        self.cookies = {cookie.name: cookie.value for cookie in self.client.cookie_jar}
        return response.json["identity"]

    def setUp(self):
        super().setUp()
        cassandra_reset()
        self.client = app.test_client()
        self.client.testing = True
        self.cookies = {}


class CassandraTest(Test):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cassandra_init()

    def setUp(self):
        super().setUp()
        cassandra_reset()


class DBTest(SQLTest):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cassandra_init()
        elasticserch_init()
        kafka_init()

    def setUp(self):
        super().setUp()
        cassandra_reset()
        elasticsearch_reset()
        kafka_reset()
