from test.base import Test

from antares import redis, log


class LockTest(Test):
    """
    This is basic sanity testing. It does not rigorously test the safety
    of the Lock.
    """

    def test(self):
        a = redis.Lock("a")
        b = redis.Lock("a")
        log.debug(redis.redis.keys())
        self.assertEqual(True, a.lock())
        log.debug(redis.redis.keys())
        self.assertEqual(False, b.lock())
        self.assertEqual(True, a.release())
        log.debug(redis.redis.keys())
        self.assertEqual(True, b.lock())
        log.debug(redis.redis.keys())
        self.assertEqual(False, a.release())
        log.debug(redis.redis.keys())
        self.assertEqual(True, b.release())
        log.debug(redis.redis.keys())
        self.assertEqual(False, b.release())
        log.debug(redis.redis.keys())

    def test_with_key_expired(self):
        a = redis.Lock("a")
        self.assertEqual(True, a.lock())
        self.assertEqual(True, a.release())

        redis.clear()
        self.assertEqual(True, a.lock())
        redis.clear()  # Delete all keys
        self.assertEqual(False, a.release())  # release() --> False
