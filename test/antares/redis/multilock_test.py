import time

from test.base import Test

from antares import redis, log


class MultiLockTest(Test):
    """
    This is basic sanity testing. It does not rigorously test the safety
    of the MultiLock.
    """

    def test_lock_and_release(self):
        a = redis.MultiLock([1, 2, 3])
        b = redis.MultiLock([50, 1, 52])
        log.debug(redis.redis.keys())
        self.assertEqual(True, a.lock())
        log.debug(redis.redis.keys())
        self.assertEqual(False, b.lock())
        self.assertEqual(True, a.release())
        log.debug(redis.redis.keys())
        self.assertEqual(True, b.lock())
        log.debug(redis.redis.keys())
        self.assertEqual(False, a.release())
        log.debug(redis.redis.keys())
        self.assertEqual(True, b.release())
        log.debug(redis.redis.keys())
        self.assertEqual(False, b.release())
        log.debug(redis.redis.keys())

    def test_lock_and_expire(self):
        a = redis.MultiLock([1, 2, 3])
        b = redis.MultiLock([50, 2, 52])
        c = redis.MultiLock([10, 11, 12])
        self.assertEqual(True, a.lock())
        self.assertEqual(False, b.lock())
        self.assertEqual(True, c.lock())
        self.assertEqual(True, c.release())
        self.assertEqual(True, a.expire(1))
        self.assertEqual(False, b.lock())
        time.sleep(1.5)
        self.assertEqual(True, b.lock())
        self.assertEqual(True, c.lock())
        self.assertEqual(True, c.release())

    def test_with_partial_expiry(self):
        a = redis.MultiLock([1, 2, 3])
        self.assertEqual(True, a.lock())
        self.assertEqual(True, a.release())  # release() --> True

        redis.clear()
        self.assertEqual(True, a.lock())
        # Delete 1 of the 3 keys
        for k in redis.redis.keys():
            redis.redis.delete(k)
            break
        self.assertEqual(True, a.release())  # release() --> True

        redis.clear()
        self.assertEqual(True, a.lock())
        redis.clear()  # Delete all keys
        self.assertEqual(False, a.release())  # release() --> False
