from unittest.mock import patch

from antares.config import config
from antares.services import errors

from test.base import Test


class ErrorsTest(Test):
    @patch("antares.services.errors.pickle.dump")
    def test_with_errors_report_serializes_dump_if_passed(self, mock_dump):
        object_of_interest = {"a": 2}
        with self.assertRaises(Exception):
            with errors.report("test", dump=object_of_interest):
                raise Exception
        mock_dump.assert_called_once()
        self.assertEqual(mock_dump.call_args[0][0], object_of_interest)
