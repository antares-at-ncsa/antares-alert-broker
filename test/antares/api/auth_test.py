import datetime
from unittest.mock import patch

import jwt
import requests
from flask_jwt_extended.exceptions import NoAuthorizationError
from passlib.hash import pbkdf2_sha256

from antares.sql.schema import STokenBlacklist, SUser

from test.base import APITest, SQLTest

DEFAULT_USER = {
    "username": "sampleuser",
    "name": "Sample User",
    "email": "sampleuser@noao.edu",
    "password": "Password1",
    "staff": False,
    "admin": False,
}


def sample_user(**kwargs):
    user = {**DEFAULT_USER}
    user.update(kwargs)
    return SUser(**user)


def commit_sample_user(session, **kwargs):
    user = sample_user()
    session.add(user)
    session.commit()
    return user


class ApiPasswordResetFlowTest(APITest):
    def test_reset_password_resets_password(self):
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login-fresh",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        access_token = result.json["access_token"]
        result = self.client.post(
            "/v1/auth/reset",
            headers={"Authorization": f"Bearer {access_token}"},
            json={"password": "NeWpaSs"},
        )
        self.assertEqual(result.status_code, 200)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": "NeWpaSs",
            },
        )
        self.assertIn("access_token", result.json)
        self.assertIn("refresh_token", result.json)
        self.assertEqual(result.status_code, 200)

    def test_reset_password_requires_fresh_token(self):
        commit_sample_user(self.s)
        # Login w/o fresh token
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        access_token = result.json["access_token"]
        result = self.client.post(
            "/v1/auth/reset",
            headers={"Authorization": f"Bearer {access_token}"},
            json={"password": "NeWpaSs"},
        )
        self.assertEqual(result.status_code, 401)
        # Login w/ fresh token
        result = self.client.post(
            "/v1/auth/login-fresh",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        access_token = result.json["access_token"]
        result = self.client.post(
            "/v1/auth/reset",
            headers={"Authorization": f"Bearer {access_token}"},
            json={"password": "NeWpaSs"},
        )
        self.assertEqual(result.status_code, 200)

    def test_login_fresh_returns_fresh_token(self):
        commit_sample_user(self.s)
        # Login w/ fresh token
        result = self.client.post(
            "/v1/auth/login-fresh",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        access_token = result.json["access_token"]
        decoded_access_token = jwt.decode(access_token, verify=False)
        self.assertTrue(decoded_access_token["fresh"])

    def test_login_fresh_does_not_return_refresh_token(self):
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login-fresh",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertNotIn("refresh_token", result.json)

    def test_login_returns_stale_token(self):
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        access_token = result.json["access_token"]
        decoded_access_token = jwt.decode(access_token, verify=False)
        self.assertFalse(decoded_access_token["fresh"])


class ApiAuthorizationTest(APITest):
    def test_cookie_based_token_refresh_works(self):
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            query_string={"type": "cookie"},
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        result = self.client.post(
            "/v1/auth/refresh",
            query_string={"type": "cookie"},
            headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]},
        )
        self.assertEqual(result.status_code, 204)

    def test_login_sets_access_and_refresh_cookies(self):
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            query_string={"type": "cookie"},
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn(
            "access_token_cookie", [cookie.name for cookie in self.client.cookie_jar]
        )
        self.assertIn(
            "csrf_access_token", [cookie.name for cookie in self.client.cookie_jar]
        )
        self.assertIn(
            "refresh_token_cookie", [cookie.name for cookie in self.client.cookie_jar]
        )
        self.assertIn(
            "csrf_refresh_token", [cookie.name for cookie in self.client.cookie_jar]
        )
        for cookie in self.client.cookie_jar:
            if cookie.name in ["access_token_cookie", "refresh_token_cookie"]:
                self.assertIn("HttpOnly", cookie.__dict__["_rest"])

    def test_logout_adds_refresh_token_to_blacklist(self):
        self.newSession()
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("refresh_token", result.json)
        # Logout
        refresh_token = result.json["refresh_token"]
        result = self.client.post(
            "/v1/auth/logout", headers={"Authorization": f"Bearer {refresh_token}"}
        )
        self.assertEqual(result.status_code, 204)
        decoded_refresh_token = jwt.decode(refresh_token, verify=False)
        blacklist_token = (
            self.s.query(STokenBlacklist)
            .filter(STokenBlacklist.jti == decoded_refresh_token["jti"])
            .first()
        )
        self.assertIsNotNone(blacklist_token)
        self.assertEqual(blacklist_token.token_type, "refresh")
        self.assertEqual(
            blacklist_token.expires,
            datetime.datetime.fromtimestamp(decoded_refresh_token["exp"]),
        )

    def test_success_response_from_logout_clears_cookies(self):
        self.newSession()
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
            query_string={"type": "cookie"},
        )
        self.assertEqual(result.status_code, 200)
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertIn("access_token_cookie", cookies)
        self.assertIn("refresh_token_cookie", cookies)
        self.assertIn("csrf_access_token", cookies)
        self.assertIn("csrf_refresh_token", cookies)
        # Logout
        result = self.client.post(
            "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
        )
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertEqual(result.status_code, 204)
        self.assertNotIn("access_token_cookie", cookies)
        self.assertNotIn("refresh_token_cookie", cookies)
        self.assertNotIn("csrf_access_token", cookies)
        self.assertNotIn("csrf_refresh_token", cookies)

    @patch("antares.api.resources.auth.views.verify_jwt_refresh_token_in_request")
    def test_401_error_response_from_logout_clears_cookies(self, mock_verify):
        self.newSession()
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
            query_string={"type": "cookie"},
        )
        self.assertEqual(result.status_code, 200)
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertIn("access_token_cookie", cookies)
        self.assertIn("refresh_token_cookie", cookies)
        self.assertIn("csrf_access_token", cookies)
        self.assertIn("csrf_refresh_token", cookies)
        # Logout
        mock_verify.side_effect = NoAuthorizationError("Token is expired")
        result = self.client.post(
            "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
        )
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertEqual(result.status_code, 204)
        self.assertNotIn("access_token_cookie", cookies)
        self.assertNotIn("refresh_token_cookie", cookies)
        self.assertNotIn("csrf_access_token", cookies)
        self.assertNotIn("csrf_refresh_token", cookies)

    @patch("antares.api.resources.auth.views.verify_jwt_refresh_token_in_request")
    def test_500_error_response_from_logout_clears_cookies(self, mock_verify):
        self.newSession()
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
            query_string={"type": "cookie"},
        )
        self.assertEqual(result.status_code, 200)
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertIn("access_token_cookie", cookies)
        self.assertIn("refresh_token_cookie", cookies)
        self.assertIn("csrf_access_token", cookies)
        self.assertIn("csrf_refresh_token", cookies)
        # Logout
        mock_verify.side_effect = Exception("Some problem")
        result = self.client.post(
            "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
        )
        cookies = requests.utils.dict_from_cookiejar(self.client.cookie_jar)
        self.assertEqual(result.status_code, 204)
        self.assertNotIn("access_token_cookie", cookies)
        self.assertNotIn("refresh_token_cookie", cookies)
        self.assertNotIn("csrf_access_token", cookies)
        self.assertNotIn("csrf_refresh_token", cookies)

    def test_cant_get_new_tokens_with_blacklisted_refresh_token(self):
        self.newSession()
        commit_sample_user(self.s)
        # Login
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("refresh_token", result.json)
        # Logout
        refresh_token = result.json["refresh_token"]
        result = self.client.post(
            "/v1/auth/logout", headers={"Authorization": f"Bearer {refresh_token}"}
        )
        self.assertEqual(result.status_code, 204)
        # Try to get fresh tokens with the blacklisted refresh token
        result = self.client.post(
            "/v1/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
        )
        self.assertEqual(result.status_code, 401)
        self.assertIn("revoked", result.json["errors"][0]["detail"].lower())

    def test_login_returns_refresh_token(self):
        self.newSession()
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("refresh_token", result.json)

    def test_refresh_token_can_be_used_for_new_access_token_and_refresh_token(self):
        self.newSession()
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        refresh_token = result.json["refresh_token"]
        result = self.client.post(
            "/v1/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        self.assertIn("refresh_token", result.json)
        self.assertEqual("bearer", result.json["token_type"])

    def test_access_token_cannot_be_used_for_new_access_token_and_refresh_token(self):
        self.newSession()
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        access_token = result.json["access_token"]
        result = self.client.post(
            "/v1/auth/refresh", headers={"Authorization": f"Bearer {access_token}"}
        )
        self.assertEqual(result.status_code, 422)
        self.assertIn("only refresh", result.json["errors"][0]["detail"].lower())

    def test_user_with_correct_password_gets_authorization(self):
        self.newSession()
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": DEFAULT_USER["password"],
            },
        )
        self.assertEqual(result.status_code, 200)
        self.assertIn("access_token", result.json)
        self.assertIn("refresh_token", result.json)
        self.assertEqual("bearer", result.json["token_type"])

    def test_login_with_non_existent_username_responds_with_401(self):
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": "IDontExist",
                "password": "DoesntMatter",
            },
        )
        self.assertEqual(result.status_code, 401)
        self.assertEqual(result.json["error"], "invalid_client")

    def test_login_with_incorrect_password_responds_with_401(self):
        self.newSession()
        commit_sample_user(self.s)
        result = self.client.post(
            "/v1/auth/login",
            json={
                "username": DEFAULT_USER["username"],
                "password": "WrongPassword",
            },
        )
        self.assertEqual(result.status_code, 401)
        self.assertEqual(result.json["error"], "invalid_client")

    def test_login_with_missing_parameters_raises(self):
        result = self.client.post(
            "/v1/auth/login",
            json={
                "password": "NoUsername!",
            },
        )
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json["error"], "invalid_request")


class SqlAuthorizationTest(SQLTest):
    def test_password_is_hashed(self):
        user = sample_user()
        self.assertNotEqual(user.password, "Password1")
        self.assertTrue(pbkdf2_sha256.verify("Password1", user.password))

    def test_check_password(self):
        user = sample_user()
        self.assertTrue(user.check_password("Password1"))
        self.assertFalse(user.check_password("WrongPassword111"))
