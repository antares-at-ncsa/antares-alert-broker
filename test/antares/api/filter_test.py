from werkzeug.datastructures import Headers

from antares.sql.schema import SFilter, SFilterVersion, SUser

from test.base import APITest


DEFAULT_FILTER = {
    "name": "My Filter",
    "public": False,
}

DEFAULT_FILTER_VERSION = {
    "comment": "My Filter Version",
}


def make_filter_post_payload(filter_):
    return {
        "data": {
            "type": "filter",
            "attributes": filter_,
        }
    }


def make_filter_patch_payload(id_, filter_):
    return {
        "data": {
            "id": id_,
            "type": "filter",
            "attributes": filter_,
        }
    }


def make_filter_version_post_payload(filter_version):
    return {
        "data": {
            "type": "filter_version",
            "attributes": filter_version,
        }
    }


def sample_filter(**kwargs):
    filter_ = {**DEFAULT_FILTER}
    filter_.update(kwargs)
    return SFilter(**filter_)


def commit_sample_filter(session, **kwargs):
    filter_ = sample_filter(**kwargs)
    session.add(filter_)
    session.commit()
    return filter_


def sample_filter_version(**kwargs):
    filter_version = {**DEFAULT_FILTER_VERSION}
    filter_version.update(kwargs)
    return SFilterVersion(**filter_version)


def commit_sample_filter_version(session, **kwargs):
    filter_version = sample_filter_version(**kwargs)
    session.add(filter_version)
    session.commit()
    return filter_version


class ApiFilterDetailTest(APITest):
    def test_unauthenticated_user_cannot_patch_filter(self):
        filter_ = commit_sample_filter(self.s, user_id=666)
        payload = make_filter_patch_payload(filter_.filter_id, {"public": False})
        response = self.client.patch(f"/v1/filters/{filter_.filter_id}", json=payload)
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_cannot_patch_filter_they_dont_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, user_id=user_id + 1)
        payload = make_filter_patch_payload(filter_.filter_id, {"public": False})
        response = self.client.patch(
            f"/v1/filters/{filter_.filter_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_gets_401_if_try_to_patch_filter_does_not_exist(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_patch_payload(666, {"public": False})
        response = self.client.patch(f"/v1/filters/666", json=payload, headers=headers)
        self.assertEqual(response.status_code, 401)

    def test_user_can_patch_filter_they_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, user_id=user_id)
        payload = make_filter_patch_payload(filter_.filter_id, {"public": True})
        response = self.client.patch(
            f"/v1/filters/{filter_.filter_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_user_can_only_patch_public_field(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, user_id=user_id)
        payload = make_filter_patch_payload(filter_.filter_id, {"enabled": True})
        response = self.client.patch(
            f"/v1/filters/{filter_.filter_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 400)
        payload = make_filter_patch_payload(
            filter_.filter_id, {"description": "0xaa0xff0x"}
        )
        response = self.client.patch(
            f"/v1/filters/{filter_.filter_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_user_can_get_a_filter_they_own(self):
        user_id = self.login_user("user", "password")
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=1)
        response = self.client.get(f"/v1/filters/{filter_.filter_id}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["data"]["id"], str(filter_.filter_id))
        self.assertEqual(response.json["data"]["attributes"]["name"], "My Filter")

    def test_user_cannot_get_a_private_filter_they_dont_own(self):
        self.login_user("user", "password")
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666, public=False
        )
        response = self.client.get(f"/v1/filters/{filter_.filter_id}")
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_can_get_a_public_filter_they_dont_own(self):
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666, public=True
        )
        response = self.client.get(f"/v1/filters/{filter_.filter_id}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["data"]["id"], str(filter_.filter_id))
        self.assertEqual(
            response.json["data"]["attributes"]["name"], "Someone Else's Filter"
        )

    def test_user_can_get_a_public_filter_they_dont_own(self):
        self.login_user("user", "password")
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666, public=True
        )
        response = self.client.get(f"/v1/filters/{filter_.filter_id}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["data"]["id"], str(filter_.filter_id))
        self.assertEqual(
            response.json["data"]["attributes"]["name"], "Someone Else's Filter"
        )

    def test_admin_can_get_a_filter_they_dont_own(self):
        self.login_user("admin", "password")
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666
        )
        response = self.client.get(f"/v1/filters/{filter_.filter_id}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["data"]["id"], str(filter_.filter_id))
        self.assertEqual(
            response.json["data"]["attributes"]["name"], "Someone Else's Filter"
        )

    def test_unauthenticated_user_cannot_access_get_endpoint(self):
        response = self.client.get(f"/v1/filters/1")
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_patch_filter(self):
        payload = make_filter_patch_payload(1, {"public": True})
        response = self.client.patch(f"/v1/filters/1", json=payload)
        self.assertEqual(response.status_code, 401)

    def test_admin_user_can_patch_filter(self):
        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_patch_payload(1, {"public": True})
        response = self.client.patch(f"/v1/filters/1", json=payload, headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_admin_user_can_only_patch_public_field(self):
        self.login_user("admin", "password")
        payload = make_filter_patch_payload(1, {"public": True})
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.patch(f"/v1/filters/1", json=payload, headers=headers)
        self.assertEqual(response.status_code, 200)
        payload = make_filter_patch_payload(
            1,
            {
                "name": "New Name Can't Change",
                "description": "New Description Can't Change",
                "public": False,
            },
        )
        response = self.client.patch(f"/v1/filters/1", json=payload, headers=headers)
        self.assertEqual(response.status_code, 400)
        self.assertIn("name", response.json["errors"][0]["detail"])
        self.assertIn("description", response.json["errors"][0]["detail"])
        self.assertNotIn("public", response.json["errors"][0]["detail"])

    def test_delete_method_not_allowed_responds_405(self):
        response = self.client.delete("/v1/filters/1")
        self.assertEqual(response.status_code, 405)


class ApiFilterListTest(APITest):
    def test_authenticated_user_can_post_filter(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post(
            f"/v1/users/{user_id}/filters", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 200)

    def test_authenticated_user_cannot_post_filter_to_other_user_gets_401(self):
        self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post(
            f"/v1/users/666/filters", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_cannot_post_filter_to_other_user_gets_401(self):
        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post(
            f"/v1/users/666/filters", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_post_filter_gets_401(self):
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post("/v1/users/666/filters", json=payload)
        self.assertEqual(response.status_code, 401)

    def test_post_method_not_allowed_filters_list(self):
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post("/v1/filters", json=payload)
        self.assertEqual(response.status_code, 405)

    def test_post_method_not_allowed_filters_list_even_logged_in_as_admin(self):
        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_post_payload(DEFAULT_FILTER)
        response = self.client.post("/v1/filters", json=payload, headers=headers)
        self.assertEqual(response.status_code, 405)

    def test_admin_user_gets_all_filters(self):
        from test.sample_filters import filters

        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(
            "/v1/filters", query_string={"page[limit]": len(filters)}, headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], len(filters))
        for filter_ in filters:
            self.assertIn(
                str(filter_["filter_id"]), [f["id"] for f in response.json["data"]]
            )

    def test_unauthenticated_user_can_get_public_filters(self):
        commit_sample_filter(
            self.s, name="Someone Else's Public Filter", user_id=666, public=True
        )
        commit_sample_filter(
            self.s, name="Someone Else's Private Filter", user_id=666, public=False
        )
        response = self.client.get(f"/v1/filters")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 1)
        self.assertEqual(len(response.json["data"]), 1)
        filter_names = [
            filter_["attributes"]["name"] for filter_ in response.json["data"]
        ]
        self.assertEqual(len(filter_names), 1)
        self.assertIn("Someone Else's Public Filter", filter_names)
        self.assertNotIn("Someone Else's Private Filter", filter_names)

    def test_regular_user_can_get_their_filters(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(f"/v1/users/{user_id}/filters", headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_user_can_get_specific_users_public_filters(self):
        commit_sample_filter(
            self.s, name="Someone Else's Public Filter", user_id=666, public=True
        )
        commit_sample_filter(
            self.s, name="Someone Else's Private Filter", user_id=666, public=False
        )
        response = self.client.get(f"/v1/users/666/filters")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 1)
        self.assertEqual(len(response.json["data"]), 1)
        filter_names = [
            filter_["attributes"]["name"] for filter_ in response.json["data"]
        ]
        self.assertEqual(len(filter_names), 1)
        self.assertIn("Someone Else's Public Filter", filter_names)
        self.assertNotIn("Someone Else's Private Filter", filter_names)

    def test_regular_user_can_get_other_users_public_filters(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        commit_sample_filter(
            self.s,
            name="Someone Else's Public Filter",
            user_id=user_id + 1,
            public=True,
        )
        commit_sample_filter(
            self.s,
            name="Someone Else's Private Filter",
            user_id=user_id + 1,
            public=False,
        )
        response = self.client.get(f"/v1/users/{user_id + 1}/filters", headers=headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 1)
        self.assertEqual(len(response.json["data"]), 1)
        filter_names = [
            filter_["attributes"]["name"] for filter_ in response.json["data"]
        ]
        self.assertEqual(len(filter_names), 1)
        self.assertIn("Someone Else's Public Filter", filter_names)
        self.assertNotIn("Someone Else's Private Filter", filter_names)

    def test_user_can_get_their_filters_and_other_users_public_filters(self):
        user_id = self.login_user("user", "password")
        commit_sample_filter(
            self.s, name="My Public Filter", user_id=user_id, public=True
        )
        commit_sample_filter(
            self.s, name="My Private Filter", user_id=user_id, public=False
        )
        commit_sample_filter(
            self.s,
            name="Someone Else's Public Filter",
            user_id=user_id + 1,
            public=True,
        )
        commit_sample_filter(
            self.s,
            name="Someone Else's Private Filter",
            user_id=user_id + 1,
            public=False,
        )
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(f"/v1/filters", headers=headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 3)
        self.assertEqual(len(response.json["data"]), 3)
        filter_names = [
            filter_["attributes"]["name"] for filter_ in response.json["data"]
        ]
        self.assertEqual(len(filter_names), 3)
        self.assertIn("My Public Filter", filter_names)
        self.assertIn("My Private Filter", filter_names)
        self.assertIn("Someone Else's Public Filter", filter_names)
        self.assertNotIn("Someone Else's Private Filter", filter_names)

    def test_staff_user_can_get_their_filters(self):
        user_id = self.login_user("staff", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(f"/v1/users/{user_id}/filters", headers=headers)
        self.assertEqual(response.status_code, 200)


class ApiFilterVersionListTest(APITest):
    def test_authenticated_user_posts_to_non_existent_filter_gets_401(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/666/versions", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_can_post_filter_version_to_their_filter(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id)
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/{filter_.filter_id}/versions", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["attributes"]["comment"], "My Filter Version"
        )

    def test_authenticated_user_cannot_post_filter_version_to_other_user_gets_401(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id + 1)
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/{filter_.filter_id}/versions", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_cannot_post_filter_version_to_other_users_public_filter_gets_401(
        self,
    ):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="My Filter", user_id=user_id + 1, public=True
        )
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/{filter_.filter_id}/versions", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_can_post_filter_version_to_other_user(self):
        user_id = self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id + 1)
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/{filter_.filter_id}/versions", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["attributes"]["comment"], "My Filter Version"
        )

    def test_unauthenticated_user_cannot_post_filter_version_gets_401(self):
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=1)
        payload = make_filter_version_post_payload(DEFAULT_FILTER_VERSION)
        response = self.client.post(
            f"/v1/filters/{filter_.filter_id}/versions", json=payload
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_gets_all_filters_filter_versions(self):
        from test.sample_filters import filters, versions_manifest

        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        for filter_ in filters:
            response = self.client.get(
                f"/v1/filters/{filter_['filter_id']}/versions", headers=headers
            )
            self.assertEqual(response.status_code, 200)
            versions = [
                version
                for version in versions_manifest
                if version["filter_id"] == filter_["filter_id"]
            ]
            self.assertEqual(response.json["meta"]["count"], len(versions))

    def test_regular_user_can_get_their_filters(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id)
        # Create 3 filter versions
        for i in range(3):
            payload = make_filter_version_post_payload({"comment": f"{i}"})
            response = self.client.post(
                f"/v1/filters/{filter_.filter_id}/versions",
                json=payload,
                headers=headers,
            )
            self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json["data"]), 3)
        for i in range(3):
            self.assertIn(
                f"{i}",
                [version["attributes"]["comment"] for version in response.json["data"]],
            )

    def test_user_can_get_their_filters(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id)
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 1)

    def test_user_cannot_get_other_users_private_filter_versions(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="My Filter", user_id=user_id + 1, public=False
        )
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_can_get_other_users_public_filter_versions(self):
        filter_ = commit_sample_filter(
            self.s, name="My Filter", user_id=666, public=True
        )
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(f"/v1/filters/{filter_.filter_id}/versions")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 2)

    def test_user_can_get_other_users_public_filter_versions(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="My Filter", user_id=user_id + 1, public=True
        )
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 2)

    def test_staff_user_can_get_their_filters(self):
        user_id = self.login_user("staff", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id)
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["meta"]["count"], 1)

    def test_staff_user_cannot_get_other_users_filters(self):
        user_id = self.login_user("staff", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id + 1)
        commit_sample_filter_version(self.s, filter_id=filter_.filter_id)
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions", headers=headers
        )
        self.assertEqual(response.status_code, 401)


class ApiFilterVersionDetailTest(APITest):
    def test_user_can_get_a_filter_version_they_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(self.s, name="My Filter", user_id=user_id)
        filter_version = commit_sample_filter_version(
            self.s, filter_id=filter_.filter_id
        )
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions/{filter_version.filter_version_id}",
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["id"], str(filter_version.filter_version_id)
        )

    def test_user_cannot_get_a_private_filter_version_they_dont_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=user_id + 1, public=False
        )
        filter_version = commit_sample_filter_version(
            self.s, filter_id=filter_.filter_id
        )
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions/{filter_version.filter_version_id}",
            headers=headers,
        )
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_can_get_a_public_filter_version_they_dont_own(self):
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666, public=True
        )
        filter_version = commit_sample_filter_version(
            self.s, filter_id=filter_.filter_id
        )
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions/{filter_version.filter_version_id}",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["id"], str(filter_version.filter_version_id)
        )

    def test_user_can_get_a_public_filter_version_they_dont_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=user_id + 1, public=True
        )
        filter_version = commit_sample_filter_version(
            self.s, filter_id=filter_.filter_id
        )
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions/{filter_version.filter_version_id}",
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["id"], str(filter_version.filter_version_id)
        )

    def test_admin_can_get_a_filter_they_dont_own(self):
        self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        filter_ = commit_sample_filter(
            self.s, name="Someone Else's Filter", user_id=666
        )
        filter_version = commit_sample_filter_version(
            self.s, filter_id=filter_.filter_id
        )
        response = self.client.get(
            f"/v1/filters/{filter_.filter_id}/versions/{filter_version.filter_version_id}",
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json["data"]["id"], str(filter_version.filter_version_id)
        )

    def test_unauthenticated_user_cannot_access_get_endpoint(self):
        response = self.client.get(f"/v1/filters/1/versions")
        self.assertEqual(response.status_code, 401)

    def test_delete_method_not_allowed_responds_405(self):
        response = self.client.delete("/v1/filters/1/versions")
        self.assertEqual(response.status_code, 405)
