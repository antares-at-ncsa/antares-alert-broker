from unittest.mock import patch, MagicMock

from antares.rtdb.catalog_config import CatalogConfig

from test.base import APITest


class ApiLocusCatalogMatchListTest(APITest):
    MOCK_CATALOG_CONFIG = CatalogConfig(
        {
            "tables": [
                {
                    "table": "test_catalog_1",
                    "catalog_id": 1,
                    "enabled": True,
                    "ra_column": "ra_1",
                    "dec_column": "dec_1",
                    "object_id_column": "object_id_1",
                    "object_id_type": "str",
                    "radius": 0.00028,
                    "radius_column": None,
                    "radius_unit": None,
                },
                {
                    "table": "test_catalog_2",
                    "catalog_id": 2,
                    "enabled": True,
                    "ra_column": "ra_2",
                    "dec_column": "dec_2",
                    "object_id_column": "object_id_2",
                    "object_id_type": "int",
                    "radius": 0.00028,
                    "radius_column": None,
                    "radius_unit": None,
                },
            ]
        }
    )

    def setUp(self):
        super().setUp()
        self.patcher = patch(
            "antares.rtdb.api._catalog_config", new=self.MOCK_CATALOG_CONFIG
        )
        self.patcher.start()

    def tearDown(self):
        super().tearDown()
        self.patcher.stop()

    @patch("antares.api.resources.locus.views.RTDB.get_catalog_objects")
    @patch("antares.api.resources.locus.views.RTDB.get_locus_by_id")
    def test_catalog_matches_include_angular_separation(
        self, mock_get_locus_by_id, mock_get_catalog_objects
    ):
        mock_locus = MagicMock()
        mock_locus.ra = 11.00
        mock_locus.dec = 11.00
        mock_get_locus_by_id.return_value = mock_locus
        mock_get_catalog_objects.return_value = {
            "test_catalog_1": [
                {
                    "object_id_1": "1",
                    "ra_1": 10.00,
                    "dec_1": 12.00,
                    "property_1a": "hello",
                    "property_1b": 2323,
                    "property_1c": 1.02,
                },
            ],
            "test_catalog_2": [
                {
                    "object_id_2": "1",
                    "ra_2": 10.00,
                    "dec_2": 12.00,
                    "property_2a": "yay",
                    "property_2b": "nay",
                },
                {
                    "object_id_2": "2",
                    "ra_2": 11.00,
                    "dec_2": 13.00,
                    "property_2a": "zay",
                    "property_2b": "bay",
                },
            ],
        }
        response = self.client.get("/v1/loci/ANT2020001/catalog-matches")
        self.assertEqual(response.status_code, 200)
        catalog_matches = response.json["data"]
        self.assertIsNotNone(catalog_matches)
        self.assertEqual(len(catalog_matches), 3)
        for (i, catalog) in enumerate(self.MOCK_CATALOG_CONFIG.tables):
            self.assertEqual(
                catalog_matches[i]["id"],
                f"{catalog['table']}:{catalog_matches[i]['attributes']['properties'][catalog['object_id_column']]}",
            )

    @patch("antares.api.resources.locus.views.RTDB.get_catalog_objects")
    @patch("antares.api.resources.locus.views.RTDB.get_locus_by_id")
    def test_user_can_get_catalog_responds_200(
        self, mock_get_locus_by_id, mock_get_catalog_objects
    ):
        mock_locus = MagicMock()
        mock_locus.ra = 11.00
        mock_locus.dec = 11.00
        mock_get_locus_by_id.return_value = mock_locus
        mock_get_catalog_objects.return_value = {
            "test_catalog_1": [
                {
                    "object_id_1": "1",
                    "ra_1": 10.00,
                    "dec_1": 12.00,
                    "property_1a": "hello",
                    "property_1b": 2323,
                    "property_1c": 1.02,
                },
            ],
            "test_catalog_2": [
                {
                    "object_id_2": "1",
                    "ra_2": 10.00,
                    "dec_2": 12.00,
                    "property_2a": "yay",
                    "property_2b": "nay",
                },
                {
                    "object_id_2": "2",
                    "ra_2": 11.00,
                    "dec_2": 13.00,
                    "property_2a": "zay",
                    "property_2b": "bay",
                },
            ],
        }
        response = self.client.get("/v1/loci/ANT2020001/catalog-matches")
        self.assertEqual(response.status_code, 200)
        catalog_matches = response.json["data"]
        self.assertIsNotNone(catalog_matches)
        self.assertEqual(len(catalog_matches), 3)
        for (i, catalog) in enumerate(self.MOCK_CATALOG_CONFIG.tables):
            self.assertEqual(
                catalog_matches[i]["id"],
                f"{catalog['table']}:{catalog_matches[i]['attributes']['properties'][catalog['object_id_column']]}",
            )

    @patch("antares.api.resources.locus.views.RTDB.get_catalog_objects")
    @patch("antares.api.resources.locus.views.RTDB.get_locus_by_id")
    def test_user_can_get_catalog_responds_empty_list_if_no_matches(
        self, _, mock_get_catalog_objects
    ):
        mock_get_catalog_objects.return_value = {}
        response = self.client.get("/v1/loci/ANT2020001/catalog-matches")
        self.assertEqual(response.status_code, 200)
        catalog_matches = response.json["data"]
        self.assertIsNotNone(catalog_matches)
        self.assertEqual(len(catalog_matches), 0)

    def test_post_method_not_allowed_responds_405(self):
        response = self.client.post("/v1/catalogs")
        self.assertEqual(response.status_code, 405)
