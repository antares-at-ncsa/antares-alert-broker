from unittest.mock import patch

from test.base import APITest


class ApiCatalogDetailTest(APITest):
    MOCK_CATALOG_TABLES = [
        {
            "catalog_id": 1,
            "table": "test_catalog_1",
        },
        {
            "catalog_id": 2,
            "table": "test_catalog_2",
        },
    ]

    def setUp(self):
        super().setUp()
        self.patcher = patch("antares.rtdb.api._catalog_config")
        self.mock_catalog_config = self.patcher.start()
        self.mock_catalog_config.tables = self.MOCK_CATALOG_TABLES

    def tearDown(self):
        super().tearDown()
        self.patcher.stop()

    def test_user_can_get_catalog_responds_200(self):
        response = self.client.get("/v1/catalogs/1")
        self.assertEqual(response.status_code, 200)
        catalog = response.json["data"]
        self.assertIsNotNone(catalog)
        self.assertEqual(catalog["id"], "1")
        self.assertEqual(catalog["attributes"]["name"], "test_catalog_1")

    def test_user_can_get_catalog_responds_404_if_not_found(self):
        response = self.client.get("/v1/catalogs/999")
        self.assertEqual(response.status_code, 404)

    def test_delete_method_not_allowed_responds_405(self):
        response = self.client.delete("/v1/catalogs/1")
        self.assertEqual(response.status_code, 405)

    def test_patch_method_not_allowed_responds_405(self):
        response = self.client.patch("/v1/catalogs/1")
        self.assertEqual(response.status_code, 405)


class ApiCatalogListTest(APITest):
    MOCK_CATALOG_TABLES = [
        {
            "catalog_id": 1,
            "table": "test_catalog_1",
        },
        {
            "catalog_id": 2,
            "table": "test_catalog_2",
        },
    ]

    def setUp(self):
        super().setUp()
        self.patcher = patch("antares.rtdb.api._catalog_config")
        self.mock_catalog_config = self.patcher.start()
        self.mock_catalog_config.tables = self.MOCK_CATALOG_TABLES

    def tearDown(self):
        super().tearDown()
        self.patcher.stop()

    def test_user_can_get_catalogs_responds_200(self):
        response = self.client.get("/v1/catalogs")
        self.assertEqual(response.status_code, 200)
        catalogs = response.json["data"]
        self.assertIsNotNone(catalogs)
        self.assertEqual(len(catalogs), 2)
        for (i, catalog) in enumerate(self.MOCK_CATALOG_TABLES):
            self.assertEqual(catalogs[i]["id"], str(catalog["catalog_id"]))
            self.assertEqual(catalogs[i]["attributes"]["name"], catalog["table"])

    def test_user_can_get_catalog_responds_empty_if_no_catalogs(self):
        self.mock_catalog_config.tables = []
        try:
            response = self.client.get("/v1/catalogs")
            self.assertEqual(len(response.json["data"]), 0)
        finally:
            self.mock_catalog_config.tables = self.MOCK_CATALOG_TABLES

    def test_post_method_not_allowed_responds_405(self):
        response = self.client.post("/v1/catalogs")
        self.assertEqual(response.status_code, 405)
