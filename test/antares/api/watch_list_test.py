from werkzeug.datastructures import Headers

from antares.sql.schema import SUser, SWatchList, SWatchObjectId
from antares.sql import engine
from antares.rtdb.api import RTDB

from test.base import APITest


DEFAULT_WATCH_LIST = {
    "name": "My Watch List",
    "objects": [
        {
            "right_ascension": 100.0,
            "declination": 20.0,
            "radius": 1.0,
            "comment": "Object 1",
        },
    ],
}


def make_watch_list_post_payload(watch_list, owner_id):
    return {
        "data": {
            "type": "watch_list",
            "attributes": watch_list,
            "relationships": {
                "owner": {"data": {"id": owner_id, "type": "user"}},
            },
        }
    }


def commit_sample_watch_list(session, **kwargs):
    watch_list = {**DEFAULT_WATCH_LIST}
    watch_list.update(kwargs)
    s_watch_list = SWatchList(name=watch_list["name"], user_id=watch_list["user_id"])
    session.add(s_watch_list)
    session.commit()
    RTDB.put_watch_list(
        wl_id=s_watch_list.watch_list_id,
        name=s_watch_list.name,
        description=s_watch_list.description,
        slack_channel=s_watch_list.slack_channel,
    )
    object_ids = SWatchObjectId.generate_ids(len(watch_list["objects"]))
    for (object_, id_) in zip(watch_list["objects"], object_ids):
        RTDB().put_watch_object(
            wl_id=s_watch_list.watch_list_id,
            wo_id=id_,
            ra=object_["right_ascension"],
            dec=object_["declination"],
            radius=object_["radius"],
            name=object_.get("comment"),
        )
    s_watch_list.objects = watch_list["objects"]
    return s_watch_list


class ApiFilterDetailTest(APITest):
    def test_user_can_get_a_watch_list_they_own(self):
        user_id = self.login_user("user", "password")
        watch_list = commit_sample_watch_list(self.s, name="My WL", user_id=user_id)
        response = self.client.get(f"/v1/watch_lists/{watch_list.watch_list_id}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["data"]["id"], str(watch_list.watch_list_id))
        self.assertEqual(response.json["data"]["attributes"]["name"], "My WL")

    def test_user_cannot_get_a_watch_list_that_doesnt_exist_returns_401(self):
        user_id = self.login_user("user", "password")
        response = self.client.get(f"/v1/watch_lists/666")
        self.assertEqual(response.status_code, 401)

    def test_user_cannot_get_a_watch_list_they_dont_own(self):
        user_id = self.login_user("user", "password")
        watch_list = commit_sample_watch_list(
            self.s, name="Someone Else's WL", user_id=user_id + 1
        )
        response = self.client.get(f"/v1/watch_lists/{watch_list.watch_list_id}")
        self.assertEqual(response.status_code, 401)

    def test_admin_cannot_get_a_watch_list_they_dont_own(self):
        user_id = self.login_user("admin", "password")
        watch_list = commit_sample_watch_list(
            self.s, name="Someone Else's WL", user_id=user_id + 1
        )
        response = self.client.get(f"/v1/watch_lists/{watch_list.watch_list_id}")
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_access_get_endpoint(self):
        response = self.client.get(f"/v1/watch_lists/1")
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_delete_watch_list(self):
        response = self.client.delete(f"/v1/watch_lists/1")
        self.assertEqual(response.status_code, 401)

    def test_user_can_delete_watch_list_they_own(self):
        user_id = self.login_user("user", "password")
        watch_list = commit_sample_watch_list(self.s, name="My WL", user_id=user_id)
        self.assertIsNotNone(self.s.query(SWatchList).get(watch_list.watch_list_id))
        self.s.flush()
        self.s.close_all()
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.delete(
            f"/v1/watch_lists/{watch_list.watch_list_id}", headers=headers
        )
        self.s = engine.get_session()
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(self.s.query(SWatchList).get(watch_list.watch_list_id))

    def test_user_cannot_delete_watch_list_they_dont_own(self):
        user_id = self.login_user("user", "password")
        watch_list = commit_sample_watch_list(self.s, name="My WL", user_id=user_id + 1)
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.delete(
            f"/v1/watch_lists/{watch_list.watch_list_id}", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_staff_user_cannot_delete_watch_list_they_dont_own(self):
        user_id = self.login_user("staff", "password")
        watch_list = commit_sample_watch_list(self.s, name="My WL", user_id=user_id + 1)
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.delete(
            f"/v1/watch_lists/{watch_list.watch_list_id}", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_cannot_delete_watch_list_they_dont_own(self):
        user_id = self.login_user("staff", "password")
        watch_list = commit_sample_watch_list(self.s, name="My WL", user_id=user_id + 1)
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.delete(
            f"/v1/watch_lists/{watch_list.watch_list_id}", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_patch_method_not_allowed_responds_405(self):
        response = self.client.patch("/v1/watch_lists/1")
        self.assertEqual(response.status_code, 405)


class ApiWatchListListTest(APITest):
    def test_authenticated_user_can_post_watch_list(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, user_id)
        response = self.client.post(
            f"/v1/users/{user_id}/watch_lists", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 200)

    def test_authenticated_user_cannot_post_watch_list_if_wrong_payload_user_id_gets_401(
        self,
    ):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, user_id + 1)
        response = self.client.post(
            f"/v1/users/{user_id}/watch_lists", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_cannot_post_watch_list_if_wrong_route_user_id_gets_401(
        self,
    ):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, user_id)
        response = self.client.post(
            f"/v1/users/{user_id + 1}/watch_lists", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_cannot_post_watch_list_to_other_user_gets_401(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, user_id + 1)
        response = self.client.post(
            f"/v1/users/{user_id + 1}/watch_lists", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_cannot_post_watch_list_to_other_user_gets_401(self):
        user_id = self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, user_id + 1)
        response = self.client.post(
            f"/v1/users/{user_id + 1}/watch_lists", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_post_filter_gets_401(self):
        payload = make_watch_list_post_payload(DEFAULT_WATCH_LIST, 666)
        response = self.client.post("/v1/users/666/watch_lists", json=payload)
        self.assertEqual(response.status_code, 401)

    def test_slash_watch_lists_route_404(self):
        """
        For the time being we intentionally don't expose a /watch_lists
        route.
        """
        response = self.client.get("v1/watch_lists")
        self.assertEqual(response.status_code, 404)

    def test_regular_user_can_get_their_watch_lists(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(f"/v1/users/{user_id}/watch_lists", headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_user_watch_lists_list_only_returns_users_watch_lists(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        commit_sample_watch_list(self.s, name="My WL", user_id=user_id)
        commit_sample_watch_list(self.s, name="Someone Else's WL", user_id=user_id + 1)
        response = self.client.get(f"/v1/users/{user_id}/watch_lists", headers=headers)
        self.assertEqual(response.json["meta"]["count"], 1)
        self.assertEqual(len(response.json["data"]), 1)
        self.assertEqual(response.json["data"][0]["attributes"]["name"], "My WL")

    def test_regular_user_cannot_get_other_users_watch_lists(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(
            f"/v1/users/{user_id + 1}/watch_lists", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_staff_user_cannot_get_other_users_watch_lists(self):
        user_id = self.login_user("staff", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(
            f"/v1/users/{user_id + 1}/watch_lists", headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_cannot_get_other_users_watch_lists(self):
        user_id = self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        response = self.client.get(
            f"/v1/users/{user_id + 1}/watch_lists", headers=headers
        )
        self.assertEqual(response.status_code, 401)
