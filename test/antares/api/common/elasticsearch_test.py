from unittest.mock import MagicMock, patch

from antares.api.common.elasticsearch import *
from astropy.coordinates import SkyCoord
import astropy.units
import marshmallow

from test.base import Test


class CoordinateStringFieldTest(Test):
    def test_colon_separated_coordinates_treated_as_hms_dms(self):
        coordinate_string = "11:19:50 70:6:10"
        coordinate = CoordinateString().deserialize(coordinate_string)
        self.assertEqual(
            coordinate.to_string(), SkyCoord("11h19m50s 70d6m10s").to_string()
        )

    def test_unitless_coordinates_treated_as_degrees(self):
        coordinate_string = "10 20"
        coordinate = CoordinateString().deserialize(coordinate_string)
        self.assertEqual(coordinate.to_string(), SkyCoord("10d 20d").to_string())

    def test_different_formats_equivalent(self):
        expected_coordinate = SkyCoord("90d 0d")
        for coordinate_string in [
            "90.0 0.0",
            "90d 0d",
            "90d+0d",
            "90d00 0d00",
            "90d00m00s 00d00m00s",
            "06h00m00s +00d00m00s",
            "06h00m00s+00d00m00s",
            "06:00:00 00:00:00",
            "06:00:00+00:00:00",
        ]:
            coordinate = CoordinateString().deserialize(coordinate_string)
            self.assertEqual(coordinate.to_string(), expected_coordinate.to_string())


class TransformDictionaryTest(Test):
    def test_transform_dictionary_transforms_keys_in_list(self):
        d = {"a": "hello", "nested": [{"a": "hello"}]}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        self.assertDictEqual(
            d, {"a": "hello, world!", "nested": [{"a": "hello, world!"}]}
        )

    def test_transform_dictionary_transforms_nested_keys(self):
        d = {"a": "hello", "nested": {"a": "hello"}}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        self.assertDictEqual(
            d, {"a": "hello, world!", "nested": {"a": "hello, world!"}}
        )

    def test_transform_dictionary_only_transforms_specified_keys(self):
        d = {"a": "hello", "b": "goodbye"}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        self.assertDictEqual(
            d,
            {
                "a": "hello, world!",
                "b": "goodbye",
            },
        )

    def test_transform_dictionary_renames_key(self):
        d = {"a": "hello", "b": "goodbye"}
        transform_dictionary(d, "a", lambda v: v, new_key="c")
        self.assertDictEqual(
            d,
            {
                "c": "hello",
                "b": "goodbye",
            },
        )


class SkyDistanceQueryTest(Test):
    @patch("antares.api.common.elasticsearch.htm.get_htm_circle_region")
    def test_transforms_query_to_htm_ranges(self, mock_get_htm_circle_region):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        mock_get_htm_circle_region.return_value = [(100, 200), (300, 400)]
        transform_sky_distance_query(query, "field_name")
        self.assertDictEqual(
            query,
            {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "field_name": {
                                        "gte": 100,
                                        "lte": 200,
                                    }
                                }
                            },
                            {
                                "range": {
                                    "field_name": {
                                        "gte": 300,
                                        "lte": 400,
                                    }
                                }
                            },
                        ]
                    }
                }
            },
        )

    @patch("antares.api.common.elasticsearch.htm.get_htm_circle_region")
    def test_transforms_query_to_htm_ranges_with_complex_query(
        self, mock_get_htm_circle_region
    ):
        query = {
            "filter": {
                "bool": {
                    "must": [
                        {"range": {"properties.ztf_dec": {"gte": 333, "lte": 333}}},
                        {
                            "sky_distance": {
                                "distance": f"{1.0/3600.0} degree",
                                "field_name": {"center": "20d 10d"},
                            },
                        },
                    ]
                },
            }
        }
        mock_get_htm_circle_region.return_value = [(100, 200), (300, 400)]
        transform_sky_distance_query(query, "field_name")
        self.assertDictEqual(
            query,
            {
                "filter": {
                    "bool": {
                        "must": [
                            {"range": {"properties.ztf_dec": {"gte": 333, "lte": 333}}},
                            {
                                "bool": {
                                    "should": [
                                        {
                                            "range": {
                                                "field_name": {
                                                    "gte": 100,
                                                    "lte": 200,
                                                }
                                            }
                                        },
                                        {
                                            "range": {
                                                "field_name": {
                                                    "gte": 300,
                                                    "lte": 400,
                                                }
                                            }
                                        },
                                    ]
                                }
                            },
                        ]
                    },
                }
            },
        )

    def test_accepts_correct_format(self):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        transform_sky_distance_query(query, "field_name")

    def test_raises_error_if_wrong_format(self):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10no"},
                }
            }
        }
        with self.assertRaises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")

    def test_raises_error_if_cant_parse_distance_string(self):
        # Check wrong units
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} arcsec",  # not supported yet
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        with self.assertRaises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")
        # Check bad number string
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"one degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        with self.assertRaises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")
