from antares.sql.schema import SLocusAnnotation
from elasticsearch import Elasticsearch
from werkzeug.datastructures import Headers

from test.base import APITest


DEFAULT_LOCUS_ANNOTATION = {
    "comment": "My comment",
    "favorited": True,
}


def make_locus_annotation_post_payload(locus_annotation, owner_id, locus_id):
    return {
        "data": {
            "type": "locus_annotation",
            "attributes": locus_annotation,
            "relationships": {
                "owner": {"data": {"id": owner_id, "type": "user"}},
                "locus": {"data": {"id": locus_id, "type": "locus"}},
            },
        }
    }


def make_locus_annotation_patch_payload(id_, locus_annotation):
    return {
        "data": {
            "id": id_,
            "type": "locus_annotation",
            "attributes": locus_annotation,
        }
    }


def sample_locus_annotation(**kwargs):
    locus_annotation = {**DEFAULT_LOCUS_ANNOTATION}
    locus_annotation.update(kwargs)
    return SLocusAnnotation(**locus_annotation)


def commit_sample_locus_annotation(session, **kwargs):
    locus_annotation = sample_locus_annotation(**kwargs)
    session.add(locus_annotation)
    session.commit()
    return locus_annotation


class ApiLocusAnnotationDetailTest(APITest):
    def test_get_annotations_include_loci_includes_locus_for_each_annotation(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        elasticsearch = Elasticsearch(["elasticsearch"])
        for i in range(100):
            elasticsearch.index(
                index="loci_dev", id=i, body={"locus_id": str(i)}, refresh=True
            )
            commit_sample_locus_annotation(self.s, owner_id=user_id, locus_id=str(i))
        try:
            response = self.client.get(
                f"/v1/users/{user_id}/locus_annotations",
                query_string={
                    "include": "locus",
                    "page[limit]": 100,
                },
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json["included"]), 100)
        finally:
            for i in range(100):
                elasticsearch.delete(index="loci_dev", id=i)

    def test_unauthenticated_user_cannot_patch_locus_annotation(self):
        locus_annotation = commit_sample_locus_annotation(
            self.s, owner_id=666, locus_id="some-uuid"
        )
        payload = make_locus_annotation_patch_payload(
            locus_annotation.locus_annotation_id, {"favorited": False}
        )
        response = self.client.patch(
            f"/v1/locus_annotations/{locus_annotation.locus_annotation_id}",
            json=payload,
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_cannot_patch_locus_annotation_they_dont_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        locus_annotation = commit_sample_locus_annotation(
            self.s, owner_id=user_id + 1, locus_id="some-uuid"
        )
        payload = make_locus_annotation_patch_payload(
            locus_annotation.locus_annotation_id, {"favorited": False}
        )
        response = self.client.patch(
            f"/v1/locus_annotations/{locus_annotation.locus_annotation_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 401)

    def test_authenticated_user_gets_401_if_try_to_patch_locus_annotation_does_not_exist(
        self,
    ):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_locus_annotation_patch_payload(666, {"favorited": False})
        response = self.client.patch(
            f"/v1/locus_annotations/666", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_user_can_patch_locus_annotation_they_own(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        locus_annotation = commit_sample_locus_annotation(
            self.s, owner_id=user_id, locus_id="some-uuid"
        )
        payload = make_locus_annotation_patch_payload(
            locus_annotation.locus_annotation_id, {"favorited": False}
        )
        response = self.client.patch(
            f"/v1/locus_annotations/{locus_annotation.locus_annotation_id}",
            json=payload,
            headers=headers,
        )
        self.assertEqual(response.status_code, 200)

    def test_get_method_not_allowed_responds_405(self):
        response = self.client.get(f"/v1/locus_annotations/1")
        self.assertEqual(response.status_code, 405)

    def test_delete_method_not_allowed_responds_405(self):
        response = self.client.delete("/v1/locus_annotations/1")
        self.assertEqual(response.status_code, 405)


class ApiLocusAnnotationListTest(APITest):
    def test_authenticated_user_can_post_locus_annotation(self):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_locus_annotation_post_payload(
            DEFAULT_LOCUS_ANNOTATION, user_id, "locus-uuid"
        )
        response = self.client.post(
            f"/v1/users/{user_id}/locus_annotations", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 200)

    def test_authenticated_user_cannot_post_locus_annotation_to_other_user_gets_401(
        self,
    ):
        user_id = self.login_user("user", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_locus_annotation_post_payload(
            DEFAULT_LOCUS_ANNOTATION, user_id, "locus-uuid"
        )
        response = self.client.post(
            f"/v1/users/{user_id + 1}/locus_annotations", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_cannot_post_locus_annotation_to_other_user_gets_401(self):
        user_id = self.login_user("admin", "password")
        headers = Headers()
        headers.set("X-CSRF-TOKEN", self.cookies["csrf_access_token"])
        payload = make_locus_annotation_post_payload(
            DEFAULT_LOCUS_ANNOTATION, user_id, "locus-uuid"
        )
        response = self.client.post(
            f"/v1/users/{user_id + 1}/locus_annotations", json=payload, headers=headers
        )
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cannot_post_locus_annotation_gets_401(self):
        payload = make_locus_annotation_post_payload(
            DEFAULT_LOCUS_ANNOTATION, owner_id=666, locus_id="locus-uuid"
        )
        response = self.client.post("/v1/users/666/locus_annotations", json=payload)
        self.assertEqual(response.status_code, 401)

    def test_regular_user_can_get_their_locus_annotations(self):
        user_id = self.login_user("user", "password")
        response = self.client.get(f"/v1/users/{user_id}/locus_annotations")
        self.assertEqual(response.status_code, 200)

    def test_locus_annotations_list_only_returns_users_locus_annotations(self):
        user_id = self.login_user("user", "password")
        commit_sample_locus_annotation(
            self.s,
            comment="My locus_annotation",
            owner_id=user_id,
            locus_id="some-uuid",
        )
        commit_sample_locus_annotation(
            self.s,
            comment="Someone Else's locus_annotation",
            owner_id=666,
            locus_id="some-uuid",
        )
        response = self.client.get(f"/v1/users/{user_id}/locus_annotations")
        self.assertEqual(response.json["meta"]["count"], 1)
        self.assertEqual(len(response.json["data"]), 1)
        self.assertEqual(
            response.json["data"][0]["attributes"]["comment"], "My locus_annotation"
        )

    def test_regular_user_cannot_get_other_users_locus_annotations(self):
        user_id = self.login_user("user", "password")
        response = self.client.get(f"/v1/users/{user_id + 1}/locus_annotations")
        self.assertEqual(response.status_code, 401)

    def test_staff_user_can_get_their_locus_annotations(self):
        user_id = self.login_user("staff", "password")
        response = self.client.get(f"/v1/users/{user_id}/locus_annotations")
        self.assertEqual(response.status_code, 200)

    def test_staff_user_cannot_get_other_users_locus_annotations(self):
        user_id = self.login_user("staff", "password")
        response = self.client.get(f"/v1/users/{user_id + 1}/locus_annotations")
        self.assertEqual(response.status_code, 401)

    def test_locus_annotation_list_endpoint_404(self):
        response = self.client.get(f"/v1/locus_annotations")
        self.assertEqual(response.status_code, 404)
