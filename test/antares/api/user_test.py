from antares.api.resources.user.schemas import UserSchema
from antares.sql.schema import SUser

from test.base import APITest

DEFAULT_USER = {
    "username": "sampleuser",
    "name": "Sample User",
    "email": "sampleuser@noao.edu",
    "password": "password",
    "staff": False,
    "admin": False,
}


def sample_user(**kwargs):
    user = {**DEFAULT_USER}
    user.update(kwargs)
    return SUser(**user)


def commit_sample_user(session, **kwargs):
    user = sample_user(**kwargs)
    session.add(user)
    session.commit()
    return user


class ApiUserFavoriteLociTest(APITest):
    def test_user_can_get_favorites(self):
        pass

    def test_user_can_add_favorite(self):
        pass

    def test_user_can_remove_favorite(self):
        pass

    def test_user_cannot_get_favorite_from_other_user(self):
        pass

    def test_user_cannot_add_favorite_to_other_user(self):
        pass

    def test_user_cannot_remove_favorite_from_other_user(self):
        pass


class ApiUserTest(APITest):
    def make_post_payload(self, user):
        return {
            "data": {
                "type": "user",
                "attributes": user,
            }
        }

    def make_patch_payload(self, id_, user):
        return {
            "data": {
                "id": id_,
                "type": "user",
                "attributes": user,
            }
        }

    def login_user(self, username, password):
        response = self.client.post(
            "/v1/auth/login",
            json={
                "username": username,
                "password": password,
            },
        )
        if response.status_code != 200:
            raise Exception(f"Unable to login as {username} with password {password}")
        return response.json["access_token"]

    def test_create_user(self):
        payload = self.make_post_payload(DEFAULT_USER)
        response = self.client.post("/v1/users", json=payload)
        self.assertEqual(response.status_code, 200)
        user = UserSchema().load(response.json, partial=True)
        self.assertEqual(user["username"], DEFAULT_USER["username"])
        self.assertEqual(user["email"], DEFAULT_USER["email"])

    def test_create_user_doesnt_respond_with_password(self):
        payload = self.make_post_payload(DEFAULT_USER)
        response = self.client.post("/v1/users", json=payload)
        self.assertEqual(response.status_code, 200)
        user = UserSchema().load(response.json, partial=True)
        self.assertNotIn("password", user)

    def test_user_can_get_self(self):
        user = commit_sample_user(self.s)
        access_token = self.login_user("sampleuser", "password")
        response = self.client.get(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)

    def test_get_user_doesnt_respond_with_password(self):
        user = commit_sample_user(self.s)
        access_token = self.login_user("sampleuser", "password")
        response = self.client.get(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        user = UserSchema().load(response.json, partial=True)
        self.assertNotIn("password", user)

    # # This test is causing the test suite to hang?
    # def test_admin_user_can_view_list_of_users(self):
    #     user = commit_sample_user(self.s, admin=True)
    #     access_token = self.login_user("sampleuser", "password")
    #     response = self.client.get(
    #         f"/v1/users",
    #         headers={"Authorization": f"Bearer {access_token}"},
    #     )
    #     self.assertEqual(response.status_code, 200)

    def test_non_admin_user_cannot_view_other_user_list(self):
        user = commit_sample_user(self.s)
        self.assertFalse(user.admin)
        access_token = self.login_user("sampleuser", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f"/v1/users", headers={"Authorization": f"Bearer {access_token}"}
        )
        self.assertEqual(response.status_code, 401)

    def test_non_admin_user_cannot_view_other_user_detail(self):
        user1 = commit_sample_user(self.s, username="sampleuser", password="password")
        user2 = commit_sample_user(self.s, username="otheruser", password="goaway")
        self.assertFalse(user1.admin)
        # Log in as user 1
        access_token = self.login_user("sampleuser", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{user1.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        # But that we can't access the other user.
        response = self.client.get(
            f"/v1/users/{user2.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 401)

    def test_non_admin_user_cannot_grant_admin_rights(self):
        user = commit_sample_user(self.s)
        self.assertFalse(user.admin)
        access_token = self.login_user("sampleuser", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(user.user_id, {"admin": True}),
        )
        self.assertEqual(response.status_code, 401)

    def test_non_admin_user_cannot_grant_staff_rights(self):
        user = commit_sample_user(self.s)
        self.assertFalse(user.admin)
        access_token = self.login_user("sampleuser", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(user.user_id, {"staff": True}),
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_user_can_grant_admin_rights(self):
        admin_user = self.s.query(SUser).filter(SUser.username == "admin").first()
        non_admin_user = self.s.query(SUser).filter(SUser.username == "user").first()
        access_token = self.login_user("admin", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{admin_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{non_admin_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(non_admin_user.user_id, {"admin": True}),
        )
        # We have to call commit() because the update was done by a different transaction.
        self.s.commit()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(non_admin_user.admin)

    def test_admin_user_can_revoke_admin_rights(self):
        admin_user1 = self.s.query(SUser).filter(SUser.username == "admin").first()
        admin_user2 = commit_sample_user(
            self.s, admin=True, username="otheruser", password="noway"
        )
        self.assertTrue(admin_user1.admin)
        self.assertTrue(admin_user2.admin)
        access_token = self.login_user("admin", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{admin_user1.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{admin_user2.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(admin_user2.user_id, {"admin": False}),
        )
        # We have to call commit() because the update was done by a different transaction.
        self.s.commit()
        self.assertEqual(response.status_code, 200)
        self.assertFalse(admin_user2.admin)

    def test_admin_user_can_grant_staff_rights(self):
        admin_user = self.s.query(SUser).filter(SUser.username == "admin").first()
        non_staff_user = self.s.query(SUser).filter(SUser.username == "user").first()
        self.assertTrue(admin_user.admin)
        access_token = self.login_user("admin", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{admin_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{non_staff_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(non_staff_user.user_id, {"staff": True}),
        )
        # We have to call commit() because the update was done by a different transaction.
        self.s.commit()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(non_staff_user.staff)

    def test_admin_user_can_revoke_staff_rights(self):
        admin_user = self.s.query(SUser).filter(SUser.username == "admin").first()
        staff_user = self.s.query(SUser).filter(SUser.username == "staff").first()
        self.assertTrue(admin_user.admin)
        self.assertTrue(staff_user.staff)
        access_token = self.login_user("admin", "password")
        # Assert that we can get this user, just to make sure there isn't any funny business
        # happening with our token.
        response = self.client.get(
            f"/v1/users/{admin_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.patch(
            f"/v1/users/{staff_user.user_id}",
            headers={"Authorization": f"Bearer {access_token}"},
            json=self.make_patch_payload(staff_user.user_id, {"staff": False}),
        )
        # We have to call commit() because the update was done by a different transaction.
        self.s.commit()
        self.assertEqual(response.status_code, 200)
        self.assertFalse(staff_user.staff)
