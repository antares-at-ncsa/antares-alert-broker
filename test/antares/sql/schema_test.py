from antares.sql.schema import SWatchObjectId
from test.base import SQLTest


class SWatchObjectIdTest(SQLTest):
    def test(self):
        def _count_rows():
            self.newSession()
            return self.s.query(SWatchObjectId).count()

        ids1 = SWatchObjectId.generate_ids(1)
        self.assertEqual(_count_rows(), 1)
        ids2 = SWatchObjectId.generate_ids(2)
        self.assertEqual(_count_rows(), 1 + 2)
        ids3 = SWatchObjectId.generate_ids(5)
        self.assertEqual(_count_rows(), 1 + 2 + 5)
        ids4 = SWatchObjectId.generate_ids(1000)
        self.assertEqual(_count_rows(), 1 + 2 + 5 + 1000)
        self.assertEqual(ids1, [1])
        self.assertEqual(ids2, [2, 3])
        self.assertEqual(ids3, [4, 5, 6, 7, 8])
        self.assertEqual(ids4, list(range(9, 9 + 1000)))
