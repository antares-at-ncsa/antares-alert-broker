from test.base import Test

from antares.utils import nearest


class NearestPointTest(Test):
    def test_nearest_point_1(self):
        p1 = (0, 0)
        points = ((-2, -2), (1, 1), (2, 2), (3, 3))
        self.assertEqual((1, 1), nearest(p1, points))

    def test_nearest_point_2(self):
        p1 = (1, 1)
        points = ((-2, -2), (1, 1), (2, 2), (3, 3))
        self.assertEqual((1, 1), nearest(p1, points))

    def test_nearest_point_3(self):
        p1 = (1, 1)
        points = ((0.5, 0.5), (2, 2))
        self.assertEqual((0.5, 0.5), nearest(p1, points))
