import math
import random
from unittest.mock import patch

import htm
from nose.plugins.attrib import attr

from antares.config import config
from antares.utils import cone_search, angle_between

from test.base import Test


class ConeSearchTest(Test):
    def test_get_region_handles_precision_error(self):
        ra = 109.1830019
        dec = 4.0257554
        radius = 0.00028
        region = cone_search.get_region(ra, dec, radius, 16)

    @patch("antares.utils.cone_search.htm.get_htm_circle_region")
    def test_get_region_handles_precision_error(self, mock_get_htm_circle_region):
        ra = 109.1830019
        dec = 4.0257554
        radius = 0.00028
        mock_get_htm_circle_region.side_effect = [htm.PrecisionError, (20, 22)]
        region = cone_search.get_region(ra, dec, radius, 16)
        self.assertEqual(region, (20, 22))
        self.assertEqual(
            mock_get_htm_circle_region.call_args[0][2],
            0.00028 + config.HTM_PRECISION_ERROR_RETRY_OFFSET,
        )
        mock_get_htm_circle_region.side_effect = [
            htm.PrecisionError,
            htm.PrecisionError,
        ]
        with self.assertRaises(htm.PrecisionError):
            cone_search.get_region(ra, dec, radius, 16)

    @patch("antares.utils.cone_search.get_region")
    def test_get_region_denormalized(self, mock_get_region):
        mock_get_region.return_value = [(20, 22), (24, 25)]
        self.assertEqual(
            # Pass nonsense values into the get_region_denormalized function,
            # it's return is solely dependent on the return value of get_region
            list(cone_search.get_region_denormalized(0, 0, 0, 16)),
            [20, 21, 22, 24, 25],
        )

    def test_get_htm20(self):
        htm20 = cone_search.get_htm20(10, 10)
        self.assertEqual(htm20, 17098002819647)

    def test_cone_search_1(self):
        region = cone_search.get_region(10, 10, 0.000001, 20)
        expected = [(17098002819647, 17098002819647)]
        self.assertEqual(region, expected)

    def test_cone_search_2(self):
        region = cone_search.get_region(10, 10, 0.00028, 20)
        expected = [
            (17098002819584, 17098002819599),
            (17098002819601, 17098002819601),
            (17098002819604, 17098002819615),
            (17098002819620, 17098002819621),
            (17098002819623, 17098002819629),
            (17098002819631, 17098002819647),
            (17098002819796, 17098002819799),
            (17098002819810, 17098002819810),
            (17098002819816, 17098002819821),
            (17098002819823, 17098002819827),
            (17098002820097, 17098002820097),
            (17098002820100, 17098002820100),
            (17098002820102, 17098002820103),
            (17098002820110, 17098002820110),
            (17098002821376, 17098002821376),
            (17098002821378, 17098002821379),
            (17098002821384, 17098002821385),
            (17098002821387, 17098002821387),
            (17098002821389, 17098002821389),
            (17098002821412, 17098002821412),
            (17098002821414, 17098002821415),
            (17098002821428, 17098002821428),
        ]
        self.assertEqual(region, expected)

    def test_check_region_intersection(self):
        radius = 1 / 3600.0
        points = [
            (
                (10.345456754685678, 10.363474568567856),
                (10.345734254685677, 10.363474568567856),
            ),
            (
                (10.345456754685678, 10.363474568567856),
                (10.345734233694454, 10.363477981731792),
            ),
            (
                (10.345456754685678, 10.363474568567856),
                (10.345430324782892, 10.363478825194651),
            ),
            (
                (10.345456754685678, 10.363474568567856),
                (10.345408300962497, 10.363504225096284),
            ),
        ]
        for (p1, p2) in points:
            self.assertLess(angle_between(p1, p2), radius)
            r1 = set(
                cone_search.denormalize_region(cone_search.get_region(*p1, radius, 16))
            )
            r2 = set(
                cone_search.denormalize_region(cone_search.get_region(*p2, radius, 16))
            )
            print(p1, p2)
            self.assertTrue(r1 & r2)

    @attr("slow")
    def test_check_region_intersection_points_within_circle(self):
        radius = 1 / 3600.0
        p1 = 10.345456754685678567, 10.363474568567856786
        r1 = set(
            cone_search.denormalize_region(cone_search.get_region(*p1, radius, 16))
        )
        r = radius * 0.01
        while r < radius:
            angle = 0
            while angle < 2 * math.pi:
                p2 = point_on_circle(*p1, r, angle)
                sep = angle_between(p1, p2)
                print(sep, radius)
                self.assertLess(sep, radius)
                r2 = set(
                    cone_search.denormalize_region(
                        cone_search.get_region(*p2, radius, 16)
                    )
                )
                print(p1, p2)
                self.assertTrue(r1 & r2)
                angle += 0.0123
            r += radius * 0.01

    @attr("slow")
    def test_check_region_intersection_random_points_within_circle(self):
        radius = 1 / 3600.0
        p1 = 10.345456754685678567, 10.363474568567856786
        r1 = set(
            cone_search.denormalize_region(cone_search.get_region(*p1, radius, 16))
        )
        for i in range(10000):
            print()
            angle = 2 * math.pi * random.random()
            p2 = point_on_circle(*p1, radius * 0.9999 * random.random(), angle)
            sep = angle_between(p1, p2)
            print(sep, radius)
            self.assertLess(sep, radius)
            r2 = set(
                cone_search.denormalize_region(cone_search.get_region(*p2, radius, 16))
            )
            print(p1, p2)
            print(r1, r2)
            print(r1 & r2)
            self.assertTrue(r1 & r2)


def point_on_circle(x, y, r, a):
    return (
        r * math.cos(a) + x,
        r * math.sin(a) + y,
    )
