import pickle
from unittest.mock import MagicMock

import numpy as np
import pandas as pd
from antares.utils import ztf_flux_correction


def test_correct_mags():
    """
    These objects were manually inspected by Monika and serve as our source-of-truth
    for the correctness of the calibration routines.
    """
    locus_ids = ["ANT2020ru36o", "ANT2020p3emg", "ANT2019g4v6o", "ANT2019heqy2"]
    for locus_id in locus_ids:
        with open(f"data/{locus_id}.p", "rb") as f:
            locus = pickle.load(f)
        df, is_var_star, corrected = ztf_flux_correction.correct_mags(locus)
        with open(f"data/{locus_id}-corrected-df.p", "rb") as f:
            corrected_df = pickle.load(f)
        assert df.equals(corrected_df)


def test_is_var_star():
    df = pd.DataFrame()
    df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
    df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
    df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
    assert ztf_flux_correction._is_var_star(df)


def test_is_var_star_false_if_distnr_higher_than_threshold():
    df = pd.DataFrame()
    df["ztf_distnr"] = np.array([1.1, 1.2, 1.3])
    df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
    df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
    assert not ztf_flux_correction._is_var_star(df, match_radius_arcsec=1.0)


def test_is_var_star_false_if_distpsnr1_higher_than_threshold():
    df = pd.DataFrame()
    df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
    df["ztf_distpsnr1"] = np.array([1.1, 1.2, 1.3])
    df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
    assert not ztf_flux_correction._is_var_star(df, match_radius_arcsec=1.0)


def test_is_var_star_false_if_sgscore1_lower_than_threshold():
    df = pd.DataFrame()
    df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
    df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
    df["ztf_sgscore1"] = np.array([0.1, 0.2, 0.3])
    assert not ztf_flux_correction._is_var_star(df, star_galaxy_threshold=1.0)


def test_correct_mags_doesnt_mutate_locus():
    with open("data/ANT2020kaqtm-timeseries.p", "rb") as f:
        timeseries = pickle.load(f)
        f.seek(0)
        timeseries_copy = pickle.load(f)
    locus = MagicMock()
    locus.timeseries = timeseries
    ztf_flux_correction.correct_mags(locus)
    assert (locus.timeseries == timeseries_copy).all()


def test_correct_mags_aborts_if_var_star():
    # Not a varstar because sgscore is too low
    df = pd.DataFrame()
    df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
    df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
    df["ztf_sgscore1"] = np.array([0.1, 0.2, 0.3])
    locus = MagicMock()
    locus.timeseries.to_pandas.return_value = df
    df, is_var_star, corrected = ztf_flux_correction.correct_mags(locus)
    assert not corrected
    assert not is_var_star
    assert np.isnan(df["ant_mag_corrected"]).all()


def test_correct_mags_calibrates_lightcurve():
    with open("data/ANT2020kaqtm-timeseries.p", "rb") as f:
        timeseries = pickle.load(f)
    locus = MagicMock()
    locus.timeseries = timeseries
    df, is_var_star, corrected = ztf_flux_correction.correct_mags(locus)
    assert corrected
    assert is_var_star
    assert np.isfinite(df[df["ant_survey"] == 1]["ant_mag_corrected"]).all()


def test_correct_mags_imputes_missing_values():
    with open("data/ANT2020kaqtm-timeseries.p", "rb") as f:
        timeseries = pickle.load(f)
    nan_index = np.where(timeseries["ant_survey"] == 1)[0][0]
    timeseries["ztf_magnr"][nan_index] = np.NaN
    locus = MagicMock()
    locus.timeseries = timeseries
    df, is_var_star, corrected = ztf_flux_correction.correct_mags(locus)
    assert corrected
    assert is_var_star
    assert np.isfinite(df[df["ant_survey"] == 1]["ant_mag_corrected"]).all()
    assert np.isfinite(df[df["ant_survey"] == 1]["ant_magerr_corrected"]).all()
    assert np.isfinite(df[df["ant_survey"] == 1]["ant_magulim_corrected"]).all()
    assert np.isfinite(df[df["ant_survey"] == 1]["ant_magllim_corrected"]).all()
