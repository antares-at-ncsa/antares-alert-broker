import antares.devkit as dk
from antares.pipeline.filters.runnable_filter import RunnableFilter
from antares.pipeline.locus import LocusData
from antares.pipeline.pipeline import FilterSequence
from test.base import Test


class F1(dk.Filter):
    NAME = "F1"
    INPUT_ALERT_PROPERTIES = ["x"]
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "foo",
            "type": "int",
            "description": "",
        },
    ]

    def run(self, ld):
        ld.alert.properties["foo"] = 1


class F2(dk.Filter):
    NAME = "F2"
    INPUT_ALERT_PROPERTIES = []
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "bar",
            "type": "int",
            "description": "",
        },
    ]

    def run(self, ld):
        ld.alert.properties["bar"] = 1


class F3(dk.Filter):
    NAME = "F3"
    INPUT_LOCUS_PROPERTIES = ["y"]
    INPUT_ALERT_PROPERTIES = []
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "baz",
            "type": "int",
            "description": "",
        },
    ]

    def run(self, ld):
        ld.alert.properties["baz"] = 1


class FilterInputRequirementsTest(Test):
    def test(self):
        fs = FilterSequence(
            1,
            filters=[
                RunnableFilter(F1()),
                RunnableFilter(F2()),
                RunnableFilter(F3()),
            ],
        )

        ld = LocusData.from_dict(locus_dict)
        fs.run(ld)
        self.assertEqual(ld.api().alert.properties.get("foo"), None)
        self.assertEqual(ld.api().alert.properties.get("bar"), 1)
        self.assertEqual(ld.api().alert.properties.get("baz"), None)

        ld = LocusData.from_dict(locus_dict)
        ld.alert.properties["x"] = 1
        fs.run(ld)
        self.assertEqual(ld.api().alert.properties.get("foo"), 1)
        self.assertEqual(ld.api().alert.properties.get("bar"), 1)
        self.assertEqual(ld.api().alert.properties.get("baz"), None)

        ld = LocusData.from_dict(locus_dict)
        ld.locus.properties["y"] = 1
        fs.run(ld)
        self.assertEqual(ld.api().alert.properties.get("foo"), None)
        self.assertEqual(ld.api().alert.properties.get("bar"), 1)
        self.assertEqual(ld.api().alert.properties.get("baz"), 1)

        ld = LocusData.from_dict(locus_dict)
        ld.alert.properties["x"] = 1
        ld.locus.properties["y"] = 1
        fs.run(ld)
        self.assertEqual(ld.api().alert.properties.get("foo"), 1)
        self.assertEqual(ld.api().alert.properties.get("bar"), 1)
        self.assertEqual(ld.api().alert.properties.get("baz"), 1)


locus_dict = {
    "locus_id": "1fad6fb4-5073-11ea-8bfa-0242ac150005",
    "ra": 88.2744186,
    "dec": -5.0010774,
    "properties": {},
    "tags": [],
    "watch_list_ids": [],
    "watch_object_ids": [],
    "catalog_objects": {},
    "alerts": [
        {
            "alert_id": "ztf_candidate:1069343660115015016",
            "locus_id": "1fad6fb4-5073-11ea-8bfa-0242ac150005",
            "mjd": 58823.34366900008,
            "properties": {
                "p": 123,
                "q": 3.14,
                "r": "hi",
            },
        }
    ],
}
