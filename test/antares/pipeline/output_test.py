import unittest.mock

from antares.pipeline import output
from antares.rtdb.api import WatchListDict, WatchObjectDict

def test_format_watch_object_for_slack():
    with unittest.mock.patch("antares.pipeline.output.slack.post") as mock_slack_post:
        watch_list = WatchListDict(
            wl_id=1,
            name="test watch list",
            description="its a test",
            slack_channel="#testing"
        )
        watch_object = WatchObjectDict(
            wl_id=1,
            wo_id=2,
            ra=100.0,
            dec=20.0,
            radius=1.0/3600.0,
            htm_level=16,
            name="test object"
        )
        output.watch_object(unittest.mock.MagicMock(), watch_list, watch_object)
        print(mock_slack_post.call_args)
