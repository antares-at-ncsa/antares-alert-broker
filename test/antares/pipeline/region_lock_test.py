from antares.pipeline.pipeline import Pipeline, RegionLockTimeout
from antares.utils import randfloat, angle_between
from test.base import DBTest


class RegionLockTest(DBTest):
    def setUp(self):
        super().setUp()

    def test(self):
        arcsecond = 1 / 3600.0
        pipe1 = Pipeline(None)
        pipe2 = Pipeline(None)

        def random_point(center, radius):
            """
            Return a random point within `radius` angular distance from `p1`.
            """
            while True:
                p = (
                    randfloat(center[0] - radius, center[0] + radius),
                    randfloat(center[1] - radius, center[1] + radius),
                )
                if angle_between(center, p) < radius:
                    return p

        def lock(pipeline, ra, dec):
            try:
                pipeline._acquire_region_lock(ra, dec, timeout=0)
                return True
            except RegionLockTimeout:
                return False

        def release(pipeline):
            pipeline._release_region_lock()

        for _ in range(1000):
            p1 = random_point((45, 45), 40)
            p2 = random_point(p1, arcsecond)
            assert angle_between(p1, p2) < arcsecond

            self.assertTrue(lock(pipe1, *p1))  # Lock 1
            self.assertFalse(lock(pipe2, *p2))  # Lock 2 (fails)
            release(pipe1)  # Release 1
            self.assertTrue(lock(pipe2, *p2))  # Lock 2 (succeeds)
            release(pipe2)  # Release 2

            self.assertIsNone(pipe1._region_lock)
            self.assertIsNone(pipe2._region_lock)
