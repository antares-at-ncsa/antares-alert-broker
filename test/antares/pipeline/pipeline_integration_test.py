import json
import time

from antares import log, utils
from antares.config import config
from antares.elasticsearch import ingest
from antares.pipeline.ingestion import ztf
from antares.pipeline.main import AlertJob
from antares.pipeline.output import kafka_alerts
from antares.pipeline.pipeline import Pipeline
from antares.rtdb import catalog_migration, api
from antares.rtdb.api import RTDB
from antares.services import kafka

from test.base import DBTest
from test_data.ztf_alerts import put_to_kafka


def log_obj(obj):
    log.info("\n" + json.dumps(obj, indent=4))


INPUT_FILES = [
    "1069343660015010008.avro",
    "1069343660115010015.avro",
    "1069343660215015001.avro",
    "1069343661515015018.avro",
    "1069343660015010011.avro",
    "1069343660115015014.avro",
    "1069343660215015013.avro",
    "1069343661515015019.avro",
    "1069343660115010006.avro",
    "1069343660115015015.avro",
    "1069343661515015016.avro",
    "1069343660115010012.avro",
    "1069343660115015016.avro",
    "1069343661515015017.avro",
]


def drain_kafka_output(topic):
    result = []
    c = kafka.KafkaConsumer(topics=[topic], random_group=True)
    while True:
        _, msg = c.poll(timeout=10)
        if msg is None:
            break
        result.append(kafka_alerts.loads(msg.value()))
    return result


class PipelineIntegrationTest(DBTest):
    def setUp(self):
        super().setUp()
        # Populate C* catalogs
        catalog_migration._do_all_inline()

    def test(self):
        # Primary Pipeline integration test and DB-populator. Run this test to
        # populate your dev instance with data.

        # Run Pipeline on each input file in order
        p = Pipeline(provenance_id=1, kafka_producer=kafka.KafkaProducer())
        for fname in INPUT_FILES:
            blob = put_to_kafka.get_avro_blob(fname)
            for ztf_alert in ztf.decode_bytes(blob):
                p.process_alert(AlertJob(ztf_alert, time.time(), time.time()))

        rtdb = RTDB()

        # check correct number of Loci exist in C*
        loci = list(rtdb.get_all_loci())
        self.assertEqual(len(loci), 14)

        # check correct number of Alerts exist in C*
        alerts = list(rtdb.get_all_alerts())
        self.assertEqual(len(alerts), 127)

        # Check correct number of alerts were sent out on Kafka
        self.assertIn("hello_dev", kafka.KafkaAdmin().list_topic_names())
        hello_dev = drain_kafka_output("hello_dev")
        self.assertEqual(len(hello_dev), 9)

        # Check that catalog matches worked
        n_matches = 0
        for cat_name, ra, dec in EXPECTED_CATALOG_MATCHES:
            matched = False
            for a in hello_dev:
                p1 = (ra, dec)
                p2 = (a["data"]["attributes"]["ra"], a["data"]["attributes"]["dec"])
                if utils.angle_between(p1, p2) < 0.01:
                    self.assertIn(cat_name, a["data"]["attributes"]["catalogs"])
                    matched = True
                    n_matches += 1
                    if n_matches == 1:
                        log.info("Sample output kafka message:")
                        log_obj(a)
                        print(a["data"]["attributes"]["lightcurve"])
                    break
            self.assertTrue(matched)

        # Check correct number of Loci exist in ES
        loci_in_es = ingest.get_all_documents(config.ARCHIVE_INDEX_NAME)
        self.assertEqual(len(loci_in_es), 14)
        self.assertEqual(
            set(l.locus_id for l in loci),
            set(d["_id"] for d in loci_in_es),
        )

        # Check ES mapping is correct
        es_mapping = ingest.MappingCache().get()
        self.assertEqual(es_mapping, EXPECTED_ES_MAPPING)
        log.info("ES mapping:")
        log_obj(es_mapping)


EXPECTED_CATALOG_MATCHES = [
    ("french_post_starburst_gals", 88.2744186, -5.0010774),
]


EXPECTED_ES_MAPPING = {
    "loci_dev": {
        "mappings": {
            "properties": {
                "catalogs": {"type": "keyword", "eager_global_ordinals": True},
                "dec": {"type": "double"},
                "htm16": {"type": "long"},
                "locus_id": {"type": "keyword"},
                "properties": {
                    "properties": {
                        "a_keyword_property": {
                            "type": "keyword",
                            "eager_global_ordinals": True,
                        },
                        "brightest_alert_id": {"type": "text"},
                        "brightest_alert_magnitude": {
                            "type": "double",
                        },
                        "brightest_alert_observation_time": {
                            "type": "double",
                        },
                        "foo": {"type": "long"},
                        "newest_alert_id": {"type": "text"},
                        "newest_alert_magnitude": {
                            "type": "double",
                        },
                        "newest_alert_observation_time": {
                            "type": "double",
                        },
                        "num_alerts": {"type": "long"},
                        "num_mag_values": {"type": "long"},
                        "oldest_alert_id": {"type": "text"},
                        "oldest_alert_magnitude": {
                            "type": "double",
                        },
                        "oldest_alert_observation_time": {
                            "type": "double",
                        },
                        "ztf_ssnamenr": {"type": "keyword"},
                        "ztf_object_id": {"type": "keyword"},
                    }
                },
                "ra": {"type": "double"},
                "tags": {"type": "keyword", "eager_global_ordinals": True},
                "watch_list_ids": {"type": "integer"},
                "watch_object_ids": {"type": "integer"},
            }
        }
    }
}
