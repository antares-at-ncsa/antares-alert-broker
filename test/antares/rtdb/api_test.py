import datetime

from antares.config import config
from antares.rtdb import bootstrap
from antares.rtdb.api import RTDB
from antares.rtdb.antcassandra import execute
from antares.rtdb.catalog_config import CatalogConfig
from antares.rtdb.models import AlertModel
from antares.rtdb.schema import CHTMLUT

from test.base import CassandraTest


mjd = 59005.171180599835


empty_catalog_config = {
    "tables": [],
}


mock_catalog_config = {
    "tables": [
        {
            "table": "catalog1",
            "catalog_id": 1,
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "de",
            "object_id_column": "sid",
            "object_id_type": "int",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "catalog2",
            "catalog_id": 2,
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "de",
            "object_id_column": "sid",
            "object_id_type": "str",
            "radius": None,
            "radius_column": "radius",
        },
    ],
}


def create_catalog_tables(cat_config):
    keyspace = config.CASSANDRA_CATALOG_KEYSPACE
    assert keyspace.endswith("_dev") or keyspace.endswith("_staging")
    for t in cat_config.tables:
        table = t["table"]
        ra_column = t["ra_column"]
        dec_column = t["dec_column"]
        id_column = t["object_id_column"]
        id_type = t["object_id_type"]
        assert id_type in {"int", "str"}, id_type
        if id_type == "int":
            id_type = "int"
        elif id_type == "str":
            id_type = "text"
        radius_column = t["radius_column"] or "nope"

        query = f"""
            CREATE TABLE "{keyspace}"."{table}"(
                {id_column} {id_type} PRIMARY KEY,
                {ra_column} double,
                {dec_column} double,
                {radius_column} double
                );
        """
        execute(query)


class RTDBTest(CassandraTest):
    def setUp(self):
        super().setUp()
        self.rtdb = RTDB(catalog_config=CatalogConfig(empty_catalog_config))

    def test_put_alert(self):
        in_alerts = _mock_alerts()
        (locus_id,) = set(a.locus_id for a in in_alerts)
        self.rtdb.put_alerts(in_alerts)
        out_alerts = self.rtdb.get_alerts(locus_id)
        self.assertEqual(in_alerts[0].__dict__, out_alerts[0].__dict__)
        self.assertEqual(in_alerts[1].__dict__, out_alerts[1].__dict__)

    def test_get_alert_missing_locus(self):
        # get_alerts() should return [] for non-existing Locus
        locus_id = "ANT2020asdasfas"
        alerts = self.rtdb.get_alerts(locus_id)
        self.assertEqual(alerts, [])

    def test_get_alert_locus_without_alerts(self):
        # get_alerts() should return [] for an existing Locus with 0 alerts
        locus = self.rtdb.get_or_create_locus(45, 45, mjd)
        assert locus.locus_id
        locus_id = locus.locus_id
        alerts = self.rtdb.get_alerts(locus_id)
        self.assertEqual(alerts, [])

    def test_get_lightcurve(self):
        p1 = (50, 50)
        locus_id = self.rtdb._put_locus(*p1, mjd).locus_id
        locus = self.rtdb.get_locus_by_id(locus_id)
        locus.lightcurve = "blah"
        self.rtdb.update_locus(locus)
        lightcurve = self.rtdb.get_lightcurve(locus_id)
        self.assertIsNotNone(lightcurve)
        self.assertEqual(lightcurve, "blah")

    def test_get_lightcurve_returns_none(self):
        lightcurve = self.rtdb.get_lightcurve("DNE")
        self.assertIsNone(lightcurve)

    def test_get_locus_by_id(self):
        p1 = (50, 50)

        loci = self.rtdb.get_loci(*p1, config.LOCUS_AGGREGATION_RADIUS_DEGREES)
        self.assertEqual(len(loci), 0)
        locus_id = self.rtdb._put_locus(*p1, mjd).locus_id
        locus = self.rtdb.get_locus_by_id(locus_id)
        self.assertEqual(locus_id, str(locus.locus_id))

    def test_locus_get_put_move(self):
        p1 = (50, 50)
        p2 = (55, 55)
        r = config.LOCUS_AGGREGATION_RADIUS_DEGREES
        loci = self.rtdb.get_loci(*p1, config.LOCUS_AGGREGATION_RADIUS_DEGREES)
        self.assertEqual(len(loci), 0)
        locus_id = self.rtdb._put_locus(*p1, mjd).locus_id
        loci = self.rtdb.get_loci(*p1, r)
        self.assertEqual(len(loci), 1)
        (locus,) = loci
        self.assertEqual(locus_id, str(locus.locus_id))
        self.rtdb.move_locus(locus_id, *p1, *p2)
        loci = self.rtdb.get_loci(*p1, r)
        self.assertEqual(len(loci), 0)  # Locus no longer at old position
        loci = self.rtdb.get_loci(*p2, r)
        self.assertEqual(len(loci), 1)  # Locus now at new position
        (locus,) = loci
        self.assertEqual(locus_id, str(locus.locus_id))

    def test_get_locus_near_same_point(self):
        from antares.utils import angle_between, randfloat
        from antares.rtdb.api import RTDB

        mjd = 59005.171180599835
        rtdb = RTDB()
        arcsecond = 1 / 3600.0

        def random_point(center, radius):
            """
            Return a random point within `radius` angular distance from `p1`.
            """
            while True:
                p = (
                    randfloat(center[0] - radius, center[0] + radius),
                    randfloat(center[1] - radius, center[1] + radius),
                )
                if angle_between(center, p) < radius:
                    return p

        for _ in range(100):
            p1 = random_point((45, 45), 40)
            for _ in range(10):
                p2 = random_point(p1, arcsecond)
                assert angle_between(p1, p2) < arcsecond
                l_1 = rtdb.get_or_create_locus(*p1, mjd)
                l_2 = rtdb.get_or_create_locus(*p2, mjd)
                assert l_1.locus_id == l_2.locus_id, (p1, p2)

    def test_locus_update(self):
        updated_at = datetime.datetime.utcnow().replace(microsecond=0)
        props = dict(a=10, b=3.14, c=[1, 2, 3], d="hi")
        tags = {"abc", "foo"}
        wl_ids = {1, 2}
        wo_ids = {4, 5}

        locus = self.rtdb.get_or_create_locus(0, 0, mjd)
        locus_id = locus.locus_id
        locus.updated_at = updated_at
        locus.properties = props
        locus.tags = tags
        locus.wl_ids = wl_ids
        locus.wo_ids = wo_ids
        self.rtdb.update_locus(locus)

        locus = self.rtdb.get_or_create_locus(0, 0, mjd)
        self.assertEqual(locus_id, locus.locus_id)
        self.assertEqual(updated_at, locus.updated_at)
        self.assertEqual(props, locus.properties)
        self.assertEqual(tags, locus.tags)
        self.assertEqual(wl_ids, locus.wl_ids)
        self.assertEqual(wo_ids, locus.wo_ids)

    def test_locus_update_does_not_move_locus(self):
        locus = self.rtdb.get_or_create_locus(0, 0, mjd)
        locus_id = locus.locus_id
        locus.ra = 1
        locus.dec = 1
        self.rtdb.update_locus(locus)
        locus = self.rtdb.get_or_create_locus(0, 0, mjd)
        self.assertEqual(locus_id, locus.locus_id)
        self.assertEqual(0, locus.ra)
        self.assertEqual(0, locus.dec)

    def test_get_locus_id(self):
        locus_id_1 = self.rtdb.get_or_create_locus(50, 50, mjd).locus_id
        locus_id_2 = self.rtdb.get_or_create_locus(50, 50.00001, mjd).locus_id
        locus_id_3 = self.rtdb.get_or_create_locus(50, 60, mjd).locus_id
        self.assertEqual(locus_id_1, locus_id_2)
        self.assertNotEqual(locus_id_1, locus_id_3)
        self.assertNotEqual(locus_id_2, locus_id_3)

    def test_watched_objects(self):
        wl_id = 1
        ra = 10
        dec = 10

        # match
        self.rtdb.put_watch_object(
            wl_id=wl_id,
            wo_id=1,
            ra=ra,
            dec=dec + 0.001,
            radius=0.002,
            name="WatchedObject 1",
        )

        # NOT match
        self.rtdb.put_watch_object(
            wl_id=wl_id,
            wo_id=2,
            ra=ra,
            dec=dec + 0.002,
            radius=0.001,
            name="WatchedObject 2",
        )

        # NOT match
        self.rtdb.put_watch_object(
            wl_id=wl_id,
            wo_id=3,
            ra=ra,
            dec=dec + 10,
            radius=0.01,
            name="WatchedObject 3",
        )

        # match
        self.rtdb.put_watch_object(
            wl_id=wl_id,
            wo_id=4,
            ra=ra,
            dec=dec + 0.05,
            radius=0.051,
            name="WatchedObject 4",
        )

        # Check all WOs were stored
        self.assertEqual(sorted(self.rtdb.list_watch_objects()), [1, 2, 3, 4])

        # Test WO search
        wo_ids = self.rtdb.search_watch_objects(ra, dec)
        self.assertEqual(sorted(wo_ids), sorted([(1, 4), (1, 1)]))

        # Test delete WOs
        self.rtdb.delete_watch_object(wl_id, 1)

        # Test WO search
        wo_ids = self.rtdb.search_watch_objects(ra, dec)
        self.assertEqual(wo_ids, [(1, 4)])

        # Test delete WOs
        self.rtdb.delete_watch_object(wl_id, 2)
        self.rtdb.delete_watch_object(wl_id, 3)
        self.rtdb.delete_watch_object(wl_id, 4)
        self.assertEqual(list(self.rtdb.list_watch_objects()), [])
        self.assertEqual(list(CHTMLUT.all()), [])


class RTDBCatalogTest(CassandraTest):
    """
    Tests that require a mock catalog schema
    """

    def setUp(self):
        super().setUp()
        bootstrap.drop_and_create_schema(config.CASSANDRA_CATALOG_KEYSPACE)
        cc = CatalogConfig(mock_catalog_config)
        create_catalog_tables(cc)
        self.rtdb = RTDB(catalog_config=cc)

    def test_cassandra_catalog_search(self):
        self.rtdb.put_catalog_object(1, 10, 0, 0)
        cat_obj = self.rtdb.get_catalog_object(1, 10)
        self.assertIsNotNone(cat_obj)
        r = config.LOCUS_AGGREGATION_RADIUS_DEGREES * 0.9
        self.rtdb.put_catalog_object_htmlut(1, 10, 0, 0, r)

        hits = self.rtdb._get_catalog_matches(0, 0)
        self.assertSetEqual(hits, {(1, "10")})
        hits = self.rtdb._get_catalog_matches(0, r * 0.9)
        self.assertSetEqual(hits, {(1, "10")})
        hits = self.rtdb._get_catalog_matches(0, 1 * 1.1)
        self.assertSetEqual(hits, set())

        self.rtdb.put_catalog_object(2, 20, 1, 1, 1.8)
        self.rtdb.put_catalog_object_htmlut(2, 20, 1, 1, 1.8)

        hits = self.rtdb._get_catalog_matches(1, 1)
        self.assertSetEqual(hits, {(2, "20")})
        hits = self.rtdb._get_catalog_matches(0, 0)
        self.assertSetEqual(hits, {(2, "20"), (1, "10")})
        hits = self.rtdb._get_catalog_matches(5, 5)
        self.assertSetEqual(hits, set())


def _mock_alerts():
    return [
        AlertModel(
            locus_id="02a12a6c-3fea-40b5-945d-9c97d158f088",
            mjd=5000.0,
            alert_id="ztf_candidate:1234567890",
            processed_at=datetime.datetime(2019, 10, 10, 5, 15, 0),
            properties=dict(
                x=10,
                y=26.191099166870117,
                z="hello",
            ),
        ),
        AlertModel(
            locus_id="02a12a6c-3fea-40b5-945d-9c97d158f088",
            mjd=5001.0,
            alert_id="ztf_candidate:1234567891",
            processed_at=datetime.datetime(2019, 10, 10, 5, 16, 0),
            properties=dict(
                x=11,
                y=26.191099166870117 * 7,
                z="goodbye",
            ),
        ),
    ]
