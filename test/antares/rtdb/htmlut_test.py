from antares.rtdb.htmlut import HTMLUT
from antares.rtdb.schema import CCatalogHTMLUT, CHTMLUT

from test.base import CassandraTest


class HTMLUTTest(CassandraTest):
    def setUp(self):
        super().setUp()

    def test_get_put_locus(self):
        h = HTMLUT()
        h.put_locus(1, "a")
        h.put_locus(1, "a")
        h.put_locus(1, "b")
        self.assertEqual(len(list(CHTMLUT.all())), 2)

        h = HTMLUT()
        h.load(1)
        expected = ["a", "b"]
        self.assertEqual(sorted(h.get_loci()), expected)

        # Loading is idempotent
        h = HTMLUT()
        h.load(1)
        h.load(1)
        self.assertEqual(sorted(h.get_loci()), expected)

    def test_get_put_cat(self):
        h = HTMLUT()
        h.put_cat_obj(1, 1, "x")
        h.put_cat_obj(1, 2, "y")
        h.put_cat_obj(1, 2, "y")
        h.put_cat_obj(2, 2, "z")
        self.assertEqual(len(list(CCatalogHTMLUT.all())), 3)

        expected = [(1, "x"), (2, "y"), (2, "z")]
        h = HTMLUT()
        h.load(1)
        h.load(2)
        self.assertEqual(sorted(h.get_cat_objs()), expected)

        h = HTMLUT()
        h.load(1)
        h.load(2)
        h.load(2)
        self.assertEqual(sorted(h.get_cat_objs()), expected)

    def test_get_put_wo(self):
        h = HTMLUT()
        h.put_watch_obj(1, 1, 5)
        h.put_watch_obj(1, 2, 6)
        h.put_watch_obj(1, 2, 6)
        h.put_watch_obj(2, 2, 6)
        h.put_watch_obj(2, 2, 7)
        self.assertEqual(len(list(CHTMLUT.all())), 4)

        expected = [(1, 5), (2, 6), (2, 7)]
        h = HTMLUT()
        h.load(1)
        h.load(2)
        self.assertEqual(sorted(h.get_watch_objs()), expected)

        h = HTMLUT()
        h.load(1)
        h.load(2)
        h.load(1)
        self.assertEqual(sorted(h.get_watch_objs()), expected)

    def test_get_put_all(self):
        h = HTMLUT()
        h.put_locus(1, "a")
        h.put_locus(1, "a")
        h.put_locus(1, "b")
        h.put_cat_obj(1, 1, "x")
        h.put_cat_obj(1, 2, "y")
        h.put_cat_obj(1, 2, "y")
        h.put_cat_obj(2, 2, "z")
        h.put_watch_obj(1, 1, 5)
        h.put_watch_obj(1, 2, 6)
        h.put_watch_obj(1, 2, 6)
        h.put_watch_obj(2, 2, 6)
        h.put_watch_obj(2, 2, 7)
        self.assertEqual(len(list(CCatalogHTMLUT.all())), 3)
        self.assertEqual(len(list(CHTMLUT.all())), 2 + 4)

        h = HTMLUT()
        h.load(1)
        h.load(2)
        expected = ["a", "b"]
        self.assertEqual(sorted(h.get_loci()), expected)
        expected = [(1, "x"), (2, "y"), (2, "z")]
        self.assertEqual(sorted(h.get_cat_objs()), expected)
        expected = [(1, 5), (2, 6), (2, 7)]
        self.assertEqual(sorted(h.get_watch_objs()), expected)

        h = HTMLUT()
        h.load(1)
        expected = ["a", "b"]
        self.assertEqual(sorted(h.get_loci()), expected)
        expected = [(1, "x"), (2, "y")]
        self.assertEqual(sorted(h.get_cat_objs()), expected)
        expected = [(1, 5), (2, 6)]
        self.assertEqual(sorted(h.get_watch_objs()), expected)

        h = HTMLUT()
        h.load(2)
        expected = []
        self.assertEqual(sorted(h.get_loci()), expected)
        expected = [(2, "z")]
        self.assertEqual(sorted(h.get_cat_objs()), expected)
        expected = [(2, 6), (2, 7)]
        self.assertEqual(sorted(h.get_watch_objs()), expected)
