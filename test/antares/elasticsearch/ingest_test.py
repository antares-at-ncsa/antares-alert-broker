# import unittest.mock
#
# from antares.elasticsearch import ingest
#
# from test.base import Test
#
#
# class TestGenerateBulkIndexActionsFromKafka(Test):
#
#     def test_ensures_mapping(self):
#         mock_kafka_client = unittest.mock.MagicMock()
#         mock_elasticsearch_client = unittest.mock.MagicMock()
#         mock_elasticsearch_client.indices.get_mapping.return_value = {
#             "test_index": {"mappings": {}}
#         }
#         mock_kafka_client.poll.side_effect = [
#             (None, {"new_alert": {"alert_id": 0, "properties": {"property": 123}}}),
#             (None, {"new_alert": {"alert_id": 1, "properties": {"property": 456}}}),
#             (None, None),
#         ]
#         actions = list(
#             ingest._generate_bulk_index_actions_from_kafka(
#                 kafka_client=mock_kafka_client,
#                 elasticsearch_client=mock_elasticsearch_client,
#                 elasticsearch_index="test_index",
#                 max_chunk_size=100,
#                 elasticsearch_ensure_explicit_mappings=True,
#             )
#         )
#         self.assertDictEqual(
#             actions[0],
#             {
#                 "_op_type": "index",
#                 "_index": "test_index",
#                 "_type": "_doc",
#                 "_id": 0,
#                 "_source": {"alert_id": 0, "properties": {"property": 123}},
#             },
#         )
#         self.assertDictEqual(
#             actions[1],
#             {
#                 "_op_type": "index",
#                 "_index": "test_index",
#                 "_type": "_doc",
#                 "_id": 1,
#                 "_source": {"alert_id": 1, "properties": {"property": 456}},
#             },
#         )
#
#     def test_generates_proper_elasticsearch_actions(self):
#         mock_kafka_client = unittest.mock.MagicMock()
#         mock_elasticsearch_client = unittest.mock.MagicMock()
#         mock_kafka_client.poll.side_effect = [
#             (None, {"new_alert": {"alert_id": 0, "properties": {"property": 123}}}),
#             (None, {"new_alert": {"alert_id": 1, "properties": {"property": 456}}}),
#             (None, None),
#         ]
#         actions = list(
#             ingest._generate_bulk_index_actions_from_kafka(
#                 kafka_client=mock_kafka_client,
#                 elasticsearch_client=mock_elasticsearch_client,
#                 elasticsearch_index="test_index",
#                 max_chunk_size=100,
#                 elasticsearch_ensure_explicit_mappings=False,
#             )
#         )
#         self.assertDictEqual(
#             actions[0],
#             {
#                 "_op_type": "index",
#                 "_index": "test_index",
#                 "_type": "_doc",
#                 "_id": 0,
#                 "_source": {"alert_id": 0, "properties": {"property": 123}},
#             },
#         )
#         self.assertDictEqual(
#             actions[1],
#             {
#                 "_op_type": "index",
#                 "_index": "test_index",
#                 "_type": "_doc",
#                 "_id": 1,
#                 "_source": {"alert_id": 1, "properties": {"property": 456}},
#             },
#         )
#
#     def test_generates_at_most_chunk_size_documents(self):
#         mock_kafka_client = unittest.mock.MagicMock()
#         mock_elasticsearch_client = unittest.mock.MagicMock()
#         mock_kafka_client.poll.return_value = (None, unittest.mock.MagicMock())
#         actions = list(
#             ingest._generate_bulk_index_actions_from_kafka(
#                 kafka_client=mock_kafka_client,
#                 elasticsearch_client=mock_elasticsearch_client,
#                 elasticsearch_index="test_index",
#                 max_chunk_size=100,
#                 elasticsearch_ensure_explicit_mappings=False,
#             )
#         )
#         self.assertEqual(len(actions), 100)
#
#     def test_generates_less_than_chunk_size_documents_if_timeout_reached_waiting_on_kafka(
#         self
#     ):
#         mock_kafka_client = unittest.mock.MagicMock()
#         mock_elasticsearch_client = unittest.mock.MagicMock()
#         mock_kafka_client.poll.side_effect = [
#             (None, unittest.mock.MagicMock()),
#             (None, unittest.mock.MagicMock()),
#             (None, None),
#         ]
#         actions = list(
#             ingest._generate_bulk_index_actions_from_kafka(
#                 kafka_client=mock_kafka_client,
#                 elasticsearch_client=mock_elasticsearch_client,
#                 elasticsearch_index="test_index",
#                 elasticsearch_ensure_explicit_mappings=False,
#             )
#         )
#         self.assertEqual(len(actions), 2)
#
#
# class TestGetAlertMappingPatch(Test):
#     def test_returns_none_if_no_updates_necessary(self):
#         mapping = {
#             "test_index": {
#                 "mappings": {
#                     "properties": {
#                         "top_level_property_1": {"type": "float"},
#                         "top_level_property_2": {"type": "float"},
#                         "properties": {
#                             "properties": {
#                                 "nested_property_1": {"type": "text"},
#                                 "nested_property_2": {"type": "text"},
#                             }
#                         },
#                     }
#                 }
#             }
#         }
#         alert = {
#             "top_level_property_1": 1.23,
#             "properties": {"nested_property_1": "test"},
#         }
#         mapping_patch = ingest._get_alert_mapping_patch(
#             "test_index", alert, mapping=mapping
#         )
#         self.assertIsNone(mapping_patch)
#
#     def test_returns_top_level_mapping(self):
#         mapping = {
#             "test_index": {
#                 "mappings": {
#                     "properties": {
#                         "top_level_property_1": {"type": "float"},
#                         "top_level_property_2": {"type": "float"},
#                         "properties": {
#                             "properties": {
#                                 "nested_property_1": {"type": "text"},
#                                 "nested_property_2": {"type": "text"},
#                             }
#                         },
#                     }
#                 }
#             }
#         }
#         alert = {
#             "top_level_property_1": 1.23,
#             "top_level_property_3": 1.23,
#             "properties": {"nested_property_1": "test"},
#         }
#         mapping_patch = ingest._get_alert_mapping_patch(
#             "test_index", alert, mapping=mapping
#         )
#         self.assertDictEqual(
#             mapping_patch, {"properties": {"top_level_property_3": {"type": "float"}}}
#         )
#
#     def test_returns_property_level_mapping(self):
#         mapping = {
#             "test_index": {
#                 "mappings": {
#                     "properties": {
#                         "top_level_property_1": {"type": "float"},
#                         "top_level_property_2": {"type": "float"},
#                         "properties": {
#                             "properties": {
#                                 "nested_property_1": {"type": "text"},
#                                 "nested_property_2": {"type": "text"},
#                             }
#                         },
#                     }
#                 }
#             }
#         }
#         alert = {
#             "top_level_property_1": 1.23,
#             "properties": {"nested_property_1": "test", "nested_property_3": 1.23},
#         }
#         mapping_patch = ingest._get_alert_mapping_patch(
#             "test_index", alert, mapping=mapping
#         )
#         self.assertDictEqual(
#             mapping_patch,
#             {
#                 "properties": {
#                     "properties": {
#                         "properties": {"nested_property_3": {"type": "float"}}
#                     }
#                 }
#             },
#         )
#
#     def test_returns_property_and_top_level_mapping(self):
#         mapping = {
#             "test_index": {
#                 "mappings": {
#                     "properties": {
#                         "top_level_property_1": {"type": "float"},
#                         "top_level_property_2": {"type": "float"},
#                         "properties": {
#                             "properties": {
#                                 "nested_property_1": {"type": "text"},
#                                 "nested_property_2": {"type": "text"},
#                             }
#                         },
#                     }
#                 }
#             }
#         }
#         alert = {
#             "top_level_property_1": 1.23,
#             "top_level_property_3": 1.23,
#             "properties": {"nested_property_1": "test", "nested_property_3": 1.23},
#         }
#         mapping_patch = ingest._get_alert_mapping_patch(
#             "test_index", alert, mapping=mapping
#         )
#         self.assertDictEqual(
#             mapping_patch,
#             {
#                 "properties": {
#                     "top_level_property_3": {"type": "float"},
#                     "properties": {
#                         "properties": {"nested_property_3": {"type": "float"}}
#                     },
#                 }
#             },
#         )
