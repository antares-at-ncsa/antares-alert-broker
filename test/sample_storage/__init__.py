import os

from antares.sql import engine
from antares.sql.schema import SStorage


def bootstrap():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    for dic in manifest:
        with open(os.path.join(dir_path, dic["file"]), "rb") as f:
            data = f.read()
        with engine.session() as s:
            s.add(
                SStorage(
                    storage_key=dic["storage_key"],
                    data=data,
                )
            )
            s.commit()


manifest = [
    {
        "storage_key": "cstubens/foo_File.txt",
        "file": "foo_File.txt",
    },
    # These files are private.
    # {
    #     "storage_key": "monika/anomaly/v2/anmly_g-R_logdt_dm.json",
    #     "file": "anmly_training_sample.npy",
    # },
    # {
    #     "storage_key": "monika/anomaly/v2/anmly_g_logdt_dm.json",
    #     "file": "anmly_g_logdt_dm.json",
    # },
    # {
    #     "storage_key": "monika/anomaly/v2/anmly_R_logdt_dm.json",
    #     "file": "anmly_R_logdt_dm.json",
    # },
    # {
    #     "storage_key": "monika/anomaly/v2/anmly_training_sample.npy",
    #     "file": "anmly_training_sample.npy",
    # },
    # {
    #     "storage_key": "rstreet_all_features.txt",
    #     "file": "rstreet_all_features.txt",
    # },
    # {
    #     "storage_key": "rstreet_pca_features.txt",
    #     "file": "rstreet_pca_features.txt",
    # },
]
