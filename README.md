# About ANTARES

ANTARES is an Astronomy Alert Broker developed by the [NOIRLab](noirlab.edu)
for optical surveys like ZTF and LSST. You can visit our deployment of ANTARES
at https://antares.noirlab.edu.

# Developer's Guide

## The Structure of ANTARES

An ANTARES deployment comprises a number of microservices and databases. This section
will describe each of the applications contained in this repository and present their
common configuration options.

### The ANTARES Web Frontend

The ANTARES web frontend is a Typescript application using the Vue.js framework. All
of its code is contained in the `antares/frontend` directory of this project.

**Image:** registry.gitlab.com/nsf-noirlab/csdc/antares/antares/frontend:v1.7.0

**Configuration:** Configuration values are specified in `antares/frontend/public/config.json`.
Our deployment pattern is to specify a Kubernetes ConfigMap that mounts to `/usr/share/nginx/html/config.json`
in the deployed container. **THESE ARE CONFIGURATION SETTINGS FOR THE CLIENT USER SO
DON'T PUT ANYTHING REMOTELY SECRET HERE**

| Name                 | Required | Default                    | Description  |
| -------------------- | -------- | -------------------------- | ------------ |
| ANTARES_API_URL      | No       | `http://localhost:8000/v1` | API URL      |
| ANTARES_FRONTEND_URL | No       | `http://localhost:8080`    | Frontend URL |

### The ANTARES API

**Image:** registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend:v1.7.0

**Configuration:** Configuration values are specified in `antares/config.py` and are
shared between all applications. Values can be passed by setting the corresponding
environment variable, or by putting values into a YAML file and setting the environment
variable `YAML_CONFIG_PATH` to the path to the file. The table below contains a
recommended set of values that you should specify to successfully deploy the API.

| Name                       | Required | Default                                       | Description                                                                                 |
| -------------------------- | -------- | --------------------------------------------- | ------------------------------------------------------------------------------------------- |
| DEBUG                      | No       | `True`                                        | Set debug flag (disable in production!)                                                     |
| LOG_LEVEL                  | No       | `"DEBUG"`                                     | Set log level                                                                               |
| SECRET_KEY                 | Yes      | `"notsecret"`                                 | Secret key used for JWT authentication                                                      |
| MYSQL_DB_URL               | Yes      | ` "mysql://root:root@mysql:3306/antares_dev"` | Connection information for the MySQL database                                               |
| SQLALCHEMY_ECHO            | No       | `True`                                        | Get additional output from MySQL transactions                                               |
| API_MAIL_SERVER            | Yes      | `""`                                          | Mail server for sending emails                                                              |
| API_MAIL_PORT              | No       | `587`                                         | Mail server port                                                                            |
| API_MAIL_USE_TLS           | No       | `False`                                       | Use TLS to connect to mail server                                                           |
| API_MAIL_USERNAME          | No       | `""`                                          | Mail server username                                                                        |
| API_MAIL_PASSWORD          | No       | `""`                                          | Mail server password                                                                        |
| API_BASE_URL               | Yes      | `"http://localhost:8000"`                     | Base service URL (should likely have a "/v1" suffix for production deploys)                 |
| API_JWT_COOKIE_DOMAIN      | No       | `None`                                        | Allow cross-domain authentication cookie use                                                |
| CASSANDRA_ECHO             | No       | `True`                                        | Get additional output from Cassandra transactions                                           |
| CASSANDRA_USER             | Yes      | `""`                                          | Cassandra username                                                                          |
| CASSANDRA_PASS             | Yes      | `""`                                          | Cassandra password                                                                          |
| CASSANDRA_HOSTS            | Yes      | `[]`                                          | List of Cassandra hosts                                                                     |
| CASSANDRA_ALERT_KEYSPACE   | Yes      | `"antares_alerts_dev"`                        | Keyspace of alert database                                                                  |
| CASSANDRA_CATALOG_KEYSPACE | Yes      | `"antares_catalogs_dev"`                      | Keyspace of catalog database                                                                |
| ELASTICSEARCH_HOSTS        | Yes      | `["elasticsearch"]`                           | List of Elasticsearch hosts                                                                 |
| ELASTICSEARCH_PORT         | No       | `9200`                                        | Port to connect to Elasticsearch host on                                                    |
| ELASTICSEARCH_OPTIONS      | No       | `{}`                                          | Dictionary of options passed as `**kwargs` to the `elasticsearch.Elasticsearch` constructor |
| ARCHIVE_INDEX_NAME         | Yes      | `"loci_dev"`                                  | Name of the Elasticsearch index that contains the locus index                               |
| FRONTEND_BASE_URL          | Yes      | `"http://localhost:8080"`                     | URL that the frontend is accessible at                                                      |

### The ANTARES Pipeline

**Image:** registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend:v1.7.0

**Configuration:** Configuration values are specified in `antares/config.py` and are
shared between all applications. Values can be passed by setting the corresponding
environment variable, or by putting values into a YAML file and setting the environment
variable `YAML_CONFIG_PATH` to the path to the file. The table below contains a
recommended set of values that you should specify to successfully deploy the pipeline.

| Name                            | Required | Default                                          | Description                                                                                                    |
| --------------------------------| -------- | ------------------------------------------------ | -------------------------------------------------------------------------------------------------------------- |
| DEBUG                           | No       | `True`                                           | Set debug flag (disable in production!)                                                                        |
| LOG_LEVEL                       | No       | `"DEBUG"`                                        | Set log level                                                                                                  |
| MYSQL_DB_URL                    | Yes      | ` "mysql://root:root@mysql:3306/antares_dev"`    | Connection information for the MySQL database                                                                  |
| SQLALCHEMY_ECHO                 | No       | `True`                                           | Get additional output from MySQL transactions                                                                  |
| ARCHIVE_INDEX_KAFKA_TOPIC_NAME  | Yes      | `"to_index_dev"`                                 | Kafka topic to pull documents from. Should match the same config setting in the pipeline.                      |
| ELASTICSEARCH_HOSTS             | Yes      | `["elasticsearch"]`                              | List of Elasticsearch hosts                                                                                    | 
| ELASTICSEARCH_PORT              | No       | `9200`                                           | Port to connect to Elasticsearch host on                                                                       |
| ELASTICSEARCH_OPTIONS           | No       | `{}`                                             | Dictionary of options passed as `**kwargs` to the `elasticsearch.Elasticsearch` constructor                    |
| ARCHIVE_ENABLE                  | Yes      | `"to_index_dev"`                                 | Kafka topic to pull documents from. Should match the same config setting in the pipeline.                      |
| PIPELINE_DISABLE_CRASHED_STAGES | No       | `True`                                           | Disable filters that crash                                                                                     |
| PIPELINE_RAISE_STAGE_ERRORS     | No       | `True`                                           | Propagate errors from filters                                                                                  |
| OUTPUTS                         | No       | `[]`                                             | Set Kafka outputs (e.g. `[{"name": "XXX", "type": "kafka", "criteria": {"has_tag": "XXX"}, "topic": "XXX"}]`) |
| WORKER_RAISE_ERRORS             | No       | `True`                                           | Propagate errors from workers                                                                                  |
| WORKER_LOG_ERRORS               | No       | `True`                                           | Log errors from workers                                                                                        |
| INPUT_KAFKA_GROUP               | Yes      | `"antares_dev"`                                  | Group name for input Kafka subscription                                                                        |
| INPUT_KAFKA_REQUEST_BATCH_SIZE  | No       | `1`                                              | Size of message batch to request from Kafka                                                                    |
| INPUT_KAFKA_REQUEST_TIMEOUT     | No       | `10`                                             | Amount of time to wait for a message from Kafka before timing out                                              |
| INPUT_KAFKA_SERVERS             | No       | `"kafka"`                                        | Kafka server name                                                                                              |
| INPUT_KAFKA_TOPICS              | No       | `""`                                             | Kafka topics to subscribe to (or "" which will cause the pipeline to subscribe to today's ZTF topic)           |
| INPUT_KAFKA_SSL_CA_LOCATION     | No       | `"http://localhost:8000"`                        | SSL certificate location for validating Kafka connection                                                       |
| INPUT_KAFKA_SASL_USER           | No       | `""`                                             | Input Kafka username                                                                                           |
| INPUT_KAFKA_SASL_PASS           | No       | `""`                                             | Input Kafka password                                                                                           |
| OUTPUT_KAFKA_SERVERS            | Yes      | `"kafka"`                                        | Output Kafka server name                                                                                       |
| OUTPUT_KAFKA_SSL_CA_LOCATION    | No       | `"http://localhost:8000"`                        | SSL certificate location for validating Kafka connection                                                       |
| OUTPUT_KAFKA_SASL_USER          | No       | `""`                                             | Output Kafka username                                                                                          |
| OUTPUT_KAFKA_SASL_PASS          | No       | `""`                                             | Output Kafka password                                                                                          |
| OUTPUT_KAFKA_NEW_TOPIC_PARAMS   | No       | `{"num_partitions": 4, "replication_factor": 1}` | Default parameters for new output Kafka topics                                                                 |
| API_BASE_URL                    | Yes      | `"http://localhost:8000"`                        | Base service URL (should likely have a "/v1" suffix for production deploys)                                    |
| CASSANDRA_CONCURRENCY           | No       | `10`                                             | Set a maximum number of concurrent queries against Cassandra                                                   |
| CASSANDRA_ECHO                  | No       | `True`                                           | Get additional output from Cassandra transactions                                                              |
| CASSANDRA_USER                  | Yes      | `""`                                             | Cassandra username                                                                                             |
| CASSANDRA_PASS                  | Yes      | `""`                                             | Cassandra password                                                                                             |
| CASSANDRA_HOSTS                 | Yes      | `[]`                                             | List of Cassandra hosts                                                                                        |
| CASSANDRA_ALERT_KEYSPACE        | Yes      | `"antares_alerts_dev"`                           | Keyspace of alert database                                                                                     |
| CASSANDRA_CATALOG_KEYSPACE      | Yes      | `"antares_catalogs_dev"`                         | Keyspace of catalog database                                                                                   |
| ELASTICSEARCH_HOSTS             | Yes      | `["elasticsearch"]`                              | List of Elasticsearch hosts                                                                                    |
| ELASTICSEARCH_PORT              | No       | `9200`                                           | Port to connect to Elasticsearch host on                                                                       |
| ELASTICSEARCH_OPTIONS           | No       | `{}`                                             | Dictionary of options passed as `**kwargs` to the `elasticsearch.Elasticsearch` constructor                    |
| ARCHIVE_INDEX_NAME              | Yes      | `"loci_dev"`                                     | Name of the Elasticsearch index that contains the locus index                                                  |
| FRONTEND_BASE_URL               | Yes      | `"http://localhost:8080"`                        | URL that the frontend is accessible at                                                                         |

## The ANTARES Index Worker

**Image:** registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend:v1.7.0

**Configuration:** Configuration values are specified in `antares/config.py` and are
shared between all applications. Values can be passed by setting the corresponding
environment variable, or by putting values into a YAML file and setting the environment
variable `YAML_CONFIG_PATH` to the path to the file. The table below contains a
recommended set of values that you should specify to successfully deploy the index worker.

| Name                                | Required | Default                                       | Description                                                                                 |
| ----------------------------------- | -------- | --------------------------------------------- | ------------------------------------------------------------------------------------------- |
| DEBUG                               | No       | `True`                                        | Set debug flag (disable in production!)                                                     |
| LOG_LEVEL                           | No       | `"DEBUG"`                                     | Set log level                                                                               |
| MYSQL_DB_URL                        | Yes      | ` "mysql://root:root@mysql:3306/antares_dev"` | Connection information for the MySQL database                                               |
| SQLALCHEMY_ECHO                     | No       | `True`                                        | Get additional output from MySQL transactions                                               |
| ARCHIVE_INDEX_KAFKA_TOPIC_NAME      | Yes      | `"to_index_dev"`                              | Kafka topic to pull documents from. Should match the same config setting in the pipeline.   |
| ARCHIVE_INDEX_NAME                  | Yes      | `"loci_dev"`                                  | Elasticsearch index to store documents in                                                   |
| ARCHIVE_INDEX_MAX_CHUNK_SIZE        | No       | `10`                                          | Number of documents in bulk Elasticsearch index operation                                   |
| ARCHIVE_INDEX_KAFKA_GROUP           | Yes      | `""`                                          | Group name for Kafka subscription                                                           |
| ARCHIVE_INDEX_KAFKA_SERVERS         | Yes      | `"kafka"`                                     | Kafka server name                                                                           |
| ARCHIVE_INDEX_KAFKA_SSL_CA_LOCATION | No       | `"http://localhost:8000"`                     | SSL certificate location for validating Kafka connection                                    |
| ARCHIVE_INDEX_KAFKA_SASL_USER       | No       | `""`                                          | Kafka username                                                                              |
| ARCHIVE_INDEX_KAFKA_SASL_PASS       | No       | `""`                                          | Kafka password                                                                              |
| ELASTICSEARCH_HOSTS                 | Yes      | `["elasticsearch"]`                           | List of Elasticsearch hosts                                                                 | 
| ELASTICSEARCH_PORT                  | No       | `9200`                                        | Port to connect to Elasticsearch host on                                                    |
| ELASTICSEARCH_OPTIONS               | No       | `{}`                                          | Dictionary of options passed as `**kwargs` to the `elasticsearch.Elasticsearch` constructor |

## Deploying ANTARES

Our recommended deployment strategy uses Helm to deploy the ANTARES stack to a
Kubernetes cluster:

```sh
helm repo add noirlab https://nsf-noirlab.gitlab.io/helm-charts
helm repo update
helm install antares noirlab/antares
```

Using either the `helm install --set KEY=VALUE ...` or `helm install -f my-config.yaml ...`
strategies to set configuration for your environment. You can also look at the `.gitlab-ci.yml`
file in this repository to see our deployment strategies.

## Dev Environment

The dev environment uses docker-compose. (Note: staging and production use
Kubernetes, not docker-compose.) The `build.py` tool performs most dev,
build, and deploy tasks.

Each container sets up notification events for changes to its filesystem,
this may cause an error related to exceeding the default number of inodes
you can watch at one time. On Debian I fix this by:

```bash
sudo sysctl -w fs.inotify.max_user_watches=524288
```

You may want to add this to your shell initialization script.

Minimum dev prerequisites:

- docker
- docker-compose

### Setting up a Debugger

PyCharm is able to connect to a docker-compose Python instance to
provide you with an interactive debugger. You can follow the guide
here: [https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html#configuring-docker]().

### Run the test suite

```bash
./build.py nosetests
```

### Launch the dev environment

Launch the dev environment, and populate it with data:

```bash
./build.py up
```

Now you will have a running ANTARES instance, with test data in
the databases, and portal accessible at [http://localhost:8080]().

You can view logs from services with the `dcl` (docker-compose logs) command:

```bash
./build.py dcl pipeline
```

Bring the dev env down with:

```bash
./build.py down
```

Open Python prompt inside the dev system with `dcp` (docker-compose python) command:

```bash
./build.py dcp index_worker
```

### Run Alerts through the dev pipeline

Send the test alerts through the system with using `dce` (docker-compose exec) command:

```bash
./build.py dce rq_worker "python -m test_data.ztf_alerts.put_to_kafka"
```
