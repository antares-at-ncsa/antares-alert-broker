include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

stages:
  - build
  - test
  - deploy
  - verify

build_frontend:
  stage: build
  image:
    name: docker/compose:1.23.2
    entrypoint: ["/bin/sh", "-c"]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:18.09-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE/frontend:master || true
    - >
      docker build
      --cache-from $CI_REGISTRY_IMAGE/frontend:master
      --tag $CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_SHA
      --tag $CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_REF_NAME
      --file antares/frontend/Dockerfile.nginx
      antares/frontend
    - docker push $CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_REF_NAME

build_backend:
  stage: build
  image:
    name: docker/compose:1.23.2
    entrypoint: ["/bin/sh", "-c"]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:18.09-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE/backend:master || true
    - >
      docker build
      --cache-from $CI_REGISTRY_IMAGE/backend:master
      --tag $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_SHA
      --tag $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_REF_NAME
      .
    - docker push $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_REF_NAME

deploy_review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.antares.noirlab.edu
    on_stop: stop_review
  tags:
    - k8s
  image:
    name: alpine/helm:3.4.1
  before_script:
    - mkdir -p $HOME/.kube
    - cp "$KUBE_CONFIG" $HOME/.kube/config
    - sed -i "s/API_HOST_NAME/api.${CI_ENVIRONMENT_SLUG}.antares.noirlab.edu/" $HELM_CONFIG
    - sed -i "s/API_SECRET_NAME/antares-review-api-${CI_ENVIRONMENT_SLUG}/" $HELM_CONFIG
    - sed -i "s/HOST_NAME/${CI_ENVIRONMENT_SLUG}.antares.noirlab.edu/" $HELM_CONFIG
    - sed -i "s/SECRET_NAME/antares-review-portal-${CI_ENVIRONMENT_SLUG}/" $HELM_CONFIG
  script:
    - helm repo add noirlab https://nsf-noirlab.gitlab.io/helm-charts
    - helm repo update
    - >
      helm upgrade --install
      --wait
      --namespace antares-review
      -f $HELM_CONFIG
      --set frontend.image.tag=$CI_COMMIT_SHA
      "antares-${CI_ENVIRONMENT_SLUG}"
      noirlab/antares
  rules:
    - if: $CI_MERGE_REQUEST_ID

stop_review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - k8s
  image:
    name: alpine/helm:3.4.1
  before_script:
    - mkdir -p $HOME/.kube
    - cp "$KUBE_CONFIG" $HOME/.kube/config
  script:
    - >
      helm delete "antares-${CI_ENVIRONMENT_SLUG}" --namespace antares-review
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual

# test:
#   stage: test
#   tags:
#     - k8s
#   only:
#     - master
#   image:
#     name: docker/compose:1.23.2
#     entrypoint: ["/bin/sh", "-c"]
#   variables:
#     DOCKER_HOST: tcp://docker:2375/
#     DOCKER_DRIVER: overlay2
#   services:
#     - docker:18.09-dind
#   before_script:
#     - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
#   script:
#     - docker pull $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_SHA
#     - >
#       docker-compose -f docker-compose.yml up -d
#       mysql
#       cassandra
#       zookeeper
#       kafka
#       elasticsearch
#       redis
#       pipeline
#     - docker-compose run -T --entrypoint "/bin/bash -c" pipeline "nosetests -v -x -A 'not slow'"

deploy_staging:
  stage: deploy
  only:
    - master
  environment:
    name: staging
    url: https://staging.antares.noirlab.edu
  tags:
    - k8s
  image:
    name: alpine/helm:3.4.1
  before_script:
    - mkdir -p $HOME/.kube
    - cp "$KUBE_CONFIG" $HOME/.kube/config
  script:
    - helm repo add noirlab https://nsf-noirlab.gitlab.io/helm-charts
    - helm repo update
    - >
      helm upgrade --install
      --wait
      --namespace antares-staging
      -f $HELM_CONFIG
      --set frontend.image.tag=$CI_COMMIT_SHA
      --set api.image.tag=$CI_COMMIT_SHA
      antares
      noirlab/antares

integration_test:
  stage: verify
  image: python:3.7
  only:
    - master
  tags:
    - k8s
  script:
    - pip install antares-client
    - python integration/test_api.py

deploy_production:
  stage: deploy
  environment:
    name: production
    url: https://antares.noirlab.edu
  tags:
    - k8s
  image:
    name: alpine/helm:3.4.1
  rules:
    - if: $CI_COMMIT_TAG && $CI_DEPLOY_FREEZE == null
      when: manual
  before_script:
    - mkdir -p $HOME/.kube
    - cp "$KUBE_CONFIG" $HOME/.kube/config
  script:
    - helm repo add noirlab https://nsf-noirlab.gitlab.io/helm-charts
    - helm repo update
    - >
      helm upgrade --install
      --wait
      --namespace antares-production
      -f $HELM_CONFIG
      --set api.image.tag=$CI_COMMIT_REF_NAME
      --set frontend.image.tag=$CI_COMMIT_REF_NAME
      --set indexWorker.image.tag=$CI_COMMIT_REF_NAME
      --set pipeline.image.tag=$CI_COMMIT_REF_NAME
      --set rqScheduler.image.tag=$CI_COMMIT_REF_NAME
      --set rqWorker.image.tag=$CI_COMMIT_REF_NAME
      antares
      noirlab/antares

deploy_to_datalab:
  stage: deploy
  tags:
    - k8s
  image:
    name: python:3.7
  rules:
    - if: $CI_COMMIT_TAG && $CI_DEPLOY_FREEZE == null
      when: manual
  before_script:
    # Set up SSH key for connecting to datalab.
    # Source: https://docs.gitlab.com/ee/ci/ssh_keys/
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - cat "$ANTARES_AT_DATALAB_ID_RSA" | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - apt-get update -y
    - apt-get install rsync -y
  script:
    - ./build.py env=$CI_COMMIT_REF_NAME deploy_datalab
