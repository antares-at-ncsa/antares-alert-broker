FROM continuumio/miniconda3:4.8.2
RUN conda install -n base -c defaults conda

# System packages, required for later builds
RUN apt-get update && apt-get install -y \
  apt-utils \
  build-essential \
  default-libmysqlclient-dev \
  dnsutils \
  gfortran \
  gsl-bin \
  libeigen3-dev \
  libgsl0-dev \
  nfs-common \
  && rm -rf /var/lib/apt/lists/*

RUN conda install \
  cassandra-driver==3.24.0 \
  ipykernel==5.1.4 \
  numpy \
  scipy \
  tensorflow

ENV CQLENG_ALLOW_SCHEMA_MANAGEMENT=True

# Create static files directory
RUN mkdir /static_files

# Install Python packages
WORKDIR /antares
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt
ADD requirements-filters.txt requirements-filters.txt
RUN pip install -r requirements-filters.txt

# Copy in the ANTARES code
WORKDIR /antares
ADD antares antares
ADD test test
ADD setup.py setup.py
ADD build.py build.py
RUN python setup.py install
