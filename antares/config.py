import json
import os
from contextlib import contextmanager

import yaml

class CatalogConfig:
    def __init__(self, cat_config):
        self._raw = cat_config
        self._tables = cat_config["tables"]
        self._by_id = {t["catalog_id"]: t for t in cat_config["tables"]}
        self._by_table_name = {t["table"]: t for t in cat_config["tables"]}

    @property
    def tables(self):
        return self._tables

    def all_enabled_ids(self):
        return sorted(t["catalog_id"] for t in self._tables if t["enabled"])

    def by_id(self, id):
        return self._by_id.get(id)

    def by_name(self, name):
        return self._by_table_name.get(name)

    def raw(self):
        return self._raw


_default_catalog_config = {
    "tables": [
        # Example, for documentation:
        #
        # {
        #     "table": "galex",              # Name of table in MySQL
        #     "catalog_id": 18,              # Must be unique in this file
        #     "display_name": "Galex"        # Display name for catalog.
        #     "enabled": True,               # If False, system should ignore this catalog.
        #     "ra_column": "AVASPRA",        # Name of 'ra' column in MySQL
        #     "dec_column": "AVASPDEC",      # Name of 'ra' column in MySQL
        #     "object_id_column": "sid",     # Name of object id column in MySQL
        #     "object_id_type": "int",       # Type of the id. Only `int` is supported.
        #     "object_name_column": "name"   # Name of the object.
        #     "radius": None,                # Default radius if 'radius_column' is None.
        #     "radius_column": "rad",        # Name of radius column in MySQL. May be None.
        #     "radius_unit": "degree",       # Units of radius column. Must be either 'degree', 'arcsecond', or None.
        # },
        {
            "table": "2mass_psc",
            "catalog_id": 1,
            "display_name": "2MASS PSC",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "decl",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "designation",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "2mass_xsc",
            "catalog_id": 2,
            "display_name": "2MASS XSC",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "decl",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "designation",
            "radius": 0.00028,
            "radius_column": "r_k20fe",
            "radius_unit": "arcsecond",
        },
        {
            "table": "bright_guide_star_cat",
            "catalog_id": 3,
            "display_name": "HST Guide Catalog",
            "enabled": True,
            "ra_column": "RightAsc_deg",
            "dec_column": "Declination_deg",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "hstID",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "chandra_master_sources",
            "catalog_id": 4,
            "display_name": "Chandra Master Sources",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "ned",
            "catalog_id": 5,
            "display_name": "NASA/IPAC Extragalactic Database",
            "enabled": True,
            "ra_column": "RA_deg",
            "dec_column": "DEC_deg",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "Object_Name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "nyu_valueadded_gals",
            "catalog_id": 6,
            "display_name": "NYU Value-Added Galaxy Catalog",
            "enabled": True,
            "ra_column": "RA",
            "dec_column": "DEC",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "OBJECT_TAG",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "sdss_gals",
            "catalog_id": 7,
            "display_name": "SDSS Galaxy Catalog",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec_",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "Objid",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "sdss_stars",
            "catalog_id": 8,
            "display_name": "SDSS Star Catalog",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec_",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "Objid",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "veron_agn_qso",
            "catalog_id": 9,
            "display_name": "VERONCAT",
            "enabled": True,
            "ra_column": "viz_RAJ2000",
            "dec_column": "viz_DEJ2000",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "Name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            #
            # REPLACED by asassn
            #
            "table": "asassn_variable_catalog",
            "catalog_id": 10,
            "display_name": "ASAS-SN I",
            "enabled": False,
            "ra_column": "RAJ2000",
            "dec_column": "DEJ2000",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "ASAS-SN Name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "rc3",
            "catalog_id": 11,
            "display_name": "RC3",
            "enabled": True,
            "ra_column": "ra_degree",
            "dec_column": "dec_degree",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "Name",
            "radius": None,
            "radius_column": "radius_degree",
            "radius_unit": "degree",
        },
        {
            "table": "allwise",
            "catalog_id": 12,
            "display_name": "AllWISE",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "decl",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "designation",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "csdr2",
            "catalog_id": 13,
            "display_name": "CSDR2",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "decl",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "gaia_dr2",
            "catalog_id": 14,
            "display_name": "Gaia DR2",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "designation",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "xmm3_dr8",
            "catalog_id": 15,
            "display_name": "3XMM-DR8",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "iauname",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "asassn",
            "catalog_id": 16,
            "display_name": "ASAS-SN II",
            "enabled": True,
            "ra_column": "raj2000",
            "dec_column": "dej2000",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "asassn_name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "french_post_starburst_gals",
            "catalog_id": 17,
            "display_name": "French Post-Starburst Galaxies",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "id",
            "object_id_type": "int",
            "object_name_column": "original_id",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "galex",
            "catalog_id": 18,
            "display_name": "GALEX",
            "enabled": True,
            "ra_column": "AVASPRA",
            "dec_column": "AVASPDEC",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "OBJID",
            "radius": None,
            "radius_column": "radius_degrees",  # = NUV_KRON_RADIUS * NUV_A_WORLD
            "radius_unit": "degree",
        },
        {
            "table": "PS1StarGalaxyCatalog",
            "catalog_id": 19,
            "display_name": "PS1 Classifications",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "id",
            "object_id_type": "int",
            "object_name_column": "id",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
        {
            "table": "milliquas",
            "catalog_id": 20,
            "display_name": "Million Quasars",
            "enabled": True,
            "ra_column": "ra",
            "dec_column": "dec",
            "object_id_column": "sid",
            "object_id_type": "int",
            "object_name_column": "name",
            "radius": 0.00028,
            "radius_column": None,
            "radius_unit": None,
        },
    ]
}


class Config(object):
    """
    ANTARES config object.

    Default values here should be suitable for running a dev environment.

    DO NOT PUT PRODUCTION CONFIG HERE.

    If any config variables contain secret information, add their names to
    __SECRETS__ so they will be obfuscated when logged to stdout or Provenance.
    To customize the obfuscation, modify format() below.
    """

    __SECRETS__ = [
        "MYSQL_DB_URL",
        "MYSQL_CATALOG_DB_URL",
        "SLACK_TOKEN",
        "OUTPUT_KAFKA_SASL_USER",
        "OUTPUT_KAFKA_SASL_PASS",
        "CASSANDRA_PASS",
        "SECRET_KEY",
    ]

    # General
    ENVIRONMENT = "dev"
    SERVICE = "dev"
    LOG_LEVEL = "DEBUG"
    LOG_FORMAT = "%(asctime)s - %(levelname)s %(threadName)s %(filename)s:%(funcName)s:%(lineno)d - %(message)s"
    DEBUG = True
    TEMP_DIR = "/tmp"
    YAML_CONFIG_PATH = ""  # Load more config from here, if set.
    SECRET_KEY = "notsecret"
    HTM_PRECISION_ERROR_RETRY_OFFSET = 1e-6

    # MySQL Database config
    MYSQL_DB_URL = "mysql://root:root@mysql:3306/antares_dev"
    MYSQL_CATALOG_DB_URL = "mysql://root:root@mysql:3306/antares_catalogs_dev"
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_ENGINE_PARAMS = dict(
        pool_size=0,  # Up to this many connections are kept open continually
        max_overflow=20,  # Up to this many additional connections may exist
        pool_recycle=60,  # Connections are recycled after this TTL
    )

    # Archive
    ARCHIVE_ENABLE = True
    ARCHIVE_INDEX_KAFKA_TOPIC_NAME = "to_index_dev"
    ARCHIVE_INDEX_NAME = "loci_dev"
    ARCHIVE_INDEX_MAX_CHUNK_SIZE = 10
    ARCHIVE_INDEX_KAFKA_POLL_TIMEOUT = 1.0
    ARCHIVE_INDEX_KAFKA_GROUP = "index_worker_dev"
    ARCHIVE_INDEX_KAFKA_SERVERS = "kafka"
    ARCHIVE_INDEX_KAFKA_SSL_CA_LOCATION = ""
    ARCHIVE_INDEX_KAFKA_SASL_USER = ""
    ARCHIVE_INDEX_KAFKA_SASL_PASS = ""

    # API
    API_ACCESS_TOKEN_EXPIRES = 60 * 10
    API_BASE_URL = "http://localhost:8000"
    API_REFRESH_TOKEN_EXPIRES = 60 * 60 * 24 * 30
    API_JWT_SECRET_KEY = SECRET_KEY
    API_JWT_BLACKLIST_ENABLED = True
    API_JWT_BLACKLIST_TOKEN_CHECKS = ["refresh"]
    API_JWT_TOKEN_LOCATION = ["cookies", "headers"]
    API_JWT_ACCESS_COOKIE_PATH = "/v1"
    API_JWT_REFRESH_COOKIE_PATH = "/v1/auth"
    API_JWT_COOKIE_DOMAIN = None
    API_JWT_COOKIE_SECURE = False
    API_JWT_COOKIE_CSRF_PROTECT = True
    API_JWT_COOKIE_SAMESITE = "Lax"
    API_JWT_SESSION_COOKIE = False
    API_MAIL_SERVER = ""
    API_MAIL_PORT = 587
    API_MAIL_USE_TLS = False
    API_MAIL_USERNAME = ""
    API_MAIL_PASSWORD = ""
    API_PROPAGATE_EXCEPTIONS = True

    # Cassandra
    CASSANDRA_ECHO = True
    CASSANDRA_CONCURRENCY = 1
    CASSANDRA_ALERT_KEYSPACE = "antares_alerts_dev"
    CASSANDRA_CATALOG_KEYSPACE = "antares_catalogs_dev"
    CASSANDRA_HOSTS = ["cassandra"]
    CASSANDRA_USER = ""
    CASSANDRA_PASS = ""
    CASSANDRA_ADMIN_USER = ""
    CASSANDRA_ADMIN_PASS = ""
    CASSANDRA_DEFAULT_TIMEOUT = 60  # (10 is the cassandra library default)

    # Statsd
    DATADOG_STATSD_HOST = "196.0.0.1"  # Non-routeable IP
    DATADOG_STATSD_PORT = 8125
    DATADOG_TRACE_ENABLE = False
    DATADOG_TRACE_AGENT_HOST = "196.0.0.1"  # Non-routeable IP
    DATADOG_TRACE_AGENT_PORT = 8126
    DATADOG_LOGGING_ENABLE = False
    DATADOG_LOGGING_LEVEL = "ERROR"
    DATADOG_LOGGING_PORT = 10519

    # Elasticsearch
    ELASTICSEARCH_HOSTS = ["elasticsearch"]
    ELASTICSEARCH_OPTIONS = {}
    ELASTICSEARCH_PORT = 9200

    # Frontend
    FRONTEND_BASE_URL = "http://localhost:8080"

    # Redis
    REDIS_HOST = "redis"
    REDIS_PORT_ = 6379  # add underscore to not collide with other env var
    REDIS_DB = 1
    REDIS_RQ_HOST = "redis"
    REDIS_RQ_PORT = 6379
    REDIS_RQ_DB = 2

    # Input Kafka
    INPUT_KAFKA_SERVERS = "kafka"
    INPUT_KAFKA_TOPICS = ["input_dev"]

    INPUT_KAFKA_GROUP = "antares_dev"
    INPUT_KAFKA_REQUEST_BATCH_SIZE = 1
    INPUT_KAFKA_REQUEST_TIMEOUT = 10
    INPUT_KAFKA_SSL_CA_LOCATION = ""
    INPUT_KAFKA_SASL_USER = ""
    INPUT_KAFKA_SASL_PASS = ""

    # Slack output
    SLACK_ENABLE = False
    SLACK_TOKEN = ""
    SLACK_ERRORS_CHANNEL = "errors_dev"
    SLACK_FILTER_ERRORS_CHANNEL = "filter_errors_dev"
    SLACK_WATCH_LIST_ENABLE = False

    # Kafka output
    OUTPUT_KAFKA_MAX_RETRIES = 2
    OUTPUT_KAFKA_SERVERS = "kafka"
    OUTPUT_KAFKA_SSL_CA_LOCATION = ""
    OUTPUT_KAFKA_NEW_TOPIC_PARAMS = {
        "num_partitions": 4,
        "replication_factor": 1,
    }
    OUTPUT_KAFKA_SASL_USER = ""
    OUTPUT_KAFKA_SASL_PASS = ""

    # Pipeline worker
    WORKER_RAISE_ERRORS = True
    WORKER_LOG_ERRORS = True

    # Pipeline control
    PIPELINE_KEEP_EVERYTHING = True
    PIPELINE_DISABLE_CRASHED_STAGES = True
    PIPELINE_RAISE_STAGE_ERRORS = True
    OUTPUTS = [
        {
            "name": "Tagged hello",
            "type": "kafka",
            "criteria": {
                "has_tag": "hello",
            },
            "topic": "hello_dev",
        },
        {
            "name": "New tagged hello",
            "type": "slack",
            "criteria": {
                "new_tag": "hello",
            },
            "channel": "@cstubens",
        },
    ]

    # Locus aggregation
    LOCUS_SEARCH_RADIUS_DEGREES = 5.0 * (1.0 / 3600.0)
    LOCUS_AGGREGATION_RADIUS_DEGREES = 1.0 * (1.0 / 3600.0)
    LOCUS_MERGE_RADIUS_DEGREES = 0.1 * (1.0 / 3600.0)
    
    # Catalog configuration
    CATALOG_CONFIG = CatalogConfig(_default_catalog_config)

    def __init__(self, yaml_config_path=None):
        """
        Load config.

        Values from config file overrule values in this python file.
        Values from env variables overrule both this file and config files.
        """
        # Load config from supplied yaml file
        if yaml_config_path:
            self.load_from_yaml_file(yaml_config_path)

        # Load config from YAML file ENV variable
        path = os.environ.get("YAML_CONFIG_PATH", self.YAML_CONFIG_PATH)
        if path:
            self.load_from_yaml_file(path)

        # Load config from environment variables
        self.set_values(os.environ)

    def set_values(self, d, convert_types=True):
        """
        Set config values from a dict

        Environment variables are always strings, so if `convert_types` is true
        we convert them to the type of the default value.

        :param d: dict
        :param convert_types: bool
        :return:
        """
        for k, v in d.items():
            if hasattr(self, k):
                if convert_types:
                    default_value = getattr(self, k)
                    t = type(default_value)
                    if t is bool:
                        # Type `bool` is a special case.
                        # v = bool(v) is not sufficient.
                        v = v.lower() == "true"
                    elif t in (float, int, str):
                        # for other types, convert using the type directly like:
                        # v = int(v)
                        # v = float(v)
                        # etc.
                        try:
                            v = t(v)
                        except Exception:
                            raise RuntimeError(
                                "could not convert environment variable {}={}"
                                " to type {}".format(k, v, t)
                            )
                    elif t is type(None):
                        pass
                    else:
                        raise RuntimeError("Cannot convert type {}".format(t))
                # Special case to handle catalog_config
                if k == 'CATALOG_CONFIG':
                    v = CatalogConfig(v)
                setattr(self, k, v)

    def load_from_yaml_file(self, path):
        print("config.py loading config from path: %s", path)
        with open(path, "r") as f:
            self.set_values(yaml.load(f, Loader=yaml.FullLoader), convert_types=False)

    def values(self, clean=False):
        """
        Return config as a dict.
        """
        d = {}
        for k in dir(self):
            if k == k.upper() and not k.startswith("_"):
                v = getattr(self, k)
                if v and clean:
                    if k == "MYSQL_DB_URL":
                        v = "**********@" + v.partition("@")[2]
                    elif k in self.__SECRETS__:
                        v = "*" * len(str(v))
                # Custom handler for CatalogConfig type
                if k == 'CATALOG_CONFIG':
                    v = { 'tables': getattr(self, k).tables }
                d[k] = v
        return d

    def format(self):
        return json.dumps(self.values(clean=True), indent=4)

    @contextmanager
    def temp(self, name, value):
        """
        Allows temp config overrides with a contextmanager.

        > with config.temp('CASSANDRA_ECHO', True):
        >     do_things()

        :param name:
        :param value:
        :return: conextmanager
        """
        assert hasattr(self, name)
        original = getattr(self, name)
        setattr(self, name, value)
        try:
            yield
        finally:
            setattr(self, name, original)


config = Config()


def reset():
    global config
    config = Config()
