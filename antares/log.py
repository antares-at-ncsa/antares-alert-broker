import logging
import sys
import traceback
from logging.handlers import SocketHandler

from pythonjsonlogger import jsonlogger

from antares.config import config

log = logging.getLogger("antares")


class DatadogHandler(SocketHandler):
    def makePickle(self, record):
        return self.format(record).encode() + b"\n"


def init(level=None, format=None):
    if level is None:
        level = config.LOG_LEVEL
    if format is None:
        format = config.LOG_FORMAT
    log.setLevel(level)

    # Remove all existing log handlers
    for handler in log.handlers:
        log.removeHandler(handler)

    # Logging to STDOUT
    formatter = logging.Formatter(format)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(formatter)
    stdout_handler.setLevel(level)
    log.addHandler(stdout_handler)

    # Logging to DataDog
    if config.DATADOG_LOGGING_ENABLE:
        dd_handler = DatadogHandler(
            config.DATADOG_STATSD_HOST, config.DATADOG_LOGGING_PORT
        )
        dd_handler.setFormatter(jsonlogger.JsonFormatter())
        dd_handler.setLevel(config.DATADOG_LOGGING_LEVEL)
        log.addHandler(dd_handler)


init()


debug = log.debug
info = log.info
warn = log.warning
error = log.error


def stacktrace():
    log.error(traceback.format_exc())


def flush():
    for h in log.handlers:
        h.flush()
