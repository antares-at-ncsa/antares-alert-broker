import os


__version__ = None


def _load_version():
    """
    Load package version from `version.txt`
    """
    global __version__
    print("Loading ANTARES from", os.path.abspath(__file__))
    this_directory = os.path.dirname(os.path.abspath(__file__))
    version_file = os.path.join(this_directory, "version.txt")
    with open(version_file, "r") as f:
        __version__ = f.read().strip()
    print(
        f"""
        _    _   _ _____  _    ____  _____ ____
       / \  | \ | |_   _|/ \  |  _ \| ____/ ___|
      / _ \ |  \| | | | / _ \ | |_| |  _| \___ \\
     / ___ \| |\  | | |/ ___ \|  _ /| |___ ___| |
    /_/   \_\_| \_| |_/_/   \_\_| \_\_____|____/   v{__version__}
    """
    )


_load_version()
