# Running mkhtm_lut.py

inputs:
* catalog in csv format

outputs:
* id file with a sequence of numbers for the id
* catalog in csv format with a new id column
* htm_lut in csv format suitable for loading into C*


Example input file: galex.csv

1. Get the number of rows

`wc -l galex.csv`  # 137,478

2. Generate the id file

`seq 1 137478 > galex_id.csv`

3. Create new galex file with the new id column

`paste -d , galex.csv galex_id.csv > galex_full.csv`

4. Generate htm_lut.csv

`./antares/rtdb/mkhtm_lut.py -f galex_full.csv -t galex`

* The `-t galex` indicates the table name in `catalog_config.py`

Then load the resulting `mkhtm_lut.csv` into the `htm_lut` table.

