from antares import log
from antares.rtdb import antcassandra
from antares.rtdb.htmlut import HTMLUT
from antares.services import metrics
from antares.utils import cone_search


@metrics.timed("rq.job", tags=["job:repair_catalog_lut"])
def repair_catalog_lut(ra, dec):
    """
    Locate HTMLUT entries at a given location which point to a non-existent
    catalog object.

    Call this function in order to investigate suspected bad HTMLUT entries.
    """
    from antares.rtdb.api import RTDB, CatalogIsDisabled, CatalogObjectNotFound

    antcassandra.init()
    rtdb = RTDB()
    for level in rtdb.CATALOG_OBJECT_INDEX_LEVELS:
        htm = cone_search.get_htm(ra, dec, level)
        lut = HTMLUT()
        lut.load(htm)
        matches = lut.get_cat_objs()
        for (cat_id, obj_id) in matches:
            try:
                rtdb.get_catalog_object(cat_id, obj_id)
            except CatalogIsDisabled:
                continue
            except CatalogObjectNotFound:
                log.info("Found bad LUT entry:")
                log.info(str((htm, cat_id, obj_id)))
                log.flush()
