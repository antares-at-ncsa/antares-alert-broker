import time

import cassandra
import cassandra.protocol
from cassandra import ConsistencyLevel
from cassandra.cqlengine.management import sync_table

from antares import log
from antares.config import config
from antares.rtdb import schema
from antares.rtdb.antcassandra import execute, CBase, init, get_keyspaces, KEYSPACE_CLASSES

# Make sure schema is imported so that models are loaded.
del schema


def bootstrap_for_dev():
    log.info(f"bootstrap_for_dev()")
    drop_and_create_schema(keyspace=config.CASSANDRA_ALERT_KEYSPACE)
    drop_and_create_schema(keyspace=config.CASSANDRA_CATALOG_KEYSPACE)


def create_or_sync():
    log.info(f"create_or_sync()")
    create_schema(keyspace=config.CASSANDRA_ALERT_KEYSPACE)
    create_schema(keyspace=config.CASSANDRA_CATALOG_KEYSPACE)


def truncate(catalogs=False):
    log.info(f"truncate()")
    truncate_schema(keyspace=config.CASSANDRA_ALERT_KEYSPACE)
    if catalogs:
        truncate_schema(keyspace=config.CASSANDRA_CATALOG_KEYSPACE)


def drop(catalogs=False):
    log.info(f"drop()")
    drop_keyspace(keyspace=config.CASSANDRA_ALERT_KEYSPACE)
    if catalogs:
        truncate_schema(keyspace=config.CASSANDRA_CATALOG_KEYSPACE)


def get_consistency_level():
    """
    consistency_level for bootstrap operations.

    In dev:
    ConsistencyLevel.ONE should be used for schema manipulations.
    ConsistencyLevel.ONE should be used for Reads/Writes.

    In staging and production:
    ConsistencyLevel.ALL should be used for schema manipulations.
    ConsistencyLevel.QUORUM should be used for Reads/Writes.
    """
    if len(config.CASSANDRA_HOSTS) == 1:
        # In dev environment there is only one C* host.
        assert config.ENVIRONMENT == "dev"
        return ConsistencyLevel.ONE
    else:
        # In staging and production, we use RF=3.
        return ConsistencyLevel.ALL


def get_replication_factor():
    if len(config.CASSANDRA_HOSTS) == 1:
        # In dev environment there is only one C* host.
        assert config.ENVIRONMENT == "dev"
        return 1
    else:
        # In staging and production, we use RF=3.
        return 3


def truncate_schema(keyspace, force=False, timeout=600):
    """
    Truncate all tables in a keyspace

    :param keyspace:
    :param force: if False, keyspace must end in '_dev' or '_staging'
    """
    log.info(f'truncate_schema(keyspace="{keyspace}")')
    consistency_level = get_consistency_level()
    if not force:
        assert keyspace.endswith("_dev") or keyspace.endswith("_staging")
    result = execute(
        "SELECT * FROM system_schema.tables",
        consistency_level=consistency_level,
        timeout=timeout,
    )
    tables = [row["table_name"] for row in result if row["keyspace_name"] == keyspace]
    log.info("Will truncate tables:")
    for table in tables:
        log.info(table)
    for table in tables:
        q = f'TRUNCATE "{keyspace}"."{table}";'
        log.info(q)
        execute(q, consistency_level=consistency_level, timeout=timeout)


def drop_keyspace(keyspace, force=False, timeout=600):
    log.info(f'drop_keyspace(keyspace="{keyspace}")')
    consistency_level = get_consistency_level()
    if not force:
        assert keyspace.endswith("_dev") or keyspace.endswith("_staging")

    # Drop existing keyspace
    try:
        execute(
            f"DROP KEYSPACE {keyspace};",
            consistency_level=consistency_level,
            timeout=timeout,
        )
    except cassandra.protocol.ConfigurationException as e:
        if "non existing keyspace" in str(e):
            pass  # Ok
        else:
            raise

    # Wait for keyspace to not exist
    while True:
        if keyspace not in get_keyspaces():
            break
        time.sleep(5)
        init()
    while True:
        result = execute(
            "SELECT * FROM system_schema.tables;",
            consistency_level=consistency_level,
            timeout=timeout,
        )
        if keyspace not in {row["keyspace_name"] for row in result}:
            break
        log.info(f'Waiting for keyspace "{keyspace}" to NOT exist')
        time.sleep(5)
        init()


def drop_table(keyspace, table, force=False, timeout=600):
    consistency_level = get_consistency_level()
    assert force or keyspace.endswith("_dev") or keyspace.endswith("_staging")
    log.info(f'Dropping table "{keyspace}"."{table}"')
    execute(
        f'DROP TABLE "{keyspace}"."{table}";',
        consistency_level=consistency_level,
        timeout=timeout,
    )

    # Wait for table to NOT exist
    while True:
        try:
            execute(
                f'SELECT * FROM "{keyspace}"."{table}";',
                consistency_level=consistency_level,
                timeout=timeout,
            )
            log.info(f"Waiting for table to NOT exist")
            time.sleep(5)
            continue
        except cassandra.InvalidRequest as e:
            if "unconfigured table" in str(e):
                break  # Good. The table doesn't exist.
            raise  # Unexpected error


def drop_tables(keyspace, timeout=600):
    log.info(f'drop_tables(keyspace="{keyspace}")')
    consistency_level = get_consistency_level()
    assert keyspace.endswith("_dev") or keyspace.endswith("_staging")

    # Get list of all tables existing in the keyspace
    result = execute(
        "SELECT * FROM system_schema.tables;",
        consistency_level=consistency_level,
        timeout=timeout,
    )
    tables = {row["table_name"] for row in result if row["keyspace_name"] == keyspace}

    # Drop the tables
    for table in tables:
        drop_table(keyspace, table, timeout=timeout)


def create_keyspace(keyspace, timeout=600, replication_factor=None):
    log.info(f'create_keyspace(keyspace="{keyspace}")')
    consistency_level = get_consistency_level()
    if replication_factor is None:
        replication_factor = get_replication_factor()
    replication = f"""
        {{'class': 'SimpleStrategy', 'replication_factor': {replication_factor}}}
    """.strip()
    del replication_factor

    # Create keyspace
    try:
        execute(
            f"CREATE KEYSPACE {keyspace} WITH replication = {replication};",
            consistency_level=consistency_level,
            timeout=timeout,
        )
    except cassandra.AlreadyExists:
        pass

    # Wait for keyspace to exist
    time.sleep(5)
    while True:
        if keyspace in get_keyspaces():
            break
        log.info(f'Waiting for keyspace "{keyspace}" to exist')
        time.sleep(5)
        init()


def create_schema(keyspace, timeout=600, replication_factor=None):
    """
    Create keyspace and tables
    """
    log.info(f'create_schema(keyspace="{keyspace}")')
    if replication_factor is None:
        replication_factor = get_replication_factor()
    consistency_level = get_consistency_level()

    # Determine tables to create
    classes = KEYSPACE_CLASSES[keyspace]
    assert classes
    if classes:
        log.info("Will create tables:")
        for cls in classes:
            log.info(cls)
    else:
        log.info(f'No ORM classes found in keyspace "{keyspace}".')
        log.info("Did you forget to import them?")

    create_keyspace(keyspace, timeout=timeout, replication_factor=replication_factor)

    # Create tables
    for cls in classes:
        while True:
            try:
                sync_table(cls)
                break
            except cassandra.protocol.ConfigurationException as e:
                if f"Keyspace {keyspace} doesnt exist" in str(e):
                    log.info("Keyspace doesnt exist yet")
                    time.sleep(5)
                    init()
                    continue
                else:
                    # Unexpected error
                    raise
            except cassandra.AlreadyExists:
                log.info("Table AlreadyExists error")
                time.sleep(5)
                init()
                continue

        # Wait for table to exist
        while True:
            try:
                execute(
                    f'SELECT * FROM "{keyspace}"."{cls.__table_name__}";',
                    consistency_level=consistency_level,
                    timeout=timeout,
                )
                break
            except cassandra.InvalidRequest:
                log.info("InvalidRequest (keyspace doesn't exist yet")
                time.sleep(5)
                init()
                continue
            except cassandra.ReadTimeout:
                log.info("ReadTimeout")
                time.sleep(5)
                init()
                continue


def drop_and_create_schema(keyspace, force=False, timeout=600, replication_factor=None):
    """
    Drop a keyspace and re-create it, including tables.
    """
    log.info(f'drop_and_create_schema(keyspace="{keyspace}")')
    drop_keyspace(keyspace, force=force, timeout=timeout)
    create_schema(keyspace, timeout=timeout, replication_factor=replication_factor)


def drop_and_create_table(keyspace, table, force=False, timeout=600):
    log.info(f'drop_and_create_table(keyspace="{keyspace}", table="{table}")')
    consistency_level = get_consistency_level()
    if not force:
        assert keyspace.endswith("_dev") or keyspace.endswith("_staging")

    # Drop existing table
    try:
        execute(
            f'DROP TABLE "{keyspace}"."{table}";',
            consistency_level=consistency_level,
            timeout=timeout,
        )
    except cassandra.protocol.InvalidRequest as e:
        if "unconfigured table" in str(e):
            pass  # Ok. Table doesn't exist.
        else:
            raise

    # Find table ORM class
    cls = None
    for c in CBase.__subclasses__():
        if c.__keyspace__ == keyspace and c.__table_name__ == table:
            cls = c
            break
    if cls:
        log.info("Will create table: %s", cls)
    else:
        raise RuntimeError("Table ORM object not found. Did you import it?")

    # Create table
    while True:
        try:
            sync_table(cls)
            break
        except cassandra.protocol.ConfigurationException as e:
            if f"Keyspace {keyspace} doesnt exist" in str(e):
                log.info("Keyspace doesnt exist yet")
                time.sleep(5)
                init()
                continue
            else:
                # Unexpected error
                raise
        except cassandra.AlreadyExists:
            log.info("Table AlreadyExists error")
            time.sleep(5)
            init()
            continue

    # Wait for table to exist
    while True:
        try:
            execute(
                f'SELECT * FROM "{keyspace}"."{cls.__table_name__}";',
                consistency_level=consistency_level,
                timeout=timeout,
            )
            break
        except cassandra.InvalidRequest:
            log.info("InvalidRequest (keyspace doesn't exist yet")
            time.sleep(5)
            init()
            continue
        except cassandra.ReadTimeout:
            log.info("ReadTimeout")
            time.sleep(5)
            init()
