import time
from collections import defaultdict, OrderedDict

from cassandra import concurrent
from cassandra import UnresolvableContactPoints, ConsistencyLevel
from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import NoHostAvailable
from cassandra.cqlengine import CQLEngineException
from cassandra.cqlengine import connection
from cassandra.cqlengine.models import Model

from antares import log
from antares.config import config

if config.DATADOG_TRACE_ENABLE:
    log.info("Patching Cassandra for DataDog Trace")
    from antares.services import trace

    trace.patch_cassandra()


def get_consistency_level():
    """
    consistency_level for Read/Write operations.

    In dev:
    ConsistencyLevel.ONE should be used for schema manipulations.
    ConsistencyLevel.ONE should be used for Reads/Writes.

    In staging and production:
    ConsistencyLevel.ALL should be used for schema manipulations.
    ConsistencyLevel.QUORUM should be used for Reads/Writes.
    """
    if len(config.CASSANDRA_HOSTS) == 1:
        # In dev environment there is only one C* host.
        assert config.ENVIRONMENT == "dev"
        return ConsistencyLevel.ONE
    else:
        # In staging and production, we use RF=3.
        return ConsistencyLevel.QUORUM


# All keyspaces which this process is configured to use
KEYSPACES = []
# All classes mapped to keyspaces {keyspace --> class[]}
KEYSPACE_CLASSES = defaultdict(list)


class CBase(Model):
    """
    Cassandra ORM base class.
    """

    __abstract__ = True

    def as_dict(self):
        return dict(self.items())


class CCatalogBase(CBase):
    __abstract__ = True


class CAlertBase(CBase):
    __abstract__ = True


def request_logger(response_future):
    """
    Log raw Cassandra queries.
    """
    if config.CASSANDRA_ECHO:
        q = response_future.query
        if hasattr(q, "query_string"):
            log.debug(q.query_string)
        elif hasattr(q, "prepared_statement"):
            log.debug(q.prepared_statement)
        log.debug("consistency_level: %s", q.consistency_level)


def init(do_bootstrap=False, n=300, default_timeout=None):
    """
    (Re)initialize Cassandra connections.

    Wait up to `n` seconds for Cassandra to come online.
    """
    if default_timeout is None:
        default_timeout = config.CASSANDRA_DEFAULT_TIMEOUT
    KEYSPACES.clear()
    KEYSPACES.extend(
        filter(
            bool,
            (config.CASSANDRA_ALERT_KEYSPACE, config.CASSANDRA_CATALOG_KEYSPACE),
        )
    )
    if KEYSPACES:
        log.info("initializing Cassandra connection for keyspaces:")
        log.info(KEYSPACES)
        pass  # Continue
    else:
        return  # Don't initialize
    _prepared_queries.clear()

    # Shutdown existing connection
    giveup_time = time.time() + n
    try:
        if connection.cluster:
            log.info("Shutting down existing Cassandra connections...")
            connection.cluster.shutdown()
            log.info("Done.")
    except CQLEngineException:
        log.info("No connection was open.")
        pass  # Connection wasn't open, couldn't be shut down.

    # Configure classes with their keyspaces
    KEYSPACE_CLASSES.clear()
    for cls in CAlertBase.__subclasses__():
        cls.__keyspace__ = config.CASSANDRA_ALERT_KEYSPACE
        KEYSPACE_CLASSES[config.CASSANDRA_ALERT_KEYSPACE].append(cls)
    for cls in CCatalogBase.__subclasses__():
        cls.__keyspace__ = config.CASSANDRA_CATALOG_KEYSPACE
        KEYSPACE_CLASSES[config.CASSANDRA_CATALOG_KEYSPACE].append(cls)

    # Connect
    while True:
        try:
            log.debug("Connecting to Cassandra...")
            auth_provider = None
            if config.CASSANDRA_USER:
                auth_provider = PlainTextAuthProvider(
                    username=config.CASSANDRA_USER,
                    password=config.CASSANDRA_PASS,
                )
            connection.setup(
                hosts=config.CASSANDRA_HOSTS,
                default_keyspace=config.CASSANDRA_ALERT_KEYSPACE,
                consistency=get_consistency_level(),
                protocol_version=3,
                auth_provider=auth_provider,
            )
            connection.get_session().default_timeout = default_timeout
            connection.get_session().add_request_init_listener(request_logger)
            log.info("Connected to Cassandra")
            break
        except (UnresolvableContactPoints, NoHostAvailable):
            if time.time() > giveup_time:
                raise
            log.info("Waiting for Cassandra...")
            time.sleep(5)
            continue

    # Detect missing keyspaces, and bootstrap
    for keyspace in KEYSPACES:
        while True:
            if keyspace in get_keyspaces():
                log.info(f'Keyspace "{keyspace}" exists.')
                break
            log.info(f'Keyspace "{keyspace}" does not exist.')
            if do_bootstrap:
                log.info(f'Bootstrapping keyspace "{keyspace}"...')
                from . import bootstrap

                bootstrap.drop_tables(keyspace)
                bootstrap.create_schema(keyspace)
                break
            if time.time() > giveup_time:
                raise RuntimeError(f'Cassandra keyspace "{keyspace}" doesnt exist')
            log.info(f'Waiting for Cassandra keyspace "{keyspace}" to exist...')
            log.info("Existing keyspaces:")
            for k in get_keyspaces():
                log.info("  %s", k)
            time.sleep(5)
            continue


MAX_PREPARED_QUERIES = 500
_prepared_queries = OrderedDict()


def prepare(query):
    session = connection.get_session()
    while len(_prepared_queries) > MAX_PREPARED_QUERIES:
        # Remove oldest entry in the cache
        _prepared_queries.popitem(last=False)
    if query not in _prepared_queries:
        _prepared_queries[query] = session.prepare(query)
    return _prepared_queries[query]


def execute(query, consistency_level=None, timeout=None, values=None):
    """
    Execute a Cassandra query.

    Queries should specify the namespace in the query explicitly.

    :param query:
    :param consistency_level:
    :param timeout:
    :return:
    """
    session = connection.get_session()
    q = prepare(query)
    if consistency_level is None:
        consistency_level = get_consistency_level()
    q.consistency_level = consistency_level
    return session.execute(q, parameters=values, timeout=timeout)


def execute_async(statement, parameters, *args, **kwargs):
    """
    Execute a Cassandra query.

    Queries should specify the namespace in the query explicitly.

    :param query:
    :param consistency_level:
    :param timeout:
    :return:
    """
    session = connection.get_session()
    statement = prepare(statement)
    return session.execute_async(statement, parameters, *args, **kwargs)


def execute_concurrent_with_args(
    statement, parameters, concurrency=config.CASSANDRA_CONCURRENCY, *args, **kwargs
):
    """
    Execute Cassandra queries concurrently

    Queries should specify the namespace in the query explicitly.
    """
    session = connection.get_session()
    statement = prepare(statement)
    return concurrent.execute_concurrent_with_args(
        session, statement, parameters, concurrency, *args, **kwargs
    )


def get_keyspaces():
    return list(
        {
            row["keyspace_name"]
            for row in execute("SELECT * FROM system_schema.keyspaces")
        }
    )


def print_schemas(include_columns=False):
    """
    Print schemas of all tables in Cassandra.
    """
    tables = list(execute("SELECT * FROM system_schema.tables"))
    columns = list(execute("SELECT * FROM system_schema.columns"))
    for keyspace in KEYSPACES:
        print()
        print("Keyspace:", keyspace)
        for table in tables:
            table_name = table["table_name"]
            if table["keyspace_name"] == keyspace:
                print("  Table:", table_name)
                if include_columns:
                    for column in columns:
                        if (
                            column["keyspace_name"] == keyspace
                            and column["table_name"] == table_name
                        ):
                            print(
                                "    ",
                                column["column_name"],
                                column["type"],
                                column["kind"],
                            )
    print()
