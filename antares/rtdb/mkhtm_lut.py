#!/usr/bin/env python3

import argparse
import os
import logging
import csv
import sys
import time
from datetime import datetime
import threading

from antares import log
from antares.rtdb import api
from antares.config import config
from antares.utils import cone_search

ARCSECONDS_PER_DEGREE = 60 * 60

log.log.setLevel(logging.ERROR)


def main():
    parser = argparse.ArgumentParser(description="""
          mkhtm_lut for ANTARES
          ====================================================
          read a catalog csv file, creating a new htm_lut.csv 
          that is to be loaded into cassandra


      """)

    parser.add_argument('-f', '--file', help="input file")
    parser.add_argument('-t', '--table_name', help="catalog table name; must be defined in catalog_coinfig.py")

    args = parser.parse_args()

    table_config = config.CATALOG_CONFIG.by_name(args.table_name)
    catalog_id = table_config["catalog_id"]
    object_id_column = table_config["object_id_column"]
    ra_column = table_config["ra_column"]
    dec_column = table_config["dec_column"]
    radius = table_config["radius"]
    radius_column = table_config["radius_column"]
    radius_unit = table_config["radius_unit"]
    assert radius_unit in {None, "degree", "arcsecond"}

    # TODO: test if htm_lut.csv doesn't already exist

    start_time = time.perf_counter()
    with open(args.file) as csvfile, open("htm_lut.csv", "w") as outfile, Spinner():
        reader = csv.DictReader(csvfile)
        n_lut = 0
        n_rows = 0

        rtdb = api.RTDB()

        for row in reader:
            n_rows += 1
            if n_rows % 10000 == 0:
                end_time = time.perf_counter()
                rowspsec = 10000 / (end_time - start_time)
                print(f'{datetime.now()} - rows read per second = {rowspsec}')
                start_time = end_time

            # Determine object radius
            row_radius = radius  # Default value is the table default radius

            # for galex only
            if args.table_name == "galex":
                row_radius = float(row['NUV_KRON_RADIUS']) * float(row['NUV_A_WORLD'])
            else:
                if radius_column is not None:
                    assert radius_unit in {"degree", "arcsecond"}
                    if row[radius_column] is not None:
                        if radius_unit == "degree":
                            row_radius = row[radius_column]
                        elif radius_unit == "arcsecond":
                            row_radius = row[radius_column] / ARCSECONDS_PER_DEGREE
                        else:
                            raise RuntimeError
            assert row_radius is not None

            ra, dec, radius = float(row[ra_column]), float(row[dec_column]), float(row_radius)
            htm_level = cone_search.determine_optimal_htm_level(
                radius=radius, choices=rtdb.CATALOG_OBJECT_INDEX_LEVELS
            )
            htms = list(cone_search.get_region_denormalized(ra, dec, radius, htm_level))
            n = len(htms)
            if n > 50:  # Sanity check
                log.warning(
                    "get_region_denormalized(%s, %s, %s, %s)", ra, dec, radius, htm_level
                )
                raise RuntimeError(f"Computed {n} HTMs in region. Bug?")
            for htm in htms:
                n_lut += 1
                outfile.write("%s, %s, %s\n" % (htm, catalog_id, row[object_id_column]))

    print(f"{n_rows} rows processed")
    print(f"{n_lut} rows created/lines in {args.file}")


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


class Spinner:
    busy = False
    delay = 0.5

    @staticmethod
    def spinning_cursor():
        while 1:
            for cursor in '|/-\\': yield cursor

    def __init__(self, delay=None):
        self.spinner_generator = self.spinning_cursor()
        if delay and float(delay): self.delay = delay

    def spinner_task(self):
        while self.busy:
            sys.stdout.write(next(self.spinner_generator))
            sys.stdout.flush()
            time.sleep(self.delay)
            sys.stdout.write('\b')
            sys.stdout.flush()

    def __enter__(self):
        self.busy = True
        threading.Thread(target=self.spinner_task).start()

    def __exit__(self, exception, value, tb):
        self.busy = False
        time.sleep(self.delay)
        if exception is not None:
            return False


if __name__ == '__main__':
    main()

