"""

This file contains an RQ-based system for dumping large time intervals of Alerts
to CSV files on an NFS share.

Given a time window, this code dumps all Alerts whose MJD is within
that time window to disk. No Locus data is dumped. No other Alerts outside of
the time window are dumped.

In the future, I hope that Spark can replace this.

-- Carl

Usage example:

    import datetime
    from antares.rtdb import dump_to_csv as dump

    # Delete any existing data in output directory
    dump.clear_data_dir()

    # Nights to dump
    nights = [
        (2020, 6, 9),
        (2020, 6, 11),
        (2020, 6, 12),
        (2020, 6, 16),
        (2020, 6, 17),
        (2020, 6, 18),
        (2020, 6, 19),
    ]

    # Select the columns to dump
    columns = ['ant_mjd', 'ant_time_received', 'ant_input_msg_time']

    # Queue up RQ jobs to dump data
    for (year, month, day) in nights:
        dump.go(
            datetime.datetime(year, month, day, 0),   # 00:00 UTC
            datetime.datetime(year, month, day, 12),  # 12:00 UTC
            columns,
        )

    # Wait for all RW jobs to finish
    dump.wait_for_empty_queue()

    # Aggregate results into a single csv file
    dump.aggregate()

"""
import csv
import os
import shutil
import time

from antares import rq, log
from antares.config import config
from antares.rtdb import antcassandra
from antares.rtdb.api import RTDB
from antares.rtdb.schema import CLocusByDay
from antares.services import metrics
from antares.utils import cmd

MAX_QUEUE_LEN = 100 * 1000


def _data_dir():
    assert config.TEMP_DIR
    return os.path.join(config.TEMP_DIR, "alert_dump")


"""
User API
"""


def go(start, end, prop_names, inline=True):
    """
    Schedule and start the dump jobs.
    """
    kwargs = dict(start=start, end=end, prop_names=prop_names)
    if inline:
        schedule_dump_alerts(**kwargs)
    else:
        rq.enqueue(func=schedule_dump_alerts, kwargs=kwargs)


def wait_for_empty_queue():
    """
    Block until RQ queue len is 0, then return None.
    """
    log.info("Waiting for empty RQ queue...")
    while len(rq.get_queue()):
        time.sleep(1)


def clear_data_dir():
    """
    Delete the output data directory and all contents.
    """
    shutil.rmtree(_data_dir())


def aggregate(fname="alert_dump.csv"):
    """
    Concat all downloaded files together into one file.
    """
    out_file = os.path.join(config.TEMP_DIR, fname)
    script = "cat {} \;"
    cmd.call(f"find '{_data_dir()}' -name '*.csv' -exec {script} > '{out_file}'")


"""
RQ job functions (internals)
"""


@metrics.timed("rq.job", tags=["job:dump_to_csv.schedule_jobs"])
def schedule_dump_alerts(start, end, prop_names):
    """

    :param start: datetime
    :param end: datetime
    :param prop_names: list of str
    """
    antcassandra.init()
    for dt in CLocusByDay.list_keys(start, end):
        rq.enqueue(
            func=dump_alerts,
            kwargs=dict(
                dt=dt,
                prop_names=prop_names,
            ),
        )
        log.info(f"Queued job {dt}")
        while len(rq.get_queue()) > MAX_QUEUE_LEN:
            log.info(f"Sleeping...")
            time.sleep(10)


@metrics.timed("rq.job", tags=["job:dump_to_csv.dump_alerts"])
def dump_alerts(dt, prop_names):
    antcassandra.init()
    data = []
    mjd_min, mjd_max = CLocusByDay.mjd_range(dt)
    log.debug("Expecting alerts with mjd in range (%s, %s)", mjd_min, mjd_max)
    rtdb = RTDB()
    locus_ids = CLocusByDay.get(dt=dt)
    log.info("Found %s matching Loci", len(locus_ids))
    if not locus_ids:
        return
    for locus_id in locus_ids:
        log.debug(f"Found locus_id {locus_id}")
        alerts = rtdb.get_alerts(locus_id)
        assert alerts
        data_for_this_locus = [
            [getattr(alert, p, alert.properties.get(p)) for p in prop_names]
            for alert in alerts
            if mjd_min <= alert.mjd < mjd_max
        ]
        log.debug("Found %s matching alerts on locus", len(data_for_this_locus))
        if not data_for_this_locus:
            raise RuntimeError(
                f"Found locus {locus_id} at {dt} but found no matching alerts"
            )
        data.extend(data_for_this_locus)
    del locus_ids
    assert data
    log.info(f"Found {len(data)} matching alerts")
    output_dir = os.path.join(_data_dir(), dt.date().isoformat(), str(dt.hour))
    os.makedirs(output_dir, exist_ok=True)
    dt_str = dt.isoformat().replace(":", "-")
    output_fname = os.path.join(output_dir, f"{dt_str}.csv")
    with open(output_fname, "w") as f:
        csv.writer(f).writerows(data)
