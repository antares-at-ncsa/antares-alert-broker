from cassandra import ConsistencyLevel

from antares import log
from antares.rtdb.schema import CCatalogHTMLUT, CHTMLUT


class HTMLUT:
    """
    Interface for accessing the HTM LUTs.
    """

    # Stored in CHTMLUT
    LOCUS_TYPE = 1
    LOCUS_FORMAT = "{locus_id}"
    WO_TYPE = 2
    WO_FORMAT = "{wl_id}:{wo_id}"

    # Stored in CCatalogHTMLUT
    CAT_FORMAT = "{cat_id}:{obj_id}"

    def __init__(self):
        self._htms_loaded = set()  # set of HTM ids
        self._loc = set()  # set of locus_id strings
        self._cat = set()  # set of (cat_id, obj_id) tuples
        self._wo = set()  # set of (wl_id, wo_id) tuples

    @staticmethod
    def _validate(**kw):
        if "htm" in kw:
            assert isinstance(kw["htm"], int)
            assert kw["htm"] > 0
        if "locus_id" in kw:
            assert isinstance(kw["locus_id"], str)
            assert kw["locus_id"]
        if "cat_id" in kw:
            assert isinstance(kw["cat_id"], int)
            assert kw["cat_id"] > 0
        if "obj_id" in kw:
            assert isinstance(kw["obj_id"], (str, int))
            if isinstance(kw["obj_id"], int):
                assert kw["obj_id"] > 0
            assert kw["obj_id"]
        if "wl_id" in kw:
            assert isinstance(kw["wl_id"], int)
            assert kw["wl_id"] > 0
        if "wo_id" in kw:
            assert isinstance(kw["wo_id"], int)
            assert kw["wo_id"] > 0

    @classmethod
    def put_locus(cls, htm, locus_id):
        cls._validate(htm=htm, locus_id=locus_id)
        value = cls.LOCUS_FORMAT.format(locus_id=locus_id)
        CHTMLUT(htm=htm, type=cls.LOCUS_TYPE, value=value).save()

    @classmethod
    def put_cat_obj(cls, htm, cat_id, obj_id):
        log.debug("put_cat_obj(%s, %s, %s)", htm, cat_id, obj_id)
        cls._validate(htm=htm, cat_id=cat_id, obj_id=obj_id)
        value = cls.CAT_FORMAT.format(cat_id=cat_id, obj_id=obj_id)
        CCatalogHTMLUT(htm=htm, value=value).save()

    @classmethod
    def put_watch_obj(cls, htm, wl_id, wo_id):
        cls._validate(htm=htm, wl_id=wl_id, wo_id=wo_id)
        value = cls.WO_FORMAT.format(wl_id=wl_id, wo_id=wo_id)
        CHTMLUT(htm=htm, type=cls.WO_TYPE, value=value).save()

    @classmethod
    def delete_locus(cls, htm, locus_id):
        cls._validate(htm=htm, locus_id=locus_id)
        value = cls.LOCUS_FORMAT.format(locus_id=locus_id)
        CHTMLUT.filter(htm=htm, type=cls.LOCUS_TYPE, value=value).delete()

    @classmethod
    def delete_cat_obj(cls, htm, cat_id, obj_id):
        cls._validate(htm=htm, cat_id=cat_id, obj_id=obj_id)
        value = cls.CAT_FORMAT.format(cat_id=cat_id, obj_id=obj_id)
        CCatalogHTMLUT.filter(htm=htm, value=value).delete()

    @classmethod
    def delete_watch_obj(cls, htm, wl_id, wo_id):
        cls._validate(htm=htm, wl_id=wl_id, wo_id=wo_id)
        value = cls.WO_FORMAT.format(wl_id=wl_id, wo_id=wo_id)
        CHTMLUT.filter(htm=htm, type=cls.WO_TYPE, value=value).delete()

    def load(self, htm):
        """
        Load all hits for an HTM coordinate.

        :param htm:
        :return:
        """
        self._htms_loaded.add(htm)

        # Load from the Locus/WatchObject LUT.
        hits = CHTMLUT.filter(htm=htm).consistency(ConsistencyLevel.ONE).all()
        for hit in hits:
            if hit.type == self.LOCUS_TYPE:
                locus_id = hit.value
                self._loc.add(locus_id)
            elif hit.type == self.WO_TYPE:
                wl_id, _, wo_id = hit.value.partition(":")
                wl_id = int(wl_id)
                wo_id = int(wo_id)
                self._wo.add((wl_id, wo_id))
            else:
                raise RuntimeError(
                    "Unrecognized HTMLUT type %s at htm %s", hit.type, htm
                )

        # Load from the Catalog LUT.
        hits = CCatalogHTMLUT.filter(htm=htm).consistency(ConsistencyLevel.ONE).all()
        for hit in hits:
            cat_id, _, obj_id = hit.value.partition(":")
            cat_id = int(cat_id)
            self._cat.add((cat_id, obj_id))

    def get_loci(self):
        return list(self._loc)

    def get_cat_objs(self):
        return list(self._cat)

    def get_watch_objs(self):
        return list(self._wo)

    def get_htms_loaded(self):
        return list(self._htms_loaded)
