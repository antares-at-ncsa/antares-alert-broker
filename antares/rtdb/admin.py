import time

import cassandra
from cassandra import ConsistencyLevel
from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import Cluster

from antares import log
from antares.config import config


def test_login(hosts, user, password):
    """
    Test Cassandra connection credentials.
    """
    cluster = Cluster(
        hosts,
        auth_provider=PlainTextAuthProvider(
            username=user,
            password=password,
        ),
    )
    cluster.connect()
    cluster.shutdown()


class CassAdmin:
    """
    Functions for creation and deletion of Cassandra users.
    """

    def __init__(self, hosts, admin_user=None, admin_pass=None, timeout=60):
        self.hosts = hosts
        self.admin_user = admin_user or config.CASSANDRA_ADMIN_USER
        self.admin_pass = admin_pass or config.CASSANDRA_ADMIN_PASS
        self.timeout = timeout

        if len(self.hosts) == 1:
            self.consistency_level = ConsistencyLevel.ONE
        else:
            self.consistency_level = ConsistencyLevel.ALL

    def get_keyspaces(self):
        return list(
            {
                row[0]
                for row in self._execute(
                    "SELECT keyspace_name FROM system_schema.keyspaces"
                )
            }
        )

    def ensure_keyspace(self, keyspace, replication_factor=3):
        log.info(f'create_keyspace(keyspace="{keyspace}")')

        replication = f"""
            {{'class': 'SimpleStrategy', 'replication_factor': {replication_factor}}}
        """.strip()
        del replication_factor

        # Create keyspace
        try:
            self._execute(
                f"CREATE KEYSPACE {keyspace} WITH replication = {replication};",
            )
        except cassandra.AlreadyExists:
            pass

        # Wait for keyspace to exist
        time.sleep(5)
        while True:
            if keyspace in self.get_keyspaces():
                break
            log.info(f'Waiting for keyspace "{keyspace}" to exist')
            time.sleep(5)

    def grant(self, username, password, keyspaces, readonly):
        for ks in keyspaces:
            self.ensure_keyspace(ks)

        csql = (
            "CREATE ROLE IF NOT EXISTS %s " + "WITH PASSWORD = '%s' AND LOGIN = TRUE"
        ) % (username, password)
        self._execute(csql)

        for ks in keyspaces:
            csql = "GRANT SELECT ON KEYSPACE %s TO %s" % (ks, username)
            self._execute(csql)

        if not readonly:
            for ks in keyspaces:
                csql = "GRANT CREATE ON ALL KEYSPACES TO %s" % (username)
                self._execute(csql)
                csql = "GRANT MODIFY ON KEYSPACE %s TO %s" % (ks, username)
                self._execute(csql)
            for ks in keyspaces:
                csql = "GRANT DROP ON KEYSPACE %s TO %s" % (ks, username)
                self._execute(csql)

    def drop_role(self, username):
        csql = "DROP ROLE %s " % username
        self._execute(csql)

    def list_all(self):
        csql = "LIST ALL"
        return self._execute(csql)

    def _execute(self, csql):
        cluster = Cluster(
            self.hosts,
            auth_provider=PlainTextAuthProvider(
                username=self.admin_user,
                password=self.admin_pass,
            ),
        )
        s = cluster.connect()
        try:
            log.info(csql)
            statement = s.prepare(csql)
            statement.consistency_level = self.consistency_level
            result = s.execute(statement, timeout=self.timeout)
            result = list(result)  # ResultSet --> list
            log.info(result)
            return result
        finally:
            cluster.shutdown()
