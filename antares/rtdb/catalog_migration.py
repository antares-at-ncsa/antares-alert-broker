"""

"My2Cass" Migration system for catalog tables.

This code can copy catalog tables from MySQL into Cassandra.
This includes:

- Inspecting MySQL schema and creating Cassandra tables.
- Scheduling copy jobs
- Executing copy jobs
- Tracking job state.

ASSUMPTIONS:
- All catalogs' object_id columns may be represented as unsigned BIGINTs.


How to ingest a new catalog from MySQL
--------------------------------------

1. Create the catalog table in MySQL and populate it with data.

2. If the catalog objects have a defined radius, then find the row in the table
   which contains the radius. If not, then determine a radius to use for the
   whole table. We use default of 1" == 1/3600 degrees.

3. Create an entry in `catalog_config.py` for the new table. eg:

    {
        "table": "galex",           # Name of table in MySQL
        "catalog_id": 18,           # Must be unique
        "enabled": True,
        "ra_column": "AVASPRA",     # Name of 'ra' column in MySQL
        "dec_column": "AVASPDEC",   # Name of 'ra' column in MySQL
        "object_id_column": "sid",  # Name of unique object id in MySQL
        "object_id_type": "int",    # Type of the id. Only `int` is supported.
        "radius": None,             # Default radius
        "radius_column": "radius_degrees",  # Name of radius column in MySQL
        "radius_unit": "degree",            # must be 'degree', 'arcsecond' or None.
    },

    In this case ^, there was no single radius column. Chien-Hsiu realized that
    we can compute a radius using a product of two columns. I created a new
    column in the table called "radius_degrees" and populated it like:

    UPDATE galex SET radius_degrees = NUV_KRON_RADIUS * NUV_A_WORLD;

    The units are degrees, not arcseconds. I named the column to make this
    explicit. Then, I generated the dict structure above.

    There is more documentation in comments in `catalog_config.py`.

4. With this catalog config in palace, re-deploy the system.
   I run my2cass on a separate branch `my2cass-staging` so that the RQ job queue
   is separate from production. So, push this branch and let it deploy.

5. Once the new `my2cass-staging` env is running, shell into it:

    ./build.py env=my2cass-staging kpp rq_scheduler

6. In the python shell, create the new (empty) catalog table in Cassandra::

    from antares.rtdb import catalog_migration as cm
    cm.create_missing_catalog_tables()

   This will create any catalog tables which don't exist.

7. Create migration jobs. Jobs are tuples of form:

    (table_name, object_id_min, object_id_max)

   This tuple defines a batch of catalog objects which must be copied.
   These jobs live as rows in an SQL table.

   To create them, call:

    cm.create_catalog_migration_jobs()

8. Sanity check. Print a report of all jobs which currently exist:

    cm.print_job_status()

   The newly created jobs will show up as 'created' but not 'started' and not
   'done'. Now you're ready to schedule and run the jobs in RQ.

9. Trigger the jobs to run.

    cm.run_catalog_migration_jobs()

   I usually run it with 20 workers. Seems to be diminished returns with more.

    ./build.py env=my2cass-staging scale_app rq_worker 20

10. Monitor. A status report will be printed to Slack every 24 hours. You can
    also get a status report at any time with:

    cm.print_job_status()


Debugging
---------

If a Job crashed, it will end up marked 'started' but not marked 'done'. Since
its marked 'started', it won't be scheduled to run again.

You will see these jobs in the output of `cm.print_job_status()`.
Then, you can inspect them using `cm.get_jobs_not_done(table)`.

You can reset all jobs which are 'started' but not 'done', and run them again:

    cm.stop()  # drop the redis queue
    # wait for running jobs to finish... (Use DataDog)

    cm.set_all_not_done_as_not_started()

    # Start up again:
    cm.run_catalog_migration_jobs()


Note:

My2Cass is idempotent. It is harmless to re-run jobs which have already run.

"""
import datetime
import math

from humanfriendly.tables import format_pretty_table
from sqlalchemy import Column
from sqlalchemy import create_engine
from sqlalchemy.dialects.mysql import INTEGER, VARCHAR, DATETIME, BIGINT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from antares import log, rq, redis
from antares.config import config
from antares.services import metrics, slack
from . import api, antcassandra, bootstrap, schema
from antares.config import config

# Schema must be imported, even if not used.
assert schema


__all__ = [
    "delete_table",
    "create_missing_catalog_tables",
    "create_catalog_migration_jobs",
    "run_catalog_migration_jobs",
    "set_all_not_done_as_not_started",
    "print_job_status",
]


ARCSECONDS_PER_DEGREE = 60 * 60
BATCH_SIZE = 1000
QUEUE = rq.QUEUE_LOW  # Run all jobs in the low-priority queue

_catalog_mysql_engine = None


"""
Top Level API user for production.

Use the functions below from a shell in production to operate my2cass.
"""


def delete_table(table, force=False):
    """
    Delete a catalog table in C* and remove all its jobs from job schedule.
    """
    bootstrap.drop_table(
        keyspace=config.CASSANDRA_CATALOG_KEYSPACE,
        table=table,
        force=force,
    )
    s = My2CassStatus()
    s.delete_jobs(table)
    s.close()


def create_missing_catalog_tables():
    """
    Create tables in Cassandra, if any are missing.
    """
    tables = My2Cass().create_missing_cass_tables()
    print("Created catalog tables:")
    print(tables)
    return tables


def create_catalog_migration_jobs(inline=True):
    """
    Create copy jobs for all tables which have 0 existing jobs.
    """
    if inline:
        _create_copy_jobs()
    else:
        rq.enqueue(_create_copy_jobs)


def run_catalog_migration_jobs(recurse=True, limit=1000):
    """
    Schedule and run all non-started copy jobs.
    """
    rq.enqueue(
        func=_schedule_copy_jobs,
        kwargs=dict(
            limit=limit,
            inline=False,
            recurse=recurse,
            max_rq_queue_len=1000,
        ),
        queue=QUEUE,
    )


def set_all_not_done_as_not_started():
    """
    If jobs failed, they may be marked as started but not done. This fn marks
    them as not started, so they can be scheduled to run again.

    :return: number of rows affected
    """
    result = _get_mysql_engine().execute(
        f"""
        UPDATE {Job.__tablename__}
        SET {Job.started.name}=NULL
        WHERE
            {Job.started.name} IS NOT NULL
            AND {Job.done.name} IS NULL;
    """
    )
    return result.rowcount


def get_jobs_not_done(table):
    """
    Get a report of all jobs which are `started` but not `done` for a table.

    This is the state that jobs end up in if they crash.
    Thus, this function is useful for debugging.
    """
    s = My2CassStatus()
    jobs = s.get_jobs_not_done(table)
    s.close()
    return jobs


def stop():
    """
    Drop the RQ queue.
    """
    rq.get_queue(QUEUE).empty()


def print_job_status(and_return_it=False):
    """
    Print a status report of all jobs.

    :param and_return_it: if True, also return the report as a string.
    :return: `str` if `and_return_it` else None
    """
    columns = ("table", "jobs", "started", "done", "not_done", "percent")
    rows = list(
        _get_mysql_engine().execute(
            f"""
        select
            `table`,
            count(*) as jobs,
            count(started) as started,
            count(done) as done,
            count(*) - count(done) as not_done,
            100 * count(done) / count(*) as percent
        from {Job.__tablename__}
        group by `table`;
    """
        )
    )

    # Generate summary row
    def sum_(i):
        return sum(r[i] for r in rows)

    total_percent = round(100 * float(sum_(3)) / sum_(1), 4)
    rows.extend(
        [
            ["" for _ in columns],
            ["TOTAL", sum_(1), sum_(2), sum_(3), sum_(4), total_percent],
        ]
    )

    # Format table
    report = format_pretty_table(rows, columns)
    print(report)
    if and_return_it:
        return report


"""
Top-level API for use in Integration Tests and dev env bootstrapping.
"""


def _do_all_inline():
    """
    Run entire my2cass workflow synchronously.

    USE ONLY IN TESTS.
    """
    assert config.ENVIRONMENT == "dev"
    bootstrap.drop_and_create_schema(config.CASSANDRA_CATALOG_KEYSPACE)

    create_missing_catalog_tables()

    my2cass_status = My2CassStatus()
    my2cass_status.drop_job_table()
    my2cass_status.setup_job_table()
    my2cass_status.close()
    del my2cass_status

    _create_copy_jobs()

    _schedule_copy_jobs(inline=True)


"""
Helper functions and internals, from here on down.
"""


def _get_mysql_engine():
    global _catalog_mysql_engine
    if _catalog_mysql_engine is None:
        _catalog_mysql_engine = create_engine(
            config.MYSQL_CATALOG_DB_URL,
            echo=config.SQLALCHEMY_ECHO,
            echo_pool=config.SQLALCHEMY_ECHO,
            **config.SQLALCHEMY_ENGINE_PARAMS,
        )
    return _catalog_mysql_engine


def _get_mysql_session():
    return sessionmaker(bind=_get_mysql_engine(), autoflush=False)()


def _get_cassandra_connection():
    return antcassandra.connection


"""
SQLAlchemy model for a DB which tracks job state.
"""


class My2CassDBBase(declarative_base()):
    __abstract__ = True


class Job(My2CassDBBase):
    __tablename__ = "_catalog_migration_job"

    job_id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True)
    table = Column(VARCHAR(100))
    object_id_min = Column(BIGINT(unsigned=True))
    object_id_max = Column(BIGINT(unsigned=True))
    created = Column(DATETIME())
    started = Column(DATETIME())
    done = Column(DATETIME())
    duration_seconds = Column(INTEGER())


class My2CassStatus:
    """
    Manages state of copy batch jobs.

    Keeps an open MySQL session.
    Should be used during finite-term jobs only and not kept forever.
    The user of this object must call `.close()` when finished with it.
    """

    def __init__(self):
        self._mysql_engine = _get_mysql_engine()
        self._session = _get_mysql_session()

    def close(self):
        self._session.close()

    def _mysql_exec(self, query):
        return self._mysql_engine.execute(query)

    def drop_job_table(self):
        assert self._mysql_engine.url.database.endswith("_dev")
        log.info("DROPPING migration jobs table")
        self._mysql_exec(f"DROP TABLE IF EXISTS `{Job.__tablename__}`")

    def delete_jobs(self, table):
        self._session.query(Job).filter(Job.table == table).delete()
        self._session.commit()

    def setup_job_table(self):
        log.info("Creating migration jobs table")
        My2CassDBBase.metadata.create_all(bind=self._mysql_engine)

    def get_tables_with_jobs_existing(self):
        rows = self._mysql_engine.execute(
            f"""
            SELECT DISTINCT `table` FROM {Job.__tablename__};
        """
        )
        return [r[0] for r in rows]

    def set_job_status(self, table_name, object_id_min, object_id_max, state):
        assert state in ("created", "started", "done")
        mn, mx = object_id_min, object_id_max
        del object_id_min, object_id_max
        log.debug("set_job_status %s %s %s %s", table_name, mn, mx, state)
        if state == "created":
            self._session.add(
                Job(
                    table=table_name,
                    object_id_min=mn,
                    object_id_max=mx,
                    created=datetime.datetime.utcnow(),
                )
            )
            self._session.commit()
            return
        else:
            j = (
                self._session.query(Job)
                .filter_by(table=table_name, object_id_min=mn, object_id_max=mx)
                .one()
            )
            if state == "started":
                j.started = datetime.datetime.utcnow()
                self._session.commit()
                return
            elif state == "done":
                j.done = datetime.datetime.utcnow()
                td = j.done - j.started
                j.duration_seconds = td.total_seconds()
                self._session.commit()
                return

    def get_job_status(self, table_name, object_id_min, object_id_max):
        j = (
            self._session.query(Job)
            .filter_by(
                table=table_name,
                object_id_min=object_id_min,
                object_id_max=object_id_max,
            )
            .one()
        )
        if j is None:
            return None
        return j

    def get_jobs_not_done(self, table_name):
        jobs = (
            self._session.query(Job)
            .filter_by(table=table_name)
            .filter(Job.done.is_(None))
            .all()
        )
        return [
            dict(
                table=job.table,
                object_id_min=job.object_id_min,
                object_id_max=job.object_id_max,
            )
            for job in jobs
        ]

    def iter(self, **kw):
        limit = kw.pop("limit", None)
        filter_by = kw
        q = self._session.query(Job).filter_by(**filter_by).order_by(Job.job_id.asc())
        if limit is not None:
            q = q.limit(limit)
        for s in q:
            yield s

    def count(self, **filter_by):
        return self._session.query(Job).filter_by(**filter_by).count()


"""
Code which handles creating Cassandra tables based on MySQL originals.
"""


class My2Cass:
    """
    Creates Cassandra Catalog tables corresponding to MySQL Catalog tables,
    and copies date.

    Note:

    Keyspace, table, and column names in Cassandra are case-insensitive unless
    enclosed in double quotes, eg: "Foo". Unquoted names are converted to lower
    case.
    """

    def __init__(self):
        self._mysql_engine = _get_mysql_engine()
        self._cass_conn = _get_cassandra_connection()

    def _mysql_exec(self, query):
        return self._mysql_engine.execute(query)

    def _cass_exec(self, query):
        return self._cass_conn.get_session().execute(query)

    def _cass_session(self):
        return self._cass_conn.get_session()

    @staticmethod
    def translate_type(mysql_type):
        return mysql_type.replace("varchar", "text").replace("date", "timestamp")

    def create_missing_cass_tables(self):
        new_tables = []
        for table_config in config.CATALOG_CONFIG.tables:
            table = table_config["table"]  # table name
            if self._check_cass_table_exists(table):
                log.info("Table already exists %s", table)
            else:
                log.info("Creating table %s", table)
                self._create_cass_table(table_config)
                new_tables.append(table)
        return new_tables

    def _check_cass_table_exists(self, table_name):
        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        result = self._cass_exec(
            f"""
            select *
            from system_schema.tables
            where
                keyspace_name = '{keyspace}'
                and
                table_name='{table_name}'
        """
        )
        return bool(list(result))

    def _create_cass_table(self, table_config):
        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        table_name = table_config["table"]
        object_id_column = table_config["object_id_column"]

        create_cols = self._get_cass_cols_defn(table_name)
        query = f"""
            CREATE TABLE "{keyspace}"."{table_name}" (
                {create_cols},
                PRIMARY KEY ("{object_id_column}")
            )
        """
        log.info("Creating Cassandra Table:")
        log.info(query)
        self._cass_exec(query)

    def _get_cass_cols_defn(self, table_name):
        """
        Create a Cassandra column definition string from a MySQL table.

        Column names are "double quoted".
        """
        schema = self._mysql_engine.url.database
        assert schema
        query = f"""
            select column_name, data_type 
            from information_schema.columns
            where 
                table_schema='{schema}'
                and
                table_name = '{table_name}'
            order by ORDINAL_POSITION
        """
        column_definitions = []
        for (column_name, mysql_data_type) in self._mysql_exec(query):
            cass_data_type = self.translate_type(mysql_data_type)
            column_definitions.append(f""""{column_name}" {cass_data_type}""")
        return ", ".join(column_definitions)

    def cass_schema_columns(self):
        """
        Return content of Cassandra information_schema.columns

        Useful in testing.
        """
        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        query = f"""
            select *
            from system_schema.columns
            where keyspace_name='{keyspace}'
        """
        return list(self._cass_exec(query))

    def get_batches_simple(self, table_name, batch_size):
        """
        Yield (min, max, n, total) batches representing batches of rows in MySQL table...
        ...assuming the object_id_column is an integer ranging from 0 to N
        with no gaps.

        If there are gaps in the keyspace, then there may be many empty/invalid
        batches returned.

        """
        log.info(table_name)
        table_config = config.CATALOG_CONFIG.by_name(table_name)
        object_id_column = table_config["object_id_column"]
        query = f"""
            SELECT min({object_id_column}), max({object_id_column})
            FROM {table_name}
        """
        rows = list(self._mysql_exec(query))
        ((object_id_min, object_id_max),) = rows
        # Assume we're dealing with an auto-incremented column with a keyspace
        # from 1...N.
        # for unknown reasons, one catalog's ID space starts at 2, not 1.
        log.info(f"object_id_min: {object_id_min} object_id_max: {object_id_max}")
        assert object_id_min in (1, 2)
        assert object_id_max < 1000 ** 4  # No catalog has a trillion rows.. right?

        # Compute number of batches
        total = math.ceil(object_id_max / float(batch_size))
        # Generate (min, max) pairs
        n = 1
        mn = object_id_min
        mx = object_id_min + batch_size - 1
        while True:
            if mx >= object_id_max:
                mx = object_id_max
            yield (mn, mx, n, total)
            if mx == object_id_max:
                break
            n += 1
            mn = mx + 1
            mx += batch_size

    def get_batches_slow(self, table_name, batch_size, initial_offset=0):
        """
        Yield (min, max) pairs representing batches of rows in MySQL table.

        Bad performance on large tables.
        """
        table_config = config.CATALOG_CONFIG.by_name(table_name)
        object_id_column = table_config["object_id_column"]
        offset = initial_offset
        while True:
            query = f"""
                SELECT min({object_id_column}), max({object_id_column})
                FROM (
                    SELECT {object_id_column}
                    FROM {table_name}
                    ORDER BY {object_id_column}
                    LIMIT {batch_size}
                    OFFSET {offset}
                ) as object_ids_in_batch
            """
            rows = list(self._mysql_exec(query))
            ((object_id_min, object_id_max),) = rows
            if object_id_min is None:
                assert object_id_max is None
                break
            yield (object_id_min, object_id_max)
            offset += batch_size

    def copy_batch(self, table_name, object_id_min, object_id_max):
        assert isinstance(object_id_min, int)
        assert isinstance(object_id_max, int)

        # Pull variables from table config
        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        table_config = config.CATALOG_CONFIG.by_name(table_name)
        catalog_id = table_config["catalog_id"]
        object_id_column = table_config["object_id_column"]
        ra_column = table_config["ra_column"]
        dec_column = table_config["dec_column"]
        radius = table_config["radius"]
        radius_column = table_config["radius_column"]
        radius_unit = table_config["radius_unit"]
        assert radius_unit in {None, "degree", "arcsecond"}

        cass_session = self._cass_session()
        rtdb = api.RTDB()

        # Query data from MySQL
        log.debug("Querying MySQL...")
        mysql_data = self._mysql_exec(
            f"""
            SELECT *
            FROM `{table_name}`
            WHERE {object_id_min} <= `{object_id_column}`
                AND `{object_id_column}` <= {object_id_max}
        """
        )
        mysql_data = list(mysql_data)
        log.debug("Got %s rows", len(mysql_data))
        assert len(mysql_data)

        # Lazy-load this on first loop
        columns = None
        cass_insert_query = None

        # Insert rows into Cassandra
        n_rows = 0
        n_lut = 0
        for row in mysql_data:
            # Collect needed variables from the row.
            row_dict = dict(row)
            log.debug(row_dict)
            row_columns = list(row.keys())
            row_values = list(row.values())
            row_ra = row_dict[ra_column]
            row_dec = row_dict[dec_column]
            row_object_id = row_dict[object_id_column]

            # Determine object radius
            row_radius = radius  # Default value is the table default radius
            if radius_column is not None:
                assert radius_unit in {"degree", "arcsecond"}
                if row_dict[radius_column] is not None:
                    if radius_unit == "degree":
                        row_radius = row_dict[radius_column]
                    elif radius_unit == "arcsecond":
                        row_radius = row_dict[radius_column] / ARCSECONDS_PER_DEGREE
                    else:
                        raise RuntimeError
            assert row_radius is not None

            # Prepare Cassandra INSERT query
            if columns is None:
                columns = ", ".join(f'"{col}"' for col in row_columns)
            assert columns == ", ".join(f'"{col}"' for col in row_columns)
            if cass_insert_query is None:
                placeholders = ", ".join("?" for _ in row_columns)
                cass_insert_query = cass_session.prepare(
                    f"""
                    INSERT INTO "{keyspace}"."{table_name}" ({columns})
                    VALUES ({placeholders})
                """
                )

            # Insert row into Cassandra
            log.debug(cass_insert_query)
            log.debug(row_values)
            cass_session.execute(cass_insert_query, row_values)
            n_rows += 1

            # Create catalog LUT entries for this object
            log.debug("Updating HTMLUT...")
            n_lut += rtdb.put_catalog_object_htmlut(
                cat_id=catalog_id,
                obj_id=row_object_id,
                ra=row_ra,
                dec=row_dec,
                radius=row_radius,
            )

        log.info(f"Inserted {n_rows} rows, {n_lut} LUT entries.")


"""
RQ Jobs
"""


@metrics.timed("rq.job", tags=["job:catalog_migration._create_copy_jobs"])
def _create_copy_jobs():
    """
    Create job records for each batch of rows in all catalog tables
    """
    log_every_n = 10000  # Print progress after every n'th job
    log.info("_create_copy_jobs")
    my2cass = My2Cass()
    my2cass_status = My2CassStatus()
    tables_with_jobs_already = my2cass_status.get_tables_with_jobs_existing()
    for table_config in config.CATALOG_CONFIG.tables:
        table_name = table_config["table"]
        log.info(table_name)
        if table_name in tables_with_jobs_already:
            log.info("jobs already exist for table. Skipping")
            continue
        for mn, mx, n, total in my2cass.get_batches_simple(table_name, BATCH_SIZE):
            if n == 1 or n % log_every_n == 0 or n == total:
                pct = str(round(100.0 * n / total, 2)) + "%"
                log.info("creating %s job %s / %s (%s)", table_name, n, total, pct)
            my2cass_status.set_job_status(table_name, mn, mx, "created")
        log.info("Done creating jobs for table %s", table_name)
    my2cass_status.close()


@metrics.timed("rq.job", tags=["job:catalog_migration._schedule_copy_jobs"])
def _schedule_copy_jobs(limit=None, inline=False, recurse=False, max_rq_queue_len=None):
    """
    Schedule to run all jobs not currently marked as "started".
    """
    log.info("_schedule_copy_jobs")
    my2cass_status = My2CassStatus()
    total = my2cass_status.count(started=None)

    # Print current progress to Slack
    print_report = redis.Lock("my2cass-progress-report", expire=24 * 3600).lock()
    if total == 0:
        # This means we're finished, so print a report even if the timer
        # hasn't expired.
        print_report = True
    if print_report:
        report = print_job_status(and_return_it=True)
        message = f"```\n{report.strip()}\n```"
        slack.post(message=message, channel="#dev_alerts", async_=False)
    del print_report

    # Halt if there are no jobs to schedule
    if total == 0:
        log.info("No non-started jobs found.")
        return

    # Schedule jobs
    n = 0
    for s in my2cass_status.iter(started=None, limit=limit):
        n += 1

        # Don't fill the queue without bound.
        while max_rq_queue_len and len(rq.get_queue()) > max_rq_queue_len:
            log.info("Queue is full (enough). Abort.")

        assert s.table
        assert s.object_id_min is not None
        assert s.object_id_max is not None
        log.info("enqueuing job %s / %s", n, total)
        job_kwargs = dict(
            table_name=s.table,
            object_id_min=s.object_id_min,
            object_id_max=s.object_id_max,
        )
        if inline:
            _do_job(**job_kwargs)
        else:
            rq.enqueue(
                func=_do_job,
                kwargs=job_kwargs,
                timeout=60 * 60,
                queue=QUEUE,
            )

    my2cass_status.close()

    if recurse:
        if n == 0:
            # No jobs where queued. We're done.
            return
        rq.enqueue(
            func=_schedule_copy_jobs,
            kwargs=dict(
                limit=limit,
                recurse=recurse,
                max_rq_queue_len=max_rq_queue_len,
            ),
            queue=QUEUE,
        )


@metrics.timed("rq.job", tags=["job:catalog_migration._do_job"])
def _do_job(table_name, object_id_min, object_id_max):
    antcassandra.init()
    batch = (table_name, object_id_min, object_id_max)
    del table_name, object_id_min, object_id_max
    my2cass_status = My2CassStatus()

    # Check status is not started already
    s = my2cass_status.get_job_status(*batch)
    assert s
    if s.done:
        log.info("Job already marked DONE. Abort...")
        return
    if s.started:
        log.info("Job already marked STARTED. Abort...")
        return
    del s

    # Copy
    log.info("Copying...")
    my2cass_status.set_job_status(*batch, "started")
    My2Cass().copy_batch(*batch)
    my2cass_status.set_job_status(*batch, "done")
    my2cass_status.close()
