import bson

EMPTY_BSON = bson.dumps({})


class LocusModel:
    def __init__(
        self,
        locus_id,
        created_at,
        updated_at,
        ra,
        dec,
        properties,
        tags,
        wl_ids,
        wo_ids,
    ):
        self.locus_id = locus_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.ra = ra
        self.dec = dec
        self.properties = properties
        self.tags = tags
        self.wl_ids = wl_ids
        self.wo_ids = wo_ids
        self.lightcurve = None

    @staticmethod
    def from_clocus(clocus):
        return LocusModel(
            locus_id=str(clocus.locus_id),
            created_at=clocus.created_at,
            updated_at=clocus.updated_at,
            ra=clocus.ra,
            dec=clocus.dec,
            properties=bson.loads(clocus.props or EMPTY_BSON),
            tags=set(clocus.tags),
            wl_ids=set(clocus.wl_ids),
            wo_ids=set(clocus.wo_ids),
        )

    def __str__(self):
        return f'Locus(locus_id="{self.locus_id}")'

    __repr__ = __str__


class AlertModel:
    def __init__(self, locus_id, mjd, alert_id, processed_at, properties):
        self.locus_id = locus_id
        self.mjd = mjd
        self.alert_id = alert_id
        self.processed_at = processed_at
        self.properties = properties

    @classmethod
    def from_clocus(cls, clocus):
        return cls(
            locus_id=str(clocus.locus_id),
            mjd=clocus.alert_mjd,
            alert_id=clocus.alert_id,
            processed_at=clocus.alert_processed_at,
            properties=bson.loads(clocus.alert_props or EMPTY_BSON),
        )

    def properties_dict(self):
        """
        Return copy of Alert's properties with 'alert_id' and 'mjd' included.
        """
        d = self.properties.copy()
        d.update(
            dict(
                alert_id=self.alert_id,
                mjd=self.mjd,
            )
        )
        return d

    def as_dict(self):
        """
        Use for logging/debugging ONLY. Does not return a standard format.
        """
        return dict(
            locus_id=self.locus_id,
            mjd=self.mjd,
            alert_id=self.alert_id,
            processed_at=self.processed_at,
            properties=self.properties,
        )

    def __str__(self):
        return f'Alert(alert_id="{self.alert_id}")'

    __repr__ = __str__
