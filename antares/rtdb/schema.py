import datetime

from cassandra.cqlengine import columns

from antares import utils
from antares.rtdb.antcassandra import CAlertBase, CCatalogBase

clustering_key = dict(
    primary_key=True,
    clustering_order="ASC",
)


#
# Catalog Schema
#


class CCatalogHTMLUT(CCatalogBase):
    __table_name__ = "htm_lut"
    htm = columns.BigInt(partition_key=True)
    value = columns.Text(**clustering_key)


#
# Alert Schema
#


class CHTMLUT(CAlertBase):
    __table_name__ = "htm_lut"
    htm = columns.BigInt(partition_key=True)
    type = columns.TinyInt(**clustering_key)
    value = columns.Text(**clustering_key)


class CLocus(CAlertBase):
    __table_name__ = "locus"

    locus_id = columns.Text(partition_key=True)
    created_at = columns.DateTime(static=True, default=datetime.datetime.utcnow)
    updated_at = columns.DateTime(static=True)
    ra = columns.Double(static=True)
    dec = columns.Double(static=True)
    props = columns.Blob(static=True)
    tags = columns.Set(columns.Text, static=True)
    wl_ids = columns.Set(columns.Integer, static=True)
    wo_ids = columns.Set(columns.Integer, static=True)
    lightcurve = columns.Text(static=True)

    alert_mjd = columns.Double(**clustering_key)
    alert_id = columns.Text(**clustering_key)
    alert_processed_at = columns.DateTime()
    alert_props = columns.Blob()


class CLocusByDay(CAlertBase):
    __table_name__ = "locus_by_day"
    day = columns.Text(partition_key=True)
    hh_mm = columns.Text(**clustering_key)
    locus_id = columns.Text(**clustering_key)

    @classmethod
    def put(cls, mjd, locus_id):
        utc_datetime = utils.mjd_to_dt(mjd)
        day = cls.day_str(utc_datetime)
        hh_mm = cls.hour_minute_str(utc_datetime)
        cls(day=day, hh_mm=hh_mm, locus_id=locus_id).save()

    @classmethod
    def get(cls, t=None, dt=None):
        """
        Get CLocusByMinute by unix timestamp or UTC datetime.

        Exactly one of `t` or `dt` must be supplied.

        :param t: unix timestamp (time.time())
        :param dt: UTC datetime
        :return: CLocusByMinute
        """
        assert (t is None) + (dt is None) == 1  # there can only be exactly one
        if t is not None:
            dt = datetime.datetime.fromtimestamp(t)
        assert dt is not None
        day = cls.day_str(dt)
        hh_mm = cls.hour_minute_str(dt)
        return [row.locus_id for row in cls.filter(day=day, hh_mm=hh_mm).all()]

    @staticmethod
    def mjd_range(utc_datetime):
        """
        Return (min, max) pair describing the MJD range which should be stored
        under a given datetime.datetme key.

        :param utc_datetime:
        :return:
        """
        assert isinstance(utc_datetime, datetime.datetime)
        start_dt = utc_datetime.replace(second=0, microsecond=0)
        end_dt = start_dt + datetime.timedelta(minutes=1)
        return utils.dt_to_mjd(start_dt), utils.dt_to_mjd(end_dt)

    @staticmethod
    def day_str(utc_datetime):
        """
        Return UTC datetime isoformat truncated to day.

        eg: '2019-10-10'

        :param utc_datetime: UTC datetime.datetime
        :return: str
        """
        return utc_datetime.isoformat()[:10]

    @staticmethod
    def hour_minute_str(utc_datetime):
        """
        Return 'HH:mm' substring of a UTC datetime isoformat.

        :param utc_datetime: UTC datetime.datetime
        :return: str
        """
        return utc_datetime.isoformat()[11:16]

    @staticmethod
    def list_keys(start, end):
        """
        Yield one datetime object per minute bucket in the time range.

        :param start: datetime.datetime
        :param end: datetime.datetime
        :return:
        """
        assert isinstance(start, datetime.datetime)
        assert isinstance(end, datetime.datetime)
        assert start < end
        delta = datetime.timedelta(minutes=1)
        i = start.replace(second=0, microsecond=0)
        while i <= end:
            yield i
            i += delta


class CLocusByAlertID(CAlertBase):
    __table_name__ = "locus_by_alert_id"
    alert_id = columns.Text(partition_key=True)
    locus_id = columns.Text()
    mjd = columns.Double()

    @classmethod
    def put(cls, alert_id, locus_id, mjd):
        cls(alert_id=alert_id, locus_id=locus_id, mjd=mjd).save()

    @classmethod
    def get(cls, alert_id):
        row = super().filter(alert_id=alert_id).first()
        if row:
            return row.locus_id, row.mjd
        return None, None


class CWatchList(CAlertBase):
    __table_name__ = "watch_list"
    wl_id = columns.Integer(partition_key=True)
    name = columns.Text()
    description = columns.Text()
    slack_channel = columns.Text()


class CWatchObject(CAlertBase):
    __table_name__ = "watch_object"
    wl_id = columns.Integer(partition_key=True)
    wo_id = columns.BigInt(**clustering_key)
    ra = columns.Double()
    dec = columns.Double()
    radius = columns.Double()  # in degrees
    htm_level = columns.TinyInt()
    name = columns.Text()


class CStorage(CAlertBase):
    __table_name__ = "storage"
    storage_key = columns.Text(partition_key=True)
    data = columns.Text()

    @classmethod
    def put(cls, storage_key, data):
        cls(storage_key=storage_key, data=data).save()

    @classmethod
    def get(cls, storage_key):
        row = super().filter(storage_key=storage_key).first()
        if row:
            return row.data
        return None

    @classmethod
    def delete_(cls, storage_key):
        row = super().filter(storage_key=storage_key).first()
        if row:
            row.delete()


# TODO
# class CSSO(CAlertBase):
#     __table_name__ = 'sso'
#     sso_id = columns.Text(partition_key=True)
#     locus_id = columns.Text(**clustering_key)
#
#     @classmethod
#     def put(cls, sso_id, locus_id):
#         cls(sso_id=sso_id, locus_id=locus_id).save()
#
#     @classmethod
#     def remove(cls, sso_id, locus_id):
#         cls.filter(sso_id=sso_id, locus_id=locus_id).delete()
#
#     @classmethod
#     def get(cls, sso_id):
#         return [
#             s.locus_id for s in cls.filter(sso_id=sso_id).all()
#         ]
