import datetime
from decimal import Decimal
from typing import Any, Iterator, List, Set, Tuple
from typing_extensions import TypedDict

import bson
from cassandra import ConsistencyLevel

from antares import log
from antares import utils
from antares.config import config
from antares.pipeline import ingestion
from antares.pipeline.ingestion import ztf
from antares.pipeline.unique_ids import LocusIDFactory
from antares.rtdb.antcassandra import execute, execute_concurrent_with_args, execute_async
from antares.rtdb.htmlut import HTMLUT
from antares.rtdb.models import LocusModel, AlertModel
from antares.rtdb.schema import (
    CCatalogHTMLUT,
    CHTMLUT,
    CLocus,
    CLocusByDay,
    CLocusByAlertID,
    CWatchList,
    CWatchObject,
)
from antares.services import metrics
from antares.services import slack
from antares.services.trace import tracer
from antares.utils import cone_search
from antares.utils import get_centroid, angle_between


class UnsupportedPropertyType(Exception):
    def __init__(self, name, cls):
        super(UnsupportedPropertyType, self).__init__(
            f"Property '{name}' given with unsupported type: {cls}."
        )


class CatalogObjectNotFound(Exception):
    pass


class CatalogIsDisabled(Exception):
    pass


class DuplicateLociDetected(Exception):
    def __init__(self, ra, dec):
        super().__init__()
        self.ra = ra
        self.dec = dec


class ZTFObjectIDOverlap(Exception):
    pass


class WatchListDict(TypedDict):
    wl_id: int
    name: str
    description: str
    slack_channel: str


class WatchObjectDict(TypedDict):
    wl_id: int
    wo_id: int
    ra: float
    dec: float
    radius: float
    htm_level: int
    name: str


class RTDB:
    """
    This object is the API for accessing the RTDB outside of the rtdb module.

    Should be instantiated and discarded within the processing of a request
    or Alert. Should not be kept forever because the RegionCache will get large.
    """

    # Locus aggregation uses a specific level
    LOCUS_HTM_LEVEL = 16

    # Loci are indexed at ALL of the following levels
    LOCUS_INDEX_LEVELS = {4, 6, 8, 10, 12, 14, 16}

    # Objects may be indexed at exactly one of the following HTM levels,
    # depending on their size.
    WATCHED_OBJECT_INDEX_LEVELS = {4, 6, 8, 10, 12, 14, 16}
    CATALOG_OBJECT_INDEX_LEVELS = {4, 6, 8, 10, 12, 14, 16}

    # Not all alert sources (surveys) are indexed by time.
    # Upper limits are not. ZTF Candidate's are. etc.
    SURVEYS_TO_INDEX_BY_TIME = {
        ingestion.ZTF_CANDIDATE_SURVEY,
    }

    def __init__(self, catalog_config=None):
        self.cat_config = catalog_config or config.CATALOG_CONFIG
        self.locus_id_factory = LocusIDFactory()

    def get_or_create_locus(
        self, ra, dec, alert_mjd, ztf_object_id=None, check_duplicates=False
    ):
        """
        Get or create a Locus for an (ra/dec) location.

        The alert_mjd is required to create new Locus IDs.

        Example values of constants:

            LOCUS_SEARCH_RADIUS_DEGREES      = 5.0"
            LOCUS_AGGREGATION_RADIUS_DEGREES = 1.0"
            LOCUS_MERGE_RADIUS_DEGREES       = 0.1"

        Algorithm:

            Let `p` be the given (ra, dec) point.

            If ztf_object_id is not None, and there exist Loci within
            LOCUS_SEARCH_RADIUS_DEGREES of `p` which share the given value of
            ztf_object_id, then return the nearest such Locus.

            If there exist any Loci within LOCUS_AGGREGATION_RADIUS_DEGREES
            of `p`, then let `a` be the nearest such Locus.
            If no such Loci exist, then create a new Locus and return it.

            If there exist one or more Loci within LOCUS_MERGE_RADIUS_DEGREES
            of `a`, and check_duplicates is True, then raise DuplicateLociDetected.

            Return `a`.

        :param ra:
        :param dec:
        :param alert_mjd:
        :param ztf_object_id:
        :param check_duplicates:
        :return: LocusModel
        """
        locus = None
        loci = self.get_loci(ra=ra, dec=dec, radius=config.LOCUS_SEARCH_RADIUS_DEGREES)

        # Ignore all Loci which are marked "replaced_by" another Locus.
        loci = [l for l in loci if l.properties.get("replaced_by") is None]
        if len(loci) > 1:
            metrics.warn("multiple_nearby_loci")

        # Select nearest Locus within LOCUS_SEARCH_RADIUS_DEGREES
        # which shares the ztf_object_id.
        if ztf_object_id is not None:
            locus = utils.nearest(
                p1=(ra, dec),
                objs=[
                    l
                    for l in loci
                    if l.properties.get("ztf_object_id") == ztf_object_id
                ],
                key=lambda l: (l.ra, l.dec),
                max_angle=config.LOCUS_SEARCH_RADIUS_DEGREES,
            )

        # If that found nothing, then
        # select nearest Locus within LOCUS_AGGREGATION_RADIUS_DEGREES
        # regardless of ztf_object_id.
        if locus is None:
            locus = utils.nearest(
                p1=(ra, dec),
                objs=loci,
                key=lambda l: (l.ra, l.dec),
                max_angle=config.LOCUS_AGGREGATION_RADIUS_DEGREES,
            )

        if locus:
            # We have found a Locus.
            # Are there other Loci within merging range of this Locus?
            # If so we must detect them and raise DuplicateLociDetected.
            if check_duplicates:
                if len(loci) > 1:
                    p = (locus.ra, locus.dec)
                    r = config.LOCUS_MERGE_RADIUS_DEGREES
                    duplicates = []
                    for l in loci:
                        log.debug("Found potential duplicate Loci")
                        angle = utils.angle_between(p, (l.ra, l.dec))
                        if angle < r:
                            log.debug(f"Found duplicate Locus {l} at angle {angle}")
                            duplicates.append(l)
                    assert len(duplicates) > 0
                    if len(duplicates) > 1:
                        metrics.warn("duplicate_loci_detected")
                        raise DuplicateLociDetected(locus.ra, locus.dec)

            return locus
        else:
            return self._put_locus(ra, dec, alert_mjd)

    @staticmethod
    def get_locus_by_id(locus_id):
        statement = f"""
            SELECT
                locus_id,
                created_at,
                updated_at,
                ra,
                dec,
                props,
                tags,
                wl_ids,
                wo_ids
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocus.__table_name__}"
            WHERE "locus_id" = ?
            LIMIT 1
        """
        locus = execute(statement, values=(locus_id,)).one()
        if locus:
            return LocusModel(
                locus_id=locus["locus_id"],
                created_at=locus["created_at"],
                updated_at=locus["updated_at"],
                ra=locus["ra"],
                dec=locus["dec"],
                properties=bson.loads(locus["props"] or bson.dumps({})),
                tags=set(locus["tags"] or ()),
                wl_ids=set(locus["wl_ids"] or ()),
                wo_ids=set(locus["wo_ids"] or ()),
            )

    def get_all_locus_ids(self):
        """
        Return ALL locus_ids. USE ONLY IN TESTS.
        """
        return list(set(l.locus_id for l in CLocus.filter()))

    def get_all_loci(self):
        """
        Yield ALL Loci. USE ONLY IN TESTS.
        """
        for locus_id in self.get_all_locus_ids():
            yield self.get_locus_by_id(locus_id)

    @staticmethod
    def get_loci(ra: float, dec: float, radius: float) -> List[LocusModel]:
        """
        Get all loci which are in the HTM lookup for a region.

        :return: list of LocusModel (may be empty)
        """
        # Fetch all the matching locus IDs for this coordinate.
        statement = f"""
            SELECT value
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CHTMLUT.__table_name__}"
            WHERE "htm" = ? AND "type" = {HTMLUT.LOCUS_TYPE}
        """
        htm_ids = cone_search.get_region_denormalized(
            ra, dec, radius, level=RTDB.LOCUS_HTM_LEVEL
        )
        results = execute_concurrent_with_args(
            statement, zip(htm_ids), raise_on_first_error=True
        )
        locus_ids = []
        for _, rows in results:
            for row in rows:
                locus_ids.append(row["value"])

        # Fetch all the matching loci from their HTM IDs.
        loci = []
        statement = f"""
            SELECT
                locus_id,
                created_at,
                updated_at,
                ra,
                dec,
                props,
                tags,
                wl_ids,
                wo_ids
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocus.__table_name__}"
            WHERE "locus_id" = ?
            LIMIT 1
        """
        results = execute_concurrent_with_args(
            statement, zip(locus_ids), raise_on_first_error=True
        )
        for (_, result) in results:
            if result:
                loci.append(result.one())

        return [
            LocusModel(
                locus_id=locus["locus_id"],
                created_at=locus["created_at"],
                updated_at=locus["updated_at"],
                ra=locus["ra"],
                dec=locus["dec"],
                properties=bson.loads(locus["props"] or bson.dumps({})),
                tags=set(locus["tags"] or ()),
                wl_ids=set(locus["wl_ids"] or ()),
                wo_ids=set(locus["wo_ids"] or ()),
            )
            for locus in loci
            if angle_between((ra, dec), (locus["ra"], locus["dec"])) < radius
        ]

    def _get_locus_htms_at_all_levels(self, ra: float, dec: float) -> Iterator[int]:
        for level in self.LOCUS_INDEX_LEVELS:
            yield cone_search.get_htm(ra, dec, level)

    def _put_locus(self, ra, dec, alert_mjd):
        # Generate a new locus_id
        def exists(l):
            return RTDB.get_locus_by_id(l) is not None

        locus_id = self.locus_id_factory.get(alert_mjd, exists)

        # Store the Locus object
        locus = CLocus(locus_id=locus_id, ra=ra, dec=dec)
        locus.save()

        # Index at all levels
        for htm in self._get_locus_htms_at_all_levels(ra, dec):
            HTMLUT.put_locus(htm, locus.locus_id)

        return LocusModel.from_clocus(locus)

    def remove_locus_from_htmlut(self, locus_id, ra, dec):
        """
        Remove a locus from the HTMLUT.

        :param locus_id:
        :param ra:
        :param dec:
        """
        htms = set(self._get_locus_htms_at_all_levels(ra, dec))
        for htm in htms:
            HTMLUT.delete_locus(htm, locus_id)

    def move_locus(self, locus_id, ra1, dec1, ra2, dec2):
        """
        Move locus from old position (ra1, dec1) to new (ra2, dec2).

        :param locus_id:
        :param ra1:
        :param dec1:
        :param ra2:
        :param dec2:
        """
        htms1 = set(self._get_locus_htms_at_all_levels(ra1, dec1))
        htms2 = set(self._get_locus_htms_at_all_levels(ra2, dec2))
        htm_to_delete = htms1 - htms2
        htm_to_add = htms2 - htms1
        for htm in htm_to_delete:
            HTMLUT.delete_locus(htm, locus_id)
        for htm in htm_to_add:
            HTMLUT.put_locus(htm, locus_id)
        CLocus.filter(locus_id=locus_id).update(ra=ra2, dec=dec2)

    @staticmethod
    def update_locus(locus):
        assert locus.locus_id
        CLocus.filter(locus_id=locus.locus_id).update(
            updated_at=locus.updated_at,
            props=bson.dumps(locus.properties or {}),
            tags=locus.tags,
            wl_ids=locus.wl_ids,
            wo_ids=locus.wo_ids,
            lightcurve=locus.lightcurve,
        )

    def merge_duplicate_loci(self, ra, dec):
        """
        Find and merge together all duplicate Loci within a region.

        This function should be called if `get_locus(ra, dec)` raised a
        `MultipleNearbyLoci` error. The ra/dec coordinates on the error object
        should be passed into this repair function, eg::

            try:
                locus = rtdb.get_locus(ra, dec, ...)
            except DuplicateLociDetected as e:
                locus, removed = rtdb.repair_duplicate_loci(e.ra, e.dec)

        The repair algorithm in this function is described below.
        Algorithm::

            Let `p` be the given ra/dec point.
            Let `r` be the radius of merging.

            Determine `S`, the set of all Loci within `r` degrees of point `p`.
            Determine `o`, the oldest Locus in `S`.
            Determine `L`, the set of all Loci in `S` which are not `o`.

            For each Locus `l` in `L`:
                Copy any alerts from `l` onto `o` which are not already on `o`.
                Mark `l` with property: `replaced_by` = `o`.`locus_id`.

            Update the centroid position of `o`.
            Return a tuple (`o`, `L`).

        :param ra:
        :param dec:
        :return: the kept Locus, and the list of removed Loci
        """
        loci = self.get_loci(ra=ra, dec=dec, radius=config.LOCUS_MERGE_RADIUS_DEGREES)
        assert len(loci)
        r = config.LOCUS_MERGE_RADIUS_DEGREES
        duplicates = [
            l for l in loci if utils.angle_between((ra, dec), (l.ra, l.dec)) < r
        ]
        del r
        assert duplicates
        if len(duplicates) == 1:
            # There is only one Locus within range.
            # So, no merging is needed.
            return duplicates[0], []
        assert len(duplicates) > 1
        metrics.warn("attempting_locus_merge")

        # All the loci in `duplicates` should have the same ztf_object_id.
        ztf_object_ids = list(
            set(filter(bool, (l.properties.get("ztf_object_id") for l in duplicates)))
        )
        if len(ztf_object_ids) > 1:
            metrics.warn("multiple_ztf_object_ids_among_duplicates")
            raise ZTFObjectIDOverlap

        # Determine which of the `duplicates` loci to keep and which to remove.
        oldest_locus = None
        for l in duplicates:
            if oldest_locus is None or l.created_at < oldest_locus.created_at:
                oldest_locus = l
        assert oldest_locus is not None
        # We now have found the locus we're going to keep.
        # Other Loci will be marked as replaced by this one.
        # Determine list of Loci to remove/replace:
        remove = [l for l in duplicates if l.locus_id != oldest_locus.locus_id]

        # Now we copy Alerts from the replaced Loci onto the one we're keeping.
        oldest_locus_alert_ids = set(
            a.alert_id for a in self.get_alerts(oldest_locus.locus_id)
        )
        alerts_to_copy = []  # Alerts to copy onto the `oldest_locus`.
        for l in remove:
            for a in self.get_alerts(l.locus_id):
                if a.alert_id not in oldest_locus_alert_ids:
                    alerts_to_copy.append(a)
                    oldest_locus_alert_ids.add(a.alert_id)
        for a in alerts_to_copy:
            a.locus_id = oldest_locus.locus_id
        if alerts_to_copy:
            self.put_alerts(alerts_to_copy)  # Store Alerts on the `oldest_locus`
        # Mark the replaced Loci as "replaced_by" the surviving Locus.
        for l in remove:
            l.properties["replaced_by"] = oldest_locus.locus_id
            # Store the "replaced_by" property in C*
            self.update_locus(l)

        # The Locus may have more Alerts now than before the merge.
        # Update its position:
        ra2, dec2 = get_centroid(
            [
                (a.properties["ant_ra"], a.properties["ant_dec"])
                for a in self.get_alerts(oldest_locus.locus_id)
                if ingestion.is_ztf_candidate(a.alert_id)
            ]
        )
        self.move_locus(
            oldest_locus.locus_id, oldest_locus.ra, oldest_locus.dec, ra2, dec2
        )

        # The oldest Locus has been modified. Re-load it from C*.
        locus = self.get_locus_by_id(oldest_locus.locus_id)
        metrics.warn("merged_loci_ok")
        return locus, remove

    @staticmethod
    def get_lightcurve(locus_id):
        """
        Return a CSV `lightcurve` blob for a Locus.

        :param locus_id:
        :return: CSV str
        """
        statement = f"""
            SELECT lightcurve
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocus.__table_name__}"
            WHERE "locus_id" = ?
            LIMIT 1
        """
        locus = execute(statement, values=(locus_id,)).one()
        if locus:
            return locus["lightcurve"]

    @staticmethod
    def get_alerts(locus_id):
        statement = f"""
            SELECT
                "locus_id",
                "alert_mjd",
                "alert_id",
                "alert_processed_at",
                "alert_props"
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocus.__table_name__}"
            WHERE "locus_id" = ?
            ORDER BY "alert_mjd"
        """
        rows = list(execute(statement, values=(locus_id,)))

        # Create Alert objects
        alerts = []
        for row in rows:
            if row["alert_mjd"] is None:
                continue  # This is a Locus without Alerts
            assert row["locus_id"]
            assert row["alert_mjd"]
            assert row["alert_id"]
            assert row["alert_processed_at"]
            assert row["alert_props"]
            alerts.append(
                AlertModel(
                    locus_id=row["locus_id"],
                    mjd=row["alert_mjd"],
                    alert_id=row["alert_id"],
                    processed_at=row["alert_processed_at"],
                    properties=bson.loads(row["alert_props"]),
                )
            )

        # Assert that `alerts` are in increasing time sequence
        for i in range(len(alerts) - 1):
            assert alerts[i].mjd <= alerts[i + 1].mjd

        return alerts

    def get_all_alerts(self):
        """
        Yield ALL alerts. USE ONLY IN TESTS.
        """
        for locus_id in self.get_all_locus_ids():
            for a in self.get_alerts(locus_id):
                yield a

    @staticmethod
    def get_alert(alert_id):
        """
        Get Alert by alert_id

        :param alert_id: str
        :return: `antares.rtdb.models.Alert`
        """
        locus_id, mjd = CLocusByAlertID.get(alert_id)
        if locus_id:
            a = CLocus.filter(
                locus_id=locus_id, alert_mjd=mjd, alert_id=alert_id
            ).first()
            if a:
                return AlertModel.from_clocus(a)

    @classmethod
    def get_alert_by_ztf_candid(cls, candid):
        """
        Get Alert by ZTF candidate_id aka candid.

        :param candid: ZTF candid as either str or int.
        :return: `antares.rtdb.models.Alert`
        """
        alert_id = ztf.ZTF_CANDIDATE_FORMAT.format(candid=candid)
        return cls.get_alert(alert_id)

    @classmethod
    def put_alerts(cls, alerts):
        """
        Store Alerts to Cassandra.

        :param alerts: list of `antares.rtdb.models.Alert`
        """
        # All alerts should have the same locus_id, which must not be empty
        assert alerts[0].locus_id
        assert len(set(a.locus_id for a in alerts)) == 1
        locus_id = alerts[0].locus_id

        timestamp = datetime.datetime.utcnow()
        statement = f"""
            INSERT INTO "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocus.__table_name__}"
            ("locus_id", "created_at", "alert_mjd", "alert_id", "alert_processed_at", "alert_props")
            VALUES (?, ?, ?, ?, ?, ?)
        """
        parameters = []
        for alert in alerts:
            log.debug(alert.as_dict())
            assert alert.alert_id
            assert alert.mjd is not None
            assert alert.processed_at
            assert alert.properties
            parameters.append(
                (
                    locus_id,
                    timestamp,
                    alert.mjd,
                    alert.alert_id,
                    alert.processed_at,
                    bson.dumps(alert.properties or {}),
                )
            )
        execute_concurrent_with_args(statement, parameters, raise_on_first_error=True)

        # TODO: Write test coverage for this.
        # statement = f"""
        #     INSERT INTO "{config.CASSANDRA_ALERT_KEYSPACE}"."{CLocusByDay.__table_name__}"
        #     ("day", "hh_mm", "locus_id") VALUES (?, ?, ?)
        # """
        # parameters = []
        for alert in alerts:
            if ingestion.get_survey(alert.alert_id) in cls.SURVEYS_TO_INDEX_BY_TIME:
                CLocusByDay.put(alert.mjd, alert.locus_id)

        # TODO: Write test coverage for this.
        # Create CLocusByAlertID rows
        for alert in alerts:
            CLocusByAlertID.put(alert.alert_id, locus_id, alert.mjd)

    def put_catalog_object(self, cat_id, obj_id, ra, dec, radius=None):
        """
        USE IN TESTING ONLY.

        :param cat_id:
        :param obj_id:
        :param ra:
        :param dec:
        :param radius:
        :return:
        """
        assert config.ENVIRONMENT == "dev"

        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        t = self.cat_config.by_id(cat_id)
        table = t["table"]
        ra_column = t["ra_column"]
        dec_column = t["dec_column"]
        id_column = t["object_id_column"]
        id_type = t["object_id_type"]
        if id_type == "int":
            obj_id = int(obj_id)
        elif id_type == "str":
            obj_id = str(obj_id)
        else:
            raise RuntimeError(f"Unknown object_id_type: {id_type}")

        columns = [id_column, ra_column, dec_column]
        values = [obj_id, ra, dec]

        if radius is not None:
            assert t["radius_column"]
            columns.append(t["radius_column"])
            values.append(radius)

        columns = ", ".join(str(c) for c in columns)
        placeholders = ", ".join("?" for _ in values)
        query = f"""
            INSERT INTO "{keyspace}"."{table}" ({columns})
            VALUES ({placeholders})
        """
        log.debug(query)
        execute(query, values=values)

    def put_catalog_object_htmlut(
        self, cat_id: int, obj_id: int, ra: float, dec: float, radius: float
    ) -> int:
        log.debug(
            "put_catalog_object_htmlut(%s, %s, %s, %s, %s)",
            cat_id,
            obj_id,
            ra,
            dec,
            radius,
        )
        ra, dec, radius = float(ra), float(dec), float(radius)
        htm_level = cone_search.determine_optimal_htm_level(
            radius=radius, choices=self.CATALOG_OBJECT_INDEX_LEVELS
        )
        htms = list(cone_search.get_region_denormalized(ra, dec, radius, htm_level))
        n = len(htms)
        if n > 50:  # Sanity check
            log.warn(
                "get_region_denormalized(%s, %s, %s, %s)", ra, dec, radius, htm_level
            )
            raise RuntimeError(f"Computed {n} HTMs in region. Bug?")
        for htm in htms:
            HTMLUT.put_cat_obj(htm, cat_id, obj_id)
        return n

    def get_catalog_object(self, catalog_id, object_id):
        """
        Load a catalog object from the Catalog DB.

        :param catalog_id:
        :param object_id:
        :return: dict
        """
        assert isinstance(catalog_id, int), catalog_id
        assert isinstance(object_id, (int, str)), object_id
        table_config = self.cat_config.by_id(catalog_id)
        if table_config is None:
            log.warn(f"Unknown catalog id: {catalog_id}")
            metrics.warn("catalog_id_not_found")
            raise CatalogIsDisabled
        keyspace = config.CASSANDRA_CATALOG_KEYSPACE
        table_name = table_config["table"]
        object_id_column = table_config["object_id_column"]
        object_id_type = table_config["object_id_type"]
        if object_id_type == "int":
            object_id = int(object_id)
        elif object_id_type == "str":
            object_id = str(object_id)
        else:
            raise RuntimeError(f"Unknown object_id_type: {object_id_type}")
        result = list(
            execute(
                f"""
                    SELECT * FROM "{keyspace}"."{table_name}"
                    WHERE "{object_id_column}" = ?
                """,
                values=(object_id,),
                consistency_level=ConsistencyLevel.ONE,
            )
        )
        assert len(result) < 2
        if len(result) == 1:
            return result[0]
        metrics.warn("catalog_object_not_found")
        raise CatalogObjectNotFound

    def get_catalogs(self) -> List[dict]:
        """
        Returns a description of all registered catalogs.
        """
        return self.cat_config.tables

    def get_catalog(self, catalog_id: int) -> dict:
        """
        Returns a description of a catalogs.
        """
        return next(
            (t for t in self.cat_config.tables if t["catalog_id"] == catalog_id), None
        )

    def get_sample_catalog_data(self, n=1):
        """
        Return `n` arbitrary rows from each catalog.

        Return value is mutated to be JSON-ready:
        Decimal is converted to float.
        Datetime is converted to ISO-8601 string.

        :param n: number of rows to fetch per catalog
        :return: dict
        """
        result = {}
        n = int(n)
        assert 1 <= n <= 1000
        for table in self.cat_config.tables:
            table_name = table["table"]
            keyspace = config.CASSANDRA_CATALOG_KEYSPACE
            rows = list(
                execute(
                    f"""
                    SELECT * FROM "{keyspace}"."{table_name}"
                    LIMIT ?
                """,
                    values=(n,),
                )
            )
            for row in rows:
                for key in list(row.keys()):
                    if isinstance(row[key], datetime.datetime):
                        row[key] = utils.format_dt(row[key])
                    elif isinstance(row[key], Decimal):
                        row[key] = float(row[key])
            result[table_name] = rows
        return result

    @tracer.wrap("antares.rtdb.api._get_catalog_matches")
    def _get_catalog_matches(self, ra: float, dec: float) -> Set[Tuple[int, int]]:
        """
        Get catalog matches as (cat_id, obj_id) tuples.

        Does not check catalog object radius.

        :param ra:
        :param dec:
        :return: list of (cat_id, obj_id) tuples
        """
        # Fetch all the matching (catalog ID, object ID)s for this coordinate.
        futures = []
        statement = f"""
            SELECT value
            FROM "{config.CASSANDRA_CATALOG_KEYSPACE}"."{CCatalogHTMLUT.__table_name__}"
            WHERE "htm" = ?
        """
        for level in self.CATALOG_OBJECT_INDEX_LEVELS:
            htm_id = cone_search.get_htm(ra, dec, level)
            futures.append(execute_async(statement, (htm_id,)))
        catalog_matches: Set[Tuple[int, int]] = set()
        for future in futures:
            for row in future.result():
                catalog_id, _, object_id = row["value"].partition(":")
                catalog_id = int(catalog_id)
                catalog_matches.add((catalog_id, object_id))
        return catalog_matches

    def get_catalog_objects(self, ra, dec):
        """
        Get catalog objects.

        Checks catalog object radius.

        :param ra: Locus ra
        :param dec: Locus dec
        :return: catalog_objects dict
        """
        catalog_objects = {}
        with metrics.timed("rtdb.get_catalog_matches"):
            matches = self._get_catalog_matches(ra, dec)
        for (cat_id, obj_id) in matches:
            log.debug("Found potential catalog hit %s %s", cat_id, obj_id)
            try:
                cat_obj = self.get_catalog_object(cat_id, obj_id)
            except CatalogIsDisabled:
                continue
            except CatalogObjectNotFound:
                # There seems to be a bad HTMLUT entry.
                metrics.warn("catalog_object_not_found")
                continue

            log.debug(cat_obj)
            cat_config = self.cat_config.by_id(cat_id)
            table = cat_config["table"]

            # determine radius
            radius = cat_config["radius"]  # Might be None
            radius_column = cat_config["radius_column"]
            if radius_column:
                radius_unit = cat_config["radius_unit"]
                assert radius_unit in {"degree", "arcsecond"}
                if cat_obj[radius_column] is not None:
                    if radius_unit == "degree":
                        radius = cat_obj[radius_column]
                    elif radius_unit == "arcsecond":
                        radius = cat_obj[radius_column] / 3600  # arcsec --> degree
                    else:
                        raise RuntimeError
            assert radius is not None

            obj_ra = cat_obj[cat_config["ra_column"]]
            obj_dec = cat_obj[cat_config["dec_column"]]
            log.debug("search coords (%s, %s)", ra, dec)
            log.debug("object coords (%s, %s)", obj_ra, obj_dec)
            log.debug("object radius %s", radius)

            # check the radius
            angle = utils.angle_between((ra, dec), (obj_ra, obj_dec))
            log.debug("angular distance %s", angle)
            if angle <= radius:
                log.debug("Confirmed catalog object hit")
                catalog_objects.setdefault(table, []).append(cat_obj)
            else:
                log.debug("Rejected catalog object hit")
                if angle > radius * 3:
                    # The distance to the object is significantly larger than
                    # the object's radius. This might have been caused by a
                    # bad HTMLUT entry.
                    # TODO: If we confirm that there are bad HTMLUT entries,
                    # then we can log these and run a cleanup of the bad catalog
                    # objects. See: antares/rtdb/repair_catalog_lut.py
                    metrics.warn("catalog_object_far_away")

        return catalog_objects

    @staticmethod
    def list_watch_objects():
        """
        return list of all watched_object_ids in RTDB.

        This does not scale to production.
        Use this only in tests.

        :return: list of ints
        """
        assert config.ENVIRONMENT == "dev"
        return [wo.wo_id for wo in CWatchObject.all()]

    @staticmethod
    def put_watch_list(wl_id, name, description, slack_channel):
        CWatchList(
            wl_id=wl_id,
            name=name,
            description=description,
            slack_channel=slack_channel,
        ).save()

    @staticmethod
    def get_watch_list(wl_id: int) -> WatchListDict:
        return CWatchList.get(wl_id=wl_id).as_dict()

    def delete_watch_list(self, wl_id):
        """
        Delete a WatchList, its WatchObjects, and their LUT entries.

        :param wl_id:
        :return:
        """
        for wo in self.get_watch_objects(wl_id):
            self.delete_watch_object(wl_id=wl_id, wo_id=wo["wo_id"])
        CWatchList.filter(wl_id=wl_id).delete()

    def get_watch_objects(self, wl_id: int) -> Iterator[WatchObjectDict]:
        for row in list(CWatchObject.filter(wl_id=wl_id).all()):
            yield row.as_dict()

    def get_watch_object(self, wl_id: int, wo_id: int) -> WatchObjectDict:
        row = CWatchObject.filter(wl_id=wl_id, wo_id=wo_id).first()
        if row:
            return row.as_dict()

    def put_watch_object(
        self, wl_id: int, wo_id: int, ra: float, dec: float, radius: float, name: str
    ) -> int:
        """Store a WatchObject and populate the LUT for it."""
        htm_level = cone_search.determine_optimal_htm_level(
            radius=radius, choices=self.WATCHED_OBJECT_INDEX_LEVELS
        )

        # Store WatchObject
        CWatchObject(
            wl_id=wl_id,
            wo_id=wo_id,
            ra=ra,
            dec=dec,
            radius=radius,
            htm_level=htm_level,
            name=name,
        ).save()

        # Store LUT entries
        n = 0
        for htm in cone_search.get_region_denormalized(ra, dec, radius, htm_level):
            HTMLUT.put_watch_obj(htm=htm, wo_id=wo_id, wl_id=wl_id)
            n += 1

        return n

    def delete_watch_object(self, wl_id: int, wo_id: int) -> None:
        """
        Delete a WatchObject and its LUT entires.

        :param wl_id:
        :param wo_id:
        :return:
        """
        wo = CWatchObject.filter(wl_id=wl_id, wo_id=wo_id).get()
        assert wo
        for htm in cone_search.get_region_denormalized(
            wo.ra, wo.dec, wo.radius, wo.htm_level
        ):
            HTMLUT.delete_watch_obj(htm=htm, wl_id=wl_id, wo_id=wo_id)
        CWatchObject.filter(wl_id=wl_id, wo_id=wo_id).delete()

    def search_watch_objects(self, ra: float, dec: float) -> List[Tuple[int, int]]:
        """
        Search the WatchObject LUT.

        :return: list of (wl_id, wo_id) tuples
        """
        # Fetch all the matching (wl_id, wo_id)s for this coordinate.
        futures = []
        statement = f"""
            SELECT type, value
            FROM "{config.CASSANDRA_ALERT_KEYSPACE}"."{CHTMLUT.__table_name__}"
            WHERE "htm" = ?
        """
        for level in self.CATALOG_OBJECT_INDEX_LEVELS:
            htm_id = cone_search.get_htm(ra, dec, level)
            futures.append(execute_async(statement, (htm_id,)))
        watch_object_candidates: (int, int) = []
        for future in futures:
            for row in future.result():
                if row["type"] == 2:  # TODO: Move this magic
                    watch_list_id, _, watch_object_id = row["value"].partition(":")
                    watch_list_id = int(watch_list_id)
                    watch_object_id = int(watch_object_id)
                    watch_object_candidates.append((watch_list_id, watch_object_id))

        watch_object_matches = []
        for (wl_id, wo_id) in watch_object_candidates:
            log.debug("found potential object hit %s %s", wl_id, wo_id)
            assert wl_id
            assert wo_id
            wo = CWatchObject.get(wl_id=wl_id, wo_id=wo_id)
            assert wo
            if None in (wo.ra, wo.dec, wo.radius):
                raise RuntimeError(f"wo is missing required fields {wo.as_dict()}")
            if utils.angle_between((ra, dec), (wo.ra, wo.dec)) <= wo.radius:
                log.debug("watched object hit confirmed")
                watch_object_matches.append((wl_id, wo_id))
            else:
                log.debug("watched object hit rejected")

        return watch_object_matches
