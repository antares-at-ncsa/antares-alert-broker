import Vuelidate from 'vuelidate';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import chai from 'chai';
import spies from 'chai-spies';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterCreate from '@/views/FilterCreate.vue';
import routes from '@/router';
import '@/plugins/bootstrap-vue';
import '@/plugins/fontawesome-vue';

const localVue = createLocalVue();
localVue.use(Vuelidate);
localVue.use(VueRouter);
localVue.use(Vuex);

chai.use(spies);

let store = new Vuex.Store({});
let router = new VueRouter(routes);

describe('FilterCreate.vue', () => {
  it('redirects to login if not authenticated', () => {
    const wrapper = shallowMount(FilterCreate, {
      store,
      router,
      localVue,
    });
  });
});
