/*
 * This currently holds two related sets of methods, and we should consider reorganizing.
 *
 * The first set of methods has to do with state management for interactive charts, such as holding
 * sets of predicates for chart groups.
 *
 * The second set of methods is a "utils"-style grabbag of things that interactive d3 charts tend
 * to require, such as normalization of field access, common scales, etc.
 */

import * as d3 from 'd3';
import * as d3Utils from './d3-utils';

//////////////////////////////////////////////////////////////////////////////
// interactive state management
//
// currently we do this from scratch, but we keep an eye towards
// crossfilter, dc, pothos, etc.

let chartGroups = {};

export function chartGroup(name)
{
  if (chartGroups[name]) {
    return chartGroups[name];
  }
  chartGroups[name] = newChartGroup(name);
  return chartGroups[name];
}

function newChartGroup(groupName)
{
  let charts = {};

  // TODO we should consider debouncing this method.
  function broadcastUpdate() {
    for (let key in charts) {
      charts[key].on["update"].call({ charts });
    }
  }

  function getChartObject(chartName) {
    if (charts[chartName]) {
      return charts[chartName];
    }
    charts[chartName] = {
      name: chartName,
      predicate: null,
      on: { update: function() {} }
    };
    return charts[chartName];
  }

  return {
    charts: charts,
    predicate(chartName, predicate) {
      getChartObject(chartName).predicate = predicate;
      broadcastUpdate();
    },
    on(chartName, event, k) {
      getChartObject(chartName).on[event] = k;
    }
  };
}

export function getSelectionPredicate(groupName, chartName)
{
  let group = chartGroup(groupName);

  return function(element) {
    for (let key in group.charts) {
      if (key !== chartName &&           // for chart predicates other than your own,
          group.charts[key].predicate && // and existing predicates,
          !group.charts[key].predicate(element)) { // return the conjunction of all tests
        return false;
      }
    }
    return true;
  };
}

//////////////////////////////////////////////////////////////////////////////
// common scales and conversions

let mjdEpoch = new Date(1858, 10, 17);
export function mjdToCal(mjdDays) {
  return new Date(mjdEpoch.getTime() + mjdDays * (1000 * 60 * 60 * 24));
}

export function passbandScale()
{
  return d3.scaleOrdinal()
    .domain(["g", "R"])
    .range([d3.lab(50, -60, -10), d3.lab(40, 60, 30)]);
}

export function rcidScale()
{
  return d3.scaleOrdinal()
    .range(d3.schemeCategory10);
}

export function alertTypeMarkScale()
{
  return function(t) {
    switch (t) {

    case "ulim":
    case "upper_limit": return "M -1 -1 L 1 -1 L 0  1 L -1 -1 Z";

    case "llim":
    case "lower_limit": return "M -1  1 L 1  1 L 0 -1 L -1  1 Z";

    case "ERROR_OTHER": return "M -2 -2 L 2 2 M -2 2 L 2 -2";

    case "mag":
    case "raw":
    case "calibrated": return "M -2 0 A 2 2 0 0 0 2 0 A 2 2 0 0 0 -2 0 Z";

    default:
      return "M -2 -2 L 2 2 M -2 2 L 2 -2";
    }
  };
}

//////////////////////////////////////////////////////////////////////////////
// normalized access to alert properties

export const alertTypes = [
  "mag", "ulim", "llim", "ERROR_OTHER"
];
