export const SET_USER = 'setUser';
export const PURGE_USER = 'purgeUser';
export const SET_USER_DATA = 'setUserData';
export const PURGE_USER_DATA = 'purgeUserData';
export const SET_AUTH = 'setAuth';
export const PURGE_AUTH = 'purgeAuth';
export const SET_LOCI = 'setLoci';
export const SET_LOCI_AGGREGATIONS = 'setLociAggregations';
export const SET_LOCI_COUNT = 'setLociCount';
export const SET_LOCI_QUERY = 'setLociSearchQuery';
export const SET_LOCUS = 'setLocus';
