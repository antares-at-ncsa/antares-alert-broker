import { MutationTree } from 'vuex';
import { LocusState } from './types';
import { ElasticsearchAggregation, Locus, RequestQuery } from '@/common/types';

import {
  SET_LOCI,
  SET_LOCI_AGGREGATIONS,
  SET_LOCI_COUNT,
  SET_LOCI_QUERY,
  SET_LOCUS,
} from '../mutations';

export const mutations: MutationTree<LocusState> = {
  [SET_LOCUS](state, locus: Locus) {
    state.locus = locus;
  },
  [SET_LOCI](state, loci: Locus[]) {
    state.loci = loci;
  },
  [SET_LOCI_AGGREGATIONS](state, aggregations: ElasticsearchAggregation) {
    state.lociAggregations = aggregations;
  },
  [SET_LOCI_COUNT](state, count: number) {
    state.lociCount = count;
  },
  [SET_LOCI_QUERY](state, query: RequestQuery) {
    state.lociQuery = query;
  },
};
