import { ElasticsearchAggregation, Locus, LocusListing, RequestQuery } from '@/common/types';

export interface LocusState {
  locus: Locus | null;
  loci: LocusListing | null;
  lociAggregations: ElasticsearchAggregation | null;
  lociCount: number;
  lociQuery: Partial<RequestQuery> | null;
}
