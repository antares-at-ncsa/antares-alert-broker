import { GetterTree } from 'vuex';
import { LocusState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<LocusState, RootState> = {
  locus(state) {
    return state.locus;
  },
  loci(state) {
    return state.loci;
  },
  lociAggregations(state) {
    return state.lociAggregations;
  },
  lociCount(state) {
    return state.lociCount;
  },
  lociQuery(state) {
    return state.lociQuery;
  },
};
