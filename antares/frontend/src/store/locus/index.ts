import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { LocusState } from './types';
import { RootState } from '../types';

export const state: LocusState = {
  locus: null,
  loci: null,
  lociAggregations: null,
  lociCount: 0,
  lociQuery: null,
};

const namespaced: boolean = false;

export const locus: Module<LocusState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
