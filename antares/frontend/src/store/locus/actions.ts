import { ActionTree } from 'vuex';

import { LocusService } from '@/api';
import { LocusState } from './types';
import { RootState } from '../types';
import { FETCH_LOCI, FETCH_LOCUS } from '../actions';
import {
  SET_LOCI,
  SET_LOCI_AGGREGATIONS,
  SET_LOCI_COUNT,
  SET_LOCI_QUERY,
  SET_LOCUS,
} from '../mutations';

export const actions: ActionTree<LocusState, RootState> = {
  async [FETCH_LOCUS]({ commit }, { locusId }) {
    const { data } = await LocusService.get(locusId);
    commit(SET_LOCUS, data);
  },
  async [FETCH_LOCI]({ commit }, { query = null }) {
    const { data, meta } = await LocusService.list(query);
    commit(SET_LOCI_QUERY, query);
    commit(SET_LOCI_COUNT, meta.count);
    commit(SET_LOCI_AGGREGATIONS, meta.aggregations);
    return commit(SET_LOCI, data);
  },
};
