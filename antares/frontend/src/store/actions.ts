export const CREATE_OR_UPDATE_LOCUS_ANNOTATION = 'createOrUpdateLocusAnnotation';
export const CHECK_AUTH = 'checkAuth';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const REGISTER = 'register';
export const CREATE_USER = 'createUser';
export const FETCH_LOCI = 'fetchLoci';
export const FETCH_USER = 'fetchUser';
export const FETCH_USER_DATA = 'fetchUserData';
export const FETCH_USERS = 'fetchUsers';
export const FETCH_LOCUS = 'fetchLocus';
