import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

import { RootState } from './types';
import { auth } from './auth';
import { locus } from './locus';
import { user } from './user';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence<RootState>({
  storage: window.localStorage,
  modules: ['auth', 'user'],
});

const store = new Vuex.Store<RootState>({
  state: {
    version: '0.1.0',
  },
  modules: {
    auth,
    locus,
    user,
  },
  plugins: [vuexLocal.plugin],
});

export default store;
