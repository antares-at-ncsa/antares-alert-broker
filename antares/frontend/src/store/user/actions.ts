import { ActionTree } from 'vuex';

import { LocusAnnotationService, UserService } from '@/api';
import { UserState } from './types';
import { RootState } from '../types';
import { FETCH_USER_DATA, CREATE_OR_UPDATE_LOCUS_ANNOTATION } from '../actions';
import { SET_USER, SET_USER_DATA } from '../mutations';

export const actions: ActionTree<UserState, RootState> = {
  async [FETCH_USER_DATA](context, { identity }) {
    const { data: user } = await UserService.get(identity);
    const locusAnnotations = await UserService.getLocusAnnotations(identity);
    context.commit(SET_USER, user);
    context.commit(SET_USER_DATA, { locusAnnotations });
  },

  /**
   * The CREATE_OR_UPDATE_LOCUS_ANNOTATION action is used to favorite/unfavorite and
   * comment a locus in such a way that the store stays in sync. Locus annotations are
   * stored in the MySQL database with some superset of the following columns:
   *
   *   - locus_annotation_id
   *   - locus_id
   *   - owner_id
   *   - favorited
   *   - comment
   *
   * Additionally there is a unique constraint on (locus_id, owner_id). We expose these
   * routes in the API for interacting with locus annotations:
   *
   *   - GET /users/<user_id>/locus_annotations
   *   - POST /users/<user_id>/locus_annotations
   *   - GET /locus_annotations/<locus_annotation_id>
   *   - PATCH /locus_annotations/<locus_annotation_id>
   *
   * We need to display, for a given search result set, if any of the loci returned are
   * already in a user's favorites. To do this we store a copy of all the users locus
   * annotations in the Vuex store (userData.locusAnnotations). The userData field
   * is populated when the FETCH_USER_DATA action is dispatched (see above).
   * userData.locusAnnotations is a map (string -> LocusAnnotation) that allows the
   * application to lookup a locus annotation given an ANTARES locus ID.
   *
   * This invariant must be met: the local userData.locusAnnotation map MUST maintain
   * coherency with the user's entries in the MySQL locus_annotations table. All
   * UI elements that favorite/unfavorite or change a comment of a locus annotation must
   * use the CREATE_OR_UPDATE_LOCUS_ANNOTATION action.
   */
  async [CREATE_OR_UPDATE_LOCUS_ANNOTATION](context, { locusId, locusAnnotation }) {
    const user = context.getters.user;
    let userData = context.getters.userData;
    if (locusId in userData.locusAnnotations) {
      const response = await LocusAnnotationService.update(
        userData.locusAnnotations[locusId].id,
        locusAnnotation,
      );
      userData.locusAnnotations[locusId] = response.data;
      context.commit(SET_USER_DATA, userData);
    } else {
      const response = await UserService.createLocusAnnotation(user.id, {
        favorited: false,
        comment: '',
        owner: { id: user.id },
        locus: { id: locusId },
        ...locusAnnotation,
      });
      userData.locusAnnotations[locusId] = response.data;
      context.commit(SET_USER_DATA, userData);
    }
  },
};
