import { MutationTree } from 'vuex';
import { AuthState } from './types';

import { PURGE_AUTH, SET_AUTH } from '../mutations';

export const mutations: MutationTree<AuthState> = {
  [SET_AUTH](state) {
    state.isAuthenticated = true;
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
  },
};
