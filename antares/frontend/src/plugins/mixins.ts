import Vue from 'vue';
import { julian } from 'astronomia';

// From https://stackoverflow.com/questions/17633462/format-a-javascript-number-with-a-metric-prefix-like-1-5k-1m-1g-etc
var ranges = [
  { divider: 1e18, suffix: 'E' },
  { divider: 1e15, suffix: 'P' },
  { divider: 1e12, suffix: 'T' },
  { divider: 1e9, suffix: 'G' },
  { divider: 1e6, suffix: 'M' },
  { divider: 1e3, suffix: 'k' },
];

Vue.mixin({
  methods: {
    formatNumber(n: number, fixed: number) {
      for (var i = 0; i < ranges.length; i++) {
        if (n >= ranges[i].divider) {
          return (n / ranges[i].divider).toFixed(fixed).toString() + ranges[i].suffix;
        }
      }
      return n.toString();
    },
    mjdToDate(mjd: number): Date {
      return julian.JDToDate(julian.JDToMJD(mjd));
    },
  },
});
