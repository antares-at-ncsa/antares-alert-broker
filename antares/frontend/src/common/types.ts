export interface ElasticsearchAggregation {}

export interface ElasticsearchFilter {
  type: string;
  field: string;
  value: any;
  text: string;
}

export interface JWTPayload {
  id: number;
}

export interface Filter {
  name: string;
  description: string;
  id: string;
  enabled: boolean;
  public: boolean;
  versions: FilterVersion[];
  latest_version: FilterVersion;
  enabled_version: FilterVersion;
  related: RelatedResources;
  priority: number;
  level: number;
}

export interface User {
  id: string;
  username: string;
  email: string;
  admin: boolean;
  staff: boolean;
}

export interface UserData {
  locusAnnotations: { [key: string]: LocusAnnotation };
}

export interface FilterVersion {
  id: string;
  code: string;
  comment: string;
  created_at: string;
}

export interface Locus {
  id: string;
  properties: { [key: string]: any };
}

export interface Alert {
  id: string;
  mjd: number;
  properties: { [key: string]: any };
}

export interface LocusProperty {
  id: string;
  type: string;
  origin: string;
  description: string;
  es_mapping: string;
}

export interface LocusListing {}

export interface RelatedResource {
  [key: string]: any;
}

export interface RelatedResources {
  [key: string]: RelatedResource;
}

export interface RequestQuery {
  page: {
    limit: number;
    offset: number;
  };
  sort: string;
  fields: { [key: string]: any };
  include: string;
  elasticsearch_query: any;
  filter: any;
}

export interface State {
  filter: Filter;
  filterEnabledVersion: FilterVersion;
  filterLatestVersion: FilterVersion;
  filterVersions: FilterVersion[];
  filters: Filter[];
  filtersCount: number;
  watchList: WatchList;
  watchLists: WatchList[];
  watchListsCount: number;
}
export interface WatchList {
  name: string;
  description: string;
  id: string;
  objects: WatchObject[];
  owner?: Partial<User> & Pick<User, 'id'>;
}

export interface WatchObject {
  comment: string;
  declination: number;
  radius: number;
  right_ascension: number;
}

export interface Tag {
  id: string;
  filter_version_id: number;
  description: string;
}

export interface CatalogSample {
  object_id: string;
  catalog_name: string;
  data: object;
}

export interface CatalogMatch {
  id: string;
  ra: number;
  dec: number;
  properties: object;
  meta: {
    catalog_id: string;
    catalog_name: string;
    separation: number;
  };
}

export interface LocusAnnotation {
  id: string;
  comment: string;
  favorited: boolean;
  owner: Partial<User>;
  locus: Partial<Locus>;
}
