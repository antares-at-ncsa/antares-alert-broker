import store from '@/store';
import { CHECK_AUTH } from '@/store/actions';
import Vue from 'vue';
import VueRouter, { Route } from 'vue-router';
import FavoriteLoci from '@/views/FavoriteLoci.vue';
import FilterCreate from '@/views/FilterCreate.vue';
import FilterDetail from '@/views/FilterDetail.vue';
import FilterList from '@/views/FilterList.vue';
import LocusDetail from '@/views/LocusDetail.vue';
import LocusList from '@/views/LocusList.vue';
import Pipeline from '@/views/Pipeline.vue';
import Login from '@/views/Login.vue';
import ForgotPassword from '@/views/ForgotPassword.vue';
import Register from '@/views/Register.vue';
import UserManagement from '@/views/UserManagement.vue';
import UserSettings from '@/views/UserSettings.vue';
import Properties from '@/views/Properties.vue';
import WatchListCreate from '@/views/WatchListCreate.vue';
import WatchListDetail from '@/views/WatchListDetail.vue';
import WatchListList from '@/views/WatchListList.vue';
import TagList from '@/views/TagList.vue';
import CatalogSampleList from '@/views/CatalogSampleList.vue';
import FAQ from '@/views/FAQ.vue';

Vue.use(VueRouter);

/**
 * The checkAuth route guard checks that a user's login session
 * is still valid before directing them to the requested page.
 * If they are not logged in, they will be allowed to continue.
 * If they are logged in but their session has expired, they will
 * be redirected to the login page.
 *
 * This guard differs from requireAuth in that if the user is not
 * logged in, in requireAuth, they will be redirected to the login
 * page and MUST login before being allowed to continue.
 *
 * checkAuth is useful for a page like the "Filters" page, where we
 * want to display some sort of "sign up or login" banner to folks
 * that were not logged in but probably don't want to do the same
 * for expired sessions.
 */
function checkAuth(to: Route, from: Route, next: any) {
  if (store.getters.isAuthenticated) {
    store.dispatch(CHECK_AUTH).then((authorized) => {
      if (!authorized) {
        next({ name: 'login', query: { reason: 'inactivity', redirect: to.fullPath } });
      } else {
        next();
      }
    });
  } else {
    next();
  }
}

/**
 * The requireAuth route guard requires that the user be logged in to
 * access the requested page. If they are not logged in, they
 * will be redirected to the login page. If they are logged in
 * but their session has expired, they will be redirected to the
 * login page.
 *
 * This guard differs from checkAuth in that if the user is not
 * logged in, in checkAuth, they will be allowed to continue to
 * the requested route.
 */
function requireAuth(to: Route, from: Route, next: any) {
  if (!store.getters.isAuthenticated) {
    next({ name: 'login', query: { redirect: to.fullPath } });
  } else {
    store.dispatch(CHECK_AUTH).then((authorized) => {
      if (!authorized) {
        next({ name: 'login', query: { reason: 'inactivity', redirect: to.fullPath } });
      } else {
        next();
      }
    });
  }
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      beforeEnter: (to, from, next) => next({ name: 'locusList' }),
    },
    {
      path: '/favorite-loci',
      name: 'favoriteLoci',
      component: FavoriteLoci,
      beforeEnter: checkAuth,
    },
    {
      path: '/filters',
      name: 'filterList',
      component: FilterList,
      beforeEnter: checkAuth,
    },
    {
      path: '/filters/create',
      name: 'filterCreate',
      component: FilterCreate,
      beforeEnter: requireAuth,
    },
    {
      path: '/filters/:filterId',
      name: 'filterDetail',
      component: FilterDetail,
      beforeEnter: requireAuth,
    },
    {
      path: '/loci/:locusId',
      name: 'locusDetail',
      component: LocusDetail,
    },
    {
      path: '/loci',
      name: 'locusList',
      component: LocusList,
    },
    {
      path: '/pipeline',
      name: 'pipeline',
      component: Pipeline,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/forgot-password',
      name: 'forgotPassword',
      component: ForgotPassword,
    },
    {
      path: '/properties',
      name: 'properties',
      component: Properties,
    },
    {
      path: '/watch-lists',
      name: 'watchListList',
      component: WatchListList,
      beforeEnter: checkAuth,
    },
    {
      path: '/watch-lists/create',
      name: 'watchListCreate',
      component: WatchListCreate,
      beforeEnter: requireAuth,
    },
    {
      path: '/watch-lists/:watchListId',
      name: 'watchListDetail',
      component: WatchListDetail,
      beforeEnter: requireAuth,
    },
    {
      path: '/user/settings',
      name: 'userSettings',
      component: UserSettings,
      beforeEnter: requireAuth,
    },
    {
      path: '/admin/user-management',
      name: 'userManagement',
      component: UserManagement,
      beforeEnter: requireAuth,
    },
    {
      path: '/tags',
      name: 'tagList',
      component: TagList,
    },
    {
      path: '/catalogs',
      name: 'catalogSampleList',
      component: CatalogSampleList,
    },
    {
      path: '/faq',
      name: 'faq',
      component: FAQ,
    },
  ],
});

export default router;
