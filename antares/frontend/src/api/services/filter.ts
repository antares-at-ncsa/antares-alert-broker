import { getClient } from '../client';
import { Filter, FilterVersion, RequestQuery } from '@/common/types';
import { authenticated, authenticatedOptional } from '@/api/services';
import store from '@/store';

interface FilterServiceDetailResponse {
  data: Filter;
  meta: { [key: string]: any } | null;
}

interface FilterServiceListResponse {
  data: Filter[];
  meta: { count: number };
}

interface FilterVersionServiceDetailResponse {
  data: FilterVersion;
  meta: { count: number };
}

interface FilterVersionServiceListResponse {
  data: FilterVersion[];
  meta: { count: number };
}

export default class {
  @authenticated()
  static async create(filter: Filter): Promise<FilterServiceDetailResponse> {
    const client = await getClient();
    return client.one('user', store.getters.user.id).all('filter').post(filter);
  }

  @authenticatedOptional()
  static async get(filterId: string, query: Partial<RequestQuery>): Promise<FilterServiceDetailResponse> {
    const client = await getClient();
    return client.find('filter', filterId, query);
  }

  @authenticatedOptional()
  static async getVersions(
    filterId: string,
    query: Partial<RequestQuery>,
  ): Promise<FilterVersionServiceListResponse> {
    const client = await getClient();
    return client.one('filter', filterId).all('filter_version').get(query);
  }

  @authenticatedOptional()
  static async getEnabledVersion(filterId: string): Promise<FilterServiceDetailResponse> {
    const client = await getClient();
    return client.one('filter', filterId).get({include: 'enabled_version'});
  }

  @authenticated()
  static async addVersion(
    filterId: string,
    filterVersion: Pick<FilterVersion, 'code' | 'comment'>,
  ): Promise<FilterVersionServiceDetailResponse> {
    const client = await getClient();
    return client.one('filter', filterId).all('filter_version').post(filterVersion);
  }

  @authenticatedOptional()
  static async list(query: Partial<RequestQuery>): Promise<FilterServiceListResponse> {
    const client = await getClient();
    return client.findAll('filter', query);
  }

  @authenticated()
  static async listUserFilters(query: Partial<RequestQuery>): Promise<FilterServiceListResponse> {
    const client = await getClient();
    return client.one('user', store.getters.user.id).all('filter').get(query);
  }

  @authenticated()
  static async makePublic(filterId: string) {
    const client = await getClient();
    return client.update('filter', {
      id: filterId,
      public: true,
    });
  }

  @authenticated()
  static async makePrivate(filterId: string) {
    const client = await getClient();
    return client.update('filter', {
      id: filterId,
      public: false,
    });
  }
}
