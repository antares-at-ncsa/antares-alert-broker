import axios from 'axios';
import { getCookieValue } from '..';
import { authenticated } from '@/api/services';

interface Credentials {
  username: string;
  password: string;
}

export default class {
  /**
   * checkAuth is meant to check that a user's authentication session is still valid.
   * By wrapping it in the @authenticated decorator, we handle the case that their
   * access token has expired but their refresh token is still valid. In that case we
   * try to get a new set of tokens from the server and transparently continue if
   * successful.
   *
   * The /auth endpoints of the API are not JSON:API compliant in terms of requests
   * and responses (except in their error responses). As a result, we use axios instead
   * of the devour client in this service. The @authenticated decorator expects thrown
   * errors to conform to how devour throws them so there's a bit of a hack in checkAuth
   * that handles massaging the axios error into something that looks like a devour one.
   */
  @authenticated()
  static async checkAuth() {
    try {
      const { data: config } = await axios.get('/config.json');
      return await axios.get(`${config.ANTARES_API_URL}/auth/check`);
    } catch (error) {
      throw error.response.data.errors;
    }
  }

  static async login(credentials: Credentials) {
    const { data: config } = await axios.get('/config.json');
    const response = await axios.post(
      `${config.ANTARES_API_URL}/auth/login`,
      credentials,
      {
        params: {
          type: 'cookie',
        },
      },
    );
    return response;
  }

  static async loginFresh(credentials: Credentials) {
    const { data: config } = await axios.get('/config.json');
    const response = await axios.post(
      `${config.ANTARES_API_URL}/auth/login-fresh`,
      credentials,
      {
        params: {
          type: 'cookie',
        },
      },
    );
    return response;
  }

  static async forgotPassword(username: string) {
    const { data: config } = await axios.get('/config.json');
    const response = await axios.post(`${config.ANTARES_API_URL}/auth/forgot`, {
      username,
    });
    return response;
  }

  static async resetPasswordWithToken(password: string, token: string) {
    const { data: config } = await axios.get('/config.json');
    const response = await axios.post(
      `${config.ANTARES_API_URL}/auth/reset`,
      { password },
      {
        headers: { Authorization: `Bearer ${token}` },
        params: {
          type: 'cookie',
        },
        withCredentials: false,
      },
    );
    return response;
  }

  static async resetPassword(password: string) {
    const { data: config } = await axios.get('/config.json');
    const response = await axios.post(
      `${config.ANTARES_API_URL}/auth/reset`,
      { password },
      {
        headers: { 'X-CSRF-TOKEN': getCookieValue('csrf_access_token') },
        params: {
          type: 'cookie',
        },
      },
    );
    return response;
  }

  static async refresh() {
    const { data: config } = await axios.get('/config.json');
    return axios.post(`${config.ANTARES_API_URL}/auth/refresh`, null, {
      headers: { 'X-CSRF-TOKEN': getCookieValue('csrf_refresh_token') },
      params: { type: 'cookie' },
    });
  }

  static async logout() {
    const { data: config } = await axios.get('/config.json');
    try {
      await axios.post(`${config.ANTARES_API_URL}/auth/logout`, null, {
        headers: { 'X-CSRF-TOKEN': getCookieValue('csrf_refresh_token') },
      });
    } catch (error) {
      if (error.response.status === 401) {
        // Accept unauthorized response for logout.
        // This likely means the refresh token is expired.
        return;
      }
      throw error;
    }
  }
}
