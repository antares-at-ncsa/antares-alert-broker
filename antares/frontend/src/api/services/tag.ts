import { getClient } from '../client';
import { RequestQuery, Tag } from '@/common/types';

interface TagServiceDetailResponse {
  data: Tag;
  meta: { [key: string]: any } | null;
}

interface TagServiceListResponse {
  data: Tag[];
  meta: { count: number };
}

export default {
  async get(name: string): Promise<TagServiceDetailResponse> {
    const client = await getClient();
    return client.find('tag', name);
  },
  async list(query: Partial<RequestQuery>): Promise<TagServiceListResponse> {
    const client = await getClient();
    return client.findAll('tag', query);
  },
};
