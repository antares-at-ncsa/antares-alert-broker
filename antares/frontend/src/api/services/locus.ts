import { RequestQuery } from '@/common/types';
import bodybuilder from 'bodybuilder';
import Papa from 'papaparse';
import { getClient } from '../client';

interface LightcurveAlert {
  ant_dec: number | string;
  ant_mag: number | string;
  ant_mag_corrected: number | string;
  ant_magerr: number | string;
  ant_magerr_corrected: number | string;
  ant_magulim_corrected: number | string;
  ant_magllim_corrected: number | string;
  ant_maglim: number | string;
  ant_mjd: number | string;
  ant_ra: number | string;
  frontend_mag_type: string;
  frontend_mag: number;
  frontend_magerr: number;
  frontend_magulim: number;
  frontend_magllim: number;
}

function parseLightcurve(lightcurve: string) {
  const parsedLightcurve = Papa.parse(lightcurve, { header: true, skipEmptyLines: true });
  if (parsedLightcurve.errors.length) {
    throw parsedLightcurve.errors;
  }
  parsedLightcurve.data.forEach((alert: LightcurveAlert) => {
    alert.ant_dec = parseFloat(<string>alert.ant_dec);
    alert.ant_mag = parseFloat(<string>alert.ant_mag);
    alert.ant_mag_corrected = parseFloat(<string>alert.ant_mag_corrected);
    alert.ant_magerr = parseFloat(<string>alert.ant_magerr);
    alert.ant_magerr_corrected = parseFloat(<string>alert.ant_magerr_corrected);
    alert.ant_magulim_corrected = parseFloat(<string>alert.ant_magulim_corrected);
    alert.ant_magllim_corrected = parseFloat(<string>alert.ant_magllim_corrected);
    alert.ant_maglim = parseFloat(<string>alert.ant_maglim);
    alert.ant_mjd = parseFloat(<string>alert.ant_mjd);
    alert.ant_ra = parseFloat(<string>alert.ant_ra);

    if (alert.ant_mag_corrected) {
      alert.frontend_mag_type = "calibrated";
      alert.frontend_mag = alert.ant_mag_corrected;
      alert.frontend_magerr = alert.ant_magerr_corrected;
      alert.frontend_magulim = alert.ant_magulim_corrected;
      alert.frontend_magllim = alert.ant_magllim_corrected;
    } else if (alert.ant_magulim_corrected) {
      alert.frontend_mag_type = "calibrated_maglim";
      alert.frontend_mag = NaN;
      alert.frontend_magerr = NaN;
      alert.frontend_magulim = alert.ant_magulim_corrected;
      alert.frontend_magllim = alert.ant_magllim_corrected;
    } else if (alert.ant_mag) {
      alert.frontend_mag_type = "raw";
      alert.frontend_mag = alert.ant_mag;
      alert.frontend_magerr = alert.ant_magerr;
      alert.frontend_magulim = NaN;
      alert.frontend_magllim = NaN;
    } else if (alert.ant_maglim) {
      alert.frontend_mag_type = "raw_maglim";
      alert.frontend_mag = NaN;
      alert.frontend_magerr = NaN;
      alert.frontend_magulim = alert.ant_maglim;
      alert.frontend_magllim = NaN;
    }
  });
  return parsedLightcurve.data;
}

export default {
  async get(locusId: string) {
    const client = await getClient();
    const response = await client.find('locus', locusId);
    response.data.lightcurve = parseLightcurve(response.data.lightcurve);
    return response;
  },
  async list(query: RequestQuery) {
    const client = await getClient();
    return client.findAll('locus', {
      ...query,
    });
  },
  async listAlerts(locusId: string, query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.one('locus', locusId).all('alert').get(query);
  },

  async listCatalogMatches(locusId: string, query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.one('locus', locusId).all('catalog-matches').get(query);
  },

  async lookupByZtfObjectId(locusId: string) {
    const query = bodybuilder().filter('term', 'properties.ztf_object_id', locusId);
    const client = await getClient();
    const { data } = await client.findAll('locus', {
      elasticsearch_query: { locus_listing: JSON.stringify(query.build()) },
    });
    if (!data.length) {
      return null;
    }
    return data[0].id;
  },
  async coneSearch(center: string, radius: number, query: RequestQuery) {
    const searchQuery = bodybuilder().filter('sky_distance', {
      distance: `${radius} degree`,
      htm16: {
        center: center,
      },
    });
    const client = await getClient();
    return client.findAll('locus', {
      elasticsearch_query: { locus_listing: JSON.stringify(searchQuery.build()) },
      ...query,
    });
  },
};
