import { getClient } from '../client';
import { LocusAnnotation } from '@/common/types';
import { authenticated } from '@/api/services';

interface LocusAnnotationServiceDetailResponse {
  data: LocusAnnotation;
  meta: { [key: string]: any } | null;
}

export default class {
  @authenticated()
  static async create(
    locusAnnotation: LocusAnnotation,
  ): Promise<LocusAnnotationServiceDetailResponse> {
    const client = await getClient();
    return client.create('locus_annotation', locusAnnotation);
  }

  @authenticated()
  static async update(locusAnnotationId: string, locusAnnotation: Partial<LocusAnnotation>) {
    const client = await getClient();
    return client.update('locus_annotation', { ...locusAnnotation, id: locusAnnotationId });
  }
}
