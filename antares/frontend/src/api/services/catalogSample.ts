import { getClient } from '../client';
import { CatalogSample, RequestQuery } from '@/common/types';

interface CatalogSampleServiceListResponse {
  data: CatalogSample[];
  meta: { count: number };
}

export default {
  async list(query: Partial<RequestQuery>): Promise<CatalogSampleServiceListResponse> {
    const client = await getClient();
    return client.findAll('catalog_sample', query);
  },
};
