import { AuthService } from '../';
import store from '@/store';
import { LOGOUT } from '@/store/actions';

/**
 * authenticated is a function decorator that handles fetching a new access/refresh
 * token set in the event that an authenticated request is met with a 401 due to
 * an expired token.
 */
export function authenticated() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      if (!store.getters.isAuthenticated) {
        throw `Must be authenticated to call ${propertyKey}`;
      }
      try {
        var result = await method.apply(this, args);
      } catch (errors) {
        try {
          if (errors[0].status === 401 && errors[0].detail.includes('expire')) {
            await AuthService.refresh();
            return method.apply(this, args);
          }
        } catch (error) {
          if (error.response.status === 401) {
            await store.dispatch(LOGOUT);
          }
          throw errors;
        }
        throw errors;
      }
      return result;
    };
    return descriptor;
  };
}

/**
 * authenticatedOptional is a function decorator that handles fetching a new access/refresh
 * token set in the event that an authenticated request is met with a 401 due to
 * an expired token. If a refresh token can't be obtained it will log the user out but
 * call the requested route anyways. If the user is not logged in it will call the
 * requested route as normal.
 */
export function authenticatedOptional() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      if (!store.getters.isAuthenticated) {
        return method.apply(this, args);
      }
      try {
        var result = await method.apply(this, args);
      } catch (errors) {
        try {
          if (errors[0].status === 401 && errors[0].detail.includes('expire')) {
            await AuthService.refresh();
            return method.apply(this, args);
          }
        } catch (error) {
          if (error.response.status === 401) {
            await store.dispatch(LOGOUT);
          }
          return method.apply(this, args);
        }
        throw errors;
      }
      return result;
    };
    return descriptor;
  };
}
