import { getClient } from '../client';
import { RequestQuery } from '@/common/types';

export default {
  async get(name: string) {
    const client = await getClient();
    return client.find('alert_property', name);
  },
  async list(query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.findAll('alert_property', query);
  },
};
