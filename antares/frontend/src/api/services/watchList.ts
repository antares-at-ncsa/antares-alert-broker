import { getClient } from '../client';
import { RequestQuery, WatchList } from '@/common/types';
import { authenticated } from '@/api/services';
import store from '@/store';

interface WatchListServiceDetailResponse {
  data: WatchList;
  meta: { [key: string]: any } | null;
}

interface WatchListServiceListResponse {
  data: WatchList[];
  meta: { count: number };
}

export default class {
  @authenticated()
  static async create(watchList: WatchList): Promise<WatchListServiceDetailResponse> {
    const client = await getClient();
    return client.one('user', store.getters.user.id).all('watch_list').post(watchList);
  }

  @authenticated()
  static async get(watchListId: string): Promise<WatchListServiceDetailResponse> {
    const client = await getClient();
    return client.find('watch_list', watchListId);
  }

  @authenticated()
  static async delete(watchListId: string): Promise<WatchListServiceDetailResponse> {
    const client = await getClient();
    return client.destroy('watch_list', watchListId);
  }

  @authenticated()
  static async list(query: Partial<RequestQuery>): Promise<WatchListServiceListResponse> {
    const client = await getClient();
    return client.one('user', store.getters.user.id).all('watch_list').get(query);
  }
}
