export interface Payload {
  jsonApi: {
    deserialize: {
      cache: {
        clear(): void;
      };
      collection: {
        call(jsonApi: object, data: object, included: object): Resource[];
      };
      resource: {
        call(jsonApi: object, data: object, included: object): Resource;
      };
    };
    models: {
      [key: string]: {
        attributes: { [key: string]: any };
      };
    };
  };
  res: {
    config: {
      type: string;
    };
    data: {
      data: {
        type: string;
        meta: object;
        links: object;
      };
      errors: object;
      included: object;
      links: object;
      meta: object;
    };
    status: number;
  };
  req: {
    method: string;
  };
}

export interface Resource {
  [key: string]: any;
  meta: object;
  links: object;
  related: { [key: string]: any };
}
