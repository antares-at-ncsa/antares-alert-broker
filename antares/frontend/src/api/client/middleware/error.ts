interface ServerErrors {
  errors: Error[];
  error: string;
}

interface Error {
  source: any;
  title: string;
  detail: string;
  status: string;
}

interface ErrorBuilderOptions {
  errorBuilder(error: Error): object;
}

function defaultErrorBuilder(error: Error) {
  const { title, detail, status } = error;
  return { title, detail, status };
}

function errorKey(index: number, source: any) {
  if (!source || source.pointer == null) {
    return index;
  }
  return source.pointer.split('/').pop();
}

function getBuildErrors(options: ErrorBuilderOptions | null) {
  return function buildErrors(serverErrors: Partial<ServerErrors>) {
    const errorBuilder = (options && options.errorBuilder) || defaultErrorBuilder;
    const errors: any = {};
    if (serverErrors.errors) {
      serverErrors.errors.forEach((error, index) => {
        errors[errorKey(index, error.source)] = errorBuilder(error);
      });
    }
    if (serverErrors.error) {
      errors.data = { title: serverErrors.error };
    }
    return errors;
  };
}

const buildErrors = getBuildErrors(null);

export default {
  name: 'errors',
  error(payload: { response: any }) {
    if (payload.response) {
      const { response } = payload;
      if (response.data) {
        if (typeof response.data === 'string') {
          const error = response.statusText
            ? `${response.statusText}: ${response.data}`
            : response.data;
          return buildErrors({ error });
        }
        return buildErrors(response.data);
      }
      return buildErrors({ error: response.statusText });
    }
    if (payload instanceof Error) {
      return payload;
    }
    return null;
  },
};
// };
