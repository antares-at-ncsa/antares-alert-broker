import axios from 'axios';
import JsonApi from 'devour-client';
import errorMiddleware from './middleware/error';
import { getCookieValue } from '..';

export async function getClient() {
  const { data: config } = await axios.get('/config.json');
  const client = new JsonApi({apiUrl: config.ANTARES_API_URL});

  // TODO: Middleware to convert request FROM camelCase TO underscore_separated
  client.insertMiddlewareBefore('axios-request', {
    name: 'serializeFilterParams',
    req: (payload: any) => {
      let csrfToken = getCookieValue('csrf_access_token');
      if (csrfToken) {
        payload.req.headers['X-CSRF-TOKEN'] = csrfToken;
      }
      if (payload.req.params && payload.req.params.filter) {
        payload.req.params.filter = JSON.stringify(payload.req.params.filter);
      }
      return payload;
    },
  });
  // client.replaceMiddleware('response', responseMiddleware);
  // TODO: Middleware to convert response FROM underscore_case TO camelCase
  // client.insertMiddlewareAfter('response', {
  //   name: 'test',
  //   req: (payload) => {
  //     console.log(payload);
  //     return payload;
  //   },
  // });
  client.replaceMiddleware('errors', errorMiddleware);

  client.define(
    'filter',
    {
      created_at: '',
      disabled_at: '',
      updated_at: '',
      name: '',
      description: '',
      latest_version: {
        jsonApi: 'hasOne',
        type: 'filter_version',
      },
      enabled_version: {
        jsonApi: 'hasOne',
        type: 'filter_version',
      },
      priority: null,
      level: null,
      public: false,
      enabled: false,
      user_id: null,
    },
    {
      type: 'filter',
    },
  );

  client.define(
    'filter_version',
    {
      code: '',
      comment: '',
      created_at: '',
      enabled_at: '',
      validated_at: '',
      disabled_at: '',
      public: true,
      filter: {
        jsonApi: 'hasOne',
        type: 'filter',
      },
    },
    {
      type: 'filter_version',
      collectionPath: 'versions',
    },
  );

  client.define(
    'locus_listing',
    {
      properties: {},
      tags: [],
      ra: null,
      dec: null,
      htm16: '',
      catalogs: [],
      locus: {
        jsonApi: 'hasOne',
        type: 'locus',
      },
      catalog_matches: {
        jsonApi: 'hasMany',
        type: 'catalog_entry',
      },
      alerts: {
        jsonApi: 'hasMany',
        type: 'alert',
      },
    },
    {},
  );

  client.define(
    'locus',
    {
      properties: {},
      tags: [],
      ra: null,
      dec: null,
      lightcurve: null,
      htm16: '',
      catalogs: [],
      alerts: {
        jsonApi: 'hasMany',
        type: 'alert',
      },
      catalog_matches: {
        jsonApi: 'hasMany',
        type: 'catalog_entry',
      },
    },
    {},
  );

  client.define(
    'alert',
    {
      mjd: '',
      properties: {},
    },
    {
      type: 'alert',
      collectionPath: 'alerts',
    },
  );

  client.define(
    'locus_annotation',
    {
      comment: '',
      favorited: false,
      owner: {
        jsonApi: 'hasOne',
        type: 'user',
      },
      locus: {
        jsonApi: 'hasOne',
        type: 'locus',
      },
    },
    {
      type: 'locus_annotation',
      collectionPath: 'locus_annotations',
    },
  );

  client.define(
    'user',
    {
      name: '',
      username: '',
      email: '',
      password: '',
      staff: false,
      admin: false,
      favorite_loci: {
        jsonApi: 'hasMany',
        type: 'locus',
      },
    },
    {
      type: 'user',
      collectionPath: 'users',
    },
  );

  client.define(
    'alert_property',
    {
      type: '',
      origin: '',
      description: '',
      es_mapping: '',
    },
    {
      type: 'alert_property',
      collectionPath: 'alert_properties',
    },
  );

  client.define(
    'locus_property',
    {
      type: '',
      origin: '',
      description: '',
      es_mapping: '',
    },
    {
      type: 'locus_property',
      collectionPath: 'locus_properties',
    },
  );

  client.define(
    'watch_list',
    {
      name: '',
      description: '',
      created_at: '',
      objects: [],
      owner: {
        jsonApi: 'hasOne',
        type: 'user',
      },
      slack_channel: '',
    },
    {
      type: 'watch_list',
      collectionPath: 'watch_lists',
    },
  );

  client.define(
    'tag',
    {
      name: '',
      filter_version_id: '',
      description: '',
    },
    {
      type: 'tag',
      collectionPath: 'tags',
    },
  );

  client.define(
    'catalog_sample',
    {
      object_id: null,
      catalog_name: null,
      data: {},
    },
    {
      type: 'catalog_sample',
      collectionPath: 'catalog_samples',
    },
  );

  client.define(
    'catalog',
    {
      name: null,
    },
    {
      type: 'catalog',
      collectionPath: 'catalogs',
    },
  );

  client.define(
    'catalog_entry',
    {
      ra: null,
      dec: null,
      properties: {},
      object_id: null,
      object_name: '',
      catalog: {
        jsonApi: 'hasOne',
        type: 'catalog',
      },
    },
    {
      type: 'catalog_entry',
      collectionPath: 'catalog-entries',
    },
  );

  return client;
}
