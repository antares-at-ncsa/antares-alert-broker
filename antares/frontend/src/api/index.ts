import { default as AuthService } from './services/auth';
import { default as AlertPropertyService } from './services/alertProperty';
import { default as LocusService } from './services/locus';
import { default as LocusPropertyService } from './services/locusProperty';
import { default as FilterService } from './services/filter';
import { default as UserService } from './services/user';
import { default as LocusAnnotationService } from './services/locusAnnotation';
import { default as WatchListService } from './services/watchList';
import { default as TagService } from './services/tag';
import { default as CatalogSampleService } from './services/catalogSample';

export { default as AuthService } from './services/auth';
export { default as AlertPropertyService } from './services/alertProperty';
export { default as LocusService } from './services/locus';
export { default as LocusPropertyService } from './services/locusProperty';
export { default as FilterService } from './services/filter';
export { default as UserService } from './services/user';
export { default as LocusAnnotationService } from './services/locusAnnotation';
export { default as WatchListService } from './services/watchList';
export { default as TagService } from './services/tag';
export { default as CatalogSampleService } from './services/catalogSample';

export function getCookieValue(name: string) {
  let value = document.cookie.match('(^|[^;]+)\\s*' + name + '\\s*=\\s*([^;]+)');
  return value ? value.pop() : '';
}
declare global {
    interface Window { ANTARES: any; }
}

window.ANTARES = {
  API: {
    AuthService,
    AlertPropertyService,
    LocusService,
    LocusPropertyService,
    FilterService,
    UserService,
    LocusAnnotationService,
    WatchListService,
    TagService,
    CatalogSampleService
  },
  Views: {}
};
