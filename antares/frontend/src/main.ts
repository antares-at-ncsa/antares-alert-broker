import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueGtag from 'vue-gtag';
import { mapGetters } from 'vuex';
import './plugins/bootstrap-vue';
import './plugins/mixins';
import './plugins/vue-clipboard2';
import './plugins/fontawesome-vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { CHECK_AUTH } from '@/store/actions';

import axios from 'axios';

axios.defaults.withCredentials = true;

Vue.use(Vuelidate);
Vue.use(VueGtag, {
  config: { id: 'G-VQDMMZSD41' },
}, router);

Vue.config.productionTip = false;

const setup = async () => {
  // Inject configuration file
  const { data: config } = await axios.get('/config.json');
  Vue.mixin({
    data() {
      return {
        ANTARES_API_URL: config.ANTARES_API_URL,
        ANTARES_FRONTEND_URL: config.ANTARES_FRONTEND_URL,
      }
    }
  })

  new Vue({
    router,
    store,
    computed: {
      ...mapGetters(['isAuthenticated']),
    },
    created() {
      // If this session still thinks it is valid, check against the API server.
      if (this.isAuthenticated) {
        this.$store.dispatch(CHECK_AUTH);
      }
    },
    render: (h) => h(App),
  }).$mount('#app');
}
setup();
