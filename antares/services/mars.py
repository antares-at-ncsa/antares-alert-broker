import enum
import re
from dataclasses import dataclass
from typing import Optional

import requests

from antares import log


class ThumbnailType(enum.Enum):
    DIFFERENCE = "difference"
    SCIENCE = "science"
    TEMPLATE = "template"


@dataclass
class Thumbnail:
    filename: str
    type: ThumbnailType
    blob: bytes


def get_ztf_thumbnail(alert_id: str, img_type: ThumbnailType) -> Optional[Thumbnail]:
    """
    Retrieves a ZTF thumbnail from Las Cumbres Observatory's MARS service.

    :param candidate_id: ZTF "candid" aka "candidate_id"
    :param img_type: ThumbnailType
    :return: dict
    """
    candidate_id = alert_id.partition(":")[2]
    # Look up the alert's candidate ID in the MARS LCO Database,
    # attempt to fetch its LCO ID.
    url = "https://mars.lco.global/?candid={}&format=json".format(candidate_id)
    response = requests.get(url)
    if not response.ok:
        log.warn(str(response))
        return None
    alert_lco_id = response.json()["results"][0]["lco_id"]

    # Fetch the thumbnail from MARS.
    url = "https://mars.lco.global/{}/cutout/{}".format(
        alert_lco_id, img_type.value.title()
    )
    response = requests.get(url)
    if not response.ok:
        log.warn(str(response))
        return None

    # Parse the filename from the response headers.
    filename_regex = re.compile("filename=(?P<filename>.*)")
    match = re.search(filename_regex, response.headers["Content-Disposition"])
    if not match:
        log.warn("Could not parse: %s", response.headers["Content-Disposition"])
        return None
    filename = match.group("filename")
    return Thumbnail(filename=filename, type=img_type, blob=response.content)
