
# antares.services

Contains wrappers around external services.

eg:
- Datadog
- Slack

Taking the example of Slack, `antares.services.slack`
knows how to talk to the Slack API. Other parts of `antares`
import it, and do not know anything about Slack's API.

Wrappers around internal DBs such as ES or SQL don't belong here.
