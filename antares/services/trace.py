import ddtrace

from antares import log
from antares.config import config


if config.DATADOG_TRACE_ENABLE:
    log.info(
        f"Enabling DataDog trace to"
        f" {config.DATADOG_TRACE_AGENT_HOST}"
        f":{config.DATADOG_TRACE_AGENT_PORT}"
    )
    ddtrace.tracer.configure(
        enabled=True,
        hostname=config.DATADOG_TRACE_AGENT_HOST,
        port=config.DATADOG_TRACE_AGENT_PORT,
    )
else:
    ddtrace.tracer.configure(enabled=False)


def patch_cassandra():
    if config.DATADOG_TRACE_ENABLE:
        ddtrace.patch(cassandra=True)


def patch_elasticsearch():
    if config.DATADOG_TRACE_ENABLE:
        ddtrace.patch(elasticsearch=True)


def patch_flask():
    if config.DATADOG_TRACE_ENABLE:
        ddtrace.patch(flask=True)


def patch_sqlalchemy():
    if config.DATADOG_TRACE_ENABLE:
        ddtrace.patch(sqlalchemy=True)


def register_db_engine(engine, service):
    if config.DATADOG_TRACE_ENABLE:
        ddtrace.Pin.override(engine, service=service)


trace_decorator = ddtrace.tracer.wrap
tracer = ddtrace.tracer
