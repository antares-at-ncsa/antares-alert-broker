import os
import socket
import traceback
import pickle
import uuid

from antares import redis, log
from antares.config import config
from . import metrics, slack


class report(object):
    def __init__(
        self,
        tag,
        channel=None,
        and_raise=False,
        extra_data=None,
        slack_async=None,
        dump=None,
    ):
        self._tag = tag
        self._channel = channel or config.SLACK_ERRORS_CHANNEL
        self._raise = and_raise or config.DEBUG
        self._extra_data = extra_data
        self._slack_kw = {}
        self._dump = dump
        if slack_async is not None:
            self._slack_kw["async_"] = slack_async

    def report_error(self, exc_type, exc_value, tb):
        if isinstance(exc_value, Exception):
            # Report to statsd
            metrics.error(self._tag, tags=[f"class:{exc_type}"])

            # Report to logging
            lines = traceback.format_exception(exc_type, exc_value, tb)
            stacktrace = "".join(lines)
            log.error(stacktrace)

            # TODO: Send to Datadog or ES?
            dump_path = None
            if self._dump:
                dump_path = os.path.join(config.TEMP_DIR, f"{uuid.uuid4()}.p")
                with open(dump_path, "wb") as f:
                    pickle.dump(self._dump, f)

            # Report to slack
            key = "error_report_slack"
            if redis.Lock(key, hashable=stacktrace, expire=5 * 60).lock():
                host = socket.gethostname()
                msg = f"`{config.ENVIRONMENT}` Error on `{host}`\n```{stacktrace}```"
                if dump_path:
                    msg += f"\nSerialized output at: {dump_path}"
                slack.post(msg, self._channel, **self._slack_kw)

            # Re-raise or swallow the exception
            if self._raise:
                return False  # Re-raise
            return True  # Swallow

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, tb):
        return self.report_error(exc_type, exc_value, tb)
