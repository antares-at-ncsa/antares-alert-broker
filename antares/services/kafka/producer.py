import backoff
from confluent_kafka import Producer
from confluent_kafka.cimpl import KafkaException

from antares import log
from antares.config import config
from antares.services import metrics
from antares.services.kafka.admin import KafkaAdmin


class TopicDoesNotExist(Exception):
    pass


class KafkaProducer(object):
    def __init__(self, kafka_config=None, create_missing_topics=True):
        if kafka_config is None:
            kafka_config = self.default_config()
        self._kafka_config = kafka_config
        self._producer = Producer(kafka_config)
        self._create_missing_topics = create_missing_topics
        self._known_topics = set()

    @backoff.on_exception(backoff.expo, BufferError, max_tries=10)
    def produce(self, topic, value, callback=None):
        self._ensure_topic(topic)
        log.debug("Sending messsage to topic '{}'".format(topic))
        try:
            self._producer.produce(topic, value=value, callback=callback)
            metrics.increment("send_kafka_msg", tags=[f"topic:{topic}"])
        except BufferError:
            # Queue is full
            self._producer.flush()
            raise  # Try again

    def flush(self):
        self._producer.flush()

    @staticmethod
    def default_config():
        c = {
            "bootstrap.servers": config.OUTPUT_KAFKA_SERVERS,
            "api.version.request": True,
            "broker.version.fallback": "0.10.0.0",
            "api.version.fallback.ms": 0,
        }

        # SASL_SSL Auth
        if config.OUTPUT_KAFKA_SASL_USER:
            assert config.OUTPUT_KAFKA_SASL_PASS
            assert config.OUTPUT_KAFKA_SSL_CA_LOCATION
            c.update(
                {
                    "sasl.mechanisms": "PLAIN",
                    "security.protocol": "SASL_SSL",
                    "sasl.username": config.OUTPUT_KAFKA_SASL_USER,
                    "sasl.password": config.OUTPUT_KAFKA_SASL_PASS,
                    "ssl.ca.location": config.OUTPUT_KAFKA_SSL_CA_LOCATION,
                }
            )

        return c

    def _ensure_topic(self, topic):
        """
        Create a topic if it does not exist.

        :param topic:
        """
        if topic in self._known_topics:
            return
        a = KafkaAdmin(self._kafka_config)
        self._known_topics = set(a.list_topic_names())
        if topic in self._known_topics:
            return
        if not self._create_missing_topics:
            raise TopicDoesNotExist
        try:
            log.info('Creating Kafka topic "%s"', topic)
            a.create_topic(topic)
            self._known_topics.add(topic)
        except KafkaException as e:
            if "TOPIC_ALREADY_EXISTS" in str(e):
                self._known_topics.add(topic)
            else:
                raise
