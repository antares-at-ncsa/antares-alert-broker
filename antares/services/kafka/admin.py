from confluent_kafka.admin import AdminClient
from confluent_kafka.cimpl import NewTopic

from antares import log
from antares.config import config


class KafkaAdmin(object):
    def __init__(self, kafka_config=None):
        if kafka_config is None:
            kafka_config = self.default_config()
        self._kafka_config = kafka_config
        self._admin = AdminClient(kafka_config)

    def create_topic(self, name, params=None):
        """
        Create a new Topic.
        """
        # Request to create new topic
        new_topic_params = config.OUTPUT_KAFKA_NEW_TOPIC_PARAMS.copy()
        if params:
            new_topic_params.update(params)
        futures = self._admin.create_topics([NewTopic(name, **new_topic_params)])
        # Wait for response
        for _, future in futures.items():
            future.result()  # Raises exception if request failed

    def delete_topic(self, topic, force=False):
        """
        Delete a topic.
        """
        assert force or topic.endswith("_dev") or topic.endswith("_staging")
        log.debug('Deleting kafka topic "%s"', topic)
        # Request to delete topic
        futures = self._admin.delete_topics([topic], operation_timeout=30)
        # Wait for response
        for _, future in futures.items():
            future.result()  # Raises exception if request failed
        assert topic not in self.list_topic_names()

    def delete_all_topics(self):
        for topic in self.list_topic_names():
            if not topic.startswith("_"):
                log.debug('Deleting topic "%s"', topic)
                self.delete_topic(topic)

    def print_status(self, topics=True, partitions=False):
        """
        Print Kafka broker information.

        From:
        https://github.com/confluentinc/confluent-kafka-python/blob/ab161a3193bd390eb9a3d01c0ef7a088ab12e796/examples/adminapi.py#L235
        """
        metadata = self.list_topics()
        print(
            "Cluster {} metadata (response from broker {}):".format(
                metadata.cluster_id, metadata.orig_broker_name
            )
        )
        print(" {} brokers:".format(len(metadata.brokers)))
        for broker in iter(metadata.brokers.values()):
            if broker.id == metadata.controller_id:
                print("  {}  (controller)".format(broker))
            else:
                print("  {}".format(broker))

        if topics:
            print(" {} topics:".format(len(metadata.topics)))
            for topic in iter(metadata.topics.values()):
                if topic.error is not None:
                    errstr = ": {}".format(topic.error)
                else:
                    errstr = ""
                print(
                    '  "{}" with {} partition(s){}'.format(
                        topic, len(topic.partitions), errstr
                    )
                )
                if partitions:
                    for partition in iter(topic.partitions.values()):
                        if partition.error is not None:
                            errstr = ": {}".format(partition.error)
                        else:
                            errstr = ""
                        print(
                            "    partition {} leader: {}, replicas: {}, isrs: {}".format(
                                partition.id,
                                partition.leader,
                                partition.replicas,
                                partition.isrs,
                                errstr,
                            )
                        )

    def list_topics(self, timeout=10):
        return self._admin.list_topics(timeout=timeout)

    def list_topic_names(self, timeout=10):
        """
        Return list of names of all topics which exist.
        """
        return [str(topic) for topic in self.list_topics(timeout).topics.values()]

    @staticmethod
    def default_config():
        c = {
            "bootstrap.servers": config.OUTPUT_KAFKA_SERVERS,
            "api.version.request": True,
            "broker.version.fallback": "0.10.0.0",
            "api.version.fallback.ms": 0,
        }

        # SASL_SSL Auth
        if config.OUTPUT_KAFKA_SASL_USER:
            assert config.OUTPUT_KAFKA_SASL_PASS
            assert config.OUTPUT_KAFKA_SSL_CA_LOCATION
            c.update(
                {
                    "sasl.mechanisms": "PLAIN",
                    "security.protocol": "SASL_SSL",
                    "sasl.username": config.OUTPUT_KAFKA_SASL_USER,
                    "sasl.password": config.OUTPUT_KAFKA_SASL_PASS,
                    "ssl.ca.location": config.OUTPUT_KAFKA_SSL_CA_LOCATION,
                }
            )

        return c
