import json

from antares import redis, log
from antares.services import errors, metrics


class KafkaLagCache:
    """
    Receive Kafka stats callbacks, report metrics, and store lags in Redis for
    availability to other processes.

    The format of the callback payload is expected to match spec in:
    https://github.com/edenhill/librdkafka/blob/master/STATISTICS.md
    """

    LAG_PREFIX = "kafka_lag"

    def __init__(self, expire=600):
        """
        :param expire: expiry TTL for redis keys
        """
        self.expire = expire

    @classmethod
    def get_lag(cls, topic):
        """
        return the most recent stats_cb data payload for a given topic.

        :param topic: name of Kafka topic
        :return: JSON string, or None.
        """
        lags = []
        for k in redis.redis.keys(f"{cls.LAG_PREFIX}:{topic}:*"):
            k = k.decode()  # bytes to str
            assert k.startswith(f"{cls.LAG_PREFIX}:{topic}:")
            lag = int(redis.redis.get(k))
            # log.debug('%s --> %s', k, lag)
            lags.append(lag)
        if not lags:
            return None
        log.debug("Got %s partition lags.", len(lags))
        total = sum(lags)
        log.debug("Total: %s", total)
        return total

    @classmethod
    def clear_lags(cls, topic):
        """
        Delete all lag data from Redis for a topic.
        """
        n = 0
        for k in redis.redis.keys(f"{cls.LAG_PREFIX}:{topic}:*"):
            k = k.decode()  # bytes to str
            assert k.startswith(f"{cls.LAG_PREFIX}:{topic}:")
            redis.redis.delete(k)
            n += 1
        return n

    def stats_cb(self, json_str):
        """
        Use this function as the Kafka "stats_cb" callback function.

        :param json_str: contains stats data from the Kafka library.
        """
        with errors.report("kafka_stats_cb", and_raise=True):
            data = json.loads(json_str)
            for t in data["topics"].keys():
                for p in data["topics"][t]["partitions"].keys():
                    if p == "-1":
                        # Partition "-1" is the "undefined/unallocated" topic.
                        # I don't know what this means.
                        continue
                    lag = data["topics"][t]["partitions"][p]["consumer_lag"]
                    if lag is None:
                        # Should not happen
                        metrics.warn("lag_is_none")
                        continue
                    assert isinstance(lag, int)
                    if lag < 0:
                        # If lag is -1, then this Kafka consumer doesn't have
                        # data on the status of this partition. I assume that
                        # this is because the partition is not assigned to this
                        # consumer. Skip it.
                        continue
                    key = f"{self.LAG_PREFIX}:{t}:{p}"
                    redis.redis.setex(key, self.expire, lag)
                    metrics.gauge(
                        "pipeline.kafka_lag", lag, tags=[f"topic:{t}", f"partition:{p}"]
                    )

                # Report total lag for the whole topic
                lag = self.get_lag(t)
                metrics.gauge("pipeline.kafka_lag_total", lag, tags=[f"topic:{t}"])
