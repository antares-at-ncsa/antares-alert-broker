import confluent_kafka

from antares import log
from .admin import KafkaAdmin
from .consumer import KafkaConsumer
from .producer import KafkaProducer


def get_message_timestamp(msg):
    """
    Get Kafka msg timestamp.

    Kafka timestamps are UNIX timestamps in integer milliseconds.
    This function converts them to fractional seconds.

    Returns one of:
    - A dict with key 'create_time'
    - A dict with key 'append_time'
    - An empty dict

    In practice it always returns the first case ie. 'create_time'.
    I don't know when or why it would return one of the other cases.

    :param msg: Kafka Message
    """
    ts_type, ts_ms = msg.timestamp()
    ts_s = ts_ms / 1000.0  # Convert integer milliseconds to fractional seconds
    if ts_type == confluent_kafka.TIMESTAMP_CREATE_TIME:
        return {"create_time": ts_s}
    if ts_type == confluent_kafka.TIMESTAMP_LOG_APPEND_TIME:
        return {"append_time": ts_s}
    if ts_type == confluent_kafka.TIMESTAMP_NOT_AVAILABLE:
        return {}
    log.warn("Unknown kafka timestamp type: %s", ts_type)
    return {}
