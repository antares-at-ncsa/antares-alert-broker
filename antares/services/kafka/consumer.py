import itertools
import time
import uuid

from confluent_kafka import Consumer
from confluent_kafka.cimpl import (
    KafkaError,
    KafkaException,
)  # pylint: disable=no-name-in-module

from antares.config import config


class KafkaConsumer(object):

    _POLLING_FREQUENCY = 1.0
    _STATS_INTERVAL_MS = 10 * 1000  # 10 seconds

    def __init__(
        self, kafka_config=None, topics=None, random_group=False, stats_cb=None
    ):
        """
        Set up a KafkaConsumer.

        :param kafka_config: config dict, or None to use default config
        :param topics: list of topics to subscribe to
        :param random_group: if True, generate a random 'group.id'
        :param stats_cb: if given, call this "stats callback" periodically
        """
        if kafka_config is None:
            kafka_config = self.default_config()
        if random_group:
            kafka_config["group.id"] = str(uuid.uuid4())
        if stats_cb:
            kafka_config["statistics.interval.ms"] = self._STATS_INTERVAL_MS
            kafka_config["stats_cb"] = stats_cb
        self._kafka_config = kafka_config
        self._consumer = Consumer(kafka_config)
        if topics:
            self._consumer.subscribe(topics)

    def iter(self, num_alerts=None):
        """
        Yield from ANTARES alert streams.

        Parameters
        -----------
        num_alerts: int
            Maximum number of alerts to yield. If None, yield alerts
            indefinitely (default, None).

        Yields
        ----------
        (topic, alert): str, dict

        """
        for i in itertools.count(start=1, step=1):
            yield self.poll()
            if num_alerts and i >= num_alerts:
                return

    def _timed_poll(self, timeout, message_parser=lambda msg: msg):
        start_time = time.perf_counter()
        while (time.perf_counter() - start_time) < timeout:
            try:
                message = self._consumer.poll(timeout=self._POLLING_FREQUENCY)
                if message is not None:
                    if message.error():
                        raise KafkaException(message.error().code())
                    topic = message.topic()
                    parsed_message = message_parser(message)
                    return topic, parsed_message
            except KafkaException as kafka_exception:
                kafka_error = kafka_exception.args[0]
                # pylint: disable=protected-access
                if kafka_error == KafkaError._PARTITION_EOF:
                    pass
                # pylint: disable=protected-access
                elif kafka_error == KafkaError._TIMED_OUT:
                    exception_fmt = "There was an error connecting to ANTARES: {}"
                    raise ConnectionError(exception_fmt.format(repr(kafka_exception)))
                else:
                    exception_fmt = "There was an error consuming from ANTARES: {}"
                    raise RuntimeError(exception_fmt.format(repr(kafka_exception)))
        return None, None

    def poll(self, timeout=None, message_parser=lambda msg: msg):
        """
        Retrieve a single alert. This method blocks until ``timeout``
        seconds have elapsed (by default, an infinite amount of time).

        Parameters
        ----------
        timeout: int
            Number of seconds to block waiting for an alert. If None,
            block indefinitely (default, None).

        Returns
        ----------
        (topic, alert): (str, dict)
            Or ``(None, None)`` if ``timeout`` seconds elapse with no response

        """
        if timeout:
            return self._timed_poll(timeout, message_parser)
        alert = None
        while alert is None:
            topic, alert = self._timed_poll(self._POLLING_FREQUENCY, message_parser)
        return topic, alert

    def consume(self, **kwargs):
        return self._consumer.consume(**kwargs)

    def subscribe(self, topics, **kwargs):
        self._consumer.subscribe(topics, **kwargs)

    def list_topics(self, **kwargs):
        return self._consumer.list_topics(**kwargs)

    def commit(self, **kwargs):
        self._consumer.commit(**kwargs)

    def close(self, **kwargs):
        self._consumer.close(**kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    @staticmethod
    def default_config():
        c = {
            "bootstrap.servers": config.INPUT_KAFKA_SERVERS,
            "group.id": config.INPUT_KAFKA_GROUP,
            # If there is no offset for this consumer group,
            # then initialize it to beginning of stream.
            "default.topic.config": {"auto.offset.reset": "earliest"},
            # With auto-commit set to False, we must call consumer.commit()
            # explicitly to commit our cursor. This means that if the consumer
            # crashes, uncommitted messages will be processed upon restart
            # or by other consumers. This is desirable and important!
            "enable.auto.commit": False,
            "max.poll.interval.ms": 1000 * 60 * 30,  # 30m
        }

        # SASL_SSL Auth
        if config.INPUT_KAFKA_SASL_USER:
            assert config.INPUT_KAFKA_SASL_PASS
            assert config.INPUT_KAFKA_SSL_CA_LOCATION
            c.update(
                {
                    "security.protocol": "SASL_SSL",
                    "sasl.mechanisms": "PLAIN",
                    "sasl.username": config.INPUT_KAFKA_SASL_USER,
                    "sasl.password": config.INPUT_KAFKA_SASL_PASS,
                    "ssl.ca.location": config.INPUT_KAFKA_SSL_CA_LOCATION,
                }
            )

        return c
