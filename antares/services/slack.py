import datetime
from json import JSONDecodeError

import requests

from antares import log, rq
from antares.config import config
from antares.services import metrics

N_RETRIES = 5
RETRY_DELAY_SECONDS = 60


class SlackError(Exception):
    pass


class ChannelNotFound(SlackError):
    pass


class ChannelIsArchived(SlackError):
    pass


def post(
    message,
    channel="#dev_alerts",
    username="ANTARES",
    icon_emoji=":telescope:",
    async_=True,
):
    """
    External API for this module.

    Send a message to Slack.
    Implements asynchronous use of RQ workers, and exponential backoff in event
    of failure.

    :param message:
    :param channel:
    :param username:
    :param icon_emoji:
    :param async_:
    :return:
    """
    if config.DEBUG:
        async_ = False
    kwargs = dict(
        message=message,
        channel=channel,
        username=username,
        icon_emoji=icon_emoji,
    )
    if async_:
        rq.enqueue(
            func=_slack_post_job,
            kwargs=kwargs,
            timeout=60,  # 1 minute
            queue=rq.QUEUE_HIGH,  # Slack jobs go to the high priority queue
        )
    else:
        return _post(**kwargs)


@metrics.timed("rq.job", tags=["job:_slack_post_job"])
def _slack_post_job(
    message, channel, username, icon_emoji, retries_remaining=N_RETRIES
):
    retries_remaining = retries_remaining - 1
    tags = ["channel:" + channel]
    try:
        _post(message, channel, username, icon_emoji)
        metrics.increment("slack.post_success", tags=tags)
    except ChannelNotFound:
        metrics.error("slack_channel_not_found")
        post(f"Slack channel not found: {channel}", "#errors")
    except ChannelIsArchived:
        metrics.error("slack_channel_is_archived")
        post(f"Slack channel is Archived: {channel}", "#errors")
    except SlackError as e:
        if "ratelimited" in str(e):
            metrics.error("slack_ratelimited")

        # Retry in `RETRY_DELAY_SECONDS` seconds
        if retries_remaining > 0:
            metrics.error("slack_retry", tags=tags)
            rq.enqueue_in(
                timedelta=datetime.timedelta(seconds=RETRY_DELAY_SECONDS),
                func=_slack_post_job,
                kwargs=dict(
                    message=message,
                    channel=channel,
                    username=username,
                    icon_emoji=icon_emoji,
                    retries_remaining=retries_remaining,
                ),
                queue=rq.QUEUE_HIGH,
            )
        else:
            metrics.error("slack_retry_give_up", tags=tags)
            if len(message) > 1000:
                message = (
                    f"`{config.ENVIRONMENT}` Failed to send slack message."
                    f" The message may have been too long for slack,"
                    f" at {len(message)} characters?"
                    f"\nError:\n\n```{e}```"
                    f"\n\nTruncated message:\n\n{message[:500].strip()}"
                    f"\n\n...\n...\n\n{message[-500:].strip()}"
                )
            else:
                message = (
                    f"`{config.ENVIRONMENT}` Failed to send slack message."
                    "\nError:\n\n```{e}```"
                    f"\n\n{message.strip()}"
                )
            post(message, channel, username, icon_emoji)


def _post(message, channel, username, icon_emoji):
    """
    Send a message to slack and raise a SlackError on failures.

    Raises:
    - SlackError
    - ChannelNotFound
    - ChannelIsArchived

    :param message:
    :param channel:
    :param username:
    :param icon_emoji:
    :return: requests.Response
    """
    if not config.SLACK_ENABLE:
        return

    url = "https://slack.com/api/chat.postMessage"
    data = {
        "token": config.SLACK_TOKEN,
        "text": message,
        "mrkdwn": "true",
        "channel": channel,
        "username": username,
        "icon_emoji": icon_emoji,
        "unfurl_links": "false",
        "unfurl_media": "false",
    }
    log.debug(f'Slack post to "{channel}":\n{message}')
    res = requests.post(url, data)
    try:
        error = res.json().get("error")
    except JSONDecodeError:
        log.error("Slack response:")
        log.error(res.content)
        raise SlackError(res.content)
    if error == "channel_not_found":
        raise ChannelNotFound(channel)
    if error == "is_archived":
        raise ChannelIsArchived(f"Slack channel `{channel}` is archived.")
    if error:
        if config.DEBUG:
            log.debug(res.json())
            return
        raise SlackError(error)
    return res


def im_list():
    if not config.SLACK_ENABLE:
        return

    url = "https://slack.com/api/im.list"
    data = {
        "token": config.SLACK_TOKEN,
    }
    res = requests.get(url, data)
    return res


def create_channel(name):
    if not config.SLACK_ENABLE:
        return

    # Max length is 21 characters (excluding the ‘#’ sign).

    url = "https://slack.com/api/channels.create"
    data = {
        "token": config.SLACK_TOKEN,
        "name": name,
        "validate": "true",
    }
    res = requests.get(url, data)
    return res
