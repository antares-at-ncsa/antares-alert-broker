import datadog
import ddtrace

from antares import log
from antares.config import config

GLOBAL_PREFIX = "antares."


# Here we centrally define all metric names used in the system.
COUNTERS = [
    "process_startup",
    "error",
    "warn",
    "send_kafka_msg",
    "pipeline.kafka_poll_timeout",
    "pipeline.kafka_msg_bytes",
    "pipeline.new_locus",
    "pipeline.move_locus",
    "pipeline.locus_data.set_locus_property",
    "pipeline.locus_data.set_alert_property",
    "pipeline.locus_data.add_tag",
    "pipeline.received_kafka_msg",
    "pipeline.skip_alert",
    "pipeline.failed_region_lock",
    "filters.property_key_error",
    "index_worker.document_indexed",
    "rq_worker.ztf_avro_file_sent_to_kafka",
    "slack.post_success",
    "ztf_avro_files_sent_to_kafka",
]
GAUGES = [
    "pipeline.ra",
    "pipeline.dec",
    "pipeline.kafka_lag",
    "pipeline.kafka_lag_total",
    "rq.queue_len",
    "rq.schedule_len",
    "cache.num_keys",
    "sql.table_rows",
    "ping",
    "ztf_tarballs.ingested_percent",
]
TIMERS = [
    "pipeline.poll_kafka",
    "pipeline.process_alert",
    "pipeline.generate_locus_id",
    "pipeline.run_filter",
    "pipeline.kafka_msg_age_on_ingest",
    "pipeline.acquire_region_lock",
    "pipeline.get_catalog_objects",
    "pipeline.search_watch_objects",
    "pipeline.get_locus",
    "pipeline.get_alerts",
    "pipeline.put_new_history_alerts",
    "pipeline.update_locus",
    "pipeline.move_locus",
    "pipeline.put_alerts",
    "rtdb.get_catalog_matches",
    "rq.job",
]
SERVICES = [
    "api",
    "frontend",
    "pipeline",
    "rq_scheduler",
    "rq_worker",
    "index_worker",
]


_service = None


# Assemble global tags which apply to all metrics
assert config.ENVIRONMENT
_global_tags = ["env:" + config.ENVIRONMENT]


# TODO: Wrap this to apply the _global_tags
instrument = ddtrace.tracer.trace


def startup():
    """
    Every container should call this once on boot.

    It should be called before any other metrics are reported.
    """
    if config.SERVICE == "dev":
        assert config.ENVIRONMENT == "dev"
        log.info('Disabling DataDog because ENVIRONMENT == "dev"')
        return

    assert config.SERVICE in SERVICES
    global _service
    assert _service is None  # Don't call this twice
    datadog.initialize(
        statsd_host=config.DATADOG_STATSD_HOST,
        statsd_port=config.DATADOG_STATSD_PORT,
    )
    _service = config.SERVICE
    _global_tags.append("service:" + _service)
    increment("process_startup")
    log.info("DataDog initialized with global tags: %s", _global_tags)


def increment(metric, value=1, tags=None, sample_rate=1):
    assert metric in COUNTERS
    metric = GLOBAL_PREFIX + metric
    tags = _make_tags(tags)
    log.debug(f"METRICS INCREMENT {metric} += {value} tags={tags}")
    return datadog.statsd.increment(
        metric=metric, value=value, tags=tags, sample_rate=sample_rate
    )


def gauge(metric, value, tags=None, sample_rate=1):
    assert metric in GAUGES
    metric = GLOBAL_PREFIX + metric
    tags = _make_tags(tags)
    log.debug(f"METRICS GAUGE {metric} = {value} tags={tags}")
    return datadog.statsd.gauge(
        metric=metric, value=value, tags=tags, sample_rate=sample_rate
    )


def timing(metric, value, tags=None, sample_rate=1):
    assert metric in TIMERS
    metric = GLOBAL_PREFIX + metric
    tags = _make_tags(tags)
    return datadog.statsd.timing(
        metric=metric, value=value, tags=tags, sample_rate=sample_rate
    )


def timed(metric=None, tags=None, sample_rate=1, use_ms=None):
    assert metric in TIMERS
    metric = GLOBAL_PREFIX + metric
    tags = _make_tags(tags)
    return datadog.statsd.timed(
        metric=metric, tags=tags, sample_rate=sample_rate, use_ms=use_ms
    )


def error(tag, tags=None):
    if tags is None:
        tags = []
    tags.append("type:{}".format(tag))
    increment("error", tags=tags)


def warn(tag, tags=None):
    if tags is None:
        tags = []
    tags.append("type:{}".format(tag))
    increment("warn", tags=tags)


def _make_tags(tags):
    if _global_tags or tags:
        return (_global_tags or []) + (tags or [])
    return None
