import itertools

import antares_client

from antares.config import config
from antares.pipeline.locus import LocusData
from antares.rtdb.api import RTDB
from antares.services import mars
from antares.sql.schema import SFilterCrashLog, SStorage

# Use the Antares config value for the API base URL instead of the hard-coded URL
antares_client.config.config["ANTARES_API_BASE_URL"] = config.API_BASE_URL

def get_locus(locus_id=None, alert_id=None, ztf_object_id=None, include_newer=False):
    """
    Load a Locus from the DB by locus_id, alert_id, or ztf_object_id.

    If none of the above are specified, a random Locus from the
    DB will be selected.

    :param ztf_object_id:
    :param locus_id:
    :param alert_id:
    :return: Locus
    """
    if ztf_object_id is not None:
        locus_id = get_locus_id_by_ztf_object_id(ztf_object_id)
        alert_id = None
    if locus_id is None and alert_id is None:
        locus_id = get_locus_id()
        alert_id = None
    locus_data = LocusData.from_rtdb(
        locus_id=locus_id, alert_id=alert_id, include_newer=include_newer
    )
    return locus_data.api()  # Return Locus object


def get_locus_id():
    """
    Get a random locus_id from the DB.
    """
    return get_locus_ids(1)[0]


def get_locus_ids(n):
    """
    Get `n` random locus_ids from the DB.

    :param n: number of locus_ids to get, up to max of 1000
    :return: list of locus_ids
    """
    if n > 1000:
        raise ValueError("n must be less than or equal to 1000")

    # Build an ES query
    query = {
        "query": {
            "function_score": {
                "random_score": {},
            }
        }
    }
    return [
        locus.locus_id
        for locus in itertools.islice(antares_client.search.search(query), n)
    ]


def get_sample_catalog_data(n=10):
    """
    Get a sample of catalog data from database for all catalogs.

    :param n: number of rows per catalog
    :return: dict
    """
    return RTDB().get_sample_catalog_data(n=n)


def search_catalogs(ra, dec):
    """
    Cone-search ANTARES' object catalogs at an (ra, dec) coordinate.

    The result is the same data as would be available to a Filter processing
    an Alert at that (ra, dec) position.

    Returned data structure is of form::

      {
        catalog_name_x: [obj_x1, obj_x2, ...],
        catalog_name_y: [obj_y1, ...]
        ...
      }

    Where ``obj_*`` objects are dicts representing rows from the catalog tables.

    :param ra:
    :param dec:
    :return: dict of lists
    """
    return RTDB().get_catalog_objects(ra, dec)


def locus_from_dict(locus_dict):
    """
    Load a Locus from it's `to_dict()` representation

    :param locus_dict: dict
    :return: Locus
    """
    return LocusData.from_dict(locus_dict).api()


def locus_from_file(filename):
    """
    Load a LocusData from a file.

    :param filename:
    :return: LocusData
    """
    return LocusData.from_file(filename).api()


def upload_file(key, file_path):
    """
    Store a datafile in ANTARES for use by filters.

    The `key` must be formatted like::

        <author>_<file-name>_<version>.<extension>

    eg::

        stubens_myFile_v1.txt
        wolf_nnmodel_v4.pickle

    Files are stored as binary strings, not ASCII.
    All file types are supported.
    The Filter which uses the file is responsible for parsing it.

    :param key: key under which to store the file.
    :param file_path: path of file.
    :return: the key, for confirmation.
    """
    return SStorage.put_file(key, file_path)


def get_crash_log(log_id):
    return SFilterCrashLog.get(log_id)


def print_crash_log(log_id):
    from antares.sql.schema import SFilterCrashLog

    cl = SFilterCrashLog.get(log_id)
    if cl is None:
        print("Crash log not found")
    print("Filter ID:", cl["filter_id"])
    print("Filter Version ID:", cl["filter_version_id"])
    print("Locus ID:", cl["locus_id"])
    print("Alert ID:", cl["alert_id"])
    print("Stacktrace:")
    print()
    print(cl["stacktrace"])


def locus_from_crash_log(crash_log_id):
    crash_log = get_crash_log(crash_log_id)
    return locus_from_dict(crash_log["locus"])


def get_locus_id_by_ztf_object_id(ztf_object_id):
    raise NotImplementedError
    # TODO


def get_thumbnails(alert_id: str):
    """
    Get thumbnail images for an alert.

    Not all alerts are guaranteed to have thumbnails.

    :param alert_id:
    :return: dict
    """
    thumbnails = {}
    for thumbnail_type in mars.ThumbnailType:
        thumbnail = mars.get_ztf_thumbnail(alert_id, thumbnail_type)
        if thumbnail:
            thumbnails[thumbnail_type.value] = {
                "alert_id": alert_id,
                "type": thumbnail_type.value,
                "file_name": thumbnail.filename,
                "blob": thumbnail.blob,
            }
        else:
            thumbnails[thumbnail_type.value] = None
    return thumbnails
