import logging
import sys


DEFAULT_LOG_LEVEL = "WARN"
log = logging.getLogger("devkit")
log.addHandler(logging.StreamHandler(sys.stdout))


def log_init(level=None, system_wide=False):
    if level is None:
        level = DEFAULT_LOG_LEVEL
    log.setLevel(level)
    for handler in log.handlers:
        handler.setLevel(level)

    if system_wide:
        logging.getLogger("antares").setLevel(level)
        for handler in logging.getLogger("antares").handlers:
            handler.setLevel(level)


log_init()
