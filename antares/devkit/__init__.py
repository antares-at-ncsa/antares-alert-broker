from .get_data import (
    get_locus,
    get_locus_ids,
    get_sample_catalog_data,
    search_catalogs,
    locus_from_dict,
    locus_from_file,
    upload_file,
    get_crash_log,
    print_crash_log,
    locus_from_crash_log,
    get_thumbnails,
)
from .log import log, log_init
from .run_filter import run_filter, run_many
from ..log import init as antares_log_init
from ..pipeline.filters.base import Filter

__all__ = [
    "init",
    "Filter",
    "get_locus",
    "get_locus_ids",
    "get_sample_catalog_data",
    "search_catalogs",
    "locus_from_dict",
    "locus_from_file",
    "upload_file",
    "get_crash_log",
    "print_crash_log",
    "locus_from_crash_log",
    "get_thumbnails",
    "run_filter",
    "run_many",
    "get_ligo_events",
]


def init(log_level="WARN", quiet=False, configure=True):
    from antares.config import config

    if config.ENVIRONMENT != "dev":
        raise RuntimeError(
            'Devkit.init() must only be called in ENVIRONMENT="dev",'
            " such as DataLab or local development."
        )

    p = print
    if quiet:
        p = lambda *a, **kw: None

    if configure:
        _configure(log_level)

    # Test connections to MySQL
    p("Connecting to MySQL server...")
    from antares.sql import engine

    engine.close()
    engine.get_engine().execute('SELECT "OK"').scalar()

    # Test connection to Cassandra
    p("Connecting to Cassandra cluster...")
    from antares.rtdb import antcassandra

    antcassandra.init()

    # Test ability to load Loci
    p("Testing loading a random Locus with `dk.get_locus()`...")
    ids = get_locus_ids(1)
    if not ids:
        p = print  # Disable quiet mode
        p("The database appears to be empty.")
        p("Please restart your Jupyter kernel and try again.")
        p("Or, contact ANTARES team for support on Slack or at antares_help@noao.edu")
        return
    get_locus()

    # Print success message
    import antares

    p("")
    p(f"ANTARES v{antares.__version__} DevKit is ready!")
    p("Website: http://antares.noirlab.edu")
    p("Documentation: http://noao.gitlab.io/antares/filter-documentation/")
    p("")


def _configure(log_level):
    # Configure logging
    log_init(log_level)
    antares_log_init(log_level)

    from antares.config import config

    # # Disable Redis connection
    # config.REDIS_HOST = "online"
    # config.REDIS_RQ_HOST = ""

    # # Configure connections to MySQL
    # config.SQLALCHEMY_ECHO = False
    # config.MYSQL_DB_URL = "mysql://antares_datalab:pro_Moonrise_epi@antdb01.dm.noao.edu:3306/antares_staging"

    # # Configure connection to Cassandra
    # config.CASSANDRA_ECHO = False
    # config.CASSANDRA_ALERT_KEYSPACE = (
    #     "antares_alerts_staging"  # TODO eventually, production
    # )
    # config.CASSANDRA_CATALOG_KEYSPACE = "antares_catalogs_production"
    # config.CASSANDRA_HOSTS = [
    #     "ant10.dm.noao.edu",
    #     "ant11.dm.noao.edu",
    #     "ant12.dm.noao.edu",
    #     "ant13.dm.noao.edu",
    #     "ant14.dm.noao.edu",
    #     "ant15.dm.noao.edu",
    # ]
    # config.CASSANDRA_USER = "antares_datalab"
    # config.CASSANDRA_PASS = "pro_Moonrise_epi"

    # # Configure connection to ElasticSearch
    # config.ELASTICSEARCH_HOSTS = ["140.252.6.22"]
    # config.ELASTICSEARCH_PORT = 9200
    # config.ARCHIVE_INDEX_NAME = "loci_staging"


def get_ligo_events():
    raise NotImplementedError
