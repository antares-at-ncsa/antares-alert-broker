import inspect
import time

import numpy as np

from antares.devkit.get_data import get_locus_ids
from antares.pipeline.filters.runnable_filter import RunnableFilter
from antares.pipeline.locus import LocusData, Locus, HaltPipeline
from .log import log, log_init


def run_filter(f, locus=None, verbose=False):
    """
    Test a Filter.

    :param f: Filter object or class
    :param locus: LocusDataAPI, locus_id, or None
    :param verbose: if True, print detailed log
    """
    log_init("DEBUG" if verbose else None, system_wide=True)
    if inspect.isclass(f):
        f = f()
    rf = RunnableFilter(f)
    rf.setup(update_outputs=False)
    try:
        return _run_filter(rf, locus=locus)
    finally:
        log_init(system_wide=True)


def _run_filter(rf, locus=None):

    if locus is None:
        (locus_id,) = get_locus_ids(1)
        ld = LocusData.from_rtdb(locus_id=locus_id)
    elif isinstance(locus, Locus):
        ld = locus._ld
        locus_id = ld.locus_id
    elif isinstance(locus, LocusData):
        ld = locus
        locus_id = ld.locus_id
    else:
        assert isinstance(locus, str)
        locus_id = locus
        ld = LocusData.from_rtdb(locus_id=locus_id)
    del locus

    # Run filter
    halt = False
    log.debug("")
    log.debug("Running filter:")
    log.debug("- - - - -")
    t1 = time.time()
    try:
        trace = rf.run(ld)
    except HaltPipeline:
        log.debug("Filter raised HaltPipeline")
        halt = True
        trace = None
    t2 = time.time()
    log.debug("- - - - -")
    log.debug("Filter completed.")
    log.debug("")
    t = t2 - t1
    del t1, t2
    if trace:
        log.error("Filter crashed:\n\n%s", trace)

    # Print computed Locus Properties
    if ld.new_locus_properties:
        log.debug("Filter computed new Locus Properties:")
        for k, v in ld.new_locus_properties.items():
            log.debug('"{}" = {}'.format(k, repr(v)))
    else:
        log.debug("Filter did not compute new Locus Properties.")

    # Print computed Alert Properties
    if ld.new_alert_properties:
        log.debug("Filter computed new Alert Properties:")
        for k, v in ld.new_alert_properties.items():
            log.debug('"{}" = {}'.format(k, repr(v)))
    else:
        log.debug("Filter did not compute new Alert Properties.")

    # Print computed tags
    if ld.new_tags:
        log.debug("Filter added Tags to the Locus:")
        for tag in ld.new_tags:
            log.debug(tag)
    else:
        log.debug("Filter did not Tag the Locus.")

    log.debug("Done.")
    log.debug("")
    return dict(
        locus_id=locus_id,
        locus_data=ld.api(),
        t=t,
        new_locus_properties=ld.new_locus_properties.copy(),
        new_alert_properties=ld.new_alert_properties.copy(),
        new_tags=ld.new_tags.copy(),
        raised_halt=halt,
    )


def run_many(f, locus_ids=None, n=100, verbose=False, keep_results=True):
    """
    Run a filter on many Loci.

    Locus IDs may be passed in as `locus_ids` or will be chosen randomly.

    Response format::

      {
        'n': <number of times filter ran>
        'results': <dict mapping alert id to result of run_filter()>
        't_50_percentile': <50th percentile of filter execution time>
        't_90_percentile': <90th percentile of filter execution time>
        't_95_percentile': <95th percentile of filter execution time>
        't_99_percentile': <99th percentile of filter execution time>
      }

    :param f: Python Filter object or class
    :param locus_ids: list of locus_id, or None
    :param n: if locus_ids is None, this many random Locus IDs will be used.
    :param verbose:
    :param keep_results: if False, `results` are discarded and not returned
    :return: dict
    """
    log_init("DEBUG" if verbose else None, system_wide=True)
    try:
        if inspect.isclass(f):
            f = f()
        rf = RunnableFilter(f)
        rf.setup(update_outputs=False)
        if not locus_ids:
            if not n:
                raise RuntimeError("run_many() requires either `alert_ids` or `n`.")
            locus_ids = get_locus_ids(n)
        del n
        results = []
        times = []
        for locus_id in locus_ids:
            result = _run_filter(rf, locus_id)
            if keep_results:
                results.append(result)
            times.append(result["t"])
        return {
            "n": len(locus_ids),
            "results": results or None,
            "t_50_percentile": np.percentile(times, 50),
            "t_90_percentile": np.percentile(times, 90),
            "t_95_percentile": np.percentile(times, 95),
            "t_99_percentile": np.percentile(times, 99),
        }
    finally:
        log_init(system_wide=True)
