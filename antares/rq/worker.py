from antares import log
from antares.config import config
from antares.rtdb import antcassandra
from antares.services import metrics
from antares.sql import engine

from . import work


def main():
    metrics.startup()
    log.info("Starting up with config:")
    log.info("\n%s", config.format())
    log.info("Testing MySQL connection")
    engine.test_connection()
    log.info("Testing Cassandra connection")
    antcassandra.init()

    work()


if __name__ == "__main__":
    main()
