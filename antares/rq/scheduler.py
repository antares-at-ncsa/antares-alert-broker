import datetime
import time

from antares import log
from antares.config import config
from antares.rq.jobs import ingest_ztf_avro_files
from antares.services import metrics
from antares.utils.threads import ThreadLoop
from . import get_scheduler, report_queue_lengths, QUEUE_DEFAULT


def main():
    s = get_scheduler()
    metrics.startup()

    # Delete existing schedule
    existing_jobs = list(s.get_jobs())
    log.info(f"Deleting {len(existing_jobs)} previously existing schedules")
    for job in existing_jobs:
        s.cancel(job)
    del existing_jobs

    # Set up current schedule
    log.info("Setting up new schedule")
    _make_schedule(s)
    log.info(f"Schedule now has {len(list(s.get_jobs()))} jobs.")

    # Report queue length repeatedly in a thread
    log.info("Starting background jobs thread")
    background_thread = ThreadLoop(fn=report_queue_lengths, period=11)
    background_thread.start()

    # Run the scheduler
    log.info("Running scheduler...")
    s.run()

    log.info("Stopping background jobs thread")
    background_thread.stop()
    log.info("Shutdown")
    time.sleep(0.5)


def _make_schedule(scheduler):
    now = datetime.datetime.utcnow()
    #
    # Please ensure that all job functions are decorated like:
    # @metrics.timed('rq.job', tags=['job:<function_name>'])
    #

    # Add to the schedule like:
    #
    # scheduler.schedule(
    #     scheduled_time=datetime.datetime.utcnow(),
    #     func=foo,
    #     kwargs=dict(),
    #     interval=60,  # 1 minute
    #     timeout=60,  # 1 minute
    #     result_ttl=2 * 60,  # 2 minutes
    #     queue_name=QUEUE_DEFAULT,
    # )

    scheduler.schedule(
        scheduled_time=now + datetime.timedelta(minutes=1),
        interval=60 * 60,  # repeat every 1 hour
        func=ingest_ztf_avro_files.scheduler_job,
        kwargs=dict(
            start_date=0,
            end_date=20200622,
        ),
        timeout=10 * 60,  # 10 minutes
        result_ttl=2 * 60 * 60,  # 2 hours
        queue_name=QUEUE_DEFAULT,
    )


if __name__ == "__main__":
    main()
