from redis.client import Redis
from rq import Queue, Worker
from rq.timeouts import JobTimeoutException
from rq_scheduler.scheduler import Scheduler

from antares import log
from antares.config import config
from antares.services import metrics, errors, slack

redis = Redis(
    host=config.REDIS_RQ_HOST, port=config.REDIS_RQ_PORT, db=config.REDIS_RQ_DB
)


QUEUE_HIGH = "high"
QUEUE_LOW = "low"
QUEUE_DEFAULT = QUEUE_LOW
QUEUES = [
    # Order is important
    QUEUE_HIGH,
    QUEUE_LOW,
]


def get_queue(name=QUEUE_DEFAULT):
    return Queue(name=name, connection=redis)


def enqueue(func, kwargs=None, queue=QUEUE_DEFAULT, timeout=-1, at_front=False):
    log.debug("enqueue() Job %s kwargs=%s timeout=%s", func, kwargs, timeout)
    return get_queue(queue).enqueue_call(
        func=func,
        kwargs=kwargs,
        timeout=timeout,
        at_front=at_front,
    )


def enqueue_in(func, timedelta, kwargs=None, timeout=-1, queue=QUEUE_DEFAULT):
    log.debug(
        "enqueue_in() Job %s timedelta=%s kwargs=%s timeout=%s",
        func,
        timedelta,
        kwargs,
        timeout,
    )
    get_scheduler().enqueue_in(
        func=func,
        time_delta=timedelta,
        timeout=timeout,
        queue_name=queue,
        **kwargs,  # Inconsistent interface compared to Queue.enqueue() !! :(
    )


def empty():
    """
    Drop all pending jobs from queue.

    :return: n jobs dropped
    """
    return get_queue().empty()


def work():
    """
    Run a worker.
    """
    w = Worker(
        queues=QUEUES,
        connection=redis,
        exception_handlers=[exc_handler],
        disable_default_exception_handler=True,
    )
    w.work()


def get_scheduler():
    """
    Get a scheduler.
    """
    return Scheduler(connection=redis, interval=60)


def report_queue_lengths():
    """
    Report queue lengths to statsd.
    """
    for name in QUEUES:
        n = len(get_queue(name))
        tags = [f"queue:{name}"]
        metrics.gauge("rq.queue_len", n, tags=tags)

    n = len(list(get_scheduler().get_jobs()))
    metrics.gauge("rq.schedule_len", n)


def exc_handler(job, exc_type, exc_value, traceback):
    tag = job.func_name
    if exc_type is JobTimeoutException:
        slack.post(f"Job {tag} timed out:```{exc_value}```", "#errors", async_=False)
    else:
        errors.report(tag=tag).report_error(exc_type, exc_value, traceback)
    return False  # Swallow exception
