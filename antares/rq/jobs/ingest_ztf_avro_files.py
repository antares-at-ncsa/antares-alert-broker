"""

Tool for loading ZTF tarballs into a Kafka topic.

This uses the temp folder on the NFS share on antdb01 as a central place to
store the state, but tarball data is kept locally inside the container, not
written to antdb01.

"""
import hashlib
import os
import tarfile
import time

import fastavro
import humanfriendly
import requests
import wget
from confluent_kafka.cimpl import KafkaException

from antares import log, rq, utils
from antares.config import config
from antares.services import kafka, metrics, slack
from antares.services.kafka.lag_cache import KafkaLagCache

TARBALLS_PER_BATCH = 10  # Number of tarballs to enqueue at a time
MAX_KAFKA_LAG = 200 * 1000  # Enqueue next batch of tarballs when lag below this
KAFKA_TOPIC = "ztf_all"
KAFTA_TOPIC_PARAMS = {
    "num_partitions": 30,
    "replication_factor": 3,
}


def delete_topic(force=False):
    assert KAFKA_TOPIC.startswith("ztf_all")
    kafka.KafkaAdmin().delete_topic(KAFKA_TOPIC, force=force)


def clear_queued_flags():
    """
    Delete ALL 'queued' flag files.

    :return: number of flag files deleted.
    """
    n = 0
    for (md5sum, file_name, date_int) in get_file_index():
        if _check(file_name, "queued"):
            _clear(file_name, "queued")
            n += 1
    return n


def get_kafka_lag():
    assert KAFKA_TOPIC.startswith("ztf_all")
    return KafkaLagCache.get_lag(KAFKA_TOPIC)


def _global_workdir():
    """
    Get a path to a persistent global working directory on an NFS share.
    """
    assert config.TEMP_DIR
    assert config.TEMP_DIR.startswith("/")
    workdir = os.path.join(config.TEMP_DIR, "ztf_tarballs/")
    if not os.path.exists(workdir):
        os.makedirs(workdir)
    return workdir


def _local_workdir():
    """
    Get a path to an ephemeral local working directory inside the container.
    """
    workdir = "/var/temp_in_container/ztf_tarballs/"
    if not os.path.exists(workdir):
        os.makedirs(workdir)
    return workdir


@metrics.timed("rq.job", tags=["job:ingest_ztf.scheduler_job"])
def scheduler_job(
    start_date=0,
    end_date=20200622,
):
    """
    Schedules tarball ingest jobs.

    RQ scheduler should be configured to run this every hour.
    """
    run_again = True
    n = 0
    lag = get_kafka_lag()
    log.info(f"Current Kafka lag is {lag}")
    if lag is None:
        log.info("No entry in redis for kafka lag for topic `ztf_all`")
        return
    elif lag < MAX_KAFKA_LAG:
        log.info("Scheduling next batch of ingest jobs...")
        n = _schedule_next_tarballs(start_date, end_date, TARBALLS_PER_BATCH)
        if n == 0:
            run_again = False
    else:
        log.info("Kafka lag is too high. Not scheduling ingest jobs.")

    # Collect some progress stats
    in_range = 0
    queued = 0
    ingested = 0
    partial = 0
    errored = 0
    for (md5sum, file_name, date_int) in get_file_index():
        if start_date <= date_int <= end_date:
            in_range += 1
            if _check(file_name, "queued"):
                queued += 1
            if _check(file_name, "ingested"):
                ingested += 1
            if _check(file_name, "partial"):
                partial += 1
            if _check(file_name, "errored"):
                errored += 1
    percent = round(100 * float(ingested + partial) / in_range, 2)
    metrics.gauge("ztf_tarballs.ingested_percent", percent)

    # Post to Slack a status update
    message = utils.unindent(
        f"""
        `{config.ENVIRONMENT}`
        The next {n} tarballs have been scheduled for ingestion.
        Current Kafka lag is {lag}.

        {in_range} total tarballs in ingestion time range.
        {queued} are currently queued.
        {ingested + partial} ({percent}%) are already ingested.

        {partial} are partially ingested tarballs (EOF corruption),
        {errored} are errored.

        View the directory listing of /data/antares_prod_temp/ztf_tarballs
        on antdb01 for details.
    """
    )
    log.info(message)
    if n > 0 or run_again is False:
        slack.post(message, "#dev_alerts", async_=False)


def _schedule_next_tarballs(start_date, end_date, n):
    scheduled = 0
    for (md5sum, file_name, date_int) in get_file_index(reverse=True):
        if start_date <= date_int <= end_date:
            if _check(file_name, "errored"):
                log.info(f"{file_name} is marked errored, skipping")
                continue
            if _check(file_name, "partial"):
                log.info(f"{file_name} is marked partial, skipping")
                continue
            if _check(file_name, "queued"):
                log.info(f"{file_name} is marked queued, skipping")
                continue
            if _check(file_name, "ingested"):
                log.info(f"{file_name} is marked ingested, skipping")
                continue
            log.info("Enqueue %s", file_name)
            rq.enqueue(
                func=download_and_ingest_day,
                kwargs=dict(file_name=file_name),
                timeout=24 * 60 * 60,  # 24h
            )
            _mark(file_name, "queued")
            scheduled += 1
        if scheduled >= n:
            break
    return scheduled


@metrics.timed("rq.job", tags=["job:ingest_ztf.download_and_ingest_day"])
def download_and_ingest_day(file_name):
    file_path = os.path.join(_local_workdir(), file_name)
    _download(file_name)
    _ingest_tar_file(file_name)
    _delete_file(file_path)


class ProgressBar(object):
    """
    wget download progress bar callback.
    """

    SECONDS_BETWEEN_LOGS = 15
    PERCENT_BETWEEN_LOGS = 5

    def __init__(self):
        self.next_t = time.time()
        self.next_percent = 0

    def __call__(self, current, total, width):
        del width
        percent = round(100.0 * current / total, 1)
        if time.time() >= self.next_t or percent >= self.next_percent:
            current = humanfriendly.format_size(current)
            total = humanfriendly.format_size(total)
            log.info(f"{current} / {total} = {percent}%")
            self.next_t = time.time() + self.SECONDS_BETWEEN_LOGS
            self.next_percent = percent + self.PERCENT_BETWEEN_LOGS


def _download(tar_file_name):
    file_path = os.path.join(_local_workdir(), tar_file_name)

    # Download tarball
    if os.path.isfile(file_path):
        log.info("Tarball already exists: %s", file_path)
    else:
        url = "https://ztf.uw.edu/alerts/public/" + tar_file_name
        log.info("Downloading %s", url)
        curdir = os.curdir  # store current directory
        os.chdir(_local_workdir())  # cd to workdir
        wget.download(url, _local_workdir(), ProgressBar())
        os.chdir(curdir)  # cd back to wherever we were before
        log.info("")

    # Get md5sum from website
    log.info("Downloading expected MD5...")
    md5sum = _download_md5(tar_file_name)
    assert md5sum

    # Check file md5
    log.info("Computing MD5 of file...")
    assert _compute_md5(file_path) == md5sum


def _download_md5(tar_file_name):
    for md5sum, f, _ in get_file_index():
        if f == tar_file_name:
            return md5sum
    return None  # Not found in list


def _ingest_tar_file(tar_file_name):
    """
    At this point a file can be marked "queued".

    Valid transitions:

    queued -> partial
    queued -> ingested
    queued -> errored

    """
    assert KAFKA_TOPIC.startswith("ztf_all")
    assert _check(tar_file_name, "queued")

    # Creata Kafka topic if it does not exist
    try:
        kafka.KafkaAdmin().create_topic(KAFKA_TOPIC, params=KAFTA_TOPIC_PARAMS)
    except KafkaException as e:
        if "TOPIC_ALREADY_EXISTS" in str(e):
            pass
        else:
            raise

    # Push avro files into Kafka
    file_path = os.path.join(_local_workdir(), tar_file_name)
    producer = kafka.KafkaProducer()
    try:
        log.info("Ingesting tarfile %s...", tar_file_name)
        with tarfile.open(file_path, "r:gz") as tar:
            for entry in tar:
                fileobj = tar.extractfile(entry)
                _send_to_kafka(fileobj, producer, KAFKA_TOPIC)
        log.info("Done ingesting.")
    except Exception as e:
        if str(e) == "empty file":
            log.info("Archive is empty {}".format(tar_file_name))
        elif str(e) == "unexpected end of data":
            # Some ZTF archives are corrupt and contain premature EOFs.
            # For the time being we will mark these as partial and maybe
            # reprocess them in the future if they're fixed.
            log.info("EOF error {}".format(tar_file_name))
            _clear(tar_file_name, "queued")
            _mark(tar_file_name, "partial")
            return
        else:
            _clear(tar_file_name, "queued")
            _mark(tar_file_name, "errored")
            raise
    _clear(tar_file_name, "queued")
    _mark(tar_file_name, "ingested")


def _touch(file_path):
    open(file_path, "a").close()


def _mark(tar_file_name, flag):
    if tar_file_name.endswith(".tar.gz"):
        tar_file_name = tar_file_name[:-7]
    workdir = _global_workdir()
    flag_file_path = os.path.join(workdir, tar_file_name + "." + flag)
    log.info("Mark %s", flag_file_path)
    _touch(flag_file_path)


def _check(tar_file_name, flag):
    if tar_file_name.endswith(".tar.gz"):
        tar_file_name = tar_file_name[:-7]
    workdir = _global_workdir()
    flag_file_path = os.path.join(workdir, tar_file_name + "." + flag)
    return os.path.exists(flag_file_path)


def _clear(tar_file_name, flag):
    if tar_file_name.endswith(".tar.gz"):
        tar_file_name = tar_file_name[:-7]
    workdir = _global_workdir()
    flag_file_path = os.path.join(workdir, tar_file_name + "." + flag)
    _delete_file(flag_file_path)


def _get_marked(flag, global_=False):
    suffix = "." + flag.strip(".")
    result = []
    if global_:
        workdir = _global_workdir()
    else:
        workdir = _local_workdir()
    for f in os.listdir(workdir):
        if f.endswith(suffix):
            result.append(f[: -len(suffix)])
    return result


def _send_to_kafka(file_obj, producer, topic):
    """
    Send a ZTF Avro file to Kafka.
    """
    data = file_obj.read()
    producer.produce(topic, data)
    metrics.increment("ztf_avro_files_sent_to_kafka")


def _delete_file(file_path):
    """
    Delete a file.
    """
    assert file_path.startswith("/")
    assert file_path.startswith(_local_workdir()) or file_path.startswith(
        _global_workdir()
    )
    assert os.path.isfile(file_path)
    assert not os.path.isdir(file_path)
    os.remove(file_path)


def _delete_dir(dir_path):
    assert dir_path.startswith("/")
    assert dir_path.startswith(_local_workdir()) or dir_path.startswith(
        _global_workdir()
    )
    assert os.path.isdir(dir_path)
    os.rmdir(dir_path)


def get_file_index(reverse=False):
    """
    Get list of .tar files on ZTF server.

    The result is sorted by date_int, ascending.

    :return: list of (md5sum, file_name, date_int) triplets
    """
    r = requests.get("https://ztf.uw.edu/alerts/public/MD5SUMS")
    file_index = [
        l.strip().split() for l in r.text.split("\n") if l and not l.startswith("#")
    ]
    result = []
    for row in file_index:
        md5sum, fpath = row
        fname = os.path.split(fpath)[-1]
        assert len(md5sum) == 32
        result.append([md5sum, fname, _file_name_to_date_int(fname)])

    return sorted(result, key=lambda row: row[2], reverse=reverse)


def _file_name_to_date_int(file_name):
    assert file_name.startswith("ztf_public_")
    assert file_name.endswith(".tar.gz")
    return int(file_name[11:19])


def _get_ztf_candidate_from_file(file_path):
    """
    Load a ZTF Candidate dict from a ZTF avro file.

    :param file_path:
    :return: dict
    """
    # Read avro file
    ztf_alert = _load_avro_file(file_path)
    return ztf_alert["candidate"]


def _load_avro_file(file_path):
    with open(file_path, "rb") as f:
        avro_reader = fastavro.reader(f)
        avro_file_objs = list(avro_reader)
    assert len(avro_file_objs) == 1
    (avro_obj,) = avro_file_objs
    return avro_obj


def _compute_md5(file_path):
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
