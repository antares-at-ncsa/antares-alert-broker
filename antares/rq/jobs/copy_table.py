from antares import log
from antares.services import metrics
from antares.sql import engine

from antares.sql.schema import SBase


@metrics.timed("rq.job", tags=["job:copy_table"])
def copy_table(src, dest, table_name):
    """

    :param src: source DB connection string
    :param dest: destination DB connection string
    :param table_name:
    :return:
    """
    # Connect to DB
    log.info("Connecting to source DB")
    e1 = engine.create_engine(src)
    log.info(e1.execute('SELECT "Connected."').scalar())
    log.info("Connecting to destination DB")
    e2 = engine.create_engine(dest)
    log.info(e2.execute('SELECT "Connected."').scalar())

    # Find `Table` obj for table_name
    table = None
    for cls in SBase.__subclasses__():
        if cls.__table__.name == table_name:
            table = cls.__table__
            break
    if table is None:
        raise RuntimeError(f"Table {table} not found among known mapped classes")

    # Copy
    rows = e1.execute(table.select())
    log.info(f"Copying {rows.rowcount} {table}")
    e2.execute(table.insert().prefix_with("IGNORE"), *rows)
