import time


def fail():
    """
    A test job which always fails.
    """
    raise RuntimeError("This job always fails")


def sleep(n=1):
    """
    A test job which sleeps for `n` seconds.

    :param n:
    """
    time.sleep(n)
