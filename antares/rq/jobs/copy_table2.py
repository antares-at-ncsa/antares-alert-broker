from sqlalchemy import MetaData

from antares import log, rq
from antares.sql.engine import get_engine, sessionmaker


def copy_table(
    src_engine, src_table, dest_engine, dest_table, keys=None, batch_size=1000
):
    """
    Copy all rows from one table to another in batches using RQ.

    :param src_engine: source engine name
    :param src_table: source table name
    :param dest_engine: destination engine name
    :param dest_table: destination table name
    :param keys: optional list of foreign key columns
    :param batch_size: default to 1000
    :return:
    """
    # Get source number of rows
    query = f"""
        SELECT COUNT(*)
        FROM {src_table}
    """
    log.info("Counting rows in source table...")
    n = get_engine(src_engine).execute(query).scalar()
    log.info("Table %s has %s rows", src_table, n)

    # Enqueue the copy jobs
    log.info("Will now enqueue ~%s copy jobs...", n // batch_size)
    jobs = []
    q = rq.get_queue()
    offset = 0
    while True:
        job = q.enqueue(
            copy_rows,
            kwargs=dict(
                src_engine=src_engine,
                src_table=src_table,
                dest_engine=dest_engine,
                dest_table=dest_table,
                offset=offset,
                limit=batch_size,
                keys=keys,
            ),
            job_timeout=12 * 60 * 60,  # 12 hours
        )
        jobs.append(job)
        offset += batch_size
        if offset >= n:
            break
    log.info("Scheduled %s jobs", len(jobs))


def copy_rows(src_engine, src_table, dest_engine, dest_table, offset, limit, keys=None):
    """
    Copy rows from one table to another.

    :param src_engine: source engine name
    :param src_table: source table name
    :param dest_engine: destination engine name
    :param dest_table: destination table name
    :param offset:
    :param limit:
    :param keys: optional list of foreign key columns
    """
    # Connect to databases
    src_engine = get_engine(src_engine)
    dest_engine = get_engine(dest_engine)

    # Get source table schema
    metadata = MetaData(src_engine)
    metadata.reflect(src_engine, only=[src_table])
    assert src_table in metadata.tables

    # Get names of primary key columns, if not given.
    if keys is None:
        keys = [
            column.name for column in metadata.tables[src_table].primary_key.columns
        ]

    # Get data
    query = (
        metadata.tables[src_table].select().order_by(*keys).limit(limit).offset(offset)
    )
    data = src_engine.execute(query)
    data = list(data)
    if not data:
        return

    # Get destination table schema
    metadata = MetaData(dest_engine)
    metadata.reflect(dest_engine, only=[dest_table])
    assert dest_table in metadata.tables

    # Insert
    table = metadata.tables[dest_table]
    session = sessionmaker(bind=dest_engine, autoflush=False)()
    session.execute("set autocommit=0;")
    session.execute("set ndb_use_transactions=0;")
    query = table.insert().prefix_with("IGNORE")
    session.execute(query, data)
    session.commit()
    session.close()
