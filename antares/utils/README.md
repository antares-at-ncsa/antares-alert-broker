
# antares.utils

Helper functions used by other parts of `antares`.

Code in `antares.utils`:
- May import from `.`
- May import `antares.log` and `antares.services.metrics`
- May not import anything else from `antares`
