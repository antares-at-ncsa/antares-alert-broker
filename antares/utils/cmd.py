import subprocess


def call(cmd):
    """
    Call a shell command.

    stdout and stderr are returned as `bytes`.

    :return: return_code, stdout, stderr
    """
    proc = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout, stderr = proc.communicate()
    return_code = proc.returncode
    return return_code, stdout, stderr


def out(cmd):
    """
    Return stdout of a command, as a string.
    """
    proc = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout, _ = proc.communicate()
    return stdout.decode("ascii").strip()
