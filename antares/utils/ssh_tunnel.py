import sshtunnel


def init_tunnel(
    ssh_user, ssh_pass, ssh_host, local_addr, local_port, remote_addr, remote_port
):
    """
    Return an SSH tunnel context manager.
    """
    # Open tunnels
    return sshtunnel.SSHTunnelForwarder(
        ssh_address_or_host=(ssh_host, 22),
        ssh_username=ssh_user,
        ssh_password=ssh_pass,
        remote_bind_address=(remote_addr, remote_port),
        local_bind_address=(local_addr, local_port),
    )
