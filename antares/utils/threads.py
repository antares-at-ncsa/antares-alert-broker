import threading
import time

from antares import log


def new_thread(f, *a, **kw):
    """
    Open a new thread and execute f(*a, **kw) in it.
    """
    t = threading.Thread(target=f, args=a, kwargs=kw)
    t.start()
    return t


class ThreadLoop(object):
    """
    Call a function repeatedly in a thread.
    """

    def __init__(self, fn, period, initial_delay=0):
        """
        Return a new ThreadLoop object.

        :param fn: a callable
        :param period: delay between executons of `fn`
        :param initial_delay: initial delay before first execution
        """
        self._fn = fn
        self._period = period
        self._initial_delay = initial_delay
        self._thread = None
        self._running = False
        self._sleep_time = 1

    def start(self):
        self._running = True

        def run():
            t_next = time.time() + self._initial_delay
            while self._running:
                if time.time() >= t_next:
                    try:
                        self._fn()
                    except Exception:
                        log.stacktrace()
                    t_next = time.time() + self._period
                time.sleep(self._sleep_time)

        self._thread = new_thread(run)

    def stop(self):
        self._running = False
        self._thread.join()
