import warnings

import numpy as np
import pandas as pd

from antares.pipeline.locus import Locus


def _is_var_star(
    df: pd.DataFrame, match_radius_arcsec=1.0, star_galaxy_threshold=0.4
) -> bool:
    """
    Returns a boolean indicating if the locus is a variable star.

    Parameters
    ----------
    df: pd.DataFrame
        Locus timeseries dataframe
    match_radius_arcsec : float, default 1.5
        Upper bound of matching radius (in arcsec) used to find the counterpart of the
        locus in PS1 and in the ZTF template
    star_galaxy_threshold : float, 0.4
        Lower bound of star-galaxy score of the PS1 counterpart. Value closer to 1
        indicates a star.

    """
    # NOTE: there are cases where there may be a ps1 nn but not on ZTF
    # (duh, ps1 is deeper) so also using distnr
    return (
        np.median(df["ztf_distpsnr1"][np.isfinite(df["ztf_distpsnr1"])])
        < match_radius_arcsec
        and np.median(df["ztf_distnr"][np.isfinite(df["ztf_distnr"])])
        < match_radius_arcsec
        and np.median(df["ztf_sgscore1"][np.isfinite(df["ztf_sgscore1"])])
        > star_galaxy_threshold
    )


def correct_mags(locus: Locus) -> pd.DataFrame:
    df = locus.timeseries.to_pandas()
    is_var_star = _is_var_star(df)
    df["ant_mag_corrected"] = np.full(len(df), np.NaN)
    df["ant_magerr_corrected"] = np.full(len(df), np.NaN)
    df["ant_magulim_corrected"] = np.full(len(df), np.NaN)
    df["ant_magllim_corrected"] = np.full(len(df), np.NaN)

    if not is_var_star:
        return df, is_var_star, False

    # Determine replacements for missing parameter values
    alert_groups = df.groupby(["ztf_fid", "ztf_field", "ztf_rcid"])
    # Numpy throws a runtime warning if you take the median of an empty slice, we can
    # safely ignore it.
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        impute_magnr = alert_groups["ztf_magnr"].agg(
            lambda x: np.median(x[np.isfinite(x)])
        )
        impute_sigmagnr = alert_groups["ztf_sigmagnr"].agg(
            lambda x: np.median(x[np.isfinite(x)])
        )
    for i, alert_group in alert_groups:
        w = np.isnan(alert_group["ztf_magnr"])
        w2 = alert_group[w].index
        df.loc[w2, "ztf_magnr"] = impute_magnr[i]
        df.loc[w2, "ztf_sigmagnr"] = impute_sigmagnr[i]

    df["sign"] = 2 * ((df["ztf_isdiffpos"] == "t") | (df["ztf_isdiffpos"] == "1")) - 1
    u = 10 ** (-0.4 * df["ztf_magnr"]) + (df["sign"] * 10 ** (-0.4 * df["ztf_magpsf"]))

    # Note: quite oddly we might encounter negative 'total' fluxes. In which case
    # we return uncorrected.
    if (u < 0.0).any():
        # TODO: Add notification
        return df, is_var_star, False

    df["ant_mag_corrected"] = -2.5 * np.log10(u)
    df["ant_magerr_corrected"] = (
        np.sqrt(
            (10 ** (-0.4 * df["ztf_magnr"]) * df["ztf_sigmagnr"]) ** 2.0
            + (10 ** (-0.4 * df["ztf_magpsf"]) * df["ztf_sigmapsf"]) ** 2.0
        )
        / u
    )

    # NOTE: upper limits are missing field and rcid before Jan 1, 2019.
    # We are imputing the magnr
    for fid in np.unique(df["ztf_fid"].values):
        # Compute the median value of magnr and sigmagnr for this filter band
        mask_measured = np.isfinite(df["ztf_magnr"]) & (df["ztf_fid"] == fid)
        # Numpy throws a runtime warning if you take the median of an empty slice, we can
        # safely ignore it.
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            impute_magnr = np.median(df.loc[mask_measured, "ztf_magnr"].values)
            impute_sigmagnr = np.median(df.loc[mask_measured, "ztf_sigmagnr"].values)
        # Fill any NaN magnr and sigmagnr values for this filter band w/ the median
        mask_empty = ~np.isfinite(df["ztf_magnr"]) & (df["ztf_fid"] == fid)
        df.loc[mask_empty, "ztf_magnr"] = impute_magnr
        df.loc[mask_empty, "ztf_sigmagnr"] = impute_sigmagnr

    df["ant_magulim_corrected"] = -2.5 * np.log10(
        10 ** (-0.4 * df["ztf_magnr"]) + 10 ** (-0.4 * df["ztf_diffmaglim"])
    )
    df["ant_magllim_corrected"] = -2.5 * np.log10(
        10 ** (-0.4 * df["ztf_magnr"]) - 10 ** (-0.4 * df["ztf_diffmaglim"])
    )
    return df, is_var_star, True
