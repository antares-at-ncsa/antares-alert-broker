from typing import List, Iterator, Set, Tuple

import htm

from antares import log
from antares.config import config


def get_region(
    ra: float, dec: float, radius: float, level: int = 20
) -> List[Tuple[int, int]]:
    """
    Return a list of HTM (min, max) ranges describing a circular region.

    :param ra: RA coordinate of center of circle
    :param dec: Dec coordinate of center of circle
    :param radius: Radius in degrees
    :param level: level of HTM to compute (eg. 16, 20, etc.)
    :return: list of HTM (min, max) pairs.
    """
    log.debug("get_region(%s, %s, %s, %s)", ra, dec, radius, level)
    try:
        region = htm.get_htm_circle_region(ra, dec, radius, level=level)
    except htm.PrecisionError:
        region = htm.get_htm_circle_region(
            ra, dec, radius + config.HTM_PRECISION_ERROR_RETRY_OFFSET, level=level
        )
    return region


def get_region_denormalized(
    ra: float, dec: float, radius: float, level: int
) -> List[int]:
    """
    Get or compute a search region as a full list of htm values.

    Where get_region returns something like [(20, 22), (24, 25)]), this
    function would yield from the series (20, 21, 22, 24, 25).

    :yield: iterable of htm ids
    """

    def f():
        for htm_min, htm_max in get_region(ra, dec, radius, level):
            assert htm_min <= htm_max
            for h in range(htm_min, htm_max + 1):
                yield h

    return list(f())


def get_htm(ra: float, dec: float, level: int = 20) -> int:
    """
    Compute HTM 20 id of a point

    :param ra:
    :param dec:
    :param level:
    :return:
    """
    log.debug("get_htm(%s, %s, %s)", ra, dec, level)
    return htm.get_htm_id(ra, dec, level)


def get_htm20(ra: float, dec: float) -> int:
    return get_htm(ra, dec, 20)


def denormalize_region(region):
    """
    Yield all individual HTMs in a region.

    >>> list(denormalize_region([(1, 1), (5, 6), (7, 10)]))
    [1, 5, 6, 7, 8, 9, 10]

    :param region: list of (min, max) pairs
    :return: iterator over all individual HTMs
    """
    result = []
    for htm_min, htm_max in region:
        assert htm_min <= htm_max
        for h in range(htm_min, htm_max + 1):
            result.append(h)
            if len(result) > 100:
                raise RuntimeError("HTM region is larger than 100 trixels")
    return result


def determine_optimal_htm_level(radius: float, choices: Set[int]) -> int:
    """
    Based on a given list of `choices`, select the best HTM level at which to
    index a circle of `radius`.

    :param radius: radius in degrees
    :param choices: set of HTM levels
    :return: htm_level
    """
    # TODO: compute a reasonable HTM Level using some proper algorithm.
    # This is a "good enough" heuristic.
    # This function only supports a single set of choices
    assert choices == {4, 6, 8, 10, 12, 14, 16}

    htm_level = 16
    if radius > 0.001:
        htm_level = 14
    if radius > 0.005:
        htm_level = 12
    if radius > 0.02:
        htm_level = 10
    if radius > 0.05:
        htm_level = 8
    if radius > 0.2:
        htm_level = 6
    if radius > 1.0:
        htm_level = 4
    return htm_level
