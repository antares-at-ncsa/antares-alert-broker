"""

A simple in-memory cache which is threadsafe and supports key expiry.

"""
import time

import wrapt

from antares.services import metrics

EXPIRE_CHECK_INTERVAL = 60

_cache = {}
_expires = {}
_next_expire_check = 0


@wrapt.synchronized
def get(key, fn, expiry=600):
    if expiry is None:
        expiry = float("inf")

    # Expire old values from cache
    _do_expire()

    # Compute value, if not present in cache
    if key not in _cache:
        _cache[key] = fn()
        _expires[key] = time.time() + expiry

    metrics.gauge("cache.num_keys", len(_cache))

    return _cache[key]


def clear():
    global _cache
    _cache = {}
    global _expires
    _expires = {}
    global _next_expire_check
    _next_expire_check = 0


def _do_expire():
    global _next_expire_check

    # Expire old values
    if time.time() >= _next_expire_check:
        to_delete = []
        for key, expiry in _expires.items():
            if expiry <= time.time():
                to_delete.append(key)
        for key in to_delete:
            del _cache[key]
            del _expires[key]
        _next_expire_check = time.time() + EXPIRE_CHECK_INTERVAL
