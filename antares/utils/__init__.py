import datetime
import hashlib
import json
import math
import random

from astropy import units as u
from astropy.coordinates import SkyCoord, Angle

import numpy as np

from antares.utils.cmd import call


def nearest(p1, objs, key=None, max_angle=None):
    """
    Return the index of the point in `points` that is nearest to `p1`.

    All points must be (ra, dec) tuples in fractional degrees.

    >>> nearest((0, 0), [(0, 1), (1, 1), (0.5, 0.5)])
    (0.5, 0.5)
    >>> nearest((0, 0), [(1, 1)])
    (1, 1)
    >>> nearest((0, 0), [(1, 1)], max_angle=0.5) is None
    True

    :param p1: (ra, dec) tuple
    :param objs: objects to select the nearest if
    :param key: func which maps an element of `objs` to an (ra, dec) tuple.
    :param max_angle: maximum angle to consider, in degrees.
    :return: index of nearest point in `points` to `p1`
    """
    if not objs:
        return None
    if key is None:

        def key(x):
            return x

    min_angle = float("inf")
    nearest = None
    if max_angle is not None:
        max_angle = Angle(max_angle * u.degree)

    p1 = SkyCoord(ra=p1[0], dec=p1[1], unit=u.degree)
    for o in objs:
        ra, dec = key(o)
        p = SkyCoord(ra=ra, dec=dec, unit=u.degree)
        angle = p1.separation(p)
        if max_angle is not None and angle > max_angle:
            continue
        if angle < min_angle:
            min_angle = angle
            nearest = o

    return nearest


def angle_between(p1, p2):
    """
    Return angle in degrees between two (ra, dec) points.

    >>> angle_between((0, 0), (0, 1))
    1.0

    :param p1: tuple like (ra, dec) in degrees
    :param p2: tuple like (ra, dec) in degrees
    :return: angle in degrees
    """
    p1 = SkyCoord(ra=p1[0] * u.degree, dec=p1[1] * u.degree)
    p2 = SkyCoord(ra=p2[0] * u.degree, dec=p2[1] * u.degree)
    angle = p1.separation(p2)
    return angle.degree


def seconds_until_end_of_utc_day():
    """
    Compute number of seconds until end of current UTC day.

    :return: int
    """
    # All times here are in UTC
    utc_now = datetime.datetime.utcnow()
    last_midnight = utc_now.replace(hour=0, minute=0, second=0, microsecond=0)
    this_midnight = last_midnight + datetime.timedelta(days=1)
    delta_until_midnight = this_midnight - utc_now
    return delta_until_midnight.total_seconds()


def unindent(s):
    """
    Unindent a block of text.

    Indentation is determined from the number of spaces at beginning of first
    non-empty line. All lines then have that many characters removed.

    >>> unindent("    foo")
    'foo'
    >>> unindent('    foo\\n        bar\\n')
    'foo\\n    bar\\n'
    >>> unindent('\\n    foo')
    '\\nfoo'

    :param s: multi-line string of text
    :return: un-indented version
    """
    lines = s.split("\n")
    first_line = None
    for line in lines:
        if line.strip():
            first_line = line
            break
    if first_line is None:
        return ""
    n_spaces = 0
    for char in first_line:
        if char == " ":
            n_spaces += 1
        else:
            break
    return "\n".join(line[n_spaces:] for line in lines)


def md5(s):
    """
    Get hex str MD5 sum of a str or bytes.
    """
    if isinstance(s, str):
        s = s.encode("utf8")
    assert isinstance(s, bytes)
    return hashlib.md5(s).hexdigest()


def md5_int(s, n_bytes=16):
    """
    Get integer MD5 sum of a str or bytes.
    """
    return int(md5(s), 16) % 2 ** (n_bytes * 8)


def group_by(iterable, key):
    """
    Group objects into a dict of lists by a key.

    :param iterable:
    :param key: attribute name, or a function
    :return: dict
    """
    get_key = key if callable(key) else lambda x: getattr(x, key)
    grouped = {}
    for obj in iterable:
        grouped.setdefault(get_key(obj), []).append(obj)


def format_dt(utc_datetime):
    """
    A system-wide standard timestamp format.
    """
    return utc_datetime.isoformat(sep=" ")


def timestamp():
    return format_dt(datetime.datetime.utcnow())


def python_pkg_manifest():
    """
    Get current Python package versions
    """
    code, out, _ = call("pip list --format json")
    assert code == 0
    return json.loads(out)


def jd_to_mjd(jd):
    """
    Convert JD to MJD.
    """
    return jd - 2400000.5


def mjd_to_jd(mjd):
    """
    Convert MJD to JD.
    """
    return mjd + 2400000.5


def mjd_to_dt(mjd):
    """
    Convert MJD to Datetime.
    """
    from astropy.time import Time

    return Time([mjd], format="mjd")[0].to_datetime()


def dt_to_mjd(utc_datetime):
    """
    Convert UTC Datetime to MJD.
    """
    from astropy.time import Time

    return Time([utc_datetime.isoformat()], format="isot", scale="utc")[0].mjd


def randfloat(min_, max_):
    """
    Return a random float from a range.
    """
    assert max_ >= min_
    return (random.random() * (max_ - min_)) + min_


def get_centroid(points):
    """
    Return the centroid of a list of (ra, dec) points.

    :param points: list of (ra, dec) points, in units of degrees.
    :return: (ra, dec) tuple, in units of degrees.
    """
    # Compute x, y, z vectors
    x, y, z = [], [], []
    for (ra, dec) in points:
        assert 0 <= ra <= 360
        assert -90 <= dec <= 90
        ra = math.radians(ra)
        dec = math.radians(dec)
        x.append(math.cos(dec) * math.cos(ra))
        y.append(math.cos(dec) * math.sin(ra))
        z.append(math.sin(dec))

    # Compute the mean xyz vector of all input xyz vectors
    x, y, z = np.mean(x), np.mean(y), np.mean(z)

    # Convert xyz vector to ra/dec in degrees
    r = np.linalg.norm(np.array([x, y, z]))  # Norm (length) of xyz vector
    ra = math.degrees(math.atan2(y, x))
    dec = 90 - math.degrees(math.acos(z / r))

    # Final range checking
    assert -180 <= ra <= 180
    if ra < 0:
        ra += 360
    assert 0 <= ra <= 360
    assert -90 <= dec <= 90
    if dec == -90 or dec == 90:
        # ra is undefined when dec is -90 or 90.
        ra = 0
    return ra, dec
