import contextlib
import datetime
import heapq
import threading
import time

import pythonping

from antares import log, redis
from antares.config import config
from antares.pipeline import base_properties, output_types, ingestion, output
from antares.pipeline.filters.load import get_filters
from antares.pipeline.filters.runnable_filter import RunnableFilter
from antares.pipeline.ingestion.ztf import ztf_candidate_to_antares_alert
from antares.pipeline.locus import LocusData, HaltPipeline, PropertyKeyError
from antares.pipeline.output import to_index
from antares.pipeline.output.trigger import OutputTrigger
from antares.rtdb.api import RTDB, DuplicateLociDetected, ZTFObjectIDOverlap
from antares.services import metrics, errors, slack
from antares.services.trace import tracer
from antares.sql.schema import SFilterCrashLog
from antares.utils import cache, cone_search, types
from antares.utils import get_centroid


class RegionLockTimeout(Exception):
    pass


class AlertJob:
    def __init__(self, raw_alert, kafka_msg_timestamp, time_received):
        """
        The object which is passed into the Pipeline.

        :param raw_alert: ZTF alert as a dict
        :param kafka_msg_timestamp: UNIX timestamp
        :param time_received: UNIX timeetamp
        """
        self.raw_alert = raw_alert
        self.kafka_msg_timestamp = kafka_msg_timestamp
        self.time_received = time_received


class Pipeline(object):

    REGION_LOCK_TIME = 30
    REGION_LOCK_EXPIRY = 0
    HISTORY_LOOKBACK_DAYS = None
    MERGE_DUPLICATE_LOCI = True
    RAISE_DUPLICATE_LOCI = False
    LOCUS_AGG_USES_HISTORY_CENTROID = True
    LOCUS_AGG_USES_ZTF_OBJECT_ID = True

    def __init__(self, provenance_id, kafka_producer=None):
        self._provenance_id = provenance_id
        self._processed_at = None
        self._l1 = FilterSequence(1)
        self._l2 = FilterSequence(2)
        self._kafka_producer = kafka_producer

        self._compute_base_locus_properties = RunnableFilter(
            base_properties.ComputeLocusProperties()
        )
        self._compute_base_locus_properties.setup()

        self._compute_base_alert_properties = RunnableFilter(
            base_properties.ComputeAlertProperties()
        )
        self._compute_base_alert_properties.setup()

        self._region_lock = None

        self._output_triggers = []
        for o in config.OUTPUTS:
            t = OutputTrigger(**o)
            log.debug('Loaded OutputTrigger "%s"', t.name)
            self._output_triggers.append(t)

        self.alert_prop_type_cache = output_types.AlertPropertyCache()

    def filter_manifest(self):
        """
        Return a manifest of current filters.

        :return: list of dicts
        """
        return self._l1.manifest() + self._l2.manifest()

    def process_alert(self, alert_job):
        """
        Process an AlertJob object.

        :param alert_job: AlertJob class
        """
        assert alert_job.raw_alert
        ztf_alert = alert_job.raw_alert
        kafka_msg_timestamp = alert_job.kafka_msg_timestamp

        # Record our stream latency
        delta = time.time() - kafka_msg_timestamp
        metrics.timing("pipeline.kafka_msg_age_on_ingest", delta)

        self._processed_at = datetime.datetime.utcnow()
        t1 = time.time()
        try:
            alerts = self._create_alert_objects(ztf_alert)

            # Note: _process_alert() is responsible for calling
            # _acquire_region_lock(). The lock is not requested
            # here in process_alert() because the base properties
            # have not been computed yet, so Ra/Dec coordinates
            # are not defined.

            self._process_alert(alerts, alert_job)
            t2 = time.time()
            metrics.timing("pipeline.process_alert", t2 - t1)
        finally:
            self._release_region_lock()
        log.debug("Done processing alert")
        self._processed_at = None

    def _acquire_region_lock(self, ra: float, dec: float, timeout: int = 300) -> None:
        # The self._region_lock should be None.
        # If it is not None, then the last Alert which this Pipeline object
        # processed failed to clean up after itself. This can occur when
        # the connection to Redis is lost during processing. If Redis is down,
        # then there's nothing we can do to heal this case. Just crash and
        # bootloop until Redis is available again.
        assert self._region_lock is None

        # Determine region to lock
        region = cone_search.get_region_denormalized(
            ra=ra,
            dec=dec,
            radius=config.LOCUS_AGGREGATION_RADIUS_DEGREES,
            level=RTDB.LOCUS_HTM_LEVEL,
        )
        lock = redis.MultiLock(region, expire=self.REGION_LOCK_TIME)
        if len(region) > 100:
            metrics.warn("large_locus_search_region")

        # Poll for the lock
        first_attempt = True
        wait = 0.1  # 100ms
        backoff_factor = 1.5
        stop_time = time.time() + timeout
        while True:
            if lock.lock():
                self._region_lock = lock
                return  # Success!
            else:
                if first_attempt:
                    metrics.increment("pipeline.failed_region_lock")
                    first_attempt = False
                if time.time() > stop_time:
                    break
                time.sleep(min(wait, stop_time - time.time()))
                wait *= backoff_factor
                continue
        raise RegionLockTimeout

    def _release_region_lock(self):
        if self._region_lock is not None:
            if self.REGION_LOCK_EXPIRY == 0:
                self._region_lock.release()
            else:
                self._region_lock.expire(self.REGION_LOCK_EXPIRY)
            self._region_lock = None

    @tracer.wrap("pipeline", service="pipeline", resource="Process Alert")
    def _process_alert(self, input_alerts, alert_job):
        """
        :param input_alerts: list of Alert
        :param alert_job: AlertJob object
        """
        rtdb = RTDB()

        # Compute Alert base properties
        with tracer.trace("pipeline.stage", resource="Compute Base Alert Properties"):
            for a in input_alerts:
                ld = LocusData([a], alert_job=alert_job)
                trace = self._compute_base_alert_properties.run(ld)
                if trace:
                    log.info(trace)
                    raise RuntimeError(
                        "Failed to compute Alert base properties:\n" + trace
                    )
                for name, value in a.properties.items():
                    self.alert_prop_type_cache.create_or_update(
                        name, types.type_str(value), "ZTF"
                    )

        alert = input_alerts[-1]
        alert_id = alert.alert_id
        assert ingestion.is_ztf_candidate(alert_id)
        if self.LOCUS_AGG_USES_HISTORY_CENTROID:
            points = [
                (a.properties["ant_ra"], a.properties["ant_dec"])
                for a in input_alerts
                if a.properties.get("ant_ra") is not None
                and a.properties.get("ant_dec") is not None
            ]
            assert points
            ra, dec = get_centroid(points)
            del points
        else:
            ra = alert.properties["ant_ra"]
            dec = alert.properties["ant_dec"]
        mjd = alert.mjd
        assert mjd is not None

        # Metrics
        metrics.gauge("pipeline.ra", ra)
        metrics.gauge("pipeline.dec", dec)

        # Lock will be released when _process_alert() returns.
        log.debug("Acquiring region lock")
        with tracer.trace("pipeline.stage", resource="Acquire Region Lock"):
            with metrics.timed("pipeline.acquire_region_lock"):
                with errors.report("acquire_region_lock", dump=alert):
                    self._acquire_region_lock(ra, dec)

        # run L1 filters
        log.debug("Running L1 Filters")
        with tracer.trace("pipeline.stage", resource="L1 Filters"):
            ld1 = LocusData(input_alerts, alert_job=alert_job)
            halt = self._l1.run(ld1)
            if halt:
                log.debug("L1 halt")
            if halt and not config.PIPELINE_KEEP_EVERYTHING:
                metrics.increment("pipeline.skip_alert", tags=["reason:l1_filters"])
                return
            del ld1

        # locus assignment
        log.debug("Running Locus Association")
        with tracer.trace("pipeline.stage", resource="Locus Association"):
            locus = None
            ztf_object_id = None
            if self.LOCUS_AGG_USES_ZTF_OBJECT_ID:
                ztf_object_id = alert_job.raw_alert.get("objectId")
            with metrics.timed("pipeline.get_locus"):
                try:
                    check_duplicates = (
                        self.MERGE_DUPLICATE_LOCI or self.RAISE_DUPLICATE_LOCI
                    )
                    locus = rtdb.get_or_create_locus(
                        ra=ra,
                        dec=dec,
                        alert_mjd=mjd,
                        ztf_object_id=ztf_object_id,
                        check_duplicates=check_duplicates,
                    )
                except DuplicateLociDetected as e:
                    if self.MERGE_DUPLICATE_LOCI:
                        try:
                            locus = self._merge_duplicate_loci(rtdb, e.ra, e.dec)
                        except ZTFObjectIDOverlap:
                            # We can't merge this.
                            # Just get the nearest Locus and don't check for duplicates.
                            locus = rtdb.get_or_create_locus(
                                ra=ra,
                                dec=dec,
                                alert_mjd=mjd,
                                ztf_object_id=ztf_object_id,
                                check_duplicates=False,
                            )
                    if self.RAISE_DUPLICATE_LOCI:
                        raise
            assert locus is not None
            del ztf_object_id
            locus_id = locus.locus_id
            for a in input_alerts:
                a.locus_id = locus_id

        # load existing alert history
        log.debug("Loading Alert History")
        with tracer.trace("pipeline.stage", resource="Load Alert History"):
            with metrics.timed("pipeline.get_alerts"):
                existing_alerts = rtdb.get_alerts(locus_id)
            existing_alert_ids = {a.alert_id for a in existing_alerts}

        # If all alerts in `input_alerts` already exist, skip
        input_alert_ids = [a.alert_id for a in input_alerts]
        assert None not in input_alert_ids
        if set(input_alert_ids) - set(existing_alert_ids) == set():
            log.debug("Skipping duplicate Alert")
            metrics.increment("pipeline.skip_alert", tags=["reason:duplicate"])
            return
        # Assert that there is at least one new, non-duplicate Alert.
        assert [a for a in input_alerts if a.alert_id not in existing_alert_ids]

        # Use L1 filters to filter out bad Alerts from `input_alerts`
        # Merge two sorted lists together to create `aggregates_alerts`
        log.debug("Aggregating Alert History")
        aggregated_alerts = list(
            heapq.merge(
                existing_alerts,
                (
                    a
                    for a in input_alerts
                    if a.alert_id not in existing_alert_ids  # Prevent duplicates
                    and (
                        config.PIPELINE_KEEP_EVERYTHING
                        or not self._l1.run(LocusData([a]))
                    )
                ),
                key=lambda a: a.mjd,
            )
        )
        # assert that `aggregated_alerts` is in increasing time sequence
        for i in range(len(aggregated_alerts) - 1):
            assert aggregated_alerts[i].mjd <= aggregated_alerts[i + 1].mjd

        # catalog association
        log.debug("Catalog Association")
        with tracer.trace("pipeline.stage", resource="Catalog Association"):
            with metrics.timed("pipeline.get_catalog_objects"):
                catalog_objects = rtdb.get_catalog_objects(locus.ra, locus.dec)

        # watched object association
        log.debug("Watch Object Association")
        with tracer.trace("pipeline.stage", resource="Watch Object Association"):
            # watched object association
            with metrics.timed("pipeline.search_watch_objects"):
                wo_matches = rtdb.search_watch_objects(locus.ra, locus.dec)
            for (wl_id, wo_id) in wo_matches:
                locus.wl_ids.add(wl_id)
                locus.wo_ids.add(wo_id)

        # create L2 LocusData
        log.debug("Creating L2 Locus Data")
        ld2 = LocusData(aggregated_alerts, alert_job=alert_job)
        ld2.locus = locus
        ld2.catalog_objects = catalog_objects
        _ = ld2.api().timeseries  # Ensure API timeseries is pre-populated

        # compute base Locus Properties
        log.debug("Computing base locus properties")
        with tracer.trace("pipeline.stage", resource="Compute Base Locus Properties"):
            trace = self._compute_base_locus_properties.run(ld2)
            if trace:
                log.info(trace)
                raise RuntimeError("Failed to compute Locus base properties:\n" + trace)

        # run L2 filters
        log.debug("Running L2 filters")
        with tracer.trace("pipeline.stage", resource="L2 Filters"):
            if not halt:
                self._l2.run(ld2)

        # Update Locus in DB
        log.debug("Updating locus")
        with tracer.trace("pipeline.stage", resource="Update Locus"):
            with metrics.timed("pipeline.update_locus"):
                locus.lightcurve = ld2.api().lightcurve_csv
                locus.updated_at = self._processed_at
                rtdb.update_locus(locus)

        # Recompute Locus position
        log.debug("Moving locus centroid")
        with tracer.trace("pipeline.stage", resource="Update Locus Centroid"):
            with metrics.timed("pipeline.move_locus"):
                ra2, dec2 = get_centroid(
                    [
                        (a.properties["ant_ra"], a.properties["ant_dec"])
                        for a in aggregated_alerts
                        if ingestion.is_ztf_candidate(a.alert_id)
                    ]
                )
                rtdb.move_locus(locus_id, locus.ra, locus.dec, ra2, dec2)

        # Outputs to Kafka, Slack
        log.debug("Outputting to kafka, slack")
        with tracer.trace("pipeline.stage", resource="Tag Notifications"):
            self._do_outputs(ld2)

        # WatchObject notifications
        log.debug("Watch object notifications")
        with tracer.trace("pipeline.stage", resource="Watch List Notifications"):
            if config.SLACK_WATCH_LIST_ENABLE:
                for (wl_id, wo_id) in wo_matches:
                    wl = rtdb.get_watch_list(wl_id)
                    if wl["slack_channel"]:
                        wo = rtdb.get_watch_object(wl_id, wo_id)
                        output.watch_object(ld2, wl, wo)

        # Determine which Alerts are new (ie. need to be stored now)
        new_alert_objs = [
            a for a in input_alerts if a.alert_id not in existing_alert_ids
        ]

        # Store new Alert(s)
        log.debug("Store new alerts")
        assert new_alert_objs
        with tracer.trace("pipeline.stage", resource="Store New Alerts"):
            with metrics.timed("pipeline.put_alerts"):
                rtdb.put_alerts(new_alert_objs)

        # Send locus to Archive via Kafka
        with tracer.trace("pipeline.stage", resource="Send to Index Worker"):
            self._send_to_index_worker(ld2)

    def _create_alert_objects(self, ztf_alert):
        """
        Construct Alert objects from ZTF alert.

        Returns list of Alert objects in increasing time sequence,
        with the current (aka new) alert at the end of the list.

        :param ztf_alert: ZTF alert
        :return: list of Alert
        """
        ztf_object_id = ztf_alert["objectId"]
        assert ztf_object_id

        alerts = []
        alert = ztf_candidate_to_antares_alert(ztf_alert["candidate"], ztf_object_id)
        if ztf_alert.get("prv_candidates"):
            log.debug("Alert has %s prv_candidates)", len(ztf_alert["prv_candidates"]))
            for prv_candidate in ztf_alert["prv_candidates"]:
                alerts.append(
                    ztf_candidate_to_antares_alert(prv_candidate, ztf_object_id)
                )
        else:
            log.debug("Alert has no prv_candidates)")
        alerts.append(alert)

        # assert that `alerts` is in increasing time sequence
        for i in range(len(alerts) - 1):
            assert alerts[i].mjd <= alerts[i + 1].mjd

        for alert in alerts:
            alert.processed_at = self._processed_at
        return alerts

    def _send_to_index_worker(self, ld):
        topic = config.ARCHIVE_INDEX_KAFKA_TOPIC_NAME
        blob = to_index.dumps_from_locusdata(ld)
        self._kafka_producer.produce(topic, blob)

    def _do_outputs(self, ld):
        assert self._kafka_producer is not None
        slack_channels = []
        kafka_topics = []
        for t in self._output_triggers:
            if t.check(ld):
                log.debug("Output trigger!")
                if t.is_slack():
                    assert t.channel
                    slack_channels.append(t.channel)
                elif t.is_kafka():
                    assert t.topic
                    kafka_topics.append(t.topic)

        # Send to slack channels
        if slack_channels:
            with errors.report("do_slack_output"):
                output.to_slack(ld, set(slack_channels))

        # Send to Kafka topics
        if kafka_topics:
            with errors.report("do_kafka_output"):
                output.to_kafka(ld, self._kafka_producer, set(kafka_topics))

    def _merge_duplicate_loci(self, rtdb, ra, dec):
        """
        Check for duplicate Loci around an (ra, dec) point, perform merging
        if needed, and return the single Locus which should now be used.

        This function should be called by the Pipeline whenever RTDB.get_locus()
        raises a DuplicateLociDetected error.

        The ra/dec parameters given should be those of one of the suspected
        duplicate Loci.

        :param rtdb:
        :param ra:
        :param dec:
        :return: LocusModel
        """
        assert ra is not None
        assert dec is not None

        # Perform the merge of Loci
        locus, replaced_loci = rtdb.merge_duplicate_loci(ra, dec)

        # Each locus in `replaced_loci` needs to be updated in ES.
        for l in replaced_loci:
            l.wl_ids = set()
            l.wo_ids = set()
            for (wl_id, wo_id) in rtdb.search_watch_objects(locus.ra, locus.dec):
                l.wl_ids.add(wl_id)
                l.wo_ids.add(wo_id)
            ld2 = LocusData([])
            ld2.locus = l
            ld2.catalog_objects = rtdb.get_catalog_objects(l.ra, l.dec)
            self._send_to_index_worker(ld2)

        return locus


class FilterSequence(object):
    """
    An ordered list of filter filters which can run() on a locus_data.
    """

    def __init__(self, level, filters=None):
        assert level in (1, 2)
        self._level = level
        self._disabled_filter_v_ids = set()
        self._disable_on_crash = None
        self._instrument = None
        self._raise_filter_errors = None

        if filters:
            # For use in tests.
            self._filters = filters
        else:
            # Load filters from DB and initialize them
            self._filters = get_filters(level)
            for f in self._filters:
                log.info(f'Calling setup() on filter "{f.name}"')
                trace = f.setup()
                if trace:
                    log.error(trace)
                    raise RuntimeError(f'setup() failed on filter "{f.name}"')

    def manifest(self):
        return [
            dict(
                filter_id=f.filter_id,
                name=f.name,
                level=f.level,
                priority=f.priority,
            )
            for f in self._filters
        ]

    def run(
        self,
        locus_data,
        disable_on_crash=config.PIPELINE_DISABLE_CRASHED_STAGES,
        instrument=True,
        raise_filter_errors=config.PIPELINE_RAISE_STAGE_ERRORS,
    ):
        """
        Run filters. Public interface of FilterSequence.

        :param locus_data:
        :param disable_on_crash:
        :param instrument:
        :param raise_filter_errors:
        :return: True if a filter raised HaltPipeline, else False
        """
        self._disable_on_crash = disable_on_crash
        self._instrument = instrument
        self._raise_filter_errors = raise_filter_errors
        try:
            self._run_filters(locus_data)
        except HaltPipeline:
            return True
        return False

    def _run_filters(self, locus_data):
        """
        Execute science filters and catch errors.

        Raises HaltPipeline.
        """
        for f in self._filters:
            if f.filter_version_id in self._disabled_filter_v_ids:
                log.debug("Skipping disabled filter {}".format(f.name))
                continue
            log.debug("Running filter {}.".format(f.name))

            if f.get_config("RUN_IN_MUTEX"):
                # Run in a process-global mutex
                key = f"filter_mutex_{f.filter_id}"
                lock = cache.get(key, fn=threading.Lock, expiry=None)
            else:
                # Run without a mutex
                lock = contextlib.nullcontext()  # A NoOp context manager

            with lock:
                locus_data.set_current_filter(f)
                with tracer.trace("pipeline.filter", resource=f"Filter: {f.name}"):
                    self._run_filter(f, locus_data)
                locus_data.set_current_filter(None)

    def _run_filter(self, f, locus_data):
        """
        Run a pipeline filter.

        :param f:
        :param locus_data:
        """
        # Determine whether to run the filter or not
        for p in f.get_config("INPUT_LOCUS_PROPERTIES"):
            if p not in locus_data.locus.properties:
                log.debug(f"Skipping filter {f.name} missing Locus property {p}")
                return
        for p in f.get_config("INPUT_ALERT_PROPERTIES"):
            if p not in locus_data.alert.properties:
                log.debug(f"Skipping filter {f.name} missing Alert property {p}")
                return

        # Instrumentation
        tags = [
            f"filter_name:{f.name}",
            # f'level:{self._level}',
        ]
        if self._instrument:
            timer = metrics.timed("pipeline.run_filter", tags=tags)
        else:
            timer = contextlib.nullcontext()  # NoOp context manager

        # Execute filter
        try:
            with timer:
                stacktrace = f.run(locus_data)
        except PropertyKeyError:
            log.stacktrace()
            log.error("filter threw PropertyKeyError. Skipping filter gracefully.")
            metrics.increment("filters.property_key_error", tags=tags)
            return  # Skip quietly
        except HaltPipeline:
            log.debug("Halt signal from filter {}".format(f.name))
            raise
        if stacktrace:
            log.warn('filter %s "%s" crashed', f.filter_id, f.name)
            log.warn(stacktrace)
            self._handle_filter_crash(f, stacktrace, locus_data)

    def _handle_filter_crash(self, f, stacktrace, locus_data):
        if self._instrument:
            metrics.error("filter")
        log_id = self._log_filter_crash(f, locus_data, stacktrace)
        if self._disable_on_crash:
            log.warn("Disabling f {}".format(f.name))
            self._disabled_filter_v_ids.add(f.filter_version_id)
            with errors.report("mark_filter_disabled"):
                f.disable(log_id)
        if self._raise_filter_errors:
            raise RuntimeError('filter %s "%s" crashed', f.filter_id, f.name)

    @staticmethod
    def _log_filter_crash(f, locus_data, stacktrace):
        alert_id = locus_data.alert_id
        locus_dict = locus_data.to_dict()
        log.info("locus_dict:")
        log.info(locus_dict)

        # Save crash log
        log_id = SFilterCrashLog.save(
            filter_id=f.filter_id,
            filter_version_id=f.filter_version_id,
            locus_id=locus_data.locus_id,
            alert_id=alert_id,
            stacktrace=stacktrace,
            locus_dict=locus_dict,
        )

        # Notify user
        notification_channel = f.get_config("ERROR_SLACK_CHANNEL")
        if notification_channel:
            with errors.report("slack"):
                msg = f"Filter `{f.name}` crashed on Alert `{alert_id}`."
                msg += f" Crash log ID: {log_id}"
                slack.post(msg, channel=notification_channel, async_=False)

        # Throttle number of Slack messages to filter_errors channel
        if not redis.Lock("error_filter_crash", stacktrace, expire=5 * 60).lock():
            return

        # Post stacktrace to Slack filter errors channel
        alert_link = "http://antares.noirlab.edu/alerts/data/{}".format(alert_id)
        msg = (
            f"{config.ENVIRONMENT} Filter `{f.filter_id}` `{f.name}` crashed on"
            f" Alert <{alert_link}|{alert_id}>:\n```\n{stacktrace.strip()}\n```"
            f" Crash log ID: {log_id}"
        )
        slack.post(msg, channel=config.SLACK_FILTER_ERRORS_CHANNEL, async_=False)

        return log_id
