import base64
import math

from antares import utils, log
from antares.config import config
from antares.redis import redis, Lock
from antares.rtdb.schema import CStorage
from antares.services import metrics


class LocusIDFactory:
    """
    Generate system-wide unique Locus IDs.
    """

    def __init__(self):
        self.year = None
        self.counter = None

    def _init_counter(self, year):
        assert len(year) == 4
        self.year = year
        self.counter = ThreadsafeCounter(f"counter:locus_id:{year}")

    def get(self, alert_mjd, exists):
        """
        Get a new, unique locus_id.

        Optionally the locus_d

        :param alert_mjd: MJD timestamp of alert observation
        :param exists: fn(locus_id) --> True if locus_id already exists
        :return: locus_id
        """
        # Get two-digit year code from the alert MJD
        assert alert_mjd
        year = utils.mjd_to_dt(alert_mjd).year
        assert isinstance(year, int)
        year = str(year)
        assert len(year) == 4

        # Check that the counter is using the right year, else re-init it
        if self.year != year:
            self._init_counter(year)
            assert self.year == year

        # Generate integer ID
        with metrics.timed("pipeline.generate_locus_id"):
            n = self.counter.incr()
        s = i_to_s(n)  # Convert int to base32 string
        assert s_to_i(s) == n  # Sanity check

        # Generate locus_id
        locus_id = f"ANT{year}{s}"
        log.debug(f"Generated locus_id {locus_id}")

        # Check it does not already exist
        if exists(locus_id):
            # Should not happen.
            # remedial action? Increment counter by BACKUP_INTERVAL?
            raise RuntimeError(f'locus_id "{locus_id}" already exists')

        return locus_id


def i_to_s(i):
    """
    int to b32 string
    """
    assert i >= 1
    n = math.ceil(math.log2(i + 1) / 8)  # Number of Bytes needed to represent
    b = i.to_bytes(n, byteorder="big", signed=False)
    s = base64.b32encode(b)
    s = s.strip(b"=")
    return s.decode("ascii").lower()


def s_to_i(s):
    """
    b32 string to int
    """
    s = s.upper().encode("ascii")
    while len(s) % 8:
        s += b"="
    b = base64.b32decode(s)
    i = int.from_bytes(b, byteorder="big", signed=False)
    return i


class ThreadsafeCounter:

    # Interval at which to do backup.
    # Should be at least 10 times higher than peak alert rate per second.
    BACKUP_INTERVAL = 10 * 1000

    def __init__(self, key, initial_value=1):
        self.key = key
        self.initial_value = initial_value

        # Assemble the increment LUA script
        self.incr_script = utils.unindent(
            f"""
            if redis.call("exists", "{key}") == 1 then
                return redis.call("incr", "{key}")
            else
                return
            end
        """
        ).strip()

    def incr(self):
        """
        Increment the counter and return new value.
        """
        new_value = self._incr()

        if new_value is None:
            # The counter is not set.
            # It needs to be loaded from backup.
            with Lock(self.key):
                if redis.exists(self.key):
                    # While we were waiting for the Lock, another process
                    # populated the key.
                    new_value = self._incr()
                    assert new_value is not None
                    return new_value
                else:
                    # It is this process' responsibility to set up the counter.
                    loaded_value = self._load_backup()
                    if loaded_value is None:
                        # If there is no saved backup,
                        # then we're incrementing the counter
                        # for the very first time.
                        redis.set(self.key, self.initial_value)
                        self._store_backup(self.initial_value)
                        return self.initial_value
                    else:
                        # Otherwise, a value was restored from backup.
                        # Increment the restored value by BACKUP_INTERVAL to
                        # account for possible lost increments.
                        new_value = loaded_value + self.BACKUP_INTERVAL
                        redis.set(self.key, new_value)
                        self._store_backup(new_value)
                        return new_value

        elif new_value % self.BACKUP_INTERVAL == 0:
            # Every BACKUP_INTERVAL increments, make a backup:
            with Lock(self.key):
                value = redis.get(self.key)
                assert value is not None
                self._store_backup(int(value))

        return new_value

    def _incr(self):
        """
        Increment counter and return new value if it is set, else return None.
        """
        n = redis.eval(self.incr_script, 0)
        if n is not None:
            return int(n)

    def _store_backup(self, value):
        """
        Store the given value in Cassandra.
        """
        assert isinstance(value, int)
        CStorage.put(self.key, str(value))

    def _load_backup(self):
        """
        Get saved value from Cassandra, or None if it doesn't exist.
        """
        value = CStorage.get(self.key)
        if value is not None:
            return int(value)

    def _drop_backup(self):
        """
        For use in tests.
        """
        assert config.ENVIRONMENT == "dev"
        CStorage.delete_(self.key)

    def _drop_from_redis(self):
        """
        For use in tests.
        """
        assert config.ENVIRONMENT == "dev"
        redis.delete(self.key)
