import json
from io import StringIO
import warnings

import numpy.ma as ma

warnings.filterwarnings("ignore", "elementwise == comparison")
warnings.filterwarnings("ignore", "converted column")
from astropy.timeseries import TimeSeries

from antares import log
from antares.rtdb import api
from antares.rtdb.models import AlertModel, LocusModel
from antares.services import metrics
from antares.services.trace import tracer
from antares.utils import mjd_to_dt


class HaltPipeline(Exception):
    pass


class PropertyKeyError(KeyError):
    pass


class PropertyOverwriteError(KeyError):
    pass


class PropertiesDictIsReadOnly(Exception):
    pass


class PropertiesDict(object):
    """
    Provides an API to Filters for Alert or Locus properties.

    This class wraps a `properties` dict (from an Alert or a Locus)
    and allows Filters to get and set properties. The `setter` function is
    responsible for validation and for setting properties.
    """

    def __init__(self, properties, setter, read_only=False):
        self._properties = properties
        self._setter = setter
        self._read_only = read_only

    def get(self, key, default=None):
        return self._properties.get(key, default)

    def keys(self):
        return self._properties.keys()

    def __contains__(self, item):
        return item in self._properties

    def __str__(self):
        return f"PropertiesDict({dict(self)})"

    __repr__ = __str__

    def __getitem__(self, name):
        return self._properties[name]

    def __setitem__(self, key, value):
        if self._read_only:
            raise PropertiesDictIsReadOnly
        return self._setter(key, value)

    # Allow object-style access to properties.
    #
    # def __getattr__(self, key):
    #     return self._properties[key]
    #
    # def __setattr__(self, name, value):
    #     if name.startswith('_'):
    #         return super().__setattr__(name, value)
    #     if self._read_only:
    #         raise ReadOnlyPropertiesDict
    #     return self._setter(name, value)


class LocusData:
    """
    Encapsulates data associated with a Locus as it moves down the pipeline.
    """

    LOCUS_OBJ_ATTRS = {"locus_id", "ra", "dec"}
    ALERT_OBJ_ATTRS = {"alert_id", "locus_id", "mjd"}

    def __init__(self, alerts, alert_job=None):
        self.current_filter = None
        self.locus = None
        self.alerts = alerts
        self.catalog_objects = None
        self.alert_job = alert_job

        # Used only for logging, debugging.
        self.new_locus_properties = {}
        self.new_alert_properties = {}
        self.new_tags = []

        self._api = None

    def set_current_filter(self, f):
        """
        :param f: RunnableFilter
        """
        self.current_filter = f

    @property
    def current_filter_name(self):
        if self.current_filter:
            return self.current_filter.name
        return "Unknown"

    @property
    def locus_id(self):
        return str(self.locus.locus_id) if self.locus else None

    @property
    def alert(self):
        return self.alerts[-1]

    @property
    def alert_id(self):
        return self.alert.alert_id

    def api(self):
        if self._api is None:
            self._api = Locus(self)
        return self._api

    def set_locus_property(self, name, value):
        if name in self.LOCUS_OBJ_ATTRS:
            raise RuntimeError(f'Property "{name}" is immutable.')
        if name in self.locus.properties:
            log.debug(f'Overwriting Locus Property "{name}"')
        if self.current_filter is None:
            raise RuntimeError("Cannot set Properties outside of a Filter context.")
        value = self.current_filter.validate_locus_property(name, value)
        if value is None:
            return
        self.locus.properties[name] = value
        self.new_locus_properties[name] = value

        # Logging and metrics
        filter_name = self.current_filter_name
        metrics.increment(
            "pipeline.locus_data.set_locus_property",
            tags=[f"name:{name}", f"filter_name:{filter_name}"],
        )
        log.debug("Stage %s set property %s = %s", filter_name, name, value)

    def set_alert_property(self, name, value):
        if name in self.ALERT_OBJ_ATTRS:
            raise RuntimeError(f'Property "{name}" is immutable.')
        if name in self.alert.properties:
            raise PropertyOverwriteError(f'Property "{name}" already exists.')
        if self.current_filter is None:
            raise RuntimeError("Cannot set Properties outside of a Filter context.")
        value = self.current_filter.validate_alert_property(name, value)
        if value is not None:
            self.alert.properties[name] = value
            self.new_alert_properties[name] = value
        if value is None:
            self.alert.properties.pop(name, None)
            self.new_alert_properties.pop(name, None)

        # Update the Locus API's Timeseries object if needed.
        if self._api and self._api._timeseries:
            ts = self._api._timeseries
            if name in ts.columns:
                ts[name][-1] = value
            else:
                arr = [None] * len(ts)
                arr[-1] = value
                ts[name] = arr

        # Logging and metrics
        filter_name = self.current_filter_name
        metrics.increment(
            "pipeline.locus_data.set_alert_property",
            tags=[f"name:{name}", f"filter_name:{filter_name}"],
        )
        log.debug("Stage %s set property %s = %s", filter_name, name, value)

    @staticmethod
    def from_rtdb(locus_id=None, alert_id=None, include_newer=False):
        """
        Load a LocusData from the DB by locus_id or alert_id.

        :param locus_id:
        :param alert_id:
        :param include_newer: include Alerts newer than the specified alert_id
        :return: LocusData
        """
        if (locus_id is None) + (alert_id is None) != 1:
            raise RuntimeError("Must specify exactly one of locus_id or alert_id")
        rtdb = api.RTDB()
        alert_mjd = None
        if alert_id:
            alert = rtdb.get_alert(alert_id)
            if not alert:
                raise RuntimeError(f'Alert not found: "{alert_id}"')
            locus_id = alert["locus_id"]
            alert_mjd = alert.mjd
            del alert
        del alert_id
        assert locus_id

        # Get Locus
        locus = rtdb.get_locus_by_id(locus_id)
        if not locus:
            raise RuntimeError(f'Locus not found: "{locus_id}"')

        # Get Alerts
        alerts = rtdb.get_alerts(locus_id)
        if not alerts:
            raise RuntimeError(f'Locus "{locus_id}" exists but no Alerts found.')
        if alert_mjd and not include_newer:
            alerts = [a for a in alerts if a.mjd <= alert_mjd]

        # Construct LocusData
        ld = LocusData(alerts)
        ld.locus = locus
        ld.catalog_objects = rtdb.get_catalog_objects(locus.ra, locus.dec)
        return ld

    def to_dict(self):
        """
        Return a JSON-ready dict representation of the Locus.

        DevKit users can load Locus from a saved file using:

        ``dk.locus_from_dict(filename))``

        :return: JSON-ready dict
        """
        return {
            "locus_id": str(self.locus.locus_id) if self.locus else None,
            "ra": self.locus.ra if self.locus else None,
            "dec": self.locus.dec if self.locus else None,
            "properties": self.locus.properties if self.locus else None,
            "tags": list(self.locus.tags) if self.locus else None,
            "watch_list_ids": list(self.locus.wl_ids) if self.locus else None,
            "watch_object_ids": list(self.locus.wo_ids) if self.locus else None,
            "catalog_objects": self.catalog_objects,
            "alerts": [
                {
                    "alert_id": a.alert_id,
                    "locus_id": a.locus_id,
                    "mjd": a.mjd,
                    "properties": a.properties,
                }
                for a in self.alerts
            ],
        }

    @staticmethod
    def from_dict(data):
        """
        Load a LocusData from a dict.

        :param data: dict
        :return: LocusData
        """
        alerts = [
            AlertModel(
                locus_id=a["locus_id"],
                mjd=a["mjd"],
                alert_id=a["alert_id"],
                processed_at=None,
                properties=a["properties"].copy(),
            )
            for a in data["alerts"]
        ]
        ld = LocusData(alerts)
        ld.locus = LocusModel(
            locus_id=data["locus_id"],
            created_at=None,
            updated_at=None,
            ra=data["ra"],
            dec=data["dec"],
            properties=data["properties"].copy(),
            tags=set(data["tags"]),
            wl_ids=set(data["watch_list_ids"]),
            wo_ids=set(data["watch_object_ids"]),
        )
        ld.catalog_objects = data["catalog_objects"].copy()
        return ld

    def to_file(self, filename):
        """
        Write a JSON representation of the Locus to file.

        DevKit users can load Locus from a saved file using:

        ``dk.locus_from_file(filename))``
        """
        with open(filename, "w") as f:
            json.dump(f, self.to_dict(), indent=4)

    @staticmethod
    def from_file(filename):
        """
        Load a LocusData from a file.

        :param filename:
        :return: LocusData
        """
        with open(filename, "r") as f:
            locus_dict = json.load(f)
        return LocusData.from_dict(locus_dict)


class Alert:
    def __init__(self, alert_id, mjd, properties_dict):
        self.alert_id = alert_id
        self.mjd = mjd
        self.properties = properties_dict


class Locus:
    """
    The object which is passed into Filters.
    """

    def __init__(self, locus_data):
        self._ld = locus_data
        self._timeseries = None

    #
    # Methods and properties identical to Client Locus:
    #

    @property
    def locus_id(self):
        return self._ld.locus.locus_id

    @property
    def ra(self):
        return self._ld.locus.ra

    @property
    def dec(self):
        return self._ld.locus.dec

    @property
    def properties(self):
        """
        Locus properties dict.

        Supports setting new Locus properties and overwriting existing ones.

        eg::

            # Get a Locus Property
            band = locus.properties['ant_passband']

            # Set a Locus Property
            locus.properties['saha_periodicity'] = periodicity

        The schema of the Locus Properties is dynamic, in the sense that new
        properties can be created at any time by the addition of new Filters.

        The list of all Locus properties extant in the system is visible at
        http://antares.noirlab.edu/properties

        :return: dict-like object ``PropertiesDict``
        """
        return PropertiesDict(
            self._ld.locus.properties,
            self._ld.set_locus_property,
        )

    @property
    def tags(self):
        """
        List of Tags which Locus currently has.

        :return: list of str
        """
        return sorted(set(self._ld.locus.tags) | set(self._ld.new_tags))

    @property
    def alerts(self):
        """
        List of all Alerts (including current Alert) on the Locus.

        Alerts returned by this function are read-only.

        Alerts have attributes:

        * alert_id
        * mjd
        * properties

        eg::

            alert_ids = [a.alert_id for a in locus.alerts]

            brightest_mag = min(a.properties['ant_mag'] for a in locus.alerts]

        The schema of the Alert Properties is dynamic, in the sense that new
        properties can be created at any time by the addition of new Filters.

        The list of all Alert properties extant in the system is visible at
        http://antares.noirlab.edu/properties

        :return: list of ``Alert``
        """
        return [
            Alert(
                alert_id=alert.alert_id,
                mjd=alert.mjd,
                properties_dict=PropertiesDict(
                    alert.properties,
                    None,
                    read_only=True,
                ),
            )
            for alert in self._ld.alerts
        ]

    @property
    def alert(self):
        """
        Current (ie. most recent) Alert on the Locus.

        Supports setting new properties on the Alert, but not overwriting.

        ::

            alert = locus.alert

        is equivalent to::

            alert = locus.alerts[-1]

        EXCEPT that only the Alert returned by ``Locus.alert`` is mutable.
        For example, you can::

            if something:
                locus.alert.properties['narayan_classification'] = 'sn1a'

        :return: ``Alert``
        """
        return Alert(
            alert_id=self._ld.alert.alert_id,
            mjd=self._ld.alert.mjd,
            properties_dict=PropertiesDict(
                self._ld.alert.properties,
                self._ld.set_alert_property,
            ),
        )

    @property
    def lightcurve(self):
        """
        An ``astropy.TimeSeries`` of select Alert properties on this Locus.

        Value contains the same data as the `lightcurve` CSV object contained in
        ANTARES' Kafka output Alerts.

        This is a subset of the data returned by `timeseries`.
        """
        COLUMNS = [
            "alert_id",
            "ant_mjd",
            "ant_survey",
            "ant_ra",
            "ant_dec",
            "ant_passband",
            "ant_mag",
            "ant_magerr",
            "ant_maglim",
            "ant_mag_corrected",
            "ant_magerr_corrected",
            "ant_magulim_corrected",
            "ant_magllim_corrected",
        ]
        return self.timeseries[COLUMNS]

    @property
    def timeseries(self):
        """
        An ``astropy.TimeSeries`` of all Alert Properties on this Locus.

        In addition to the Alert Properties, two additional columns are
        included:

        - ant_mag_corrected
        - ant_magerr_corrected

        These two columns contain mag/magerr corrected for variable stars.

        This TimeSeries object can be easily converted to Pandas or Numpy::

            import antares.devkit as dk
            dk.init()

            # Get a random Locus to play with
            locus = dk.get_locus()

            # Get an `astropy.TimeSeries` of all properties
            ts = locus.timeseries
            print(ts)

            # The `astropy.TimeSeries` object can be converted
            # into many other formats if desired.

            # For example, get a `pandas.core.frame.DataFrame`:
            df = locus.timeseries.to_pandas()
            print(df)

            # You can get each column as a separate `pandas.core.series.Series` object:
            alert_id = df['alert_id']
            passband = df['ant_passband']
            print(passband)
            # etc.

            # If you prefer numpy, you can get the timeseries as a `numpy.ndarray`:
            nd = locus.timeseries.to_pandas().to_numpy()
            print(nd)

            # You can also get individual columns as separate `numpy.ndarray` objects:
            ts = locus.timeseries[['alert_id', 'ant_passband']]
            alert_id, passband = ts.to_pandas().to_numpy().T
            print(alert_id)
            print(passband)

        """
        if self._timeseries:
            return self._timeseries

        with tracer.trace("pipeline.locus.build_timeseries"):
            alerts = self._ld.alerts
            with tracer.trace("pipeline.locus.build_timeseries.build_data"):
                data = [
                    {"alert_id": alert.alert_id, **alert.properties} for alert in alerts
                ]
                times = [mjd_to_dt(alert.mjd) for alert in alerts]

            # Create a TimeSeries (without corrected mag values)
            with tracer.trace("pipeline.locus.build_timeseries.init_timeseries"):
                from antares.utils.ztf_flux_correction import correct_mags

                self._timeseries = TimeSeries(data=data, time=times)
                # Add columns `ant_mag_corrected`, `ant_magerr_corrected`
            with tracer.trace("pipeline.locus.build_timeseries.correct_mags"):
                df, _, _ = correct_mags(self)
                assert len(df["ant_mag_corrected"]) == len(self._timeseries)
                self._timeseries["ant_mag_corrected"] = ma.masked_invalid(
                    df["ant_mag_corrected"]
                )
                self._timeseries["ant_magerr_corrected"] = ma.masked_invalid(
                    df["ant_magerr_corrected"]
                )
                self._timeseries["ant_magulim_corrected"] = ma.masked_invalid(
                    df["ant_magulim_corrected"]
                )
                self._timeseries["ant_magllim_corrected"] = ma.masked_invalid(
                    df["ant_magllim_corrected"]
                )

            return self._timeseries

    @property
    def catalog_objects(self):
        """
        Astro catalog objects which match the current Locus.

        Returned data structure is of form::

            {
                catalog_name_x: [obj_x1, obj_x2, ...],
                catalog_name_y: [obj_y1, ...]
                ...
            }

        Where ``obj_*`` objects are dicts representing rows from the catalog
        tables.

        The schema of each catalog is different.
        You can use the Devkit to get a sample containing objects from all ANTARES
        catalogs::

            import antares.devkit as dk
            data = dk.get_sample_catalog_data()

        :return: dict of lists of dicts
        """
        return self._ld.catalog_objects

    @property
    def watch_list_ids(self):
        """
        All WatchList IDs which the Alert matches.

        :return: list of ints
        """
        return set(self._ld.wl_ids or [])

    @property
    def watch_object_ids(self):
        """
        All WatchObject IDs which the Alert matches.

        :return: list of ints
        """
        return set(self._ld.wo_ids or [])

    #
    # Additional methods and properties not available in Client Locus:
    #

    def tag(self, name):
        """
        Tag the Locus.

        :param name: Tag name
        """
        assert self._ld.locus
        self._ld.current_filter.validate_tag(name)
        self._ld.locus.tags.add(name)
        self._ld.new_tags.append(name)
        metrics.increment(
            "pipeline.locus_data.add_tag",
            tags=[f"tag:{name}", f"filter_name:{self._ld.current_filter_name}"],
        )

    @property
    def lightcurve_csv(self):
        """
        Return `self.lightcurve` as a CSV string.
        """
        ts = self.lightcurve
        f = StringIO()
        ts.write(f, format="csv")
        s = f.getvalue()
        log.debug("\n%s", s)
        return s

    @property
    def halt(self):
        """
        Return the HaltPipeline exception class.

        Stages can raise this exception to halt the pipeline like::

          raise locus.halt

        :return: class `HaltPipeline`
        """
        return HaltPipeline

    @property
    def alert_job(self):
        """
        The AlertJob object which first enters the Pipeline.
        """
        return self._ld.alert_job

    @property
    def raw_alert(self):
        """
        The raw Alert data which ANTARES recieved.
        """
        return self._ld.alert_job.raw_alert

    def to_dict(self):
        """
        Return a JSON-ready dict representation of the Locus.

        :return: JSON-ready dict
        """
        return self._ld.to_dict()

    def to_file(self, filename):
        """
        Write a JSON representation of the Locus to file.
        """
        self._ld.to_file(filename)

    def __str__(self):
        return f'LocusDataAPI(locus_id="{self._ld.locus_id}")'

    __repr__ = __str__
