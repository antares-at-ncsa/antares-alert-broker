import numpy as np
import pandas as pd

from antares.pipeline import ingestion
from antares.pipeline.filters.load import Filter
from antares.services.trace import tracer

ZTF_FIDS = {1: "g", 2: "R", 3: "i"}


def _none_if_nan(x):
    """
    Return None if x is NaN, else return x.

    >>> _none_if_nan(3.1)
    3.1
    >>> _none_if_nan(float('nan')) is None
    True

    :param x:
    :return:
    """
    if isinstance(x, float) and np.isnan(x):
        return None
    if x == float("nan"):
        return None
    return x


class ComputeAlertProperties(Filter):
    NAME = "Basic Alert Properties"
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "ant_ra",
            "type": "float",
            "description": "ra coordinate. Defined on ZTF Candidates, but not ZTF Upper Limits.",
            "origin": "ANTARES",
        },
        {
            "name": "ant_dec",
            "type": "float",
            "description": "dec coordinate. Defined on ZTF Candidates, but not ZTF Upper Limits.",
            "origin": "ANTARES",
        },
        {
            "name": "ant_mjd",
            "type": "float",
            "description": "MJD time of observation",
            "origin": "ANTARES",
        },
        {
            "name": "ant_time_received",
            "type": "int",
            "description": "UNIX timestamp when ANTARES received the alert",
            "origin": "ANTARES",
        },
        {
            "name": "ant_input_msg_time",
            "type": "int",
            "description": "UNIX timestamp of input message. eg, Kafka msg timestamp.",
            "origin": "ANTARES",
        },
        {
            "name": "ant_mag",
            "type": "float",
            "description": "Magnitude, if this Alert is a mag measurement type",
            "origin": "ANTARES",
        },
        {
            "name": "ant_magerr",
            "type": "float",
            "description": "Magnitude error, if this Alert is a mag reading type",
            "origin": "ANTARES",
        },
        {
            "name": "ant_passband",
            "type": "str",
            "description": "One of: 'g', 'R', 'i'",
            "origin": "ANTARES",
        },
        {
            "name": "ant_maglim",
            "type": "float",
            "description": "Magnitude upper limit",
            "origin": "ANTARES",
        },
        {
            "name": "ant_survey",
            "type": "int",
            "description": "Origin or type of the Alert. 1 = ZTF Candidate, 2 = ZTF Upper Limit.",
            "origin": "ANTARES",
        },
    ]

    def run(self, locus):
        """
        compute standard properties and add them to an Alert.
        """
        if "ztf_ra" in locus.alert.properties:
            locus.alert.properties["ant_ra"] = locus.alert.properties["ztf_ra"]

        if "ztf_dec" in locus.alert.properties:
            locus.alert.properties["ant_dec"] = locus.alert.properties["ztf_dec"]

        locus.alert.properties["ant_mjd"] = locus.alert.mjd
        locus.alert.properties["ant_time_received"] = locus.alert_job.time_received
        locus.alert.properties[
            "ant_input_msg_time"
        ] = locus.alert_job.kafka_msg_timestamp

        if "ztf_magpsf" in locus.alert.properties:
            locus.alert.properties["ant_mag"] = locus.alert.properties["ztf_magpsf"]

        if "ztf_sigmapsf" in locus.alert.properties:
            locus.alert.properties["ant_magerr"] = locus.alert.properties[
                "ztf_sigmapsf"
            ]

        if "ztf_fid" in locus.alert.properties:
            locus.alert.properties["ant_passband"] = ZTF_FIDS[
                locus.alert.properties["ztf_fid"]
            ]

        if "ztf_diffmaglim" in locus.alert.properties:
            locus.alert.properties["ant_maglim"] = locus.alert.properties[
                "ztf_diffmaglim"
            ]

        locus.alert.properties["ant_survey"] = ingestion.get_survey(
            locus.alert.alert_id
        )


class ComputeLocusProperties(Filter):
    NAME = "Basic Locus Properties"
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "brightest_alert_id",
            "type": "str",
            "description": "Brightest mag reading Alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "brightest_alert_magnitude",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Magnitude of the brightest alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "brightest_alert_observation_time",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Observation time (MJD) of the brightest alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "newest_alert_id",
            "type": "str",
            "description": "Most recent Alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "newest_alert_magnitude",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Magnitude of the most recent alert on this Locus",
            "origin": "ANTARES",
        },
        {
            "name": "newest_alert_observation_time",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Observation time (MJD) of the most recent alert on this Locus",
            "origin": "ANTARES",
        },
        {
            "name": "num_alerts",
            "type": "int",
            "description": "Number of total Alerts on a Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "num_mag_values",
            "type": "int",
            "description": "Number of magnitude readings on a Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "oldest_alert_id",
            "type": "str",
            "description": "Oldest alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "oldest_alert_magnitude",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Magnitude of the oldest alert on this Locus.",
            "origin": "ANTARES",
        },
        {
            "name": "oldest_alert_observation_time",
            "es_mapping": {
                "type": "double",
            },
            "type": "float",
            "description": "Observation time (MJD) of the oldest alert on this Locus",
            "origin": "ANTARES",
        },
        {
            "name": "ztf_ssnamenr",
            "type": "str",
            "es_mapping": {
                "type": "keyword",
            },
            "description": "Name of nearest known solar system object (from MPC archive)",
            "origin": "ANTARES",
        },
        {
            "name": "ztf_object_id",
            "type": "str",
            "es_mapping": {
                "type": "keyword",
            },
            "description": "ZTF Object ID",
            "origin": "ANTARES",
        },
        {
            "name": "replaced_by",
            "type": "str",
            "es_mapping": {
                "type": "keyword",
            },
            "description": "Locus ID which replaces this one, if any.",
            "origin": "ANTARES",
        },
    ]

    @tracer.wrap("pipeline.filter", resource="Filter: Base Locus Properties")
    def run(self, locus):
        """
        Compute base Locus Properties.
        """
        locus.properties["ztf_object_id"] = locus.raw_alert["objectId"]
        if locus.alert.properties.get("ztf_ssnamenr"):
            locus.properties["ztf_ssnamenr"] = locus.alert.properties["ztf_ssnamenr"]

        # brightest mag, num_mags
        mag = locus.timeseries["ant_mag"]
        locus.properties["num_alerts"] = len(mag)
        locus.properties["num_mag_values"] = sum(1 for m in mag if _none_if_nan(m))

        # We convert the locus timeseries to a pandas data frame for ease of use.
        # All the properties that we generate with it expect to be candidate
        # observations so we drop upper limits. There is also a chance that a
        # locus may have multiple alerts with identical MJDs, and the conversion
        # from a `astropy.TimeSeries` to a `pandas.DataFrame` makes the `time`
        # field the index of the resultant data frame. We want each row to have
        # a unique index so we reset the index after we drop all the alerts
        # without the `ant_mag` property (i.e. those that are upper limits).
        with tracer.trace("pipeline.to_pandas"):
            df = locus.timeseries.to_pandas()
        df = df[df["ant_mag"].notnull()].reset_index(drop=True)
        if not df.empty:
            # First alert in the timeseries dataframe
            oldest_alert = df.iloc[0]
            locus.properties["oldest_alert_id"] = oldest_alert.alert_id
            locus.properties["oldest_alert_magnitude"] = oldest_alert.ant_mag
            locus.properties["oldest_alert_observation_time"] = oldest_alert.ant_mjd
            # Last alert in the timeseries dataframe
            newest_alert = df.iloc[-1]
            locus.properties["newest_alert_id"] = newest_alert.alert_id
            locus.properties["newest_alert_magnitude"] = newest_alert.ant_mag
            locus.properties["newest_alert_observation_time"] = newest_alert.ant_mjd
            # Alert with the largest "ant_mag" in the timeseries dataframe
            brightest_alert = df.loc[df["ant_mag"].idxmin()]
            assert isinstance(brightest_alert, pd.Series)
            locus.properties["brightest_alert_id"] = brightest_alert.alert_id
            locus.properties["brightest_alert_magnitude"] = brightest_alert.ant_mag
            locus.properties[
                "brightest_alert_observation_time"
            ] = brightest_alert.ant_mjd
