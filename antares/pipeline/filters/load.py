"""

Loads SFilters from SQL and implements RunnableFilter on top of them.

"""
import inspect
import traceback

from antares import log
from antares.pipeline.filters.base import Filter
from antares.pipeline.filters.runnable_filter import RunnableFilter
from antares.sql.schema import SFilter, SFilterVersion


def get_filters(level):
    filters = [
        RunnableSFilterVersion(SFilterVersion.get(filter_.enabled_version_id))
        for filter_ in SFilter.get_enabled(level)
    ]
    log.info("Loaded {} enabled level {} filters.".format(len(filters), level))
    return filters


class RunnableSFilterVersion(RunnableFilter):
    def __init__(self, f):
        """
        Implement RunnableFilter from an SFilterVersion DB object.

        :param f: SFilter
        """
        super().__init__(None)
        assert f.filter_id
        assert f.filter_version_id
        assert f.code
        filter_ = SFilter.get(f.filter_id)
        assert filter_.level
        self._filter_id = f.filter_id
        self._filter_version_id = f.filter_version_id
        self._name = SFilterVersion.get_name(f.filter_version_id)
        self._level = filter_.level
        self._priority = filter_.priority
        self._code = (
            "import antares.devkit as dk\n\n"
            + "def print(*a, **kw): pass\n\n"
            + f.code
            + f"\n\n__locals_extractor__.update(locals())"
        )

    def setup(self, update_outputs=True):
        """
        Compile filter code and call setup()

        :return: None on success, else return a stacktrace str
        """
        # Compile the code and extract its `locals()`
        extracted_locals = {}
        try:
            exec(self._code, {"__locals_extractor__": extracted_locals})
        except Exception:
            log.debug("Exception while exec()-ing filter code")
            return self.format_traceback()

        # Find Filter class
        classes = [
            obj
            for obj in extracted_locals.values()
            if inspect.isclass(obj)
            and Filter.__subclasscheck__(obj)
            and obj is not Filter
        ]
        if not classes:
            log.error(classes)
            raise RuntimeError("No Filter subclasses found in code")
        if len(classes) > 1:
            log.error(classes)
            raise RuntimeError("Multiple Filter subclasses found in code")
        (cls,) = classes
        del classes

        # Instantiate filter class
        self._filter = cls()
        self._validate_filter_config()
        if update_outputs:
            self._update_output_types()
        self._load_files()

        # Call setup()
        try:
            self._filter.setup()
        except Exception:
            log.debug("Exception in setup()")
            return self.format_traceback()

    def format_traceback(self):
        return format_traceback(self._code, traceback.format_exc(), self._name)

    @property
    def filter_id(self):
        return self._filter_id

    @property
    def filter_version_id(self):
        return self._filter_version_id

    @property
    def name(self):
        return self._name

    @property
    def level(self):
        return self._level

    @property
    def priority(self):
        return self._priority

    def disable(self, log_id):
        SFilterVersion.disable(self._filter_version_id, log_id)

    def get_config(self, name):
        return getattr(self._filter, name)

    def __str__(self):
        return f"<RunnableSFilterVersion {self._name}>"

    __repr__ = __str__


def format_traceback(code, traceback, file_name):
    """
    Format a traceback which originated from eval()'d code.

    When using eval(), tracebacks don't include the source code lines,
    and the file name shows up as '<string>'.

    This function places the source code lines into the stacktrace,
    making it readable as if it originated from normal Python code.

    :param code: raw code in which the exception occurred
    :param traceback: string of traceback as reported by traceback.format_exc()
    :param file_name: name to replace '<string>' in traceback
    :return: str
    """
    lines = traceback.split("\n")
    out_lines = []
    call_stack_line_header = 'File "<string>", line '
    for line in lines:
        line_num = None
        if line.strip().startswith(call_stack_line_header):
            line_num = int(
                line.strip()[len(call_stack_line_header) :].split()[0].strip(",")
            )
        if line_num is None:
            out_lines.append(line)
            continue
        line = line.replace('File "<string>", line', f'Filter "{file_name}", line')
        out_lines.append(line)
        err_line = code.split("\n")[line_num - 1].strip()
        out_lines.append("    " + err_line)
    output = "\n".join(out_lines)
    return output
