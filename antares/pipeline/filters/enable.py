"""

Business logic lives here to enable a filter version.

This consists of:
- checking that filter has been validated
- creating (or updating) properties and tags in SQL
- marking the filter as enabled
- marking other versions as disabled

"""

# TODO
