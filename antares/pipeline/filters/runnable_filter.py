import re
import traceback

from antares import log
from antares.pipeline import output_types
from antares.pipeline.locus import HaltPipeline
from antares.sql.schema import SStorage


class FilterConfigError(Exception):
    pass


class RunnableFilter(object):
    def __init__(self, f):
        """

        :param f: Filter
        """
        self._filter = f

    def setup(self, update_outputs=True):
        self._validate_filter_config()
        if update_outputs:
            self._update_output_types()
        self._load_files()

        # Call setup()
        try:
            self._filter.setup()
        except Exception:
            log.debug("Caught Exception")
            return traceback.format_exc()

    def _update_output_types(self):
        """
        Ensure that output types defined in Filter match those in SQL.

        Should be called once per filter on startup.
        """
        for dic in self.get_config("OUTPUT_LOCUS_PROPERTIES"):
            output_types.create_or_update_locus_property(
                name=dic["name"],
                type=dic["type"],
                origin=dic.get("origin", "Filter"),
                fv_id=self.filter_version_id,
                description=dic["description"],
                es_mapping=dic.get("es_mapping"),
            )
        for dic in self.get_config("OUTPUT_ALERT_PROPERTIES"):
            output_types.create_or_update_alert_property(
                name=dic["name"],
                type=dic["type"],
                origin=dic.get("origin", "Filter"),
                fv_id=self.filter_version_id,
                description=dic["description"],
                es_mapping=dic.get("es_mapping"),
            )
        for dic in self.get_config("OUTPUT_TAGS"):
            output_types.create_or_update_tag(
                name=dic["name"],
                fv_id=self.filter_version_id,
                description=dic["description"],
            )

    def _load_files(self):
        self._filter.files = {
            name: SStorage.get_data(name) for name in self._filter.REQUIRES_FILES or []
        }

    def run(self, locus_data):
        """
        Execute filter.

        :param locus_data: LocusData (not LocusDataAPI)
        :return: None on success, else return a stacktrace str
        """
        lda = locus_data.api()
        locus_data.set_current_filter(self)
        try:
            self._filter.run(lda)
            return
        except HaltPipeline:
            raise
        except Exception:
            log.debug("Caught Exception")
            return self.format_traceback()
        finally:
            locus_data.set_current_filter(None)

    def format_traceback(self):
        return traceback.format_exc()

    def validate_locus_property(self, name, value):
        for prop_dict in self._filter.OUTPUT_LOCUS_PROPERTIES:
            if prop_dict["name"] == name:
                value = _validate_property_value(value)
                if value is not None:
                    value = _validate_property_type(value, prop_dict["type"])
                return value
        raise RuntimeError("Undeclared Locus property")

    def validate_alert_property(self, name, value):
        for prop_dict in self._filter.OUTPUT_ALERT_PROPERTIES:
            if prop_dict["name"] == name:
                value = _validate_property_value(value)
                if value is not None:
                    value = _validate_property_type(value, prop_dict["type"])
                return value
        raise RuntimeError("Undeclared Alert property")

    def validate_tag(self, name):
        if name not in {dic["name"] for dic in self._filter.OUTPUT_TAGS}:
            raise RuntimeError("Undeclared Tag")

    def _validate_filter_config(self):
        for dic in self._filter.OUTPUT_LOCUS_PROPERTIES:
            if not dic.get("name"):
                raise FilterConfigError('OUTPUT_LOCUS_PROPERTIES must have a "name"')
            if not dic.get("type"):
                raise FilterConfigError('OUTPUT_LOCUS_PROPERTIES must have a "type"')
            if not dic.get("description"):
                raise FilterConfigError(
                    'OUTPUT_LOCUS_PROPERTIES must have a "description"'
                )
            _validate_property_name(dic["name"])
            _validate_property_type_definition(dic["type"])
        for dic in self._filter.OUTPUT_ALERT_PROPERTIES:
            if not dic.get("name"):
                raise FilterConfigError('OUTPUT_ALERT_PROPERTIES must have a "name"')
            if not dic.get("type"):
                raise FilterConfigError('OUTPUT_ALERT_PROPERTIES must have a "type"')
            if not dic.get("description"):
                raise FilterConfigError(
                    'OUTPUT_ALERT_PROPERTIES must have a "description"'
                )
            _validate_property_name(dic["name"])
            _validate_property_type_definition(dic["type"])
        for dic in self._filter.OUTPUT_TAGS:
            if not dic.get("name"):
                raise FilterConfigError('OUTPUT_TAGS must have a "name"')
            if not dic.get("description"):
                raise FilterConfigError('OUTPUT_TAGS must have a "description"')
            _validate_tag_name(dic["name"])

    @property
    def filter_id(self):
        return None

    @property
    def filter_version_id(self):
        return None

    @property
    def name(self):
        return self._filter.NAME

    @property
    def level(self):
        return None

    @property
    def priority(self):
        return None

    def disable(self, log_id):
        raise NotImplementedError

    def get_config(self, name):
        assert name == name.upper()
        assert not name.startswith("_")
        return getattr(self._filter, name)

    def __str__(self):
        return "<RunnableFilter {}>".format(self.name)

    __repr__ = __str__


VALID_PROPERTY_TYPES = {"int", "float", "str"}


def _validate_property_type_definition(type_str):
    if type_str not in VALID_PROPERTY_TYPES:
        raise FilterConfigError(
            f'Output property type "{type_str}" must be one of {VALID_PROPERTY_TYPES}'
        )


def _validate_filter_name(name):
    assert bool(re.match(r"^[a-zA-Z][a-zA-Z0-9_\-. ]*$", name))
    assert len(name) <= 30


PROPERTY_NAME_REGEX = r"^[a-z][a-z0-9_]*$"
TAG_NAME_REGEX = r"^[a-z][a-z0-9_]*$"


def _validate_property_name(name):
    if not re.match(PROPERTY_NAME_REGEX, name):
        raise FilterConfigError(
            f'Property names must match regex "{PROPERTY_NAME_REGEX}"'
        )
    if not len(name) <= 100:
        raise FilterConfigError(f"Property names must be <= 100 characters long")


def _validate_tag_name(name):
    if not re.match(TAG_NAME_REGEX, name):
        raise FilterConfigError(f'Tag names must match regex "{TAG_NAME_REGEX}"')
    if not len(name) <= 100:
        raise FilterConfigError(f"Tag names must be <= 100 characters long")
    # Reminder:
    # Kafka topic names have a maximum length of 249 characters
    assert len(name) <= 249


def _validate_property_value(value):
    if value is None:
        return None
    if isinstance(value, float):
        value = float(value)  # Convert numpy floats to vanilla floats
    if isinstance(value, int):
        value = int(value)  # Convert numpy ints to vanilla ints
    if value == float("nan"):
        return None
    assert not isinstance(value, bool)
    assert isinstance(value, (str, int, float)), type(value)
    assert value not in {float("+inf"), float("-inf")}
    return value


def _validate_property_type(value, type_str):
    _validate_property_type_definition(type_str)
    if type_str == "int":
        return int(value)
    if type_str == "float":
        return float(value)
    if type_str == "str":
        assert isinstance(value, str)
        return value
    raise RuntimeError("Unexpected type_str")
