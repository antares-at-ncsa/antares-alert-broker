class Filter:
    """
    ANTARES Filter base class.
    """

    FILTER_ID = None
    FILTER_VERSION_ID = None
    NAME = None
    ERROR_SLACK_CHANNEL = None
    RUN_IN_MUTEX = False
    REQUIRES_FILES = [
        # 'cstubens_myfile.txt',
    ]

    INPUT_LOCUS_PROPERTIES = [
        # 'num_mag_values',
    ]
    INPUT_ALERT_PROPERTIES = [
        # 'mag',
    ]
    INPUT_TAGS = [
        # 'foo',
    ]

    OUTPUT_LOCUS_PROPERTIES = [
        # {
        #     'name': 'foo',
        #     'type': 'int',
        #     'description': 'blah blah',
        # },
    ]
    OUTPUT_ALERT_PROPERTIES = [
        # {
        #     'name': 'foo',
        #     'type': 'int',
        #     'description': 'blah blah',
        # },
    ]
    OUTPUT_TAGS = [
        # {
        #     'name': 'bar',
        #     'description': 'blah blah',
        # },
    ]

    def __init__(self):
        self.files = {}  # Loaded by FilterWrapper

    def setup(self):
        pass

    def run(self, locus_data):
        raise NotImplementedError

    @classmethod
    def get_config_names(cls):
        return [
            name
            for name in cls.__dict__
            if not name.startswith("_") and name == name.upper()
        ]
