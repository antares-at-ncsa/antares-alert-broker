from antares.devkit.get_data import get_locus_ids
from antares.pipeline.locus import LocusData
from antares.sql.schema import SFilterVersion

from .load import RunnableSFilterVersion


def validate(filter_v_id):
    """
    Validate an SFilterVersion

    :param filter_v_id:
    :return: stacktrace (str) or None
    """
    f = RunnableSFilterVersion(SFilterVersion.get(filter_v_id))
    trace = f.setup(update_outputs=False)
    if trace:
        return "Error during setup():\n\n" + trace
    locus_ids = get_locus_ids(10)
    assert locus_ids
    for locus_id in locus_ids:
        trace = f.run(LocusData.from_rtdb(locus_id=locus_id))
        if trace:
            return f"Error during testing.\nLocus: {locus_id}\n\n" + trace
