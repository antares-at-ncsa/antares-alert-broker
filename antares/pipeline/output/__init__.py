from urllib.parse import urljoin

from antares.config import config
from antares.pipeline.output import kafka_alerts
from antares.rtdb.api import WatchListDict, WatchObjectDict
from antares.services import slack


def to_slack(ld, channels):
    for channel in channels:
        slack.post(_format_msg(ld), channel)


def to_kafka(ld, producer, topics):
    payload_bytes = kafka_alerts.dumps_from_locusdata(ld)
    for topic in topics:
        producer.produce(topic, payload_bytes)


def watch_object(ld, wl: WatchListDict, wo: WatchObjectDict):
    wl_url = urljoin(config.FRONTEND_BASE_URL, f'watch-lists/{wl["wl_id"]}')
    wl_link = f"<{wl_url}|{wl['name']}>"
    msg = (
        f"Hit on watch list {wl_link}:"
        + "\n"
        + f"Object: {wo['name']} ({wo['ra']}, {wo['dec']})"
        + "\n"
        + f"{_format_msg(ld)}"
    )
    return slack.post(msg, wl["slack_channel"])


def _format_msg(ld):
    locus_link = urljoin(config.FRONTEND_BASE_URL, f"loci/{ld.locus_id}")

    p = ld.alert.properties
    ztf_magpsf = p.get("ztf_magpsf", "Unknown")
    if isinstance(ztf_magpsf, float):
        ztf_magpsf = round(ztf_magpsf, 2)

    msg = (
        "*<{locus_link}|Alert {alert_id}>*  ra=`{ra}` dec=`{dec}`"
        " mag=`{ztf_magpsf}` passband=`{passband}`".format(
            alert_id=ld.alert_id,
            locus_link=locus_link,
            ra=ld.alert.properties["ant_ra"],
            dec=ld.alert.properties["ant_dec"],
            ztf_magpsf=ztf_magpsf,
            passband=p.get("passband", "Unknown"),
        )
    )
    return msg
