import zlib

import bson


def dumps(obj):
    """
    Serialize a Python object to GZIPed BSON.

    :param obj:
    :return:
    """
    return zlib.compress(bson.dumps(obj))


def loads(payload):
    """
    Deserialize a GZIPed BSON message to a Python object.

    :param payload:
    :return:
    """
    return bson.loads(zlib.decompress(payload))
