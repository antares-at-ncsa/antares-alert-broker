import re

from antares import log

SLACK_TYPE = "slack"
KAFKA_TYPE = "kafka"
OUTPUT_TYPES = {
    SLACK_TYPE,
    KAFKA_TYPE,
}

CLAUSE_NEW_TAG = "new_tag"
CLAUSE_HAS_TAG = "has_tag"
CRITERIA_CLAUSES = {
    CLAUSE_NEW_TAG,
    CLAUSE_HAS_TAG,
}


class OutputTrigger:
    """
    Represents a logical trigger which determines whether a LocusData should
    generate an Output alert.

    Also stores the destination of the output: a Slack channel, or Kafka topic.

    Intended to be instantiated from YAML OUTPUTS, eg:

        OUTPUTS:

         - name: Newly tagged Astrorapid objects
           type: slack
           criteria:
             - new_tag: 'astrorapid_\w*'
           channel: astrorapid_new

         - name: All Alerts on Astrorapid-tagged objects
           type: kafka
           criteria:
             - has_tag: 'astrorapid_\w*'
           topic: astrorapid_all

    """

    def __init__(self, name, type, criteria, **kw):
        assert name
        assert type in OUTPUT_TYPES
        assert isinstance(criteria, dict)
        assert criteria
        topic = kw.get("topic", None)
        channel = kw.get("channel", None)
        del kw
        if type == SLACK_TYPE:
            assert channel
        elif type == KAFKA_TYPE:
            assert topic
        assert set(criteria.keys()).issubset(CRITERIA_CLAUSES)
        self.name = name
        self.type = type
        self.criteria = criteria
        self.channel = channel
        self.topic = topic

    def check(self, ld):
        """
        Return True if this OutputTrigger should fire on a `LocusData`.

        :param ld: `LocusData`
        :return: bool
        """
        log.debug('Checking OutputTrigger "%s"', self.name)
        log.debug("Criteria:")
        log.debug(self.criteria)
        for clause, pattern in self.criteria.items():
            if clause == CLAUSE_NEW_TAG:
                if any(re.match(pattern, tag) for tag in ld.new_tags):
                    return True
            elif clause == CLAUSE_HAS_TAG:
                if any(re.match(pattern, tag) for tag in ld.locus.tags):
                    return True
        return False

    def is_slack(self):
        return self.type == SLACK_TYPE

    def is_kafka(self):
        return self.type == KAFKA_TYPE
