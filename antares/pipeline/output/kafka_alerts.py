"""

Serialization/deserialization for sending alerts to kafka.

"""
from antares.config import config

from . import serialize


def dumps_from_locusdata(ld):
    """
    Return a serialized Kafka alert.

    From requirements document:

        Kafka output content is not identical to the Locus Document Format.
        It contains:

            Locus properties
            Matched catalog names
            Locus Tags
            Link to Locus page on Portal
            Link to download full Locus Document
            "Lightcurve", a CSV table of select Alert properties

    :param ld: `antares.pipeline.locus.LocusData`
    :return: bytes
    """
    locus_id = ld.locus.locus_id
    data = {
        "data": {
            "type": "locus",
            "id": locus_id,
            "attributes": {
                "tags": list(ld.locus.tags),
                "ra": ld.locus.ra,
                "dec": ld.locus.dec,
                "properties": ld.locus.properties,
                "lightcurve": ld.locus.lightcurve,
                "catalogs": list(ld.catalog_objects.keys()),
            },
            "relationships": {
                "alerts": {
                    "links": {
                        "related": f"{config.API_BASE_URL}/loci/{locus_id}/alerts",
                    },
                },
            },
            "links": {
                "self": f"{config.API_BASE_URL}/loci/{locus_id}",
            },
        },
        "links": {
            "self": f"{config.API_BASE_URL}/loci/{locus_id}",
        },
        "meta": {
            "portal": f"{config.FRONTEND_BASE_URL}/loci/{locus_id}",
        },
    }
    return dumps(data)


def dumps(dic):
    """
    Serialize a JSON-ready dict to bytes

    :param dic: dict
    :return: bytes
    """
    return serialize.dumps(dic)


def loads(blob):
    """
    Deserialize a message to a JSON-ready python object

    :param blob: bytes
    :return: dict
    """
    return serialize.loads(blob)
