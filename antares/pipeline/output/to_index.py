"""

Serialization/deserialization for sending messages to the index_worker.

"""
from antares import log
from antares.utils import cone_search

from . import serialize


def dumps_from_locusdata(ld):
    """
    Return Kafka msg bytes to be sent do index worker.

    :param ld: `antares.pipeline.locus.LocusData`
    :return: bytes
    """
    data = dict(
        locus_id=ld.locus.locus_id,
        ra=ld.locus.ra,
        dec=ld.locus.dec,
        htm16=cone_search.get_htm(ld.locus.ra, ld.locus.dec, 16),
        properties=ld.locus.properties,
        tags=list(ld.locus.tags),  # convert set to list
        catalogs=list(ld.catalog_objects.keys()),  # convert dict keys to list
        watch_list_ids=list(ld.locus.wl_ids),  # convert set to list
        watch_object_ids=list(ld.locus.wo_ids),  # convert set to list
    )
    log.debug(data)
    return dumps(data)


def dumps(dic):
    """
    Serialize a JSON-ready dict to bytes

    :param dic: dict
    :return: bytes
    """
    return serialize.dumps(dic)


def loads(blob):
    """
    Deserialize a message to a JSON-ready python object

    :param blob: bytes
    :return: dict
    """
    return serialize.loads(blob)
