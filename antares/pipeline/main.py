import datetime
import math
import sys
import time
import signal

from confluent_kafka.cimpl import KafkaError
from humanfriendly.tables import format_pretty_table
from redis.exceptions import ConnectionError as RedisConnectionError

import antares
from antares import log, redis, utils
from antares.config import config
from antares.pipeline.ingestion.ztf import decode_kafka_message
from antares.pipeline.pipeline import Pipeline, AlertJob
from antares.rtdb import antcassandra
from antares.services import errors, kafka, metrics, slack
from antares.services.kafka.lag_cache import KafkaLagCache
from antares.sql import engine
from antares.sql.schema import SProvenance


# Setting this to True gracefully shuts down the process
stop = False


def main():
    """
    Run ANTARES pipeline on the ZTF Kafka stream.

    The pipeline is self-bootstrapping IFF ENVIRONMENT == 'dev'.

    If ENVIRONMENT != 'dev', then the pipeline will wait for the
    SQL DB to exist before starting up.
    """
    register_signal_handlers()
    metrics.startup()
    log.info("Starting up with config:")
    log.info("\n%s", config.format())

    # Wait for Redis to come online
    while True:
        try:
            redis.wait_for_online()
            break
        except RedisConnectionError:
            pass

    # Check that all connections are good.
    # Wait gracefully for DBs to be up, and for schemas to exist.
    log.info("Testing MySQL connection")
    engine.test_connection()
    log.info("Testing Cassandra connection")
    antcassandra.init()
    antcassandra.print_schemas()
    if config.INPUT_KAFKA_SERVERS:
        log.info("Testing Kafka input connection")
        kafka.KafkaAdmin(
            kafka_config=kafka.KafkaConsumer.default_config()
        ).print_status()
    if config.OUTPUT_KAFKA_SERVERS:
        log.info("Testing Kafka output connection")
        kafka.KafkaAdmin(
            kafka_config=kafka.KafkaProducer.default_config()
        ).print_status()

    # Save config to Provenance
    provenance_id = save_provenance(print_report=True)

    # Determine kafka topic(s) to subscribe to
    if config.INPUT_KAFKA_TOPICS:
        topics = config.INPUT_KAFKA_TOPICS
    else:
        topics = get_todays_kafka_topics()
    log.info(f"Will connect to Kafka topics: {topics}")

    # Determine time to terminate. (k8s will auto restart the container)
    seconds_until_halt = utils.seconds_until_end_of_utc_day()
    stop_time = int(time.time() + seconds_until_halt)
    log.info(
        "Scheduling to terminate in {} hours at timestamp {}".format(
            round(seconds_until_halt / 3600.0, 2), stop_time
        )
    )
    del seconds_until_halt

    # Set up consumer connection to Kafka broker
    log.info(f"Subscribing to Kafka topics...")
    stats_cache = KafkaLagCache()
    consumer = kafka.KafkaConsumer(topics=topics, stats_cb=stats_cache.stats_cb)

    # Work
    try:
        with errors.report("fatal", and_raise=True, slack_async=False):
            loop(consumer, stop_time, provenance_id)
    finally:
        # System Halt
        # close() does not call commit() because 'enable.auto.commit' is False.
        # This is important! Don't want to commit() on crashes.
        log.info("Closing Kafka connection")
        consumer.close()
        log.info("Bye!")


def loop(consumer, stop_time, provenance_id):
    BATCH_SIZE = config.INPUT_KAFKA_REQUEST_BATCH_SIZE
    TIMEOUT = config.INPUT_KAFKA_REQUEST_TIMEOUT

    FIRST_ALERT_THRESHOLD = 60 * 60  # 60 minutes
    t_last_alert = 0
    n = 0

    log.info("Begin processing alerts...")
    pipeline = Pipeline(
        provenance_id=provenance_id, kafka_producer=kafka.KafkaProducer()
    )
    while True:
        log.debug("Looping")
        if stop:
            return  # stop and shut down

        # Detect when we have reached the stop_time
        if stop_time is not None and time.time() >= stop_time:
            log.info("We have reached stop_time.")
            break

        # Get records from Kafka
        log.debug("Requesting messages from Kafka...")
        t1 = time.time()
        msgs = consumer.consume(num_messages=BATCH_SIZE, timeout=TIMEOUT)
        t2 = time.time()
        log.debug("Got {} messages from Kafka".format(len(msgs)))
        if msgs:
            metrics.timing("pipeline.poll_kafka", t2 - t1)
            total_bytes = sum(len(msg.value()) for msg in msgs if msg.value())
            metrics.increment("pipeline.kafka_msg_bytes", value=total_bytes)
        else:
            metrics.increment("pipeline.kafka_poll_timeout")

        # Process alerts
        time_received = time.time()
        for msg in msgs:
            # Emit a log statement on first 10 and 1000th alert
            n += 1
            log_n = math.log10(n)
            if n <= 10 or n % 1000 == 0 or log_n == int(log_n):
                log.info("Processing message number {}".format(n))

            # Detect if msg is a Kafka error
            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    metrics.error("kafka_partition_eof")
                    log.debug("Kafka Partition EOF")
                else:
                    metrics.error("kafka_read")
                    log.error("Kafka error msg: {}".format(msg.error()))
                continue

            # Notify of first Alert in a batch
            if time.time() - t_last_alert >= FIRST_ALERT_THRESHOLD:
                if redis.Lock("first_alert", expire=FIRST_ALERT_THRESHOLD).lock():
                    slack.post(
                        f"`{config.ENVIRONMENT}` Alerts incoming",
                        channel="#dev_alerts",
                        async_=False,
                    )
            t_last_alert = time.time()

            # Process alerts from msg
            topic = msg.topic() or "unknown"
            msg_t = kafka.get_message_timestamp(msg)["create_time"]
            metrics.increment("pipeline.received_kafka_msg", tags=[f"topic:{topic}"])
            for ztf_alert in decode_kafka_message(msg):
                try:
                    pipeline.process_alert(AlertJob(ztf_alert, msg_t, time_received))
                except:
                    exc_info = sys.exc_info()
                    if config.WORKER_LOG_ERRORS:
                        errors.report(
                            "process_alert",
                            extra_data=dict(provenance_id=provenance_id),
                            slack_async=False,
                        ).report_error(*exc_info)
                    if config.WORKER_RAISE_ERRORS:
                        raise
        # Only commit() after processing all without error
        consumer.commit()


def save_provenance(print_report=False):
    data = {
        "antares_version": antares.__version__,
        "config": config.values(clean=True),
        "filters": Pipeline(provenance_id=None).filter_manifest(),
        "catalogs": config.CATALOG_CONFIG.raw(),
        "python_packages": utils.python_pkg_manifest(),
    }
    provenance_id = SProvenance.log(data)

    if print_report:
        lock_key = f"startup-report:{config.ENVIRONMENT}"
        if redis.Lock(lock_key, expire=12 * 3600).lock():
            message = f"`{config.ENVIRONMENT}`"
            rows = data["filters"]
            if rows:
                columns = list(rows[0].keys()) if rows else []
                data = [[r[c] for c in columns] for r in rows]
                report = format_pretty_table(data, columns).strip()
                message += f" Starting up with {len(rows)} active Filters:"
                message += f"\n```\n{report}\n```"
            else:
                message += f" Starting up with 0 active Filters."
            if config.OUTPUTS:
                columns = ["name", "type", "criteria", "topic", "channel"]
                data = [[str(o.get(c, "")) for c in columns] for o in config.OUTPUTS]
                message += f"\nand {len(config.OUTPUTS)} Outputs:"
                report = format_pretty_table(data, columns).strip()
                message += f"\n```\n{report}\n```"
            else:
                message += f"\nand 0 Outputs."
            slack.post(message=message, channel="#dev_alerts", async_=False)

    return provenance_id


def get_todays_kafka_topics():
    """
    Generate topic name of form 'ztf_YYYYMMDD_programid1'

    ZTF's Kafka topics are daily in UTC time, so this function uses UTC.

    :return: str
    """
    utc_today = datetime.datetime.utcnow().date()
    date_str = utc_today.isoformat().replace("-", "")
    return [
        f"ztf_{date_str}_programid1",
        f"ztf_{date_str}_programid3_public",
    ]


def register_signal_handlers():
    # K8s sends the SIGTERM signal to request shutdown, then waits for a grace
    # period, then sends SIGKILL to forcefully shut down the container.
    signal.signal(signal.SIGTERM, stop_signal_handler)


def stop_signal_handler(signal_number, frame):
    global stop
    stop = True
    log.warn("Recieved UNIX signal %s", signal_number)
    log.warn("Frame: %s", frame)


if __name__ == "__main__":
    main()
