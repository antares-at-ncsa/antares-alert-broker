import io

import fastavro

from antares import utils
from antares.rtdb.models import AlertModel

ZTF_CANDIDATE_PREFIX = "ztf_candidate"
ZTF_CANDIDATE_FORMAT = ZTF_CANDIDATE_PREFIX + ":{candid}"
ZTF_UPPER_LIMIT_PREFIX = "ztf_upper_limit"
ZTF_UPPER_LIMIT_FORMAT = ZTF_UPPER_LIMIT_PREFIX + ":{ztf_object_id}-{pid}"


def decode_bytes(msg_bytes):
    bytes_io = io.BytesIO(msg_bytes)
    bytes_io.seek(0)
    avro_reader = fastavro.reader(bytes_io)
    if avro_reader:
        return list(avro_reader)
    return []


def decode_kafka_message(msg):
    """
    Kafka message --> list of dicts

    Deserializes the Kafka message as one or more Avro files.

    :param confluent_kafka.Message msg:
    :return: list of dicts
    """
    msg_bytes = msg.value()
    return decode_bytes(msg_bytes)


def ztf_candidate_to_antares_alert(candidate, ztf_object_id):
    """
    Given a ZTF alert candidate `c`, return an alert dict formatted for ANTARES.

    :param candidate: ZTF candidate or prv_candidate as dict
    :param ztf_object_id:
    :return: Alert
    """
    mjd = utils.jd_to_mjd(candidate["jd"])

    # Add ZTF properties
    p = {}
    p.update({"ztf_" + k: v for k, v in candidate.items() if v is not None})

    # Create the alert_id
    if candidate.get("candid") is not None:
        # This is a Candidate
        alert_id = ZTF_CANDIDATE_FORMAT.format(candid=candidate["candid"])
    else:
        # This is an Upper Limit
        assert ztf_object_id
        assert candidate["pid"]
        assert candidate.get("magpsf") is None
        assert candidate.get("diffmaglim") is not None
        alert_id = ZTF_UPPER_LIMIT_FORMAT.format(
            ztf_object_id=ztf_object_id,
            pid=candidate["pid"],
        )

    # Create Alert object
    return AlertModel(
        locus_id=None, processed_at=None, mjd=mjd, alert_id=alert_id, properties=p
    )
