from . import ztf


ZTF_CANDIDATE_PREFIX = ztf.ZTF_CANDIDATE_PREFIX
ZTF_CANDIDATE_SURVEY = 1
ZTF_UPPER_LIMIT_PREFIX = ztf.ZTF_UPPER_LIMIT_PREFIX
ZTF_UPPER_LIMIT_SURVEY = 2


SURVEYS = {
    ZTF_CANDIDATE_PREFIX: ZTF_CANDIDATE_SURVEY,
    ZTF_UPPER_LIMIT_PREFIX: ZTF_UPPER_LIMIT_SURVEY,
}


# Prefixes in decreasing order of . Used by `get_survey()`.
_prefixes_by_length = sorted(SURVEYS.keys(), key=len, reverse=True)
assert len(_prefixes_by_length[0]) > len(_prefixes_by_length[-1])


def get_survey(alert_id):
    """
    Determine what SURVEY type an alert_id is.

    By sorting the list of surveys in decreasing order of length, this function
    resolves ambiguities created by prefixes being themselves prefixes of other
    prefixes. eg:

    Let's say we have two alert prefixes:

    1) lsst_candidate
    2) lsst_candidate_v2

    The first it itself a prefix of the second, so checking for alert type with
    `alert_id.startswith('lsst_candidate')` will return True for both prefixes.

    Therefore, this function MUST be used when checking the type of an alert_id.

    :param alert_id:
    :return: survey (int)
    """
    for prefix in _prefixes_by_length:
        if alert_id.startswith(prefix):
            return SURVEYS[prefix]
    raise RuntimeError("Unrecognized alert_id format: %s", alert_id)


def is_ztf_candidate(alert_id):
    """
    Check if an alert_id is a ZTF Candidate.

    DO NOT check this using `alert_id.startswith(ZTF_CANDIDATE_PREFIX)`. That
    approach is not guaranteed to work in the future as we add more Alert types.
    If one alert prefix is itself a subset of another alert prefix, then using
    `startswith()` is ambiguous. `get_survey()` resolves this.
    Always use `get_survey()` or `is_ztf_candidate()`.

    :param alert_id:
    :return: bool
    """
    return get_survey(alert_id) == ZTF_CANDIDATE_SURVEY
