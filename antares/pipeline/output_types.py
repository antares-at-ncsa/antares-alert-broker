import wrapt

from antares import redis
from antares.sql.engine import session
from antares.sql.schema import SLocusProperty, SAlertProperty, STag

LOCK_EXPIRE = 60


class TypeMisMatch(Exception):
    pass


# The following functions use local locking (synchronized) and
# system-wide locking (redis.Lock) to prevent a thundering herd
# of many SQL database connections at the same time.
# Creating new properties in the DB is a rare occurrence, so this
# small performance hit is fine.


@wrapt.synchronized
def create_or_update_locus_property(name, type, origin, fv_id, description, es_mapping):
    with redis.Lock(f"update_locus_prop:{name}", expire=LOCK_EXPIRE):
        return SLocusProperty.create_or_update(
            name, type, origin, fv_id, description, es_mapping
        )


@wrapt.synchronized
def create_or_update_alert_property(name, type, origin, fv_id, description, es_mapping):
    with redis.Lock(f"update_alert_prop:{name}", expire=LOCK_EXPIRE):
        return SAlertProperty.create_or_update(
            name, type, origin, fv_id, description, es_mapping
        )


@wrapt.synchronized
def create_or_update_tag(name, fv_id, description):
    with redis.Lock(f"update_tag:{name}", expire=LOCK_EXPIRE):
        return STag.create_or_update(name, fv_id, description)


class AlertPropertyCache:
    def __init__(self):
        self._types = {}  # name --> type string

    def _refresh(self):
        self._types = {}
        with session() as s:
            for sap in s.query(SAlertProperty).all():
                self._types[sap.name] = sap.type

    def create_or_update(self, name, type, origin):
        if name not in self._types:
            self._refresh()
        if name not in self._types:
            create_or_update_alert_property(name, type, origin, None, None, None)
            self._refresh()
        assert name in self._types
        if self._types[name] != type:
            raise TypeMisMatch
