import time
import uuid

from redis.client import Redis
from redis.exceptions import ConnectionError

from antares import utils, log
from antares.config import config

redis = Redis(host=config.REDIS_HOST, port=config.REDIS_PORT_, db=config.REDIS_DB)


def clear():
    redis.flushdb(config.REDIS_DB)


def wait_for_online(n=300):
    """
    Wait for up to `n` seconds for Redis to be online.
    """
    log.info("Waiting for Redis to be online...")
    t_stop = time.time() + n
    while True:
        try:
            redis.keys()
            break
        except ConnectionError:
            if time.time() < t_stop:
                time.sleep(1)
                continue
            else:
                raise


class LockTimeout(Exception):
    pass


class Lock(object):
    """
    A non-blocking Mutex using Redis.
    """

    UNLOCK_SCRIPT = utils.unindent(
        """
        if redis.call("get", KEYS[1]) == ARGV[1] then
            return redis.call("del", KEYS[1])
        else
            return 0
        end
    """
    ).strip()

    def __init__(self, key, hashable=None, expire=30, timeout=10):
        if hashable is None:
            self._key = "lock:{}".format(key)
        else:
            self._key = "lock:{}:{}".format(key, utils.md5(hashable))
        self._unique_id = None
        self._expire = expire
        self._timeout = timeout

    def lock(self, block=False):
        """
        Try to acquire lock.

        Return True if successful.
        Return False if lock is locked.

        `expiry` is in seconds.

        If `block`, then exponentially backoff and try keep trying.
        After `timeout` seconds, raise LockTimeout if not aquired.
        """
        wait = 0.01  # 10ms
        wait_increase_factor = 1.5
        self._unique_id = str(uuid.uuid4())

        stop_time = time.time() + self._timeout
        while True:
            if self._lock():
                return True
            elif block:
                if time.time() > stop_time:
                    raise LockTimeout
                time.sleep(wait)
                wait *= wait_increase_factor
                continue
            else:
                return False

    def _lock(self):
        return redis.set(self._key, self._unique_id, nx=True, ex=self._expire)

    def release(self):
        """
        Release the lock.

        Returns True if this Lock object held the Lock AND it was released.
        Returns False if this Lock object does not hold the lock, or if it has
        already expired.
        """
        return bool(redis.eval(self.UNLOCK_SCRIPT, 1, self._key, self._unique_id))

    def __enter__(self):
        self.lock(block=True)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


class MultiLock:
    """
    A multi-key mutex using Redis.

    "multi" refers to locking multiple mutexes at once, atomically. If any of
    the mutexes are already locked, the lock should not be acquired.

    This algorithm is based on the Redlock algorithm which is the recommended
    way to create mutexes on top of Redis.
    Redlock: https://redis.io/topics/distlock

    Unlike Redlock, MultiLock does NOT make use of multiple Redis nodes. It is
    not designed with withstand failures of Redis nodes. Instead, the motivation
    here is to allow locking of groups of mutexes on a single Redis node.
    Fault tolerance to Redis node failure is not handled here.

    Example:
    - Process 1 attempts to lock keys ('a', 'b', 'c'), and succeeds.
    - Process 2 attempts to lock keys ('a', 'x', 'y'), but fails because 'a'
      is already locked.

    Like the vanilla Redlock algorithm, this MultiLock makes use of Redis'
    ability to execute LUA scripts as atomic operations.

    Unlike Redlock, MultiLock uses the Redis MSETNX command. MSETNX sets
    multiple keys/value pairs if NONE of the keys exist. Vanilla Redlock
    uses SET with the NX flag, which sets a single key if it does not exist.

    # Lock Acquire Algorithm

    The algorithm to acquire the lock is as follows.

        unique_id = uuid()
        ok = `MSETNX key1 unique_id key2 unique_id ... keyN unique_id`
        if ok:
            EXPIRE key1 expiry
            EXPIRE key2 expiry
            ...
            EXPIRE keyN expiry
        return ok

    ^ The MSETNX and the EXPIRE commands run as a single atomic LUA script so
    that the EXPIRE commands run even if the calling process crashes mid-lock().

    # Lock Release Algorithm

    The algorithm to release the lock is as follows.

        if `GET key1` == unique_id:
            DELETE key1
        if `GET key` == unique_id:
            DELETE key2
        ...
        if `GET keyN` == unique_id:
            DELETE keyN

    This unrolled loop ^ is implemented in a single LUA script so that it runs
    as an atomic operation even if the calling process crashes mid-release().

    """

    def __init__(self, keys, expire=30, timeout=10):
        keys = list(keys)
        assert len(keys) > 0
        self._keys = [f"multilock:{k}" for k in keys]
        self._expire = expire
        self._timeout = timeout
        self._unique_id = None

    def lock(self, block=False):
        """
        Try to acquire lock.

        Return True if successful.
        Return False if lock is locked.

        `expiry` is in seconds.

        If `block`, then exponentially backoff and try keep trying.
        After `timeout` seconds, raise LockTimeout if not acquired.
        """
        wait = 0.01  # 10ms
        wait_increase_factor = 1.5

        self._unique_id = str(uuid.uuid4())
        stop_time = time.time() + self._timeout
        while True:
            if self._lock():
                return True
            elif block:
                if time.time() > stop_time:
                    raise LockTimeout
                time.sleep(wait)
                wait *= wait_increase_factor
                continue
            else:
                return False

    def _lock(self):
        """
        Lock many keys at once using a LUA script.

        Keys are passed to script as KEYS[1] ... KEYS[N]
        unique ID is passed as ARGV[1]
        expiry in seconds is passed as ARGV[2]

        :return: bool
        """
        newline_and_indent = "\n" + (4 * 4 * " ")  # newline + indent level 4
        n = len(self._keys)
        msetnx_args = ", ".join(f"KEYS[{i}], ARGV[1]" for i in range(1, n + 1))
        msetnx = f'redis.call("msetnx", {msetnx_args})'
        expires = newline_and_indent.join(
            f'redis.call("expire", KEYS[{i}], ARGV[2])' for i in range(1, n + 1)
        )
        script = utils.unindent(
            f"""
            if {msetnx} == 1 then
                {expires}
                return 1
            else
                return 0
            end
        """
        ).strip()
        log.debug("\n%s", script)
        args = (n, *self._keys, self._unique_id, self._expire)
        log.debug(args)
        result = redis.eval(script, *args)
        log.debug(result)
        return bool(result)

    def release(self):
        """
        Release the lock.

        It is not guaranteed that all keys expire at the same time.
        Therefore, it is possible that at the time of release(), some keys are
        still held while others are not. Nevertheless, release() should be
        called only once, not more.

        Therefore, this release() must release each key independently IF it
        is still held.

        THis multi-conditional-release must not occur as multiple separate LUA
        scripts because this process could crash between each release, leaving
        some keys locked and others unlocked. Therefore, this function assembles
        a single LUA script.

        If no keys are released, then either they have all expired OR they are
        held by another process.

        Returns True if 1 or more keys were released.
        Returns False if no keys were released.
        """
        n = len(self._keys)
        scripts = (
            ["local n = 0"]
            + [
                utils.unindent(
                    f"""
                        if redis.call("get", KEYS[{i}]) == ARGV[1] then
                            n = n + redis.call("del", KEYS[{i}])
                        end
                    """
                ).strip()
                for i in range(1, n + 1)
            ]
            + ["return n"]
        )
        script = "\n".join(scripts)
        log.debug("\n%s", script)
        args = (n, *self._keys, self._unique_id)
        log.debug(args)
        result = redis.eval(script, *args)
        log.debug(result)
        return bool(result)

    def expire(self, expire=None):
        """
        Schedule all keys in the lock to expire in `expire` seconds.
        """
        if expire is None:
            expire = self._expire
        assert isinstance(expire, int)
        n = len(self._keys)
        scripts = (
            ["local n = 0"]
            + [
                utils.unindent(
                    f"""
                        if redis.call("get", KEYS[{i}]) == ARGV[1] then
                            n = n + redis.call("expire", KEYS[{i}], {expire})
                        end
                    """
                ).strip()
                for i in range(1, n + 1)
            ]
            + ["return n"]
        )
        script = "\n".join(scripts)
        log.debug("\n%s", script)
        args = (n, *self._keys, self._unique_id)
        log.debug(args)
        result = redis.eval(script, *args)
        log.debug(result)
        return bool(result)

    def __enter__(self):
        self.lock(block=True)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()
