"""
Open an IPython shell with ANTARES database connections open.
"""
from antares.config import config

config.LOG_LEVEL = "DEBUG"
config.SQLALCHEMY_ECHO = False
config.CASSANDRA_ECHO = False

from antares import log

log.init()  # Pick up change to LOG_LEVEL
log.info(config.format())

import IPython
from antares.utils import cmd


# Imports for hacking around in the shell
from antares.sql import engine
from antares.rtdb import antcassandra


def main():
    try:
        engine.test_connection()
        antcassandra.init()
    except:
        log.stacktrace()
    print_header()
    IPython.embed(colors="Linux")


def print_header():
    print("Loaded environment:")
    print(sorted(k for k in globals().keys() if not k.startswith("_")))

    # Print the current environment
    env = config.ENVIRONMENT
    n = 3 if env == "production" else 1
    print()
    for _ in range(n):
        print("              " + "v" * len(env))
    print("ENVIRONMENT = " + env)
    for _ in range(n):
        print("              " + "^" * len(env))

    # Print the current hostname
    print()
    print("HOSTNAME = " + cmd.out("hostname"))
    print()


if __name__ == "__main__":
    main()
