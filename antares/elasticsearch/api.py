from elasticsearch_dsl import Search

from antares.config import config
from antares.elasticsearch.ingest import connect as es_connect


def get_locus(locus_id):
    pass


def get_loci_by_ids(locus_ids):
    client = es_connect()
    search = Search(using=client, index=config.ARCHIVE_INDEX_NAME)
    search = search[0 : len(locus_ids)]
    search = search.filter("terms", locus_id=locus_ids)
    search_result = client.search(
        index=config.ARCHIVE_INDEX_NAME, body=search.to_dict()
    )
    docs = search_result["hits"]["hits"]
    count = search_result["hits"]["total"]["value"]
    return [doc["_source"] for doc in docs], count


def get_loci():
    pass
