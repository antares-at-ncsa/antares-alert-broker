import collections
import json
import os
import random
import time
import uuid

import elasticsearch
import elasticsearch.helpers
from elasticsearch import Elasticsearch, NotFoundError

import antares.sql.engine
import antares.sql.schema
from antares import log
from antares.config import config
from antares.pipeline.output import to_index
from antares.redis import redis
from antares.services import kafka, metrics, trace


if config.DATADOG_TRACE_ENABLE:
    log.info("Patching Elasticsearch for DataDog Trace")
    trace.patch_elasticsearch()


PYTHON_TYPE_STR_TO_ES_TYPE = {str: "text", float: "float", int: "long"}
MAPPING_CACHE_KEY = str(uuid.uuid1())  # Unique to this Python process


def connect(test_connection=False):
    log.info("Connecting to ElasticSearch %s", config.ELASTICSEARCH_HOSTS)
    elasticsearch_client = Elasticsearch(
        config.ELASTICSEARCH_HOSTS, **config.ELASTICSEARCH_OPTIONS
    )
    if test_connection and not elasticsearch_client.ping():
        raise ConnectionError("Unable to connect to Elasticsearch")
    return elasticsearch_client


def wait_for_online(bootstrap=False):
    # Wait for ES to come online
    while True:
        try:
            connect(test_connection=True)
            break
        except ConnectionError:
            log.info("Waiting for ES to come online...")
            time.sleep(5)
            continue

    # Create index
    if bootstrap:
        es = connect()
        index = config.ARCHIVE_INDEX_NAME
        assert index.endswith("_dev") or index.endswith("_staging")
        try:
            es.indices.get_mapping(index)
        except NotFoundError:
            log.info('Bootstrapping index "%s"', index)
            es.indices.create(index=index)


def _request(method, url, params=None, body=None):
    params = params or {}
    return connect().transport.perform_request(method, url, params=params, body=body)


def describe_index(index):
    return _request("GET", f"/{index}")


def get_aliases():
    return _request("GET", "/_aliases")


def get_all_documents(index, n=1000):
    """
    Get all (<= n) documents in an index. USE ONLY IN TESTS.
    """
    return _request("GET", f"/{index}/_search/?size={n}")["hits"]["hits"]


def reindex(source, dest):
    return _request(
        "POST", "/_reindex", body={"source": {"index": source}, "dest": {"index": dest}}
    )


def delete_index(index=None):
    """
    Delete an index if it exists, else return None.
    """
    if index is None:
        index = config.ARCHIVE_INDEX_NAME
    assert index.endswith("_dev") or index.endswith("_staging")
    log.info(f'Deleting index "{index}"')
    try:
        return _request("DELETE", f"/{index}")
    except NotFoundError:
        return


def get_mappings(index):
    r = _request("GET", f"/{index}")
    (name,) = list(r.keys())
    return r[name]["mappings"]


def put_mappings(index, mappings):
    assert "properties" in mappings
    assert isinstance(mappings["properties"], dict)
    return _request("PUT", f"/{index}/_mapping", body=mappings)


def create_index(index, mappings):
    return _request("PUT", f"/{index}", body={"mappings": mappings})


def _get_mapping_patch(index, locus, mapping):
    """
    Compute the patch that would need to be applied to the Elasticsearch
    `index` mapping in order to have explicit type definitions for all
    of the fields in the `locus` document.

    :param index: Elasticsearch index to compare against
    :type index: str

    :param locus: Locus document
    :type locus: dict-like

    :param mapping_cache: Current mappings for the index
    :type mapping_cache: dict

    :returns: Mapping patch in the correct format to pass to
        `elasticsearch.client.Elasticsearch.indices.put_mapping`. If the index
        already has explicit mappings for all the fields in `locus` this
        function will return None.
    :rtype: collections.defaultdict or None

    """
    f = lambda: collections.defaultdict(f)
    mapping_patch = collections.defaultdict(f)

    # Check the top level keys in the document. These correspond to columns in
    # the Locus table.
    document_top_level_keys = set(locus) - {
        "properties"
    }  # ignore the top level properties key
    mapping_top_level_keys = set()
    try:
        mapping_top_level_keys = set(mapping[index]["mappings"]["properties"])
    except KeyError:
        pass
    if not document_top_level_keys.issubset(mapping_top_level_keys):
        difference = document_top_level_keys - mapping_top_level_keys
        log.debug("Adding mapping(s) for: {}".format(difference))
        for key in difference:
            mapping_patch["properties"][key] = get_mapping_patch(
                key, locus[key], property_=False
            )

    # Check the property keys in the document. These correspond to columns in
    # the various LocusProperty tables.
    document_property_keys = set(locus.get("properties", set()))
    mapping_property_keys = set()
    try:
        mapping_property_keys = set(
            mapping[index]["mappings"]["properties"]["properties"]["properties"]
        )
    except KeyError:
        pass
    if not document_property_keys.issubset(mapping_property_keys):
        difference = document_property_keys - mapping_property_keys
        log.debug("Adding mapping(s) for: properties[{}]".format(difference))
        for key in difference:
            mapping_patch["properties"]["properties"]["properties"][
                key
            ] = get_mapping_patch(key, locus["properties"][key], property_=True)
    return mapping_patch or None


def get_mapping_patch(key, value, property_=False):
    # Check mapping.json for top-level
    if not property_:
        path = os.path.join(os.path.dirname(__file__), "mapping.json")
        with open(path) as f:
            mapping = json.load(f)
        try:
            return mapping["mappings"]["properties"][key]
        except KeyError:
            pass
    # Check SQL DB for property-level
    if property_:
        with antares.sql.engine.session() as s:
            locus_property = s.query(antares.sql.schema.SLocusProperty).get(key)
            if not locus_property:
                raise ValueError(
                    f"Index worker recieved locus property that does not exist in DB: {key}"
                )
            if locus_property.es_mapping:
                return json.loads(locus_property.es_mapping)

    # Default
    return {"type": PYTHON_TYPE_STR_TO_ES_TYPE[type(value)]}


class MappingCache:

    KEY_PREFIX = "es_mapping_cache"
    KEY_FORMAT = KEY_PREFIX + ":{process_id}"
    EXPIRY = 600

    def __init__(self, client=None, index=None, key=None):
        self._client = client or connect()
        self._index = index or config.ARCHIVE_INDEX_NAME
        self._key = self.KEY_FORMAT.format(process_id=key or uuid.uuid4())

    def get(self):
        value = redis.get(self._key)
        if value:
            return json.loads(value)
        return self.refresh()

    def refresh(self):
        mapping = self._client.indices.get_mapping(self._index)
        self.set(mapping)
        return mapping

    def set(self, mapping):
        value = json.dumps(mapping)
        redis.setex(self._key, self.EXPIRY, value)

    @classmethod
    def drop_all(cls):
        for k in redis.keys(cls.KEY_PREFIX + ":*"):
            redis.delete(k)


def _generate_bulk_index_actions_from_kafka(
    kafka_client,
    index,
    max_chunk_size=1000,
    kafka_poll_timeout=None,
    document_id_getter=lambda _: uuid.uuid1(),
    message_parser=lambda msg: msg,
    es_ensure_mappings=True,
    es_client=None,
):
    """
    Generate ElasticSearch bulk actions.

    By default this will yield alert index actions from the topics that
    `kafka_client` is subscribed to infinitely. This may or may not be
    desired behavior depending on how these actions are used. In fact
    it probably isn't desired behavior but we make it the default interface
    because it is likely what the caller expects.

    Consider passing this generator to the `elasticsearch.helpers.bulk`
    bulk operation helper: that function takes a parameter `max_chunk_size`
    that is, by default, 500. It will consume items from a generator until
    either it has received `max_chunk_size` items or the generator has raised
    a `StopIteration`--then it will send the actions to the Elasticsearch
    server.

    This is fine if your input stream is high-volume and uniform, if you'd
    like to minimize HTTP requests or if indexing documents is not
    time-critical. The other benefit is that the `bulk()` function can
    be passed the default construction of this generator and it will index
    documents indefinitely.

    The way that we use it requires additional flow control though. Our desired
    behavior is: fetch up to `max_chunk_size` documents to index--if there
    aren't `max_chunk_size` documents available, wait enough time to know that
    we haven't just hit some network latency and then send the chunk of
    documents that we have, regardless of its size.

    The approximate worst case is that after receiving its first alert,
    exhausting the generator takes (in seconds):

       kafka_poll_timeout * (max_chunk_size - 1)

    """
    # Fetch the index mapping. We do this as an optimization so that
    # the `_get_alert_mapping_patch` function does not do it for each
    # alert. If we change the index's mapping in the future we must
    # manually update this object!
    #
    mapping = None
    mapping_cache = None
    if es_ensure_mappings:
        assert es_client
        assert index
        mapping_cache = MappingCache(es_client, index, MAPPING_CACHE_KEY)

    # The Kafka client doesn't provide a way to explicitly establish
    # a connection to streams--it happens implicitly on the first
    # call to poll. The first call to poll in this generator function
    # will wait to receive a message indefinitely.
    #
    # Once we've established a connection we can be reasonably sure
    # that if we have to wait a long time to get a message from the Kafka
    # stream there is nothing new to receive.
    #
    for i in range(max_chunk_size):
        timeout = None if i == 0 else kafka_poll_timeout
        _, document = kafka_client.poll(timeout, message_parser=message_parser)
        if document is None:
            break

        # Remove None values from document
        for k, v in list(document.items()):
            if v is None:
                del document[k]
        for k, v in list(document["properties"].items()):
            if v is None:
                del document["properties"][k]

        # Update type mappings if needed
        if es_ensure_mappings:
            assert mapping_cache
            if mapping is None:
                mapping = mapping_cache.get()
            mapping_patch = _get_mapping_patch(index, document, mapping)
            if mapping_patch is not None:
                # New mapping(s)!
                es_client.indices.put_mapping(mapping_patch, index=index)
                mapping = None

        # Yield a batch operation
        yield {
            "_op_type": "index",
            "_index": index,
            "_type": "_doc",
            "_id": document_id_getter(document),
            "_source": document,
        }


def index_alerts_from_kafka(
    from_kafka_topics,
    to_elasticsearch_index,
    elasticsearch_ensure_explicit_mappings=True,
    max_chunk_size=1000,
    kafka_config=None,
    kafka_poll_timeout=None,
    loop=True,
):
    """
    Indefinitely index alerts from Kafka topics.

    N.B.: I think that an incorrect Kafka API key/secret pair will cause
    a silent error that, symptomatically, will look as if you're connected
    to a topic with no messages in it.

    :raises: ConnectionError, elasticsearch.helpers.BulkIndexError
    """
    elasticsearch_client = connect(test_connection=True)
    with kafka.KafkaConsumer(kafka_config, from_kafka_topics) as kafka_client:
        log.info("Subscribed to Kafka topics: %s", from_kafka_topics)
        log.info(
            "Subscribed to Kafka partitions: {}".format(
                kafka_client._consumer.position(kafka_client._consumer.assignment())
            )
        )
        while True:
            actions = _generate_bulk_index_actions_from_kafka(
                kafka_client,
                to_elasticsearch_index,
                max_chunk_size=max_chunk_size,
                kafka_poll_timeout=kafka_poll_timeout,
                message_parser=lambda msg: to_index.loads(msg.value()),
                document_id_getter=lambda doc: doc["locus_id"],
                es_ensure_mappings=elasticsearch_ensure_explicit_mappings,
                es_client=elasticsearch_client,
            )
            try:
                number_documents_indexed, _ = elasticsearch.helpers.bulk(
                    elasticsearch_client, actions, chunk_size=max_chunk_size
                )
                metrics.increment(
                    "index_worker.document_indexed", value=number_documents_indexed
                )
                log.info("Indexed {} documents".format(number_documents_indexed))
            except ConnectionError:
                raise
            except elasticsearch.helpers.BulkIndexError:
                raise
            kafka_client.commit()
            if not loop:
                break  # This is useful in tests.
