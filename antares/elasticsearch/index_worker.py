import elasticsearch.exceptions

from antares import log
from antares import redis
from antares.config import config
from antares.elasticsearch import ingest
from antares.services import errors, metrics

ES_RETRY_ERRORS = (
    ConnectionError,
    elasticsearch.exceptions.ConnectionError,
    elasticsearch.exceptions.TransportError,
)


def _kafka_config():
    c = {
        "bootstrap.servers": config.ARCHIVE_INDEX_KAFKA_SERVERS,
        "group.id": config.ARCHIVE_INDEX_KAFKA_GROUP,
        "default.topic.config": {"auto.offset.reset": "earliest"},
        "enable.auto.commit": False,
    }
    if config.ARCHIVE_INDEX_KAFKA_SASL_USER:
        assert config.ARCHIVE_INDEX_KAFKA_SASL_PASS
        assert config.ARCHIVE_INDEX_KAFKA_SSL_CA_LOCATION
        c.update(
            {
                "security.protocol": "SASL_SSL",
                "sasl.mechanisms": "PLAIN",
                "sasl.username": config.ARCHIVE_INDEX_KAFKA_SASL_USER,
                "sasl.password": config.ARCHIVE_INDEX_KAFKA_SASL_PASS,
                "ssl.ca.location": config.ARCHIVE_INDEX_KAFKA_SSL_CA_LOCATION,
            }
        )
    return c


def sync_elasticsearch(loop=True):
    log.info("Starting Elasticsearch indexing job")
    ingest.index_alerts_from_kafka(
        [config.ARCHIVE_INDEX_KAFKA_TOPIC_NAME],
        config.ARCHIVE_INDEX_NAME,
        max_chunk_size=config.ARCHIVE_INDEX_MAX_CHUNK_SIZE,
        kafka_config=_kafka_config(),
        kafka_poll_timeout=config.ARCHIVE_INDEX_KAFKA_POLL_TIMEOUT,
        elasticsearch_ensure_explicit_mappings=True,
        loop=loop,
    )


def main():
    metrics.startup()
    log.info("Starting up with config:")
    log.info("\n%s", config.format())
    with errors.report("index_worker"):
        redis.wait_for_online()
        ingest.wait_for_online()
        while True:
            try:
                sync_elasticsearch()
            except elasticsearch.exceptions.NotFoundError:
                log.warn("Caught index NotFoundError. Re-initializing...")
                ingest.wait_for_online(bootstrap=True)  # Create index if does not exist
                continue
            except ES_RETRY_ERRORS as e:
                log.debug(
                    "Caught expected Elasticsearch connection error: {}".format(e)
                )
                metrics.error("index_worker_connection_error")
                continue


if __name__ == "__main__":
    main()
