import time
from contextlib import contextmanager

from sqlalchemy.exc import IntegrityError, OperationalError
from sqlalchemy.pool import QueuePool, NullPool

from antares import log
from antares.config import config

if config.DATADOG_TRACE_ENABLE:
    log.info("Patching SQLALchemy for DataDog Trace")
    from antares.services import trace

    trace.patch_sqlalchemy()

from sqlalchemy import create_engine as _create_engine
from sqlalchemy.orm import sessionmaker


# Config
DEFAULT_ENGINE = "antares"
CATALOG_ENGINE = "antares_catalogs"


def _url(engine_name):
    # Re-create this dict every time, to pick up config changes.
    return {
        DEFAULT_ENGINE: config.MYSQL_DB_URL,
        CATALOG_ENGINE: config.MYSQL_CATALOG_DB_URL,
    }[engine_name]


# SQLAlchemy `Engine` objects
_engines = {}


def close():
    """
    Close all connections and sessions.
    """
    for e in _engines.values():
        e.dispose()
    _engines.clear()


def create_engine(url):
    engine_params = (config.SQLALCHEMY_ENGINE_PARAMS or {}).copy()
    if engine_params.get("pool_size") == 0:
        # The QueuePool takes pool_size=0 to mean "unlimited".
        # So, to implement a pool of size 0 we use the NullPool instead.
        poolclass = NullPool
        # The NullPool does not accept the following params:
        engine_params.pop("pool_size", None)
        engine_params.pop("max_overflow", None)
        engine_params.pop("pool_recycle", None)
    else:
        poolclass = QueuePool
    return _create_engine(
        url,
        echo=config.SQLALCHEMY_ECHO,
        echo_pool=config.SQLALCHEMY_ECHO,
        poolclass=poolclass,
        **engine_params,
    )


def get_engine(name=DEFAULT_ENGINE):
    if name not in _engines:
        _engines[name] = create_engine(_url(name))
        if config.DATADOG_TRACE_ENABLE:
            trace.register_db_engine(_engines[name], f"mysql-{name.replace('_', '-')}")
    return _engines[name]


def wait_for_online(n=300):
    """
    Wait up to `n` seconds for the MySQL server to be online.

    Does not check if schema exists.
    """
    giveup_time = time.time() + n
    while True:
        try:
            log.info(get_engine().execute('SELECT "Connected to SQL DB"').scalar())
            return
        except OperationalError as e:
            if "Connection refused" in str(e):
                if time.time() > giveup_time:
                    raise
                log.info("Waiting for MySQL to come online...")
                time.sleep(5)
                continue
            if """(1049, "Unknown database""" in str(e):
                return


def test_connection(do_bootstrap=False, mocks=True, return_bool=False, n=300):
    """
    Wait up to `n` seconds for MySQL server online and schema exists.

    Should be called by processes on boot.

    If `return_bool` is True, return True if the DB exists, else False.
    Do not bootstrap.

    """
    if return_bool:
        n = 0
    giveup_time = time.time() + n
    while True:
        try:
            log.info(get_engine().execute('SELECT "Connected to SQL DB"').scalar())
            return True
        except OperationalError as e:
            if "Connection refused" in str(e):
                if time.time() > giveup_time:
                    raise
                log.info("Waiting for MySQL to come online...")
                time.sleep(5)
                continue
            if """(1049, "Unknown database""" in str(e):
                if return_bool:
                    return False
                if do_bootstrap and (
                    config.MYSQL_DB_URL.endswith("_dev")
                    or config.MYSQL_DB_URL.endswith("_staging")
                ):
                    log.info("Bootstrapping MySQL DB...")
                    from . import bootstrap

                    bootstrap.bootstrap_dev(mocks=mocks)
                    return
                else:
                    if time.time() > giveup_time:
                        raise
                    log.info("Waiting for MySQL schema to exist...")
                    time.sleep(5)
                    continue
            raise


def get_session(name=DEFAULT_ENGINE):
    """Return an SQLAlchemy Session for the DB.

    Caller is responsible for closing the Session.

    Returns:
        `sqlalchemy.orm.session.Session`
    """
    return sessionmaker(bind=get_engine(name=name), autoflush=False)()


@contextmanager
def session(name=DEFAULT_ENGINE):
    """
    Get an Alert DB Session using a contextmanager.

    eg:
    with alert_db_session() as s:
        s.execute('...')

    """
    s = get_session(name=name)
    try:
        yield s
    finally:
        s.close()


class rollback_on_duplicate(object):
    """
    A context manager that rolls back a session if a duplicate insert error
    occurs.
    """

    def __init__(self, s):
        self._session = s

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, tb):
        if isinstance(exc_value, IntegrityError):
            if "Duplicate" in str(exc_value):
                self._session.rollback()
                return True  # Swallow the exception
            else:
                return False  # Allow exception to propagate
