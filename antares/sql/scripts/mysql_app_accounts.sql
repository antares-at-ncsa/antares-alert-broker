--
-- ANTARES MySQL app users
--
--
-- Run this when setting up security on the MySQL data store
-- which is used to store filters, locus annotations, tags, and other data
-- but not catalogs or alerts
--
-- You may safely re-run this script without requiring to reset anything
--

SET @app_pipeline := 'app_pipeline';
SET @pipeline_password := 'changeme';        -- <<<<<<<<<<   Change this to the correct password

set @app_api := 'app_api';
SET @api_password := 'changeme';             -- <<<<<<<<<<   Change this to the correct password

SET @allowed_hosts := '%';
SET @db := 'antares_staging';                -- <<<<<<<<<<   Change this to the correct database name

/*************************************************************************************************************/
-- app_pipeline

SET @q = CONCAT('CREATE USER IF NOT EXISTS "',@app_pipeline,'"@"',@allowed_hosts,'" IDENTIFIED BY"',@mysql_password,'" ');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT USAGE ON ',@db,'.* TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT, UPDATE ON ',@db,'.alert_property TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT, UPDATE ON ',@db,'.locus_property TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT, UPDATE ON ',@db,'.tag TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT INSERT ON ',@db,'.provenance TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE ON ',@db,'.filter TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE ON ',@db,'.filter_version TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT ON ',@db,'.filter_crash_log TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT ON ',@db,'.storage TO "',@app_pipeline,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

/*************************************************************************************************************/
-- app_api
SET @q = CONCAT('CREATE USER IF NOT EXISTS "',@app_api,'"@"',@allowed_hosts,'" IDENTIFIED BY"',@mysql_password,'" ');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT USAGE ON ',@db,'.* TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT ON ',@db,'.alert_property TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT ON ',@db,'.locus_property TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT ON ',@db,'.tag TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE, INSERT ON ',@db,'.filter TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE, INSERT ON ',@db,'.filter_version TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE, INSERT ON ',@db,'.watch_list TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE, INSERT ON ',@db,'.watch_object_id TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, UPDATE, INSERT ON ',@db,'.user TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT ON ',@db,'.token_blacklist TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;

SET @q = CONCAT('GRANT SELECT, INSERT, UPDATE ON ',@db,'.locus_annotation TO "',@app_api,'"@"',@allowed_hosts,'"');
PREPARE cmd FROM @Q; EXECUTE cmd; DEALLOCATE PREPARE cmd;
