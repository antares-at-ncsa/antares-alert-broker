--
--
--  Create application accounts in C*
--
--
-- you can see the existing roles with: list roles

/*************************************************************************************************************/
-- app_pipeline

USE antares_production_catalogs;

CREATE ROLE IF NOT EXISTS app_pipeline WITH LOGIN = true AND
    PASSWORD = 'changeme';                                                  -- <<<<<<<<<<<<<<<<<<<< change the password

GRANT SELECT ON KEYSPACE antares_production_catalogs TO app_pipeline;

USE antares_staging_alerts;

GRANT SELECT ON htm_lut to app_pipeline;
GRANT MODIFY ON htm_lut to app_pipeline;

GRANT SELECT ON locus to app_pipeline;
GRANT MODIFY ON locus to app_pipeline;

GRANT SELECT ON locus_by_day to app_pipeline;
GRANT MODIFY ON locus_by_day to app_pipeline;

GRANT SELECT ON locus_by_alert_id to app_pipeline;
GRANT MODIFY ON locus_by_alert_id to app_pipeline;

GRANT SELECT ON watch_list to app_pipeline;

GRANT SELECT ON watch_object to app_pipeline;

GRANT SELECT ON storage to app_pipeline;
GRANT MODIFY ON storage to app_pipeline;

/*************************************************************************************************************/
-- app_api



CREATE ROLE IF NOT EXISTS app_api WITH LOGIN = true AND
    PASSWORD = 'changeme';                                                  -- <<<<<<<<<<<<<<<<<<<< change the password

GRANT SELECT ON KEYSPACE antares_production_catalogs TO app_api;

USE antares_staging_alerts;

GRANT SELECT ON htm_lut TO app_api;

GRANT SELECT ON locus TO app_api;

GRANT SELECT ON watch_list TO app_api;
GRANT MODIFY ON watch_list TO app_api;

GRANT SELECT ON watch_object TO app_api;
GRANT MODIFY ON watch_object TO app_api;

-- ============================ EOF ========================
