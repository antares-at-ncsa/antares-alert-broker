from antares import log
from antares.services import metrics
from . import engine


@metrics.timed("rq.job", tags=["job:report_table_sizes"])
def report_table_sizes():
    """
    Report DB table sizes to DataDog
    """
    with engine.session() as s:
        database = s.bind.url.database
        query = """
            SELECT
                table_name,
                table_rows
            FROM information_schema.TABLES
            WHERE table_schema = :database
        """
        rows = s.execute(query, dict(database=database))
        for row in rows:
            table, n = list(row)
            log.debug("Table metrics: {}, {}".format(table, n))
            metrics.gauge(
                "sql.table_rows", n, tags=["table:{}.{}".format(database, table)]
            )
