import datetime
import json

import bson
from passlib.hash import pbkdf2_sha256
from sqlalchemy import Column, func, event, UniqueConstraint
from sqlalchemy.dialects.mysql import (
    INTEGER,
    BIGINT,
    TINYINT,
    VARCHAR,
    BOOLEAN,
    TEXT,
    LONGTEXT,
    LONGBLOB,
    DATETIME,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property

from antares.sql.engine import session
from antares.utils.json_encoder import json_dumps

_Base = declarative_base()


utcnow = datetime.datetime.utcnow


class SBase(_Base):
    __abstract__ = True

    @classmethod
    def get(cls, obj_id):
        with session() as s:
            return s.query(cls).get(obj_id)

    def as_dict(self):
        """
        Translate an object to a dict

        :return: dict
        """
        return {col: getattr(self, col) for col in self.__table__.columns.keys()}


class WrongPropertyType(Exception):
    pass


class ESMappingsAreImmutable(Exception):
    pass


class PropertyMixin:
    @classmethod
    def create_or_update(
        cls, name, type, origin, filter_version_id, description, es_mapping
    ):
        """
        Create or update a Property.

        Updates the filter_version_id and description only.

        Raises WrongPropertyType if the type does not match existing.

        :param name:
        :param type:
        :param origin:
        :param filter_version_id:
        :param description:
        :param es_mapping:
        :return: True if newly created, False if updated existing.
        """
        assert type in {"int", "float", "str"}
        if es_mapping is not None:
            es_mapping = json_dumps(es_mapping, indent=4, sort_keys=True)
        with session() as s:
            p = cls.get(name)
            if p:
                if p.type != type:
                    raise WrongPropertyType
                if p.es_mapping != es_mapping:
                    raise ESMappingsAreImmutable
                p.origin = origin
                p.filter_version_id = filter_version_id
                p.description = description
                s.commit()
                return False
            else:
                s.add(
                    cls(
                        name=name,
                        type=type,
                        origin=origin,
                        filter_version_id=filter_version_id,
                        description=description,
                        es_mapping=es_mapping,
                    )
                )
                s.commit()
                return True


class SAlertProperty(SBase, PropertyMixin):
    __tablename__ = "alert_property"
    name = Column(VARCHAR(100), primary_key=True)
    type = Column(VARCHAR(10))
    origin = Column(VARCHAR(100))
    filter_version_id = Column(INTEGER(unsigned=True))
    description = Column(TEXT)
    es_mapping = Column(TEXT)


class SLocusProperty(SBase, PropertyMixin):
    __tablename__ = "locus_property"
    name = Column(VARCHAR(100), primary_key=True)
    type = Column(VARCHAR(10))
    origin = Column(VARCHAR(100))
    filter_version_id = Column(INTEGER(unsigned=True))
    description = Column(TEXT)
    es_mapping = Column(TEXT)


class STag(SBase):
    __tablename__ = "tag"
    name = Column(VARCHAR(100), primary_key=True)
    filter_version_id = Column(INTEGER(unsigned=True))
    description = Column(TEXT)

    @classmethod
    def create_or_update(cls, name, filter_version_id, description):
        with session() as s:
            tag = cls.get(name)
            if tag:
                tag.filter_version_id = filter_version_id
                tag.description = description
                s.commit()
                return False
            else:
                s.add(
                    cls(
                        name=name,
                        filter_version_id=filter_version_id,
                        description=description,
                    )
                )
                s.commit()
                return True


class SProvenance(SBase):
    __tablename__ = "provenance"
    provenance_id = Column(BIGINT(unsigned=True), primary_key=True)
    created_at = Column(DATETIME, default=utcnow)
    data = Column(LONGTEXT)

    @classmethod
    def log(cls, data):
        """
        Log a JSON-able data structure to Provenance and return the provenance_id.

        :param data: a JSON-able data structure
        :return: int, provenance_id
        """
        data = json_dumps(data, indent=4)
        p = cls(data=data)
        with session() as s:
            s.add(p)
            s.commit()
            assert p.provenance_id
            return p.provenance_id


class SFilter(SBase):
    __tablename__ = "filter"
    filter_id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    name = Column(VARCHAR(100), nullable=False, unique=True)
    created_at = Column(DATETIME, default=utcnow)
    disabled_at = Column(DATETIME)
    updated_at = Column(DATETIME)
    user_id = Column(INTEGER)
    description = Column(VARCHAR(2048))
    level = Column(INTEGER)
    priority = Column(INTEGER)
    latest_version_id = Column(INTEGER(unsigned=True))
    enabled_version_id = Column(INTEGER(unsigned=True))
    public = Column(TINYINT, nullable=False, default=False)

    @hybrid_property
    def enabled(self):
        return self.enabled_version_id is not None

    @classmethod
    def get_enabled(cls, level=None):
        """
        Return all enabled rows in order of priority.

        Rows with priority = NULL are sorted to the bottom.

        :param level: Optional `level` to filter on. May be int or the str 'NULL'.
        :return: list of PipelineStage
        """
        with session() as s:
            q = s.query(cls).filter(cls.enabled_version_id.isnot(None))
            if level == "NULL":
                q = q.filter_by(level=None)
            if isinstance(level, int):
                q = q.filter_by(level=level)
            return q.order_by(func.isnull(cls.priority).asc(), cls.priority.asc()).all()


class SFilterVersion(SBase):
    __tablename__ = "filter_version"
    filter_version_id = Column(
        INTEGER(unsigned=True), autoincrement=True, primary_key=True
    )
    filter_id = Column(INTEGER(unsigned=True))
    created_at = Column(DATETIME, default=utcnow)
    code = Column(TEXT)
    comment = Column(VARCHAR(255))
    validated_at = Column(DATETIME)
    enabled_at = Column(DATETIME)
    disabled_at = Column(DATETIME)
    disabled_log_id = Column(INTEGER(unsigned=True))
    public = Column(TINYINT, nullable=False, default=False)

    @classmethod
    def get_name(cls, filter_v_id):
        with session() as s:
            fv = s.query(cls).get(filter_v_id)
            f = s.query(SFilter).get(fv.filter_id)
            return f.name

    @classmethod
    def enable(cls, filter_v_id):
        """
        Enable a filter version and disable other versions.

        :param filter_v_id:
        :return:
        """
        with session() as s:
            # Get this version
            fv = s.query(cls).get(filter_v_id)
            if fv is None:
                raise KeyError("FilterVersion not found")

            # Mark this version as enabled
            fv.enabled_at = utcnow()
            fv.disabled_at = None

            # Mark other versions as DISABLED
            other_versions = (
                s.query(cls)
                .filter(
                    cls.filter_id == fv.filter_id, cls.filter_version_id != filter_v_id
                )
                .all()
            )
            for other_fv in other_versions:
                if other_fv.enabled_at:
                    other_fv.enabled_at = None
                    other_fv.disabled_at = utcnow()

            # Mark this as currently enabled version of SFilter
            f = s.query(SFilter).get(fv.filter_id)
            f.enabled_version_id = filter_v_id

            s.commit()

    @classmethod
    def disable(cls, filter_v_id, log_id=None):
        with session() as s:
            fv = s.query(cls).get(filter_v_id)
            if log_id:
                fv.disabled_log_id = log_id
            fv.enabled_at = None
            fv.disabled_at = utcnow()
            f = s.query(SFilter).get(fv.filter_id)
            f.enabled_version_id = None
            f.disabled_at = fv.disabled_at
            s.commit()


@event.listens_for(SFilterVersion, "after_insert")
def update_latest_filter_version(mapper, connection, target):
    q = (
        SFilter.__table__.update()
        .where(SFilter.filter_id == target.filter_id)
        .values(
            latest_version_id=target.filter_version_id, updated_at=target.created_at
        )
    )
    connection.execute(q)
    return


class SFilterCrashLog(SBase):
    __tablename__ = "filter_crash_log"
    log_id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    created_at = Column(DATETIME, default=utcnow)
    filter_id = Column(INTEGER(unsigned=True))
    filter_version_id = Column(INTEGER(unsigned=True))
    locus_id = Column(VARCHAR(100))
    alert_id = Column(VARCHAR(100))
    stacktrace = Column(LONGTEXT)
    locus = Column(LONGTEXT)

    @classmethod
    def save(
        cls, filter_id, filter_version_id, locus_id, alert_id, stacktrace, locus_dict
    ):
        lg = cls(
            filter_id=filter_id,
            filter_version_id=filter_version_id,
            locus_id=locus_id,
            alert_id=alert_id,
            stacktrace=stacktrace,
            locus=json_dumps(locus_dict, indent=4),
        )
        with session() as s:
            s.add(lg)
            s.commit()
            assert lg.log_id is not None
            return lg.log_id

    @classmethod
    def get(cls, log_id):
        with session() as s:
            lg = s.query(cls).get(log_id)
            if not lg:
                raise ValueError(f"SFilterCrashLog {log_id} not found")
            d = lg.as_dict()
            d["locus"] = json.loads(d["locus"])
            return d


class SWatchList(SBase):
    __tablename__ = "watch_list"
    watch_list_id = Column(BIGINT(unsigned=True), autoincrement=True, primary_key=True)
    name = Column(VARCHAR(100), nullable=False)
    created_at = Column(DATETIME, default=utcnow)
    user_id = Column(INTEGER)
    slack_channel = Column(VARCHAR(100))
    enabled = Column(BOOLEAN)
    description = Column(VARCHAR(255))


class SWatchObjectId(SBase):
    """
    This table is used to generate WatchObject IDs. It does not store data.

    This method of creating IDs is easy and safe and efficient enough, but not
    the most efficient. If efficiency is a problem we could look at using a
    COUNTER column in a dedicated C* table.

    This object should not be queried or manipulated other than by calling
    `generate_ids()`.

    """

    __tablename__ = "watch_object_id"
    watch_object_id = Column(
        BIGINT(unsigned=True), autoincrement=True, primary_key=True
    )

    @classmethod
    def generate_ids(cls, n):
        """
        Generate `n` new unique WatchObject IDs.

        :param n: int
        :return: list of int
        """
        with session() as s:
            objs = [cls() for _ in range(n)]
            s.add_all(objs)
            s.commit()
            ids = [o.watch_object_id for o in objs]
            # It should be safe to delete these rows. The autoincrement counter
            # will remain even if they are deleted...
            # for o in objs:
            #     s.delete(o)
            # s.commit()
        return ids


class SStorage(SBase):
    __tablename__ = "storage"
    storage_key = Column(VARCHAR(255), primary_key=True)
    created_at = Column(DATETIME, default=utcnow)
    data = Column(LONGBLOB)

    @classmethod
    def put(cls, storage_key, data):
        """
        Save a blob to storage.

        :param storage_key: Storage.storage_key value
        :param data: string or bytes object
        """
        storage = cls(storage_key=storage_key, data=data)
        with session() as s:
            s.add(storage)
            s.commit()
            assert storage.storage_key is not None
            return storage.storage_key

    @classmethod
    def put_bson(cls, key, obj):
        return cls.put(key, bson.dumps(obj))

    @classmethod
    def put_file(cls, key, fpath):
        with open(fpath, "rb") as f:
            return cls.put(key, f.read())

    @classmethod
    def get_data(cls, storage_key, require=False):
        """
        Get Storage by key.

        :param storage_key: Storage.storage_key value
        :return: bytes or None
        """
        with session() as s:
            st = s.query(cls).get(storage_key)
            if st:
                return st.data
            if require:
                raise KeyError(f'SStorage not found "{storage_key}"')

    @classmethod
    def get_bson(cls, key):
        data = cls.get_data(key)
        if data:
            return bson.loads(data)

    @classmethod
    def update(cls, storage_key, data):
        with session() as s:
            storage = s.query(cls).get(storage_key)
            if storage:
                storage.data = data
                s.commit()
                return storage.storage_key
            else:
                return SStorage.put(storage_key, data)

    @classmethod
    def update_bson(cls, storage_key, obj):
        cls.update(storage_key, bson.dumps(obj))

    @classmethod
    def keys(cls, prefix=None, like=None):
        assert bool(prefix) + bool(like) < 2
        if prefix:
            like = prefix + "%"
        if not like:
            like = "%"
        with session() as s:
            rows = s.query(cls.storage_key).filter(cls.storage_key.like(like)).all()
            return [r[0] for r in rows]

    @classmethod
    def delete(cls, key):
        with session() as s:
            s.query(cls.storage_key).filter(cls.storage_key == key).delete(
                synchronize_session=False
            )
            s.commit()


class SUser(SBase):
    __tablename__ = "user"
    user_id = Column(INTEGER, primary_key=True)
    name = Column(VARCHAR(32))
    username = Column(VARCHAR(32), unique=True, nullable=False)
    email = Column(VARCHAR(255), nullable=False)
    _password = Column(VARCHAR(255), nullable=False)
    staff = Column(BOOLEAN, nullable=False, default=False)
    admin = Column(BOOLEAN, nullable=False, default=False)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = pbkdf2_sha256.hash(password)

    def check_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)


class STokenBlacklist(SBase):
    __tablename__ = "token_blacklist"
    token_id = Column(INTEGER, primary_key=True)
    token_type = Column(VARCHAR(10), nullable=False)
    jti = Column(VARCHAR(36), nullable=False)
    expires = Column(DATETIME, nullable=False)


class SLocusAnnotation(SBase):
    __tablename__ = "locus_annotation"
    __table_args__ = (UniqueConstraint("owner_id", "locus_id", name="_owner_locus_uc"),)
    locus_annotation_id = Column(INTEGER, primary_key=True)
    owner_id = Column(INTEGER, nullable=False)
    locus_id = Column(VARCHAR(36), nullable=False)
    comment = Column(VARCHAR(255))
    favorited = Column(BOOLEAN, default=False)
