import sqlalchemy_utils

from antares import log
from . import engine, schema

__all__ = [
    "bootstrap_dev",
    "create_main_db",
    "create_catalog_db",
    "drop_main_db",
    "drop_catalog_db",
    "truncate_main_db",
]


def bootstrap_dev(mocks=True):
    drop_main_db()
    create_main_db(mocks=mocks)


def load_main_mocks():
    log.info("Loading Filter mocks")
    from test import sample_filters

    sample_filters.bootstrap()
    log.info("Loading Storage mocks")
    from test import sample_storage

    sample_storage.bootstrap()
    log.info("Loading User mocks")
    from test import sample_users

    sample_users.bootstrap()


def load_catalog_mocks():
    log.info("Loading catalog mocks")
    from test import sample_catalogs

    sample_catalogs.bootstrap()


def drop_main_db():
    e = engine.get_engine(name=engine.DEFAULT_ENGINE)
    url = str(e.url)  # convert SQLAlchemy URL class to str
    assert url.endswith("_dev") or url.endswith("_staging")
    drop_db(url)


def drop_catalog_db():
    e = engine.get_engine(name=engine.CATALOG_ENGINE)
    url = str(e.url)  # convert SQLAlchemy URL class to str
    assert url.endswith("_dev") or url.endswith("_staging")
    drop_db(url)


def drop_db(url):
    assert url.endswith("_dev") or url.endswith("_staging")
    log.info("Dropping DB:")
    log.info(url)
    if sqlalchemy_utils.database_exists(url):
        sqlalchemy_utils.drop_database(url)


def create_main_db(mocks=False):
    e = engine.get_engine(name=engine.DEFAULT_ENGINE)
    if not sqlalchemy_utils.database_exists(e.url):
        log.info("Creating DB %s", e.url)
        sqlalchemy_utils.create_database(e.url)
        schema.SBase.metadata.create_all(bind=e)
        if mocks:
            load_main_mocks()


def create_catalog_db(mocks=False):
    e = engine.get_engine(name=engine.CATALOG_ENGINE)
    if not sqlalchemy_utils.database_exists(e.url):
        log.info("Creating DB %s", e.url)
        sqlalchemy_utils.create_database(e.url)
        if mocks:
            load_catalog_mocks()


def truncate_main_db(mocks=False):
    """
    Truncate main DB. Does not affect catalogs.

    :param mocks: If True then load mocks after truncating.
    """
    e = engine.get_engine()
    database = e.url.database
    assert database.endswith("_dev") or database.endswith("_staging")
    for cls in schema.SBase.__subclasses__():
        if cls.__tablename__:
            table = cls.__tablename__
            e.dispose()
            e.execute(f"""TRUNCATE {database}.{table};""")
    if mocks:
        load_main_mocks()
