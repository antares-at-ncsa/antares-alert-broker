import collections
import io

import requests

URL_IMAGE_LIST = "http://ps1images.stsci.edu/cgi-bin/ps1filenames.py"
URL_IMAGE_CUTOUT = "http://ps1images.stsci.edu/cgi-bin/fitscut.cgi"
ARCSEC_PER_PIXEL = 0.25


ImageRecord = collections.namedtuple(
    "ImageRecord",
    [
        "projcell",
        "skycell",
        "ra",
        "dec",
        "filter",
        "mjd",
        "type",
        "filename",
        "shortname",
        "badflag",
    ],
)


def get_image_list(ra, dec):
    """
    Get the available images at this location from Pan-STARRS.

    Parameters
    ----------
    ra: float
    dec: float

    Returns
    ----------
    [ImageRecord]

    """
    response = requests.get(URL_IMAGE_LIST, params={"ra": ra, "dec": dec, "sep": ","})
    response.raise_for_status()
    # The image list comes back as a CSV file.
    table = [line.split(",") for line in response.content.decode().split()]
    return [ImageRecord(*row) for row in table[1:]]


# Get Image Cutout
def get_image_cutout(ra, dec, r, g=None, b=None, image_size=256, format="jpg"):
    """
    Parameters
    ----------
    ra: float
    dec: float
    r: str {'g', 'r', 'i', 'z', 'y'}
    g: str {'g', 'r', 'i', 'z', 'y'}
    b: str {'g', 'r', 'i', 'z', 'y'}
    image_size: int
        Width of the (square) image cutout
    format: str {'jpg', 'png', 'fits'}

    Raises
    ----------
    ValueError
        If `r`, `g` or `b` are set to an invalid or unavailable filter code
    requests.HTTPError
        If the request to Pan-STARRS fails

    Returns
    ----------
    io.BytesIO

    """
    # Validate arguments
    FILTER_CODES = {"g", "r", "i", "z", "y"}
    AVAILABLE_FORMATS = {"jpg", "png", "fits"}
    if r not in FILTER_CODES:
        raise ValueError(f"r must be one of {FILTER_CODES}")
    if g and g not in FILTER_CODES:
        raise ValueError(f"g must be one of {FILTER_CODES}")
    if b and b not in FILTER_CODES:
        raise ValueError(f"b must be one of {FILTER_CODES}")
    if format not in AVAILABLE_FORMATS:
        raise ValueError(f"format must be one of {AVAILABLE_FORMATS}")

    # Check that images are available in the requested filters
    image_list = get_image_list(ra, dec)
    available_filters = {image.filter for image in image_list}
    if r not in available_filters:
        raise ValueError(
            f"filter {r} not available at this position, choose one of {available_filters}"
        )
    if g and g not in available_filters:
        raise ValueError(
            f"filter {g} not available at this position, choose one of {available_filters}"
        )
    if b and b not in available_filters:
        raise ValueError(
            f"filter {b} not available at this position, choose one of {available_filters}"
        )

    # Build request to Pan-STARRS
    params = {
        "ra": ra,
        "dec": dec,
        "size": image_size,
        "format": format,
        "output_size": image_size,
        "red": next(image for image in image_list if image.filter == r).filename,
    }
    if g:
        params["green"] = next(
            image for image in image_list if image.filter == g
        ).filename
    if b:
        params["blue"] = next(
            image for image in image_list if image.filter == b
        ).filename

    # Get Image Cutout
    response = requests.get(URL_IMAGE_CUTOUT, params=params)
    response.raise_for_status()
    return io.BytesIO(response.content)
