import gzip
import io
import os
from typing import Dict

import astropy.coordinates
import astropy.io
import astropy.units
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from astropy.coordinates import BarycentricTrueEcliptic
from reportlab.lib import pagesizes, units
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Frame, Paragraph

from antares.config import config
from antares.rtdb.api import RTDB
from antares.services import mars
from . import services

FINDER_CHART_DEFAULTS = {
    "arcsec_per_pixel": 0.25,
    "compass_origin_x": 245,
    "compass_origin_y": 245,
    "compass_height": 70,
    "compass_width": 50,
    "compass_head_width": 10,
    "compass_fontsize": 18,
    "compass_color": "#000000",
    "crosshair_color": "#FFFFFF",
    "scalebar_thickness": 10,
    "scalebar_color": "#000000",
    "scalebar_fontsize": 18,
}

IMAGE_MARGIN = 50
FINDER_CHART_WIDTH = 512
FINDER_CHART_HEIGHT = 512


def get_thumbnail_images(
    alert_id, inverse=False
) -> Dict[mars.ThumbnailType, io.BytesIO]:
    thumbnail_images = {}
    for thumbnail_type in mars.ThumbnailType:
        thumbnail: mars.Thumbnail = mars.get_ztf_thumbnail(alert_id, thumbnail_type)
        if not thumbnail:
            continue
        # Decompress the FITS binary data and load it with astropy.
        fits_blob = io.BytesIO()
        fits_blob.write(gzip.decompress(thumbnail.blob))
        fits_blob.seek(0)
        fits_img = astropy.io.fits.open(fits_blob)
        # Plot the thumbnail so we can save it as a JPEG
        plt.axis()
        plt.setp(plt.gca(), frame_on=True, xticks=(), yticks=())
        plt.tight_layout()
        cmap = "gray_r" if inverse else "gray"
        plt.imshow(
            fits_img[0].data,
            cmap=cmap,
            norm=colors.SymLogNorm(
                linscale=1.0,
                linthresh=15.0,
                vmin=fits_img[0].data.min(),
                vmax=fits_img[0].data.max(),
            ),
        )
        jpg_thumbnail = io.BytesIO()
        plt.savefig(jpg_thumbnail, format="jpg", bbox_inches="tight", pad_inches=0)
        plt.close()
        jpg_thumbnail.seek(0)
        thumbnail_images[thumbnail_type] = jpg_thumbnail
    return thumbnail_images


def draw_scalebar(axis, size, resolution, thickness=3, color="yellow", fontsize=18):
    width = size / resolution
    axis.plot((10, 10 + width), (30, 30), color=color, linewidth=thickness)
    axis.text(4, 20, f"{size} arcsec", color=color, fontsize=fontsize)
    return axis


def draw_crosshair(axis, position, length, color="yellow", thickness=1):
    axis.plot(position[0], position[1], "b+", color=color, mew=thickness, ms=length)
    return axis


def draw_compass(
    axis, position, height, width, fontsize=18, color="yellow", head_width=5
):
    axis.arrow(
        position[0],
        position[1],
        0,
        -height,
        color=color,
        width=2,
        head_width=head_width,
        shape="right",
    )
    axis.text(
        position[0] - 18, position[1] - height, "N", color=color, fontsize=fontsize
    )
    axis.arrow(
        position[0],
        position[1],
        -width,
        0,
        color=color,
        width=2,
        head_width=head_width,
        shape="left",
    )
    axis.text(
        position[0] - width - head_width,
        position[1] - 5,
        "E",
        color=color,
        fontsize=fontsize,
    )
    return axis


def annotate_image_cutout(
    image_cutout, crosshair=True, scalebar=True, compass=True, inverse=True, **kwargs
):
    settings = dict(kwargs, **FINDER_CHART_DEFAULTS)
    # Setup plot
    plt.figure(figsize=(8, 8))
    plt.setp(plt.gca(), frame_on=True, xticks=(), yticks=())
    plt.tight_layout()
    axis = plt.gca()

    # Load image
    cmap = "gray"
    if inverse:
        cmap = "gray_r"
    img = plt.imread(image_cutout, format="jpg")
    width, height, *_ = img.shape
    plt.imshow(img, cmap=cmap)

    # Draw annotations
    if scalebar:
        draw_scalebar(
            axis,
            10,
            services.panstarrs.ARCSEC_PER_PIXEL,
            thickness=settings["scalebar_thickness"],
            color=settings["scalebar_color"],
            fontsize=settings["scalebar_fontsize"],
        )
    if crosshair:
        draw_crosshair(
            axis, (width / 2, height / 2), 20, color=settings["crosshair_color"]
        )
    if compass:
        compass_origin_x = width - 10
        compass_origin_y = height - 10
        draw_compass(
            axis,
            (compass_origin_x, compass_origin_y),
            40,
            20,
            fontsize=settings["compass_fontsize"],
            color=settings["compass_color"],
            head_width=settings["compass_head_width"],
        )

    # Write data to buffer
    data = io.BytesIO()
    plt.savefig(data, format="jpg", bbox_inches="tight", pad_inches=0)
    plt.close()
    data.seek(0)
    return data


def draw_header(canvas):
    width, height = canvas._pagesize
    canvas.drawImage(
        os.path.join(os.path.dirname(__file__), "assets", "img", "antares.png"),
        1.75 * units.cm,
        height - 2 * units.cm,
        width=8 * units.cm,
        preserveAspectRatio=True,
        anchor="sw",
        mask="auto",
    )
    canvas.drawImage(
        os.path.join(os.path.dirname(__file__), "assets", "img", "antares-a.png"),
        width - 3.1 * units.cm,
        height - 2 * units.cm,
        width=1.3 * units.cm,
        preserveAspectRatio=True,
        anchor="sw",
        mask="auto",
    )
    canvas.setLineWidth(0.5)
    canvas.line(
        1.75 * units.cm,
        height - 2.1 * units.cm,
        width - 1.75 * units.cm,
        height - 2.1 * units.cm,
    )


def draw_finder_chart(canvas, ra, dec, cutout_r, cutout_g, cutout_b, **kwargs):
    width, height = canvas._pagesize
    image_cutout = services.panstarrs.get_image_cutout(
        ra, dec, cutout_r, cutout_g, cutout_b, image_size=512
    )
    finder_chart = annotate_image_cutout(image_cutout, **kwargs)
    canvas.drawImage(
        ImageReader(finder_chart),
        1.75 * units.cm,
        height - 16.45 * units.cm,
        width=13.5 * units.cm,
        height=13.5 * units.cm,
        anchor="sw",
    )
    textobject = canvas.beginText()
    textobject.setTextOrigin(1.8 * units.cm, height - 16.75 * units.cm)
    textobject.setFont("Courier", 10)
    textobject.textLine(text="Finder Chart")
    canvas.drawText(textobject)


def draw_thumbnails(canvas, alert_id, **kwargs):
    width, height = canvas._pagesize
    thumbnail_images = get_thumbnail_images(alert_id)
    for i, thumbnail_type in enumerate(mars.ThumbnailType):
        canvas.drawImage(
            ImageReader(thumbnail_images[thumbnail_type]),
            15.75 * units.cm,
            height - 7 * units.cm - i * 4.7 * units.cm,
            width=4 * units.cm,
            height=4 * units.cm,
            anchor="sw",
        )
        textobject = canvas.beginText()
        textobject.setTextOrigin(
            15.75 * units.cm, height - 7.3 * units.cm - i * 4.7 * units.cm
        )
        textobject.setFont("Courier", 10)
        textobject.textLine(text="ZTF {}".format(thumbnail_type.value.title()))
        canvas.drawText(textobject)


def draw_metadata(canvas, metadata):
    width, height = canvas._pagesize
    frame = Frame(1.6 * units.cm, 8 * units.cm, width - 3.5 * units.cm, 2.8 * units.cm)
    style = ParagraphStyle("default")
    style.fontName = "Courier"
    for key, value in metadata.items():
        text = f"<b>{key}</b> {value}"
        frame.addFromList([Paragraph(text, style)], canvas)


def render_report(locus_id, path, cutout_r, cutout_g=None, cutout_b=None, **kwargs):
    alerts = RTDB.get_alerts(locus_id)
    alerts = reversed(alerts)
    alert = next(filter(lambda alert: alert.properties["ant_survey"] == 1, alerts))
    canvas = Canvas(path, pagesize=pagesizes.letter)
    draw_header(canvas)
    draw_finder_chart(
        canvas,
        alert.properties["ant_ra"],
        alert.properties["ant_dec"],
        cutout_r,
        cutout_g,
        cutout_b,
        **kwargs,
    )
    draw_thumbnails(canvas, alert.alert_id, **kwargs)
    coordinate = astropy.coordinates.SkyCoord(
        ra=alert.properties["ant_ra"] * astropy.units.degree,
        dec=alert.properties["ant_dec"] * astropy.units.degree,
    )
    ecliptic = coordinate.transform_to(BarycentricTrueEcliptic())
    draw_metadata(
        canvas,
        {
            "ICRS:": "<i>(ra)</i> {} <i>(dec)</i> {}".format(
                coordinate.ra.to_string(astropy.units.hour, precision=2),
                coordinate.dec.to_string(astropy.units.degree, precision=2),
            ),
            "Galactic:": "<i>(l)</i> {} <i>(b)</i> {}".format(
                coordinate.galactic.l.to_string(astropy.units.degree, decimal=True),
                coordinate.galactic.b.to_string(astropy.units.degree, decimal=True),
            ),
            "Ecliptic:": "<i>(λ)</i> {} <i>(β)</i> {}".format(
                ecliptic.lon.to_string(astropy.units.degree, decimal=True),
                ecliptic.lat.to_string(astropy.units.degree, decimal=True),
            ),
            "ANTARES Locus ID:": f"<a color='blue' href='{config.FRONTEND_BASE_URL}/loci/{locus_id}'>{locus_id}</a>",
        },
    )

    canvas.save()
