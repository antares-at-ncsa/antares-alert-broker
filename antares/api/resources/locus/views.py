import datetime
import decimal
import io
from http import HTTPStatus

import astropy
import flask_restful
from elasticsearch_dsl import Search
from flask import abort, request, send_file, url_for

from antares import utils
from antares.api.common import resource
from antares.api.common.exceptions import SeeOther
from antares.api.common.elasticsearch import transform_sky_distance_query
from antares.config import config
from antares.elasticsearch.ingest import connect as es_connect
from antares.rtdb.api import RTDB
from . import finder_chart
from .schemas import LocusListingSchema, LocusSchema
from ..catalog.schemas import CatalogEntrySchema


def transform_elasticsearch_query(query):
    """Applys transformations to an Elasticsearch query."""
    transform_sky_distance_query(query, "htm16", 16)


def serialize_coordinates(ra, dec) -> dict:
    """
    Takes ra, dec and returns a dictionary of coordinates in different formats.

    Returns
    ----------
    {
        "icrs": {
            "ra": ra,
            "dec": dec,
        },
        "galactic": {
            "l": ...,
            "b": ...,
        },
    }
    """
    ra = float(ra)
    dec = float(dec)
    coordinate = astropy.coordinates.SkyCoord(ra=ra, dec=dec, frame="icrs", unit="deg")
    return {
        "icrs": {"ra": ra, "dec": dec},
        "galactic": {"l": coordinate.galactic.l.deg, "b": coordinate.galactic.b.deg},
    }


class LocusFinderChart(flask_restful.Resource):
    methods = ["POST", "GET"]

    def post(self, locus_id, *args, **kwargs):
        if "application/pdf" not in request.accept_mimetypes:
            return None, HTTPStatus.NOT_ACCEPTABLE
        buffer = io.BytesIO()
        finder_chart.render_report(locus_id, buffer, "r")
        buffer.seek(0)
        return send_file(
            buffer,
            mimetype="application/pdf",
            as_attachment=True,
            attachment_filename=f"antares-{locus_id}.pdf",
        )


class LocusDetail(resource.ResourceDetailReadOnly):
    resource_schema = LocusSchema

    def get_resource(self, locus_id, *args, **kwargs):
        rtdb = RTDB()
        locus = rtdb.get_locus_by_id(locus_id)
        if locus is None:
            abort(HTTPStatus.NOT_FOUND)
        if "replaced_by" in locus.properties:
            raise SeeOther(
                url_for("locus_detail", locus_id=locus.properties["replaced_by"])
            )
        locus.lightcurve = rtdb.get_lightcurve(locus_id)
        locus.resource_meta = {
            "coordinates": serialize_coordinates(locus.ra, locus.dec)
        }
        return locus, {}


class LocusList(resource.ResourceListReadOnly):
    resource_schema = LocusListingSchema

    def get_resource_collection(self, *args, **kwargs):
        s = Search()

        # We specify a handful of aggregations that run over the returned resultset.
        # These fields all live in the "resource_meta" field of the returned response.
        s = s.extra(explain=True)
        s.aggs.bucket("tags", "terms", field="tags", size=100)
        s.aggs.bucket("catalogs", "terms", field="catalogs", size=100)
        s.aggs.metric("max_num_measurements", "max", field="properties.num_mag_values")
        s.aggs.metric("min_num_measurements", "min", field="properties.num_mag_values")

        # Pagination and sorting
        limit = kwargs["limit"]
        offset = kwargs["offset"]
        s = s[offset : offset + limit]
        if kwargs["sort"]:
            s = s.sort({kwargs["sort"]["field"]: {"order": kwargs["sort"]["order"]}})

        # Allow the caller to pass an ES query body to the API. We use "query" in the
        # strictest sense of the term in an ES request. That is, users cannot pass
        # custom aggregations, sizes, or other search parameters. They may only pass
        # the bit that lives under the top-level "query" field in the ES request body.
        if kwargs["elasticsearch_query"] and kwargs["elasticsearch_query"].get("query"):
            query = kwargs["elasticsearch_query"]["query"]
            transform_elasticsearch_query(query)
            s.update_from_dict({"query": query})

        # Some documents in the index have a "properties.replaced_by" field that indicates
        # they have been merged into another locus. We favor the newer locus so we will exclude
        # any documents that have this field.
        s = s.exclude("exists", field="properties.replaced_by")

        # Now we search and format the returned documents appropriately.
        body = s.to_dict()
        client = es_connect()
        search_result = client.search(index=config.ARCHIVE_INDEX_NAME, body=body)
        docs = search_result["hits"]["hits"]
        count = search_result["hits"]["total"]["value"]
        loci = []
        for doc in docs:
            locus = doc["_source"]
            locus["resource_meta"] = {"explanation": doc["_explanation"]}
            loci.append(locus)

        return loci, count, {"aggregations": search_result["aggregations"]}


class LocusCatalogMatchList(resource.ResourceListReadOnly):
    resource_schema = CatalogEntrySchema

    def get_resource_collection(self, locus_id, *args, **kwargs):
        rtdb = RTDB()

        # Try and find the locus in our database. Return 404 NOT FOUND if it isn't there
        # or a 303 SEE OTHER if it has the `properties.replaced_by` property from being
        # merged with another locus.
        locus = rtdb.get_locus_by_id(locus_id)
        if locus is None:
            abort(HTTPStatus.NOT_FOUND)
        if "replaced_by" in locus.properties:
            raise SeeOther(
                url_for(
                    "locus_catalog_match_list", locus_id=locus.properties["replaced_by"]
                )
            )

        ##
        catalog_matches = []
        catalog_objects = rtdb.get_catalog_objects(locus.ra, locus.dec)
        for (catalog_name, matches) in catalog_objects.items():
            catalog_config = rtdb.cat_config.by_name(catalog_name)
            for match in matches:
                # These fields aren't JSON-serializable with the Python stdlib json
                # module. We replace any datetimes with a string representation and
                # any Decimals with a float representation.
                for key in match.keys():
                    if isinstance(match[key], datetime.datetime):
                        match[key] = utils.format_dt(match[key])
                    elif isinstance(match[key], decimal.Decimal):
                        match[key] = float(match[key])

                # Pull out some key fields.
                ra = match[catalog_config["ra_column"]]
                dec = match[catalog_config["dec_column"]]
                radius = astropy.coordinates.Angle(
                    match.get(
                        catalog_config.get("radius_column"),
                        catalog_config.get("radius"),
                    ),
                    unit=catalog_config.get("radius_unit") or "degree",
                )

                # Calculate the angular separation between the locus and the matched
                # object.
                match_coordinates = astropy.coordinates.SkyCoord(
                    ra=ra,
                    dec=dec,
                    frame="icrs",
                    unit="deg",
                )
                locus_coordinates = astropy.coordinates.SkyCoord(
                    ra=locus.ra, dec=locus.dec, frame="icrs", unit="deg"
                )
                separation = match_coordinates.separation(locus_coordinates)

                # Build and append our catalog match representation.
                catalog_matches.append(
                    {
                        "catalog_entry_id": f"{catalog_name}:{match.get(catalog_config.get('object_id_column'), '')}",
                        "catalog_id": catalog_config["catalog_id"],
                        "dec": dec,
                        "object_id": match.get(
                            catalog_config.get("object_id_column"), ""
                        ),
                        "object_name": match.get(
                            catalog_config.get("object_name_column"), ""
                        ),
                        "properties": match,
                        "ra": ra,
                        "resource_meta": {
                            "catalog_name": catalog_config.get("display_name"),
                            "radius": radius.arcsecond,
                            "separation": separation.arcsecond,
                        },
                    }
                )

        return catalog_matches, len(catalog_matches), {}
