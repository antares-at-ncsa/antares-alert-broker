from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class LocusListingSchema(Schema):
    class Meta:
        type_ = "locus_listing"
        self_view_many = "locus_list"

    id = fields.Str(attribute="locus_id")
    htm16 = fields.Int()
    ra = fields.Float()
    dec = fields.Float()
    properties = fields.Dict()
    locus = Relationship(
        related_view="locus_detail",
        related_view_kwargs={"locus_id": "<locus_id>"},
        schema="LocusSchema",
        type_="locus",
        many=False,
    )
    alerts = Relationship(
        related_view="alert_list",
        related_view_kwargs={"locus_id": "<locus_id>"},
        schema="AlertSchema",
        type_="alert",
        many=True,
    )
    # TODO: Will break client versions <= v1.0.4
    # catalog_matches = Relationship(
    #     related_view="locus_catalog_match_list",
    #     related_view_kwargs={"locus_id": "<locus_id>"},
    #     schema="CatalogEntrySchema",
    #     type_="catalog_entry",
    #     many=True,
    # )
    tags = fields.List(fields.Str())
    catalogs = fields.List(fields.Str())
    resource_meta = fields.ResourceMeta()
    document_meta = fields.DocumentMeta()


class LocusSchema(Schema):
    class Meta:
        type_ = "locus"
        self_view = "locus_detail"
        self_view_kwargs = {"locus_id": "<locus_id>"}
        self_view_many = "locus_list"

    id = fields.Str(attribute="locus_id")
    htm16 = fields.Int()
    ra = fields.Float()  # in deg
    dec = fields.Float()  # in deg
    properties = fields.Dict()
    lightcurve = fields.Raw()
    alerts = Relationship(
        related_view="alert_list",
        related_view_kwargs={"locus_id": "<locus_id>"},
        schema="AlertSchema",
        type_="alert",
        many=True,
    )
    # TODO: Will break client versions <= v1.0.4
    # catalog_matches = Relationship(
    #     related_view="locus_catalog_match_list",
    #     related_view_kwargs={"locus_id": "<locus_id>"},
    #     schema="CatalogEntrySchema",
    #     type_="catalog_entry",
    #     many=True,
    # )
    tags = fields.List(fields.Str())
    catalogs = fields.List(fields.Str())
    resource_meta = fields.ResourceMeta()
