from marshmallow import Schema as PlainSchema
from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class _WatchedObjectSchema(PlainSchema):
    right_ascension = fields.Float(required=True)
    declination = fields.Float(required=True)
    radius = fields.Float(required=True)
    comment = fields.Str()


class WatchListSchema(Schema):
    class Meta:
        type_ = "watch_list"
        self_view = "watch_list_detail"
        self_view_kwargs = {"watch_list_id": "<watch_list_id>"}
        self_view_many = "watch_list_list"

    id = fields.Integer(attribute="watch_list_id", as_string=True)
    name = fields.String(required=True)
    created_at = fields.DateTime()
    slack_channel = fields.String()
    enabled = fields.Boolean()
    description = fields.String()
    objects = fields.List(fields.Nested(_WatchedObjectSchema), required=True)
    owner = Relationship(
        related_view="user_detail",
        related_view_kwargs={"user_id": "<user_id>"},
        schema="UserSchema",
        type_="user",
        many=False,
    )
