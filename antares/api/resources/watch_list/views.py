"""
API Resource exposing user watch lists.

Routes
----------

/users/<user_id:int>/watch_lists (GET, POST)
/watch_lists/<watch_list_id:int> (GET, DELETE)

Notes
----------

These routes expose the "watch list" and "watch objects" as a single
"watch list" resource. A watch list resource has, among other fields,
an `object` property that comprises a list of objects the user would
like to watch.

These routes are additionally interesting because they have to
fetch/create/delete data from both the C* and SQL databases. 
"""

from flask_jwt_extended import current_user
from sqlalchemy.exc import IntegrityError

from antares.api.common import resource
from antares.api.common.decorators import login_required
from antares.api.common.exceptions import *
from antares.api.common.sql_utils import query_collection
from antares.rtdb.api import RTDB
from antares.sql.engine import session as start_session
from antares.sql.schema import SWatchList, SWatchObjectId
from .schemas import WatchListSchema


class WatchListList(resource.ResourceList):
    resource_schema = WatchListSchema

    @login_required
    def get_resource_collection(self, *args, **kwargs):
        if current_user.user_id != kwargs.get("user_id"):
            raise Unauthorized(None)
        with start_session() as session:
            # Get the collection of watch lists from SQL
            watch_lists, count = query_collection(
                session,
                SWatchList,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
                filters=(SWatchList.user_id == kwargs.get("user_id"),),
            )
            # Manually join each watch list's set of watch objects onto
            # it.
            watch_lists = [watch_list.as_dict() for watch_list in watch_lists]
            for watch_list in watch_lists:
                watch_list["objects"] = [
                    {
                        "right_ascension": watch_object["ra"],
                        "declination": watch_object["dec"],
                        "radius": watch_object["radius"],
                        "comment": watch_object["name"],
                    }
                    for watch_object in RTDB().get_watch_objects(
                        watch_list["watch_list_id"]
                    )
                ]
        return watch_lists, count, {}

    @login_required
    def create_resource(self, resource, *args, **kwargs):
        if (
            current_user.user_id != kwargs.get("user_id")
            or kwargs.get("user_id") != resource["owner"]
        ):
            raise Unauthorized(None)
        with start_session() as session:
            try:
                watch_list = SWatchList(
                    name=resource["name"],
                    description=resource.get("description"),
                    slack_channel=resource.get("slack_channel"),
                    user_id=resource["owner"],
                )
                session.add(watch_list)
                session.flush()
            except IntegrityError as error:
                raise ResourceConflictException(str(error.orig.args))

            try:
                RTDB.put_watch_list(
                    wl_id=watch_list.watch_list_id,
                    name=watch_list.name,
                    description=watch_list.description,
                    slack_channel=watch_list.slack_channel,
                )
            except:
                # TODO: Rollback SQL changes
                raise

            try:
                objects = resource["objects"]
                object_ids = SWatchObjectId.generate_ids(len(objects))
                for (object_, id_) in zip(objects, object_ids):
                    RTDB().put_watch_object(
                        wl_id=watch_list.watch_list_id,
                        wo_id=id_,
                        ra=object_["right_ascension"],
                        dec=object_["declination"],
                        radius=object_["radius"],
                        name=object_.get("comment"),
                    )
            except:
                raise

            session.commit()
            session.refresh(watch_list)
            watch_list.objects = objects
            return watch_list, {}


class WatchListDetail(resource.ResourceDetail):
    resource_schema = WatchListSchema
    methods = ["GET", "DELETE"]

    @login_required
    def get_resource(self, watch_list_id, *args, **kwargs):
        with start_session() as session:
            watch_list = session.query(SWatchList).get(watch_list_id)
            if watch_list is None or watch_list.user_id != current_user.user_id:
                raise Unauthorized(None)
            watch_list = watch_list.as_dict()
            watch_list["objects"] = [
                {
                    "right_ascension": watch_object["ra"],
                    "declination": watch_object["dec"],
                    "radius": watch_object["radius"],
                    "comment": watch_object["name"],
                }
                for watch_object in RTDB().get_watch_objects(
                    watch_list["watch_list_id"]
                )
            ]
        return watch_list, {}

    @login_required
    def delete_resource(self, watch_list_id, *args, **kwargs):
        with start_session() as session:
            watch_list = session.query(SWatchList).get(watch_list_id)
            if watch_list is None or watch_list.user_id != current_user.user_id:
                raise Unauthorized(None)
            session.delete(watch_list)
            session.commit()
            RTDB().delete_watch_list(watch_list_id)
        return {}
