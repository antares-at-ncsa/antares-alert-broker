from http import HTTPStatus

from flask import abort

from antares.api.common.resource import ResourceDetailReadOnly, ResourceListReadOnly
from antares.rtdb.api import RTDB
from .schemas import CatalogSchema


class CatalogList(ResourceListReadOnly):
    resource_schema = CatalogSchema

    def get_resource_collection(self, *args, **kwargs):
        """
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        catalogs = RTDB().get_catalogs()
        return catalogs, len(catalogs), {}


class CatalogDetail(ResourceDetailReadOnly):
    resource_schema = CatalogSchema

    def get_resource(self, catalog_id, *args, **kwargs):
        """
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        catalog = RTDB().get_catalog(catalog_id)
        if catalog is None:
            abort(HTTPStatus.NOT_FOUND)
        return catalog, {}
