from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class CatalogSchema(Schema):
    class Meta:
        type_ = "catalog"
        self_view = "catalog_detail"
        self_view_kwargs = {"catalog_id": "<catalog_id>"}
        self_view_many = "catalog_list"

    id = fields.Str(attribute="catalog_id")
    name = fields.Str(attribute="table")


class CatalogEntrySchema(Schema):
    class Meta:
        type_ = "catalog_entry"
        self_view = "catalog_entry_detail"
        self_view_kwargs = {"catalog_entry_id": "<catalog_entry_id>"}

    id = fields.Str(attribute="catalog_entry_id")
    object_id = fields.Str()
    object_name = fields.Str()
    name = fields.Str()
    ra = fields.Float()
    dec = fields.Float()
    properties = fields.Dict()
    catalog = Relationship(
        related_view="catalog_detail",
        related_view_kwargs={"catalog_id": "<catalog_id>"},
        schema="CatalogSchema",
        type_="catalog",
    )
    resource_meta = fields.ResourceMeta()
