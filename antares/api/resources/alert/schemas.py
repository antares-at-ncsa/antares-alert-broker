from marshmallow_jsonapi import fields

from antares.api.common.schema import Schema


class AlertSchema(Schema):
    class Meta:
        type_ = "alert"

    id = fields.Str()
    mjd = fields.Float()
    properties = fields.Dict()
    processed_at = fields.DateTime()
    test = fields.Str()
