from antares.api.common import resource
from antares.rtdb.api import RTDB
from .schemas import AlertSchema


class AlertList(resource.ResourceListReadOnly):
    resource_schema = AlertSchema

    def get_resource_collection(self, locus_id, **kwargs):
        rtdb = RTDB()
        alerts = [
            {"id": alert.alert_id, "mjd": alert.mjd, "properties": alert.properties}
            for alert in rtdb.get_alerts(locus_id=locus_id)
        ]
        return alerts, len(alerts), {"count": len(alerts)}
