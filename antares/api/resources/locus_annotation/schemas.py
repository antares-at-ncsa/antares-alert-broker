from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class LocusAnnotationSchema(Schema):
    class Meta:
        type_ = "locus_annotation"

    id = fields.Integer(attribute="locus_annotation_id", as_string=True)
    comment = fields.String()
    favorited = fields.Boolean()
    locus = Relationship(
        related_view="locus_detail",
        related_view_kwargs={"locus_id": "<locus_id>"},
        schema="LocusSchema",
        type_="locus",
        many=False,
        data_key="locus",
    )
    owner = Relationship(
        related_view="user_detail",
        related_view_kwargs={"user_id": "<owner_id>"},
        schema="UserSchema",
        type_="user",
        many=False,
    )
