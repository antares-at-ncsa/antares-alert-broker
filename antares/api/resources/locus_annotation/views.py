from flask_jwt_extended import current_user
from sqlalchemy.exc import IntegrityError

from antares.api.common import resource
from antares.api.common.decorators import login_required
from antares.api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    Unauthorized,
)
from antares.api.common.sql_utils import query_collection, build_query_filters
from antares.elasticsearch.api import get_loci_by_ids
from antares.sql.engine import session as start_session
from antares.sql.schema import SLocusAnnotation
from .schemas import LocusAnnotationSchema


class LocusAnnotationList(resource.ResourceList):
    resource_schema = LocusAnnotationSchema

    @login_required
    def get_resource_collection(self, **kwargs):
        if not current_user.admin and current_user.user_id != kwargs.get("user_id"):
            raise Unauthorized(None)
        with start_session() as session:
            annotations, count = query_collection(
                session,
                SLocusAnnotation,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                filters=(
                    SLocusAnnotation.owner_id == kwargs.get("user_id"),
                    *build_query_filters(SLocusAnnotation, kwargs["filter"]),
                ),
            )
            annotations = list(annotation.as_dict() for annotation in annotations)
            if "locus" in kwargs["include"]:
                locus_ids = [annotation["locus_id"] for annotation in annotations]
                # TODO: get_loci_by_ids takes a limit parameter so we should probably
                # pass the same limit that we use above?
                loci, _ = get_loci_by_ids(locus_ids)
                for annotation in annotations:
                    annotation["locus"] = next(
                        (
                            locus
                            for locus in loci
                            if locus["locus_id"] == annotation["locus_id"]
                        ),
                        None,
                    )
        return annotations, count, None

    @login_required
    def create_resource(self, resource, **kwargs):
        if (
            current_user.user_id != kwargs.get("user_id")
            or kwargs.get("user_id") != resource["owner"]
        ):
            raise Unauthorized(None)
        with start_session() as session:
            try:
                annotation = SLocusAnnotation(
                    owner_id=resource["owner"],
                    locus_id=resource["locus"],
                    comment=resource["comment"],
                    favorited=resource["favorited"],
                )
                session.add(annotation)
                session.flush()
            except IntegrityError as error:
                raise ResourceConflictException(str(error.orig.args))
            session.commit()
            session.refresh(annotation)
        return annotation, None


class LocusAnnotationDetail(resource.ResourceDetail):
    methods = ["PATCH"]
    resource_schema = LocusAnnotationSchema

    @login_required
    def patch_resource(self, resource, locus_annotation_id, *args, **kwargs):
        allowed_fields = {"comment", "favorited"}
        del resource["locus_annotation_id"]
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        with start_session() as session:
            locus_annotation = session.query(SLocusAnnotation).get(locus_annotation_id)
            if (
                locus_annotation is None
                or locus_annotation.owner_id != current_user.user_id
            ):
                raise Unauthorized(None)
            session.query(SLocusAnnotation).filter(
                SLocusAnnotation.locus_annotation_id == locus_annotation_id
            ).update(resource)
            session.commit()
            locus_annotation = session.query(SLocusAnnotation).get(locus_annotation_id)
        return locus_annotation, {}
