from marshmallow import Schema, fields, validate


class LoginQueryParameters(Schema):
    type = fields.Str(missing="token", validate=validate.OneOf(["token", "cookie"]))


class ForgotPasswordCredentials(Schema):
    username = fields.Str(required=True)


class LoginCredentials(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)


class PasswordResetCredentials(Schema):
    password = fields.Str(required=True)


class TokenResponse(Schema):
    access_token = fields.Str(required=True)
    refresh_token = fields.Str(required=False)
    token_type = fields.Str(required=True)
    expires_in = fields.Int()
