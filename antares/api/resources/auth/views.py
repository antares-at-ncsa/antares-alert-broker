"""
The /auth endpoints of the ANTARES API don't conform to the JSON:API
specification. We subclass flask_restful.Resource in this module for
views instead of the antares.api.common.resource.Resource* classes.
"""
import datetime
from http import HTTPStatus

from flask import request, jsonify
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    current_user,
    fresh_jwt_required,
    get_raw_jwt,
    jwt_required,
    jwt_refresh_token_required,
    set_refresh_cookies,
    set_access_cookies,
    unset_jwt_cookies,
    verify_jwt_refresh_token_in_request,
)
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_mail import Message
from flask_restful import Resource
from marshmallow import ValidationError

from antares.api.main import mail
from antares.config import config
from antares.sql.engine import session as start_session
from antares.sql.schema import STokenBlacklist, SUser
from .schemas import (
    ForgotPasswordCredentials,
    LoginCredentials,
    LoginQueryParameters,
    TokenResponse,
    PasswordResetCredentials,
)


def _construct_token_response_body(
    user, fresh_access_token=False, include_refresh_token=False
):
    payload = {
        "access_token": create_access_token(
            user,
            expires_delta=datetime.timedelta(seconds=config.API_ACCESS_TOKEN_EXPIRES),
            fresh=fresh_access_token,
        ),
        "token_type": "bearer",
    }
    if include_refresh_token:
        payload["refresh_token"] = create_refresh_token(
            user,
            expires_delta=datetime.timedelta(seconds=config.API_REFRESH_TOKEN_EXPIRES),
        )
    return TokenResponse().load(payload)


def _construct_token_response_cookie(
    user, fresh_access_token=False, include_refresh_token=False
):
    access_token = create_access_token(
        user,
        expires_delta=datetime.timedelta(seconds=config.API_ACCESS_TOKEN_EXPIRES),
        fresh=fresh_access_token,
    )
    if include_refresh_token:
        refresh_token = create_refresh_token(
            user,
            expires_delta=datetime.timedelta(seconds=config.API_REFRESH_TOKEN_EXPIRES),
        )
    response = jsonify({"identity": user.user_id})
    set_access_cookies(response, access_token)
    if include_refresh_token:
        set_refresh_cookies(response, refresh_token)
    response.status_code = HTTPStatus.OK
    return response


def construct_token_response(
    user, type_="token", fresh_access_token=False, include_refresh_token=False
):
    if type_ == "token":
        return _construct_token_response_body(
            user, fresh_access_token, include_refresh_token
        )
    elif type_ == "cookie":
        return _construct_token_response_cookie(
            user, fresh_access_token, include_refresh_token
        )
    else:
        raise ValueError('type_ must be one of ["token", "cookie"]')


class AuthCheck(Resource):
    methods = ["GET"]

    @jwt_required
    def get(self):
        response = jsonify({})
        response.status_code = HTTPStatus.NO_CONTENT
        return response


class Forgot(Resource):
    methods = ["POST"]

    def post(self):
        try:
            credentials = ForgotPasswordCredentials().load(request.get_json())
        except ValidationError as error:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        with start_session() as session:
            user = (
                session.query(SUser)
                .filter(SUser.username == credentials["username"])
                .first()
            )
            if user and user.email:
                message = self._build_email(user)
                mail.send(message)

        response = jsonify({})
        response.status_code = HTTPStatus.NO_CONTENT
        return response

    def _build_email(self, user):
        access_token = create_access_token(
            user,
            expires_delta=datetime.timedelta(seconds=config.API_ACCESS_TOKEN_EXPIRES),
            fresh=True,
        )
        message = Message(
            "ANTARES Password Reset",
            sender="antares_help@antares.noao.edu",
            recipients=[user.email],
        )
        url = f"{config.FRONTEND_BASE_URL}/forgot-password?token={access_token}"
        message.html = "".join(
            [
                "<p>Click or copy and paste the link below into your browser "
                "to reset your ANTARES password. If you didn't request to reset "
                "your password, you can ignore this email.</p>",
                f'<a href="{url}">{url}</a>',
            ]
        )
        return message


class Login(Resource):
    methods = ["POST"]

    def post(self):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            credentials = LoginCredentials().load(request.get_json())
        except ValidationError as error:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        with start_session() as session:
            user = (
                session.query(SUser)
                .filter(SUser.username == credentials["username"])
                .first()
            )
            if not user or not SUser.check_password(user, credentials["password"]):
                return {"error": "invalid_client"}, HTTPStatus.UNAUTHORIZED

        return construct_token_response(
            user, query_parameters["type"], include_refresh_token=True
        )


class LoginFresh(Resource):
    methods = ["POST"]

    def post(self):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            credentials = LoginCredentials().load(request.get_json())
        except ValidationError as error:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        with start_session() as session:
            user = (
                session.query(SUser)
                .filter(SUser.username == credentials["username"])
                .first()
            )
            if not user or not SUser.check_password(user, credentials["password"]):
                return {"error": "invalid_client"}, HTTPStatus.UNAUTHORIZED

        return construct_token_response(
            user,
            query_parameters["type"],
            fresh_access_token=True,
            include_refresh_token=False,
        )


class Reset(Resource):
    methods = ["POST"]

    @fresh_jwt_required
    def post(self):
        """
        Reset a user's password.

        Routes
        ----------
        POST /auth/reset

        Notes
        ----------
        Successfully resetting a user's password requires a fresh JWT token.
        A user can obtain a fresh token from the /auth/login-fresh endpoint.

        Resetting a user's password DOES NOT blacklist their current access
        or refresh tokens and DOES NOT sign them out of current sessions. It
        likely should but the risk that this attack surface exposes is
        acceptable for the time-being. If, in the future, we implement a
        method of tracking tokens by user ID, we should address this issue.

        """
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            credentials = PasswordResetCredentials().load(request.get_json())
        except ValidationError:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST
        with start_session() as session:
            user = session.query(SUser).get(current_user.user_id)
            user.password = credentials["password"]
            session.commit()
        return construct_token_response(
            current_user,
            query_parameters["type"],
            fresh_access_token=False,
            include_refresh_token=True,
        )


class Logout(Resource):
    methods = ["POST"]

    def post(self):
        """
        The POST /auth/logout route serves two purposes. One is to add a valid refresh
        token to our database's blacklist. The other is to clear the authorization
        cookies from the user's browser, if they're set. These are HTTP only cookies
        to prevent XSS attacks but the browser will continue sending the cookies and
        raising "invalid token" errors or whatever if the server doesn't clear them.
        We assume that if a client POST /auth/logout they want to clear their cookies
        and so no matter what happens, we clear the cookies in the response (and so
        all the try/except logic that could probably be cleaner (TODO)).
        """
        try:
            verify_jwt_refresh_token_in_request()
            jwt = get_raw_jwt()
            with start_session() as session:
                token_blacklist = STokenBlacklist(
                    **{
                        "token_type": "refresh",
                        "jti": jwt["jti"],
                        "expires": datetime.datetime.fromtimestamp(jwt["exp"]),
                    }
                )
                session.add(token_blacklist)
                session.commit()
        finally:
            response = jsonify({})
            unset_jwt_cookies(response)
            response.status_code = HTTPStatus.NO_CONTENT
            return response


class Refresh(Resource):
    methods = ["POST"]

    @jwt_refresh_token_required
    def post(self):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
        except ValidationError:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Blacklist old refresh token
        jwt = get_raw_jwt()
        with start_session() as session:
            token_blacklist = STokenBlacklist(
                **{
                    "token_type": "refresh",
                    "jti": jwt["jti"],
                    "expires": datetime.datetime.fromtimestamp(jwt["exp"]),
                }
            )
            session.add(token_blacklist)

        # Create access and refresh tokens
        access_token = create_access_token(
            current_user,
            expires_delta=datetime.timedelta(seconds=config.API_ACCESS_TOKEN_EXPIRES),
        )
        refresh_token = create_refresh_token(
            current_user,
            expires_delta=datetime.timedelta(seconds=config.API_REFRESH_TOKEN_EXPIRES),
        )

        # If the requester asked for a token...
        if query_parameters["type"] == "token":
            return TokenResponse().load(
                {
                    "access_token": access_token,
                    "refresh_token": refresh_token,
                    "token_type": "bearer",
                }
            )

        # Otherwise, if they asked for a cookie...
        elif query_parameters["type"] == "cookie":
            response = jsonify({})
            set_access_cookies(response, access_token)
            set_refresh_cookies(response, refresh_token)
            response.status_code = HTTPStatus.NO_CONTENT
            return response
