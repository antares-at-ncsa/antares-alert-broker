from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class FilterSchema(Schema):
    class Meta:
        type_ = "filter"
        self_view = "filter_detail"
        self_view_kwargs = {"filter_id": "<filter_id>"}
        self_view_many = "filter_list"

    id = fields.Integer(attribute="filter_id", as_string=True)
    name = fields.Str()
    created_at = fields.DateTime()
    disabled_at = fields.DateTime()
    updated_at = fields.DateTime()
    level = fields.Int()
    priority = fields.Int()
    user_id = fields.Int()
    description = fields.Str()
    latest_version = Relationship(
        related_view="filter_version_detail",
        related_view_kwargs={
            "filter_id": "<filter_id>",
            "filter_version_id": "<latest_version_id>",
        },
        schema="FilterVersionSchema",
        type_="filter_version",
    )
    enabled_version = Relationship(
        related_view="filter_version_detail",
        related_view_kwargs={
            "filter_id": "<filter_id>",
            "filter_version_id": "<enabled_version_id>",
        },
        schema="FilterVersionSchema",
        type_="filter_version",
    )
    public = fields.Bool(default=False)
    enabled = fields.Bool(default=False)


class FilterVersionSchema(Schema):
    class Meta:
        type_ = "filter_version"
        self_view = "filter_version_detail"
        self_view_kwargs = {
            "filter_version_id": "<filter_version_id>",
            "filter_id": "<filter_id>",
        }
        self_view_many = "filter_version_list"

    id = fields.Str(attribute="filter_version_id")
    filter = Relationship(
        related_view="filter_detail",
        related_view_kwargs={"filter_id": "<filter_id>"},
        schema=FilterSchema,
        type_="filter",
    )
    created_at = fields.DateTime()
    code = fields.Str()
    comment = fields.Str()
    validated_at = fields.DateTime()
    enabled_at = fields.DateTime()
    disabled_at = fields.DateTime()
    # disabled_log_id = fields.Int()
    public = fields.Boolean()
