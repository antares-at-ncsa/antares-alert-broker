from flask_jwt_extended import current_user
from sqlalchemy.exc import IntegrityError

from antares.api.common.decorators import login_optional, login_required
from antares.api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    ResourceNotFoundException,
    Unauthorized,
)
from antares.api.common.resource import (
    ResourceDetail,
    ResourceDetailReadOnly,
    ResourceList,
)
from antares.api.common.sql_utils import query_collection, build_query_filters
from antares.sql.engine import session as start_session
from antares.sql.schema import SFilter, SFilterVersion
from .schemas import FilterSchema, FilterVersionSchema


class FilterList(ResourceList):
    resource_schema = FilterSchema

    @login_optional
    def get_resource_collection(self, *args, **kwargs):
        # Build an SQL query filter set from the request.
        query_filters = tuple()
        # If the `user_id` kwarg is set, that means that we've reached this view under
        # a route like /users/<user_id>/filters. We should only return filters that are
        # owned by the user_id specified here.
        if kwargs.get("user_id"):
            query_filters += (SFilter.user_id == kwargs.get("user_id"),)
        # If the user is not logged in, they can only view public filters:
        if not current_user:
            query_filters += (SFilter.public,)
        else:
            # Admins can list all filters in the system but other users can only list their
            # filters and any public ones owned by other users.
            if not current_user.admin:
                query_filters += (
                    (SFilter.user_id == current_user.user_id) | SFilter.public,
                )
        if kwargs["filter"]:
            query_filters += tuple(build_query_filters(SFilter, kwargs["filter"]))

        # Run the query
        with start_session() as session:
            filters, count = query_collection(
                session,
                SFilter,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
                filters=query_filters,
            )
        return filters, count, None

    @login_required
    def create_resource(self, resource, *args, **kwargs):
        if current_user.user_id != kwargs.get("user_id"):
            raise Unauthorized(None)
        with start_session() as session:
            try:
                filter_ = SFilter(**resource, user_id=current_user.user_id)
                session.add(filter_)
                session.commit()
                session.refresh(filter_)
                return filter_, None
            except IntegrityError as error:
                raise ResourceConflictException(str(error.orig.args))


class FilterDetail(ResourceDetail):
    resource_schema = FilterSchema
    methods = ["GET", "PATCH"]

    @login_optional
    def get_resource(self, filter_id, *args, **kwargs):
        with start_session() as session:
            filter_ = session.query(SFilter).get(filter_id)
            if filter_ is None:
                raise ResourceNotFoundException(
                    f"No resource filter with ID {filter_id}"
                )
            if (
                not (current_user and current_user.admin)
                and not (current_user and filter_.user_id == current_user.user_id)
                and not filter_.public
            ):
                raise Unauthorized(None)
            filter_ = filter_.as_dict()
            if "latest_version" in kwargs["include"]:
                latest_version = session.query(SFilterVersion).get(
                    filter_["latest_version_id"]
                )
                filter_["latest_version"] = latest_version
            if "enabled_version" in kwargs["include"]:
                enabled_version = session.query(SFilterVersion).get(
                    filter_["enabled_version_id"]
                )
                filter_["enabled_version"] = enabled_version

            return filter_, None

    @login_required
    def patch_resource(self, resource, filter_id, *args, **kwargs):
        allowed_fields = {"public"}
        del resource["filter_id"]
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        with start_session() as session:
            filter_ = session.query(SFilter).get(filter_id)
            if not current_user.admin and (
                filter_ is None or filter_.user_id != current_user.user_id
            ):
                raise Unauthorized(None)
            session.query(SFilter).filter(SFilter.filter_id == filter_id).update(
                resource
            )
            session.commit()
            filter_ = session.query(SFilter).get(filter_id)
        return filter_, {}


class FilterVersionDetail(ResourceDetailReadOnly):
    resource_schema = FilterVersionSchema

    @login_optional
    def get_resource(self, filter_id, filter_version_id, *args, **kwargs):
        with start_session() as session:
            filter_ = session.query(SFilter).get(filter_id)
            if filter_ is None:
                raise Unauthorized(None)
            if (
                not (current_user and current_user.admin)
                and not (current_user and filter_.user_id == current_user.user_id)
                and not filter_.public
            ):
                raise Unauthorized(None)
            return session.query(SFilterVersion).get(filter_version_id), {}


class FilterVersionList(ResourceList):
    resource_schema = FilterVersionSchema

    @login_optional
    def get_resource_collection(self, filter_id, *args, **kwargs):
        with start_session() as session:
            filter_ = session.query(SFilter).get(filter_id)
            if filter_ is None:
                raise Unauthorized(None)
            if (
                not (current_user and current_user.admin)
                and not (current_user and filter_.user_id == current_user.user_id)
                and not filter_.public
            ):
                raise Unauthorized(None)
            filter_versions, count = query_collection(
                session,
                SFilterVersion,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
                filters=(SFilterVersion.filter_id == filter_id,),
            )
        return filter_versions, count, None

    @login_required
    def create_resource(self, resource, filter_id, **kwargs):
        with start_session() as session:
            filter_ = session.query(SFilter).get(filter_id)
            if filter_ is None:
                raise Unauthorized(None)
            if not current_user.admin and filter_.user_id != current_user.user_id:
                raise Unauthorized(None)
            filter_version = SFilterVersion(**resource, filter_id=filter_id)
            session.add(filter_version)
            session.commit()
            session.refresh(filter_version)
            return filter_version, None
