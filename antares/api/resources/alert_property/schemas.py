from marshmallow_jsonapi import fields

from antares.api.common.schema import Schema


class AlertPropertySchema(Schema):
    class Meta:
        type_ = "alert_property"
        self_view = "alert_property_detail"
        self_view_kwargs = {"alert_property_name": "<name>"}
        self_view_many = "alert_property_list"

    id = fields.Str(attribute="name")
    type = fields.Str()
    origin = fields.Str()
    # filter_version = Relationship(
    #     related_view="filter_version_detail",
    #     related_view_kwargs={
    #         "filter_id": "<filter_id>",
    #         "filter_version_id": "<latest_version_id>",
    #     },
    #     schema="FilterVersionSchema",
    #     type_="filter_version",
    # )
    description = fields.Str()
    es_mapping = fields.Str()
