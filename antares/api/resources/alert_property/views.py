from antares.api.common.exceptions import ResourceNotFoundException
from antares.api.common.resource import ResourceDetailReadOnly, ResourceListReadOnly
from antares.api.common.sql_utils import query_collection
from antares.sql.engine import session as start_session
from antares.sql.schema import SAlertProperty
from .schemas import AlertPropertySchema


class AlertPropertyList(ResourceListReadOnly):
    resource_schema = AlertPropertySchema

    def get_resource_collection(self, *args, **kwargs):
        with start_session() as session:
            alert_properties, count = query_collection(
                session,
                SAlertProperty,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
            )
            return alert_properties, count, None


class AlertPropertyDetail(ResourceDetailReadOnly):
    resource_schema = AlertPropertySchema

    def get_resource(self, alert_property_name, *args, **kwargs):
        with start_session() as session:
            alert_property = session.query(SAlertProperty).get(alert_property_name)
            if alert_property is None:
                raise ResourceNotFoundException(
                    f"No resource alert_property with ID {alert_property_name}"
                )
            return alert_property, None
