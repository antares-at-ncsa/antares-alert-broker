from antares.api.common.exceptions import ResourceNotFoundException
from antares.api.common.resource import ResourceDetailReadOnly, ResourceListReadOnly
from antares.sql.engine import session as start_session
from antares.sql.schema import STag
from .schemas import TagSchema


class TagList(ResourceListReadOnly):
    resource_schema = TagSchema

    def get_resource_collection(self, *args, **kwargs):
        with start_session() as session:
            tags = (
                session.query(STag)
                .order_by(STag.name.desc())
                .limit(kwargs["limit"])
                .offset(kwargs["offset"])
            )
            count = session.query(STag).count()
        return tags, count, None


class TagDetail(ResourceDetailReadOnly):
    resource_schema = TagSchema

    def get_resource(self, tag_name, *args, **kwargs):
        with start_session() as session:
            tag = session.query(STag).get(tag_name)
            if tag is None:
                raise ResourceNotFoundException(f"No resource tag with ID {tag_name}")
            return tag, None
