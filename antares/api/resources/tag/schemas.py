from marshmallow_jsonapi import fields

from antares.api.common.schema import Schema


class TagSchema(Schema):
    class Meta:
        type_ = "tag"
        self_view = "tag_detail"
        self_view_kwargs = {"tag_name": "<name>"}
        self_view_many = "tag_list"

    id = fields.String(attribute="name")
    filter_version_id = fields.Integer()
    description = fields.String()
