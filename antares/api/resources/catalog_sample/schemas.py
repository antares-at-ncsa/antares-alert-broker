from marshmallow_jsonapi import fields

from antares.api.common.schema import Schema


class CatalogSampleSchema(Schema):
    class Meta:
        type_ = "catalog_sample"
        self_view_many = "catalog_sample"

    # Note: The `id` column does NOT uniquely identify the CatalogSample.
    # The true unique ID is the combination of (catalog_name, object_id).
    id = fields.String(attribute="object_id")
    catalog_name = fields.String()

    # The Catalog Object data is dynamic with respect to the API.
    # Each catalog table (identified here by catalog_name) has a unique schema.
    # This dynamic data is encapsulated into this dict field:
    data = fields.Dict()
