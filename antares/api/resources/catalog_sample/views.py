from antares.api.common.resource import ResourceListReadOnly
from antares.rtdb.api import RTDB
from antares.config import config
from .schemas import CatalogSampleSchema


class CatalogSampleList(ResourceListReadOnly):
    resource_schema = CatalogSampleSchema

    def get_resource_collection(self, *args, **kwargs):
        """
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        catalog_objects = RTDB().get_sample_catalog_data(n=5)
        catalog_samples = []
        for catalog_name, objects in catalog_objects.items():
            table_config = config.CATALOG_CONFIG.by_name(catalog_name)
            object_id_column = table_config["object_id_column"]
            for obj in objects:
                # Create CatalogSampleSchema format
                catalog_samples.append(
                    {
                        "object_id": obj[object_id_column],
                        "catalog_name": catalog_name,
                        "data": obj,
                    }
                )
        count = len(catalog_samples)
        return catalog_samples, count, None
