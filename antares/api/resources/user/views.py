from flask_jwt_extended import current_user
from sqlalchemy.exc import IntegrityError

from antares.api.common import resource
from antares.api.common.decorators import login_required, admin_required
from antares.api.common.exceptions import *
from antares.sql.engine import session as start_session
from antares.sql.schema import SUser
from .schemas import UserSchema


class UserList(resource.ResourceList):
    resource_schema = UserSchema

    @admin_required
    def get_resource_collection(self, *args, **kwargs):
        with start_session() as session:
            users = (
                session.query(SUser)
                .order_by(SUser.username.desc())
                .limit(kwargs["limit"])
                .offset(kwargs["offset"])
            )
            count = session.query(SUser).count()
        return users, count, {}

    def create_resource(self, resource, *args, **kwargs):
        with start_session() as session:
            try:
                user = SUser(
                    name=resource["name"],
                    email=resource["email"],
                    username=resource["username"],
                    password=resource["password"],
                )
                session.add(user)
                session.flush()
            except IntegrityError as error:
                raise ResourceConflictException(str(error.orig.args))
            session.commit()
            session.refresh(user)
            return user, {}


class UserDetail(resource.ResourceDetail):
    resource_schema = UserSchema

    @login_required
    def get_resource(self, user_id, *args, **kwargs):
        if current_user.user_id != user_id and not current_user.staff:
            raise Unauthorized(None)
        with start_session() as session:
            user = session.query(SUser).get(user_id)
        if user is None:
            raise ResourceNotFoundException(f"No resource user with ID {user_id}")
        return user, {}

    @admin_required
    def patch_resource(self, resource, user_id, *args, **kwargs):
        del resource["user_id"]
        with start_session() as session:
            session.query(SUser).filter(SUser.user_id == user_id).update(resource)
            session.commit()
            user = session.query(SUser).get(user_id)
        return user, {}


# # This code block is commented out but I've left it in the code base
# # as an example of how to use a /relationships endpoint in the API.
# class FavoriteLociRelationships(resource.ResourceRelationships):
#     class ResourceSchema(Schema):
#         class Meta:
#             type_ = "locus"

#         id = marshmallow.fields.String(attribute="locus_id")

#     resource_schema = ResourceSchema

#     @login_required
#     def get_relationships(self, user_id, **kwargs):
#         if current_user.user_id != user_id:
#             raise Unauthorized(None)
#         with start_session() as session:
#             favorite_loci, count = query_collection(
#                 session,
#                 SFavoriteLocus,
#                 limit=kwargs["limit"],
#                 offset=kwargs["offset"],
#                 filters=(SFavoriteLocus.user_id == user_id,),
#             )
#         return favorite_loci, count, None

#     @login_required
#     def create_relationships(self, user_id, related_ids, **kwargs):
#         if current_user.user_id != user_id:
#             raise Unauthorized(None)
#         with start_session() as session:
#             for locus_id in related_ids:
#                 favorite_locus = SFavoriteLocus(user_id=user_id, locus_id=locus_id,)
#                 session.add(favorite_locus)
#             session.commit()

#     @login_required
#     def delete_relationships(self, user_id, related_ids, **kwargs):
#         if current_user.user_id != user_id:
#             raise Unauthorized(None)
#         with start_session() as session:
#             for locus_id in related_ids:
#                 session.query(SFavoriteLocus).filter(
#                     SFavoriteLocus.user_id == user_id
#                 ).filter(SFavoriteLocus.locus_id == locus_id).delete()
#             session.commit()
