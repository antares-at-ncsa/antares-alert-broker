from marshmallow_jsonapi import fields

from antares.api.common.schema import Relationship, Schema


class UserSchema(Schema):
    class Meta:
        type_ = "user"
        self_view = "user_detail"
        self_view_kwargs = {"user_id": "<user_id>"}
        self_view_many = "user_list"

    id = fields.Integer(attribute="user_id", as_string=True)
    name = fields.String(required=True)
    username = fields.String(required=True)
    email = fields.String(required=True)
    password = fields.String(required=True, load_only=True)
    staff = fields.Boolean()
    admin = fields.Boolean()

    locus_annotations = Relationship(
        related_view="user_locus_annotations",
        related_view_kwargs={"user_id": "<user_id>"},
        schema="LocusAnnotationSchema",
        type_="locus_annotation",
        many=True,
    )
