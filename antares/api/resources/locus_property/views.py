from antares.api.common.exceptions import ResourceNotFoundException
from antares.api.common.resource import ResourceDetailReadOnly, ResourceListReadOnly
from antares.api.common.sql_utils import query_collection
from antares.sql.engine import session as start_session
from antares.sql.schema import SLocusProperty
from .schemas import LocusPropertySchema


class LocusPropertyList(ResourceListReadOnly):
    resource_schema = LocusPropertySchema

    def get_resource_collection(self, *args, **kwargs):
        with start_session() as session:
            locus_properties, count = query_collection(
                session,
                SLocusProperty,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
            )
            return locus_properties, count, None


class LocusPropertyDetail(ResourceDetailReadOnly):
    resource_schema = LocusPropertySchema

    def get_resource(self, locus_property_name, *args, **kwargs):
        with start_session() as session:
            locus_property = session.query(SLocusProperty).get(locus_property_name)
            if locus_property is None:
                raise ResourceNotFoundException(
                    f"No resource locus_property with ID {locus_property_name}"
                )
            return locus_property, None
