from .alert.views import AlertList
from .alert_property.views import AlertPropertyDetail, AlertPropertyList
from .auth.views import AuthCheck, Forgot, Login, LoginFresh, Logout, Refresh, Reset
from .catalog.views import CatalogDetail, CatalogList
from .catalog_sample.views import CatalogSampleList
from .filter.views import (
    FilterDetail,
    FilterList,
    FilterVersionDetail,
    FilterVersionList,
)
from .locus.views import LocusDetail, LocusList, LocusFinderChart, LocusCatalogMatchList
from .locus_annotation.views import LocusAnnotationDetail, LocusAnnotationList
from .locus_property.views import LocusPropertyDetail, LocusPropertyList
from .tag.views import TagList, TagDetail
from .user.views import (
    UserDetail,
    UserList,
)
from .watch_list.views import WatchListDetail, WatchListList


def initialize_routes(api):
    api.add_resource(AlertList, "/loci/<string:locus_id>/alerts", endpoint="alert_list")
    api.add_resource(
        AlertPropertyList, "/alert_properties", endpoint="alert_property_list"
    )
    api.add_resource(
        AlertPropertyDetail,
        "/alert_properties/<string:alert_property_name>",
        endpoint="alert_property_detail",
    )
    api.add_resource(AuthCheck, "/auth/check", endpoint="auth_check")
    api.add_resource(Login, "/auth/login", endpoint="login")
    api.add_resource(Forgot, "/auth/forgot", endpoint="forgot")
    api.add_resource(LoginFresh, "/auth/login-fresh", endpoint="login_fresh")
    api.add_resource(Logout, "/auth/logout", endpoint="logout")
    api.add_resource(Refresh, "/auth/refresh", endpoint="refresh")
    api.add_resource(Reset, "/auth/reset", endpoint="reset")
    api.add_resource(FilterList, "/filters", endpoint="filter_list", methods=["GET"])
    api.add_resource(FilterDetail, "/filters/<int:filter_id>", endpoint="filter_detail")
    api.add_resource(
        FilterVersionList,
        "/filters/<int:filter_id>/versions",
        endpoint="filter_version_list",
    )
    api.add_resource(
        FilterVersionDetail,
        "/filters/<int:filter_id>/versions/<int:filter_version_id>",
        endpoint="filter_version_detail",
    )
    api.add_resource(LocusList, "/loci", endpoint="locus_list")
    api.add_resource(LocusDetail, "/loci/<string:locus_id>", endpoint="locus_detail")
    api.add_resource(
        LocusFinderChart,
        "/loci/<string:locus_id>/finder-charts",
        endpoint="locus_finder_charts",
    )
    api.add_resource(
        LocusCatalogMatchList,
        "/loci/<string:locus_id>/catalog-matches",
        endpoint="locus_catalog_match_list",
    )
    api.add_resource(
        LocusAnnotationDetail,
        "/locus_annotations/<int:locus_annotation_id>",
        endpoint="locus_annotation_detail",
    )
    api.add_resource(
        LocusPropertyList, "/locus_properties", endpoint="locus_property_list"
    )
    api.add_resource(
        LocusPropertyDetail,
        "/locus_properties/<string:locus_property_name>",
        endpoint="locus_property_detail",
    )
    api.add_resource(UserList, "/users", endpoint="user_list")
    api.add_resource(UserDetail, "/users/<int:user_id>", endpoint="user_detail")
    api.add_resource(
        LocusAnnotationList,
        "/users/<int:user_id>/locus_annotations",
        endpoint="user_locus_annotations",
    )
    api.add_resource(
        FilterList,
        "/users/<int:user_id>/filters",
        endpoint="user_filters_list",
    )
    api.add_resource(
        WatchListList,
        "/users/<int:user_id>/watch_lists",
        endpoint="user_watch_list_list",
    )
    api.add_resource(
        WatchListDetail,
        "/watch_lists/<int:watch_list_id>",
        endpoint="watch_list_detail",
    )
    api.add_resource(
        TagList,
        "/tags",
        endpoint="tag_list",
    )
    api.add_resource(
        TagDetail,
        "/tags/<string:tag_name>",
        endpoint="tag_detail",
    )
    api.add_resource(
        CatalogList,
        "/catalogs",
        endpoint="catalog_list",
    )
    api.add_resource(
        CatalogDetail,
        "/catalogs/<int:catalog_id>",
        endpoint="catalog_detail",
    )
    api.add_resource(
        CatalogSampleList,
        "/catalog_samples",
        endpoint="catalog_sample_list",
    )
