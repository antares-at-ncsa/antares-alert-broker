from http import HTTPStatus


class BaseException(Exception):
    title = NotImplemented
    detail = NotImplemented
    status_code = NotImplemented

    def __init__(self, detail):
        Exception.__init__(self)
        self.detail = detail

    def to_dict(self):
        return {"title": self.title, "detail": self.detail, "status": self.status_code}


class ResourceNotFoundException(BaseException):
    status_code = HTTPStatus.NOT_FOUND
    title = "Resource Not Found"


class ResourceConflictException(BaseException):
    status_code = HTTPStatus.CONFLICT
    title = "Resource Conflict"


class BadRequest(BaseException):
    status_code = HTTPStatus.BAD_REQUEST
    title = "Bad Request"


class Unauthorized(BaseException):
    status_code = HTTPStatus.UNAUTHORIZED
    title = "Unauthorized"


class BaseRedirect(Exception):
    status_code = NotImplemented

    def __init__(self, location):
        Exception.__init__(self)
        self.location = location


class SeeOther(BaseRedirect):
    status_code = HTTPStatus.SEE_OTHER
