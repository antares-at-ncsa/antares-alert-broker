import operator

from sqlalchemy_utils import sort_query


def query_collection(session, model, limit=10, offset=0, sort=None, filters=None):
    query = session.query(model)
    if filters:
        query = query.filter(*filters)
    count = query.count()
    if sort:
        field = sort["field"]
        if sort["order"] == "asc":
            query = sort_query(query, f"{field}")
        else:
            query = sort_query(query, f"-{field}")
    query = query.limit(limit).offset(offset)
    return query, count


def build_query_filters(model, filters):
    query_filters = []
    for filter_ in filters:
        op = getattr(operator, filter_["op"])
        field = getattr(model, filter_["field"])
        value = filter_["value"]
        query_filters.append(op(field, value))
    return query_filters
