import marshmallow
import marshmallow_jsonapi
import marshmallow_jsonapi.flask
import werkzeug
from flask import request, url_for
from marshmallow_jsonapi.fields import BaseRelationship, DocumentMeta, ResourceMeta
from marshmallow_jsonapi.utils import get_dump_key, resolve_params

TYPE = "type"
ID = "id"


class Relationship(marshmallow_jsonapi.flask.Relationship):
    def get_self_url(self, obj):
        if self.self_url:
            params = resolve_params(obj, self.self_url_kwargs, default=self.default)
            non_null_params = {
                key: value for key, value in params.items() if value is not None
            }
            if non_null_params:
                return self.self_url.format(**non_null_params)
        return None

    def get_url(self, obj, view_name, view_kwargs):
        if view_name:
            kwargs = marshmallow_jsonapi.utils.resolve_params(
                obj, view_kwargs, default=self.default
            )
            kwargs["endpoint"] = view_name
            try:
                return url_for(_external=True, **kwargs)
            except werkzeug.routing.BuildError:
                if (
                    None in kwargs.values()
                ):  # most likely to be caused by empty relationship
                    return None
                raise
        return None


class Schema(marshmallow_jsonapi.flask.Schema):
    @marshmallow.post_dump(pass_many=True, pass_original=True)
    def format_json_api_response(self, data, original, many, **kwargs):
        """Post-dump hook that formats serialized data as a top-level JSON API object.
        See: http://jsonapi.org/format/#document-top-level
        """
        ret = self.format_items(data, many, original)
        ret = self.wrap_response(ret, many)
        ret = self.render_included_data(ret)
        ret = self.render_meta_document(ret)
        return ret

    def format_item(self, item, original):
        """Format a single datum as a Resource object.
        See: http://jsonapi.org/format/#document-resource-objects
        """
        # http://jsonapi.org/format/#document-top-level
        # Primary data MUST be either... a single resource object, a single resource
        # identifier object, or null, for requests that target single resources
        if not item:
            return None

        ret = self.dict_class()
        ret[TYPE] = self.opts.type_

        # Get the schema attributes so we can confirm `dump-to` values exist
        attributes = {
            (get_dump_key(self.fields[field]) or field): field for field in self.fields
        }

        for field_name, value in item.items():
            attribute = attributes[field_name]
            if attribute == ID:
                ret[ID] = value
            elif isinstance(self.fields[attribute], DocumentMeta):
                if not self.document_meta:
                    self.document_meta = self.dict_class()
                self.document_meta.update(value)
            elif isinstance(self.fields[attribute], ResourceMeta):
                if "meta" not in ret:
                    ret["meta"] = self.dict_class()
                ret["meta"].update(value)
            elif isinstance(self.fields[attribute], BaseRelationship):
                if value:
                    if "relationships" not in ret:
                        ret["relationships"] = self.dict_class()
                    ret["relationships"][self.inflect(field_name)] = value
            else:
                if "attributes" not in ret:
                    ret["attributes"] = self.dict_class()
                ret["attributes"][self.inflect(field_name)] = value

        links = self.get_resource_links(item, original)
        if links:
            ret["links"] = links
        return ret

    def format_items(self, data, many, original):
        """Format data as a Resource object or list of Resource objects.
        See: http://jsonapi.org/format/#document-resource-objects
        """
        if many:
            return [
                self.format_item(item, original_item)
                for item, original_item in zip(data, original)
            ]
        else:
            return self.format_item(data, original)

    def get_resource_links(self, item, original):
        """Hook for adding links to a resource object."""
        if self.opts.self_url:
            ret = self.dict_class()
            kwargs = resolve_params(original, self.opts.self_url_kwargs or {})
            ret["self"] = self.generate_url(self.opts.self_url, **kwargs)
            return ret
        return None

    def get_top_level_links(self, data, many):
        try:
            return super().get_top_level_links(data, many)
        except werkzeug.routing.BuildError:
            if many and len(data) == 0:
                if self.opts.self_url_many:
                    self_link = self.generate_url(
                        self.opts.self_url_many, **request.view_args
                    )
                    return {"self": self_link}
            raise

    def generate_url(self, view_name, **kwargs):
        try:
            return super().generate_url(view_name, _external=True, **kwargs)
        except werkzeug.routing.BuildError:
            return None


class ResourceIdentifierSchema(marshmallow.Schema):
    type = marshmallow.fields.String()
    id = marshmallow.fields.String()


class RelationshipsRequestSchema(marshmallow.Schema):
    data = marshmallow.fields.List(marshmallow.fields.Nested(ResourceIdentifierSchema))
    meta = marshmallow.fields.Dict()
