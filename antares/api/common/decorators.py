import functools

from flask_jwt_extended import (
    verify_jwt_in_request,
    verify_jwt_in_request_optional,
    current_user,
)

from .exceptions import Unauthorized


def login_optional(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request_optional()
        return f(*args, **kwargs)

    return decorated_function


def login_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        return f(*args, **kwargs)

    return decorated_function


def staff_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        if not current_user.staff:
            raise Unauthorized(None)
        return f(*args, **kwargs)

    return decorated_function


def admin_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        if not current_user.admin:
            raise Unauthorized(None)
        return f(*args, **kwargs)

    return decorated_function
