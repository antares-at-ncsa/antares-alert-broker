"""
This is the entrypoint to the ANTARES HTTP API. This API conforms to
version 1.0 of the JSON:API specification.
"""

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_restful import Api

from antares.config import config
from antares.services import trace
from antares.rtdb import antcassandra
from .handlers import register_error_handlers, register_jwt_handlers

if config.DATADOG_TRACE_ENABLE:
    trace.patch_flask()

mail = Mail()
jwt = JWTManager()
api = Api(prefix="/v1")


def create_app():
    app = Flask("api")
    app.config["DEBUG"] = config.DEBUG
    # flask_restful hijacks the Flask error handler registry which causes some issues
    # with how we register error handlers. To keep error handling consistent with the
    # development environment we can set the PROPAGATE_EXCEPTIONS flag to True.
    # See: https://github.com/vimalloc/flask-jwt-extended/issues/86#issuecomment-444983119
    # for more information.
    app.config["PROPAGATE_EXCEPTIONS"] = config.API_PROPAGATE_EXCEPTIONS
    app.config["MAIL_SERVER"] = config.API_MAIL_SERVER
    app.config["MAIL_PORT"] = config.API_MAIL_PORT
    app.config["MAIL_USE_TLS"] = config.API_MAIL_USE_TLS
    app.config["MAIL_USERNAME"] = config.API_MAIL_USERNAME
    app.config["MAIL_PASSWORD"] = config.API_MAIL_PASSWORD
    app.config["JWT_SECRET_KEY"] = config.SECRET_KEY
    app.config["JWT_BLACKLIST_ENABLED"] = config.API_JWT_BLACKLIST_ENABLED
    app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = config.API_JWT_BLACKLIST_TOKEN_CHECKS
    app.config["JWT_TOKEN_LOCATION"] = config.API_JWT_TOKEN_LOCATION
    app.config["JWT_ACCESS_COOKIE_PATH"] = config.API_JWT_ACCESS_COOKIE_PATH
    app.config["JWT_REFRESH_COOKIE_PATH"] = config.API_JWT_REFRESH_COOKIE_PATH
    app.config["JWT_COOKIE_DOMAIN"] = config.API_JWT_COOKIE_DOMAIN
    app.config["JWT_COOKIE_SECURE"] = config.API_JWT_COOKIE_SECURE
    app.config["JWT_COOKIE_CSRF_PROTECT"] = config.API_JWT_COOKIE_CSRF_PROTECT
    app.config["JWT_COOKIE_SAMESITE"] = config.API_JWT_COOKIE_SAMESITE
    app.config["JWT_SESSION_COOKIE"] = config.API_JWT_SESSION_COOKIE

    CORS(app, supports_credentials=True)
    jwt.init_app(app)
    register_jwt_handlers(jwt)
    mail.init_app(app)

    from .resources.routes import initialize_routes

    initialize_routes(api)
    api.init_app(app)
    register_error_handlers(app)

    return app


if __name__ == "__main__":
    antcassandra.init()
    app = create_app()
    app.run(debug=config.DEBUG, host="0.0.0.0", port=8000)
