from http import HTTPStatus

import sqlalchemy
from flask import jsonify

from antares.sql import engine as sql_engine
from antares.sql.schema import STokenBlacklist, SUser
from .common.exceptions import BaseException, Unauthorized, SeeOther


def register_jwt_handlers(jwt):
    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decoded_token):
        """
        Check if a user-supplied token is in our SQL token blacklist.

        Notes
        -----------
        We only blacklist refresh tokens and assume that shorter-lived,
        access tokens will expire in a short enough window that we don't
        need to force-expire them.
        """
        with sql_engine.session() as session:
            return session.query(
                sqlalchemy.sql.exists().where(
                    STokenBlacklist.jti == decoded_token["jti"]
                )
            ).scalar()

    @jwt.user_claims_loader
    def add_claims_to_access_token(user):
        return {
            "user_id": user.user_id,
            "username": user.username,
        }

    @jwt.user_identity_loader
    def add_claims_to_access_token(user):
        return user.user_id

    @jwt.user_loader_callback_loader
    def user_loader_callback(identity):
        return SUser.get(identity)

    @jwt.user_loader_error_loader
    def user_error_callback(_):
        raise Unauthorized(None)

    @jwt.claims_verification_failed_loader
    def claims_verification_failed():
        raise Unauthorized(None)

    @jwt.revoked_token_loader
    def revoked_token():
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Token has been revoked",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.needs_fresh_token_loader
    def needs_fresh_token():
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Fresh token required",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.invalid_token_loader
    def invalid_token(error):
        payload = [
            {
                "status": HTTPStatus.UNPROCESSABLE_ENTITY,
                "title": "Unauthorized",
                "detail": error,
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNPROCESSABLE_ENTITY
        return response

    @jwt.expired_token_loader
    def expired_token(_):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Token has expired",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.unauthorized_loader
    def unauthorized(error):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": error,
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response


def register_error_handlers(app):
    @app.errorhandler(HTTPStatus.NOT_FOUND)
    def handle_404(error):
        payload = {
            "status": HTTPStatus.NOT_FOUND,
            "title": "404 Not Found",
            "detail": "The requested URL was not found.",
        }
        response = jsonify({"errors": [payload]})
        response.status_code = HTTPStatus.NOT_FOUND
        return response

    @app.errorhandler(SeeOther)
    def handle_303(error):
        response = jsonify({})
        response.status_code = HTTPStatus.SEE_OTHER
        response.headers["Location"] = error.location
        return response

    @app.errorhandler(Exception)
    def handle_exception(error):
        if isinstance(error, BaseException):
            response = jsonify({"errors": [error.to_dict()]})
            response.status_code = error.status_code
            return response
        payload = {
            "status": HTTPStatus.INTERNAL_SERVER_ERROR,
            "title": "Internal Server Error",
            "detail": "A server-side problem occured.",
        }
        if app.config["DEBUG"]:
            payload["detail"] = str(error)
        response = jsonify({"errors": [payload]})
        response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
        return response
