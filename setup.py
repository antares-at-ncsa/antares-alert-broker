from setuptools import setup, find_packages

from antares import __version__


with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setup(
    name="ANTARES",
    version=__version__,
    description="ANTARES Alert Broker from NOAO",
    author="Carl Stubens",
    author_email="cstubens@noao.edu",
    url="https://gitlab.com/noao/antares/pipeline",
    packages=[p for p in find_packages() if p.startswith("antares")],
    include_package_data=True,
    install_requires=requirements,
    data_files=[("antares", ["antares/version.txt"])],
)
