
ANTARES Documentation
=====================

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2

   index
   devkit/index

Basic Concepts
--------------

.. _antares: http://antares.noirlab.edu
.. _tags: http://antares.noirlab.edu/tags
.. _client: https://noao.gitlab.io/antares/client

ANTARES_ receives Alerts and executes Filters on them in realtime.
Filters crunch the data contained in each Alert and may produce data
of their own, such as tags and classifications. Tags are used to direct
Alerts to output streams which can then be recieved by other systems.
You write Filters in Python and submit them to run live in ANTARES.

Filters have access to the following data:

- The incoming Alert and all data contained within it.
- The Locus object, which represents the point on the sky.
- The history of past Alerts at the same Locus in the sky.
- Catalog objects which match the Alert.
- WatchLists and WatchObjects which match the Alert.

Filters can take action based on the input data. Filters can:

- Set and update properties on the Locus. Locus
  properties are searchable in the database using the ANTARES website
  or the Client_ library.
- Set properties on the Alert.
- Set text Tags_ on the Locus.
  Tags are searchable and browsable on the ANTARES website.

Tags_ can be directed in realtime to Kafka streams.

The following image gives a simplified view of ANTARES:

.. image:: /_static/img/pipeline_explanation.png
   :width: 100%

Writing Filters
---------------

Get started writing Filters with the :ref:`devkit`.

Accessing Data
--------------

Query for data and connect to ANTARES' output streams using the Client_.
