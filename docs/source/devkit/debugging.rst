.. _devkit.debugging:

Debugging Filters in Production
===============================

.. _pipeline: http://antares.noirlab.edu/pipeline
.. _dashboard: https://p.datadoghq.com/sb/f2b72d13f-9588842b6c358eb76596cf44a14d212e

If your filter crashes, ANTARES will disable the filter and send an error report.

There are no negative consequences of this (technical or otherwise) -- so don't worry.
We provide some tools to help you debug the issue.

The Pipeline_ page on the main ANTARES website shows which filters are currently
enabled.

This dashboard_ shows how many Alerts have come through ANTARES in
the past day, and which filters ran on them.

We also provide crash logs when filters fail, and the ability to re-run them
on the alert which caused them to break.

Filter Error Notifications
--------------------------

ANTARES sends out error notifications using Slack.
You are responsible for directing these notifications to the Slack User or Channel
of your choice by setting the variable ``ERROR_SLACK_CHANNEL`` in your filter class, like so::

    class MyFilter(dk.Filter):
        ERROR_SLACK_CHANNEL = '<slack_member_id>'
        ...

And replace ``<slack_member_id>`` with your slack member ID. Get your member ID from
your slack profile. Open your slack profile and, in the "..." menu, click on "Copy member ID".
It will be an alphanumeric code like ``UP414JK1GD``. eg::

    ERROR_SLACK_CHANNEL = 'UP414JK1GD'

You may also use a Slack channel instead of a member ID. Please create and use your own
channel. Do not use an existing channel such as ``#general``. eg::

    ERROR_SLACK_CHANNEL = '#my_team_channel'


Print Crash logs
----------------

The notifications in Slack will look like this::

    Filter "Your Filter" has crashed on Alert 12345. Crash log ID: 6789

Now you can use this information to see what went wrong. Run the following in DataLab::

    import antares.devkit as dk
    dk.init()

    crash_log_id = 6789  # Copy the value from your crash notification.
    dk.print_crash_log(crash_log_id)

You can also try to reproduce the error by running the filter again::

    class MyFilter(dk.Filter):
        ...

    locus_dict = dk.get_crash_log(crash_log_id)['locus_dict']
    locus = dk.load_locus_from_locus_dict(locus_dict)

    report = dk.run_filter(MyFilter, locus=locus)

