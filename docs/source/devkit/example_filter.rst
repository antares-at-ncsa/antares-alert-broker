
.. _devkit.example_filter:

Example of a real Filter
========================

As an example, here's a version of the "High SNR" filter
which is one of the defaults included in ANTARES. It tags
Loci which have at least one Alert with a high signal-noise ratio::

    class HighSNR(dk.Filter):
        NAME = "High SNR"
        ERROR_SLACK_CHANNEL = ""  # Put your Slack user ID here
        REQUIRED_LOCUS_PROPERTIES = [
            'ztf_object_id',
        ]
        REQUIRED_ALERT_PROPERTIES = [
            'passband',
            'ztf_sigmapsf',
        ]
        OUTPUT_LOCUS_PROPERTIES = []
        OUTPUT_ALERT_PROPERTIES = []
        OUTPUT_TAGS = [
            {
                'name': 'high_snr',
                'description': 'Locus has one or more Alerts with high SNR.',
            },
        ]

        def run(self, locus):
            """
            If this Alert has a high SNR, then tag the Locus "high_snr".
            """
            # The threshold is dependent on the band that is being imaged.
            # These thresholds should flag ~2-3% of alerts.
            snr_threshold = {
                'g': 50.0,
                'R': 55.0,
            }
            passband = locus.alert.properties['ant_passband']
            if passband not in snr_threshold:
                print(f'passband {passband} is not supported by this filter.')
                return  # Do nothing.
            threshold = snr_threshold[passband]
            sigmapsf = locus.alert.properties['ztf_sigmapsf']  # Get the ZTF Alert property "sigmapsf"
            alert_snr = 1.0 / sigmapsf
            alert_id = locus.alert.alert_id  # Get the ANTARES alert_id
            ztf_object_id = locus.properties['ztf_object_id']  # Get the ZTF Object ID
            print(f'Alert {alert_id}')
            print(f'Object {ztf_object_id}')
            print(f'snr = {alert_snr}')
            if alert_snr > threshold:
                print('High SNR detected')
                locus.tag('high_snr')
