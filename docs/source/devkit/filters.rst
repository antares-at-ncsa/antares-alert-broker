
.. _devkit.filters:

Structure of a Filter
=====================

This page describes all of the properties and functions of a Filter.
Some are optional and some are required, as noted.

::

  import antares.devkit as dk

  # Filters must inherit from `dk.Filter`.
  # There are no requirements for the name of the filter class other than
  # that it be valid Python code.
  # The formal name of the filter is whatever you enter into the form
  # on the ANTARES website when you submit the filter. The name of the
  # Filter class here is therefore unimportant, but ought to be descriptive.

  class MyFilter(dk.Filter):

      # Required.
      #
      # This allows you to receive error logs through Slack.
      # See footnotes below for more details.
      ERROR_SLACK_CHANNEL = '<my_slack_member_id>'

      # Optional.
      #
      # List of Locus properties which the Filter depends on.
      # If an incoming Alert's Locus does not have all properties listed here,
      # then the Filter will not run on it.
      INPUT_LOCUS_PROPERTIES = [
          # eg:
          'ztf_object_id',
          # etc.
      ]

      # Optional.
      #
      # List of Alert properties which the Filter depends on.
      # If an incoming Alert does not have all properties listed here, then
      # the Filter will not run on it.
      INPUT_ALERT_PROPERTIES = [
          # eg:
          'passband',
          'mag',
          'ztf_magpsf',
          # etc.
      ]

      # Optional.
      #
      # List of Tag names which the Filter depends on.
      # If an incoming Alert's Locus does not have all Tags listed here, then
      # the Filter will not run on it.
      INPUT_TAGS = [
          # eg:
          'high_snr',
          # etc.
      ]

      # Required.
      #
      # A list of all Alert properties which the filter may set.
      # If your filter doesn't set properties, then value should be an
      # empty list.
      # 'name' must be formatted like '<author>_<property_name>'.
      # 'type' must be one of the strings: 'int', 'float', or 'str'.
      # 'description' should briefly describe what the property means.
      OUTPUT_LOCUS_PROPERTIES = [
          # eg:
          {
              'name': 'stubens_interest_score',
              'type': 'float',
              'description': 'interestingness of the alert by algorithm XYZ',
          },
          {
              'name': 'stubens_object_class',
              'type': 'str',
              'description': 'probable class of object by algorithm ABC',
          },
          # etc.
      ]

      # Required.
      #
      # A list of all Alert properties which the filter may set.
      # If your filter doesn't set properties, then value should be an
      # empty list.
      # 'name' must be formatted like '<author>_<property_name>'.
      # 'type' must be one of the strings: 'int', 'float', or 'str'.
      # 'description' should briefly describe what the property means.
      OUTPUT_ALERT_PROPERTIES = [
          # eg:
          {
              'name': 'stubens_g_minus_r',
              'type': 'float',
              'description': 'estimated g-minus-r magnitude',
          },
          # etc.
      ]

      # Required.
      #
      # A list tags names which this Filter may produce.
      # If your filter does't tag Loci, then this list should be empty.
      # 'name' must be formatted like '<author>_<property_name>'.
      # 'description' should briefly describe what the tag means.
      OUTPUT_TAGS = [
          # eg:
          {
              'name': 'stubens_transients',
              'description': 'Probable transient according to method PQE'
          },
          # etc.
      ]

      # Optional.
      #
      # If your filter requires access to data files, they must be declared here.
      # See footnotes below for more details on how to work with data files.
      REQUIRES_FILES = [
          # eg:
          'soraisam_myFile.txt',
          'soraisam_myOtherFile.bin',
          # etc.
      ]

      # Optional.
      #
      # This function is called once per night when ANTARES reboots and
      # filters are instantiated. If your filter needs to do any work to prepare
      # itself to run, that logic should go here.
      # Examples:
      #  - Loading data from files
      #  - Constructing datastructures
      #  - Instantiating machine-learning model objects
      def setup(self):
          ...

      # Required.
      #
      # This is the function which is called to process an Alert.
      # All setup work should have been done in `setup()` in order to make this
      # function run as efficiently as possible.
      # See footnotes below for description of the `locus` object.
      def run(self, locus):
          ...

**See Also**

- :ref:`devkit.testing_filters` for how to run and test Filters.

- :ref:`devkit.files` for details about loading data files into your filter.

- :ref:`devkit.debugging` for details about using ``ERROR_SLACK_CHANNEL``.

- :ref:`devkit.locus` for details about the ``locus`` object which is passed to ``run()``.
