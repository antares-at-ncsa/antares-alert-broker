
.. _devkit.data:

Getting Data
============

Helper functions for getting data from the database and loading it from files.

The methods below are available on the `devkit` package eg::

    import antares.devkit as dk
    locus_ids = dk.get_locus_ids(n=100)


.. autofunction:: antares.devkit.get_locus
.. autofunction:: antares.devkit.get_locus_ids
.. autofunction:: antares.devkit.get_sample_catalog_data
.. autofunction:: antares.devkit.search_catalogs
.. autofunction:: antares.devkit.locus_from_dict
.. autofunction:: antares.devkit.locus_from_file
.. autofunction:: antares.devkit.get_crash_log
.. autofunction:: antares.devkit.print_crash_log
.. autofunction:: antares.devkit.locus_from_crash_log
.. autofunction:: antares.devkit.get_thumbnails
