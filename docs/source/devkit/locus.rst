
.. _devkit.locus:

The Locus Object
================

.. _client: http://noao.gitlab.io/antares/client
.. _properties: http://antares.noirlab.edu/properties

ANTARES filters are Python classes which must have a function ``run(self, locus)``.
The single parameter ``locus``, is an instance of a class ``Locus``. It provides
access to all data associated with the Locus:

* Locus Properties
* Locus Tags
* Alerts received by ANTARES, and their Properties
* Catalog Objects
* WatchObjects and WatchLists which the Locus matches

The Properties_ page on the Portal documents the datatype and meaning of individual
LocusProperties and AlertProperties.

The ``Locus`` class is almost identical to the Locus objects returned by the
ANTARES Client_ library, but has some additional methods and properties.

You can also interactively explore the ``Locus`` directly using the devkit::

    import antares.devkit as dk

    ld = dk.get_locus()  # Get some arbitrary Locus
    # ld = dk.get_locus(locus_id)  # or get a specific Locus by ID

    print(ld)
    print(ld.properties)  # Locus Properties
    print(ld.alert)  # The most recent Alert on the Locus

    help(ld.timeseries)

    # etc.


The ``Locus`` object spec is as follows.

.. autoproperty:: antares.pipeline.locus.Locus.locus_id
.. autoproperty:: antares.pipeline.locus.Locus.ra
.. autoproperty:: antares.pipeline.locus.Locus.dec
.. autoproperty:: antares.pipeline.locus.Locus.properties
.. autoproperty:: antares.pipeline.locus.Locus.tags
.. autoproperty:: antares.pipeline.locus.Locus.alerts
.. autoproperty:: antares.pipeline.locus.Locus.alert
.. autoproperty:: antares.pipeline.locus.Locus.lightcurve
.. autoproperty:: antares.pipeline.locus.Locus.timeseries
.. autoproperty:: antares.pipeline.locus.Locus.catalog_objects
.. autoproperty:: antares.pipeline.locus.Locus.watch_list_ids
.. autoproperty:: antares.pipeline.locus.Locus.watch_object_ids
.. automethod:: antares.pipeline.locus.Locus.tag
.. automethod:: antares.pipeline.locus.Locus.to_dict
.. automethod:: antares.pipeline.locus.Locus.to_file
