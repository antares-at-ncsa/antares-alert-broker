
.. _devkit.testing_filters:

Testing Filters
===============

Let's run a simple ``HelloWorld`` filter::

    class HelloWorld(dk.Filter):
        OUTPUT_TAGS = [
            {
                'name': 'hello_world',
                'description': 'hello!',
            },
        ]

        def run(self, locus):
            print('Hello Locus ', locus.locus_id)
            locus.tag('hello_world')

Run the filter on a randomly chosen real Locus from the database:

.. autofunction:: antares.devkit.run_filter

eg::

    # Execute HelloWorld filter on a random locus
    report = dk.run_filter(HelloWorld)

    # `run_filter()` returns a report of what the filter did. Take a look at it:
    print(report)


Run the filter on multiple Loci:

.. autofunction:: antares.devkit.run_many

eg::

    report = run_many(HelloWorld, n=100)


Constructing Locus Objects
--------------------------

You can construct your own Locus objects for testing::

    import antares.devkit as dk

    ra, dec = 88.2744186, -5.0010774
    locus_dict = {
        'locus_id': 'locus1',
        'ra': ra,
        'dec': dec,
        'properties': {
            'num_alerts': 2,
            'num_mag_values': 2,
        },
        'tags': [],
        'watch_list_ids': [],
        'watch_object_ids': [],
        'catalog_objects': dk.search_catalogs(ra, dec),
        'alerts': [
            {
                'alert_id': 'alert1',
                'locus_id': 'locus1',
                'mjd': 58794.272488399874,
                'properties': {
                    'ant_mag': 15.1,
                },
            },
            {
                'alert_id': 'alert2',
                'locus_id': 'locus1',
                'mjd': 58799.50587960007,
                'properties': {
                    'ant_mag': 15.2,
                }
            },
        ],
    }

    locus = dk.locus_from_dict(locus_dict)

    dk.run_filter(HelloWorld, locus)
