
.. _devkit.submissions:

Submitting Filters to ANTARES
=============================

.. _filters: http://antares.noirlab.edu/filters

When you're ready to submit your filter to ANTARES, go to the filters_ page on the ANTARES
website and click "Add".

**Note:** We highly recommend setting an ``ERROR_LOG_SLACK_CHANNEL`` on your filter so that you
will receive notifications of errors. See :ref:`devkit.debugging`.

When you submit your filter you will need to provide:

- **Name** -- A unique name for your filter. Name your filter like:

  - Format: ``<author or group>_<name>_<version>``
  - eg: ``stubens_sn1a_candidates_v1``

- **Description** -- A brief text description of your filter. Will be publicly visible.

- **Handler** -- The name of the filter class in your code. The handler name
  does not need to be unique outside of your code.

- **Code** -- A block of code which includes:

  - your import statements
  - filter class
  - any helper functions that your filter needs
