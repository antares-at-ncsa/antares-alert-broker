#!/usr/bin/env python3

import os
import subprocess
import sys
import time
from contextlib import contextmanager

print("Running from", sys.executable)

BASIC_FUNCTIONS = [
    "help",
    "clean",
    "docker_login",
    "docker_clean",
    "build",
    "pull",
    "push",
    "down",
    "up",
    "nosetests",
    "shell",
    "kpl",
    "kps",
    "kpp",
    "kpd",
    "kpe",
    "dcl",
    "dce",
    "dcp",
    "make_docs",
]
ALL_FUNCTIONS = [
    "help",
    "clean",
    "docker_login",
    "docker_clean",
    "build",
    "pull_build_push",
    "pull",
    "push",
    "down",
    "up",
    "nosetests",
    "deploy_config",
    "delete_config",
    "deploy_app",
    "delete_app",
    "scale_app",
    "deploy_docker_secret",
    "delete_docker_secret",
    "deploy_redis",
    "delete_redis",
    "shell",
    "sort_requirements",
    "deploy_datalab",
    "set_version",
    "ensure_cass_user",
    "deploy_kube",
    "delete_kube",
    "delete_namespace",
    "kpl",
    "kps",
    "kpp",
    "kpd",
    "kpe",
    "dcl",
    "dce",
    "dcp",
    "make_docs",
    "set_go_signal",
]

FUNCTION_ALIASES = {
    "help": "help_",
}

KUBE_APPS = {
    "frontend": "frontend",
    "api": "backend",
    "index_worker": "backend",
    "pipeline": "backend",
    "rq_worker": "backend",
    "rq_scheduler": "backend",
}

DOCKER_IMAGES = {
    "frontend": {
        "repo": "frontend",
        "context": "./antares/frontend/",
        "dockerfile": "./antares/frontend/Dockerfile.nginx",
    },
    "backend": {"repo": "backend", "context": ".", "dockerfile": "./Dockerfile"},
}

# Define globals, which will be set in configure() below.
ENV = ""
APP = ""
NAMESPACE = ""
VERSION_FILE = ""
ANTARES_VERSION = ""
DOCKERFILE = ""
DOCKER_REGISTRY = ""
DOCKER_REPO = ""
DOCKER_TAG = ""
CONTEXT = ""
IMAGE_NAME = ""
DRY_RUN = ""

PRINT_COMMANDS = True

CUSTOMIZABLE_VARS = {
    "ENV",
    "APP",
    "IMAGE_NAME",
    "DRY_RUN",
}


def main():
    """
    Call function(s) specified in sys.argv

    Some arguments are parsed as used as parameters to Python code:

    - `env=staging` results in the global variable `ENV` being set to 'staging'.
    - `-x` results in `x=True` kwarg being passed to the function.
    - `--x` results in `x=True` kwarg being passed to the function.
    - `--x=foo` results in `x='foo'` being passed to the function.

    eg:
    $ ./build.py
    $ ./build.py ENV=staging deploy-kube
    $ ./build.py ENV=production delete-app,deploy-app ztf-worker
    # ./build.py kpl ztf
    $ ./build.py set-version
    $ ./build.py env=staging deploy-kube --preserve_redis
    $ ./build.py up --down=false

    """
    # Parse command line input
    args = [a for a in sys.argv[1:] if "=" not in a and not a.startswith("-")]
    flags = {}
    kwargs = {}
    for arg in sys.argv[1:]:
        # flags like '--verbose' or '--something=123'
        if arg.startswith("--"):
            if "=" in arg[2:]:
                name, _, value = arg[2:].partition("=")
                kwargs[name] = value
            else:
                name = arg[2:]
                kwargs[name] = True
        # simple switches like '-v'
        elif arg.startswith("-"):
            kwargs[arg[1:]] = True
        # global variable overrides like 'env=staging'
        elif not arg.startswith("-") and "=" in arg:
            name, value = arg.strip().split("=")
            name = name.upper()
            if name not in CUSTOMIZABLE_VARS:
                print(f"Cannot override variable {name}")
                sys.exit(1)
            flags[name] = value
    if args:
        fns = [fn.replace("-", "_") for fn in args[0].split(",")]
        fns = [FUNCTION_ALIASES.get(fn, fn) for fn in fns]
        args = args[1:]
    else:
        fns = ["build"]  # Default function
    for fn in fns:
        if fn not in globals():
            print(f"Function {fn} does not exist.")
            sys.exit(1)

    # Set global variables
    configure(**flags)

    # Call function(s)
    for fn in fns:
        print(f"Calling {fn} with args: {args}, kwargs: {kwargs}")
        result = globals()[fn](*args, **kwargs)
        if result is not None:
            print(result)


def help_(fn=None, **kw):
    """
    Get help for build.py commands

    eg:
        ./build.py help
        ./build.py help --all
        ./build.py help <command>

    """
    all_ = bool_arg(kw.get("all", False))
    p = print
    p()
    p()
    p('ANTARES build tool "build.py"')
    p("=============================")
    p()
    if fn:
        p("Command:", fn)
        p(globals()[FUNCTION_ALIASES.get(fn, fn)].__doc__)
    else:
        functions = ALL_FUNCTIONS if all_ else BASIC_FUNCTIONS
        width = max(len(f) for f in functions) + 2
        p("Commands:")
        p()
        for name in functions:
            p(
                "  ",
                name.ljust(width),
                (globals()[FUNCTION_ALIASES.get(name, name)].__doc__ or "")
                .strip()
                .split("\n")[0],
            )
        p()
        p("Use `./build.py help <command>` learn more.")


@contextmanager
def quiet():
    """
    `with` this contextmanager, PRINT_COMMANDS is set to False, then restored
    to it's previous value.
    """
    original = PRINT_COMMANDS
    globals()["PRINT_COMMANDS"] = False
    try:
        yield
    finally:
        globals()["PRINT_COMMANDS"] = original


def code(cmd):
    """
    Execute a shell command and return its return code.
    """
    if PRINT_COMMANDS:
        print("$", cmd)
        sys.stdout.flush()
    proc = subprocess.Popen(cmd, shell=True)
    proc.communicate()
    return proc.returncode


def out(cmd):
    """
    Return stdout of a shell command, but exit() if the command failed.
    """
    if PRINT_COMMANDS:
        print("$", cmd)
        sys.stdout.flush()
    proc = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout, stderr = proc.communicate()
    if proc.returncode != 0:
        sys.exit(1)
    return stdout.decode("ascii").strip()


def run(cmd):
    """
    Execute a shell command and sys.exit(1) if it fails.
    """
    if PRINT_COMMANDS:
        print("$", cmd)
        sys.stdout.flush()
    if DRY_RUN:
        return
    proc = subprocess.Popen(cmd, shell=True)
    proc.communicate()
    if proc.returncode != 0:
        print("Command failed. build.py exiting.")
        sys.exit(1)


def meh(cmd):
    """
    Execute a command and ignore failures.
    """
    if PRINT_COMMANDS:
        print("$", cmd)
        sys.stdout.flush()
    proc = subprocess.Popen(cmd, shell=True)
    proc.communicate()


def ask(question, default=False):
    """
    Prompt user for a yes/no answer.

    :param question:
    :param default:
    :return: bool
    """
    options = "[Y/n]" if default else "[y/N]"
    default_response = "y" if default else "n"
    prompt = f"{question} {options}: "
    response = input(prompt).lower() or default_response
    return response == "y"


def bool_arg(a):
    return str(a).lower() == "true"


def g(name, value):
    """
    Set a value in globals()

    Print value if it is changing.
    """
    if PRINT_COMMANDS and value != globals().get(name):
        print(f"{name}={value}")
    globals()[name] = value


def g_del(name):
    """
    Delete a value from globals()
    """
    del globals()[name]


config_vars = {}


def configure(**vars):
    """
    Define all required global variables (above) for use in functions.
    """
    if vars:
        config_vars.update(vars)
    del vars

    # Set customizable variables
    for name, value in config_vars.items():
        if name not in CUSTOMIZABLE_VARS:
            print(f"Cannot override {name}")
            sys.exit(1)
        g(name, value)

    # Set ENV
    if not ENV:
        if "ENV" in os.environ:
            if PRINT_COMMANDS:
                print("Loaded ENV from environment variable")
            g("ENV", os.environ["ENV"])
        else:
            g("ENV", "staging")

    # Set other downstream variables
    g("VERSION_FILE", "antares/version.txt")
    g("ANTARES_VERSION", out(f"cat {VERSION_FILE}"))
    g("NAMESPACE", f"antares-{ENV}")
    g("DOCKER_REGISTRY", "registry.gitlab.com")
    g("DOCKER_TAG", ANTARES_VERSION)
    if APP:
        g("IMAGE_NAME", KUBE_APPS[APP])
    if IMAGE_NAME:
        image_config = DOCKER_IMAGES[IMAGE_NAME]
        g("DOCKERFILE", image_config["dockerfile"])
        repo = image_config["repo"]
        g(
            "DOCKER_REPO",
            f"{DOCKER_REGISTRY}/nsf-noirlab/csdc/antares/antares/{repo}/{ENV}",
        )
        g("CONTEXT", image_config["context"])
    else:
        g_del("DOCKERFILE")
        g_del("DOCKER_REPO")
        g_del("CONTEXT")


def clean():
    """
    Clean the environment of builds, temp files.
    """
    run("rm -rf build dist ANTARES.egg-info")
    run("rm -rf docs/build")
    run("find . -name .DS_Store -delete")
    # Docker runs as root and so the pycache files are owned by root
    run("find . -name '*.pyc' -delete")
    run("find . -name __pycache__ -exec rm -rf {} +")


def docker_login():
    """
    Log in to ANTARES docker-registry.
    """
    run(f"docker login {DOCKER_REGISTRY}")


def docker_clean():
    """
    Delete all docker containers and images.
    """
    meh(f"docker rm -f `docker ps -aq`")
    meh(f"docker rmi -f `docker images -q`")


def build():
    """
    Build docker images for Kubernetes.
    """
    clean()
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        run(
            f"""
            docker build \\
                --file {DOCKERFILE} \\
                --tag {DOCKER_REPO}:{DOCKER_TAG} \\
                {CONTEXT}
        """
        )


def pull_build_push():
    """
    Cached. Does not rebuild entire image unless needed.
    """
    clean()
    # Pull
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        meh(f"docker pull {DOCKER_REPO}:latest")
    # Build
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        run(
            f"""
            docker build \\
                --file {DOCKERFILE} \\
                --cache-from {DOCKER_REPO}:latest \\
                --tag {DOCKER_REPO}:{DOCKER_TAG} \\
                --tag {DOCKER_REPO}:latest \\
                {CONTEXT}
        """
        )
    # Push
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        run(f"docker push {DOCKER_REPO}:{DOCKER_TAG}")
        run(f"docker push {DOCKER_REPO}:latest")


def pull():
    """
    Pull docker images for the current environment.

    eg:
        ./build.py env=staging pull
    """
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        run(f"docker pull {DOCKER_REPO}:{DOCKER_TAG}")


def push():
    """
    Build and push docker images for the current environment.

    eg:
        ./build.py env=staging push
    """
    build()
    for image_name in DOCKER_IMAGES:
        configure(IMAGE_NAME=image_name)
        run(f"docker push {DOCKER_REPO}:{DOCKER_TAG}")


def down():
    """
    Bring down the docker-compose dev environment.
    """
    dc = "COMPOSE_HTTP_TIMEOUT=600 docker-compose"
    run(f"{dc} down")


def up(**kw):
    """
    Bring dev environment online and populate by running integration test.

    Restarts the syste, before running.

    if '--down=false' is given, then the system will not be restarted.
    """
    clean()
    do_down = bool_arg(kw.get("down", True))
    dc = "COMPOSE_HTTP_TIMEOUT=600 docker-compose"
    integration_test = (
        "nosetests -sx test/antares/pipeline/pipeline_integration_test.py"
    )

    if do_down:
        down()
    run(f"{dc} up -d")
    run(f"{dc} exec pipeline " + integration_test)
    print()
    print("Integration test passed.")
    print("Try out the Portal at http://localhost:8080")


def nosetests():
    """
    Bring dev env up and run test suite.
    """
    clean()
    dc = "COMPOSE_HTTP_TIMEOUT=600 docker-compose"
    doctests = "nosetests --with-doctest antares/"
    nosetests = "nosetests -x test/antares/"

    run(f"{dc} up -d")
    run(f"{dc} exec pipeline " + doctests)
    run(f"{dc} exec pipeline " + nosetests)


def deploy_config(app):
    """
    Deploy a k8s configmap.

    eg:
        ./build.py env=staging deploy_config pipeline
    """
    meh(f"kubectl create namespace {NAMESPACE}")
    app = app.replace("-", "_")
    kube_app = app.replace("_", "-")
    run(
        f"""
        kubectl create configmap {kube_app} \\
            --namespace={NAMESPACE} \\
            --from-file={config_file_path(app)}
    """
    )
    run(
        f"""
        kubectl describe configmap {kube_app} \\
            --namespace={NAMESPACE}
    """
    )


def delete_config(app):
    """
    Delete a k8s configmap.

    eg:
        ./build.py env=staging delete_config pipeline
    """
    app = app.replace("-", "_")
    kube_app = app.replace("_", "-")
    meh(
        f"""
        kubectl delete configmap {kube_app} \\
            --namespace={NAMESPACE}
    """
    )


def config_file_path(app, absolute=False):
    app = app.replace("-", "_")
    path = f"./deploy/{app}/{ENV}.yaml"
    if absolute:
        return os.path.abspath(path)
    return path


def config_file_exists(app):
    path = config_file_path(app)
    if os.path.exists(path):
        print(f"Config file {path} exists")
        return True
    else:
        print(f"Config file {path} does not exist")
        return False


def require_yaml():
    try:
        import yaml
    except ImportError:
        print("This command requires `yaml`.")
        print("Please `pip install pyyaml`.")
        sys.exit(1)


def load_config_file(app):
    """
    Load app's config file and return it as a python datastructure.
    """
    require_yaml()
    import yaml

    path = config_file_path(app)
    with open(path, "r") as f:
        return yaml.load(f, Loader=yaml.FullLoader)


def deploy_app(app):
    """
    Deploy a Help app.

    eg:
        ./build.py env=staging deploy_app rq_worker
    """
    app = app.replace("-", "_")
    configure(APP=app)
    kube_app = app.replace("_", "-")
    name = f"{NAMESPACE}-{kube_app}"
    sets = {
        "version": ANTARES_VERSION,
        "environment": ENV,
        "service": app,
        "image.repository": DOCKER_REPO,
        "image.tag": DOCKER_TAG,
    }

    # Get Helm config overrides from yaml config
    config = load_config_file(app)
    replica_count = config.get("REPLICA_COUNT")
    if replica_count is not None:
        sets["replicaCount"] = int(replica_count)
    port_external = config.get("PORT_EXTERNAL")
    if port_external:
        sets["portExternal"] = int(port_external)

    sets_lines = " \\\n            ".join(
        f"--set {key}={val}" for key, val in sets.items()
    )
    run(
        f"""
        helm install \\
            --namespace {NAMESPACE} \\
            --name {name} \\
            {sets_lines} \\
            deploy/{app}/charts/
    """
    )


def delete_app(app):
    """
    Delete a Help amm fromm k8s.

    eg:
        ./build.py env=staging delete_app rq_worker
    """
    app = app.replace("-", "_")
    kube_app = app.replace("_", "-")
    name = f"{NAMESPACE}-{kube_app}"
    meh(f"helm delete --purge {name}")


def scale_app(app, n):
    """
    Scale a k8s deployment up or down.

    eg:
        ./build.py env=staging scale_app rq_worker 50
    """
    kube_app = app.replace("_", "-")
    n = int(n)
    run(
        f"""
        kubectl scale \\
            --namespace={NAMESPACE} \\
            --replicas={n} \\
            deployment/{kube_app}-{ANTARES_VERSION}
    """
    )


def deploy_docker_secret():
    """
    Deploy the Docker image pull secret to k8s, so it can pull images.
    """
    run(f"NAMESPACE={NAMESPACE} ./deploy/image-pull-secret.sh")


def delete_docker_secret():
    """
    Delete docker image pull secret from k8s.
    :return:
    """
    meh(f"kubectl delete --namespace={NAMESPACE} secret/antares-production-deploy")


def deploy_redis():
    """
    Deploy redis to k8s.
    """
    run(f"kubectl create --namespace={NAMESPACE} -f deploy/redis/redis.yaml")


def delete_redis():
    """
    Delete a redis deployment form k8s.
    """
    meh(f"kubectl delete --namespace={NAMESPACE} -f deploy/redis/redis.yaml")


def shell():
    """
    Open the ANTARES python debugging shell locally
    """
    meh("ipython -m antares.shell")


def sort_requirements():
    """
    Sort the contents of the requirements.txt file alphabetically.
    """
    run("cat requirements.txt | sort -f > requirements.txt.sorted")
    run("mv requirements.txt.sorted requirements.txt")


def deploy_datalab(id_rsa_location=None):
    """
    Deploy antares package to the DataLab jupyter kernel.
    """
    for datalab_host in ["gp02.datalab.noao.edu", "gp12.datalab.noao.edu"]:
        connection = f"antares@{datalab_host}"
        if id_rsa_location:
            ssh = f'ssh -i "{id_rsa_location}" {connection}'
        else:
            ssh = f"ssh {connection}"
        kernel = "/data0/sw.tmpfs/antares-kernel-0.4"  # dir of Jupyter kernel
        py = f"{kernel}/bin/python3.7"
        pip = f"{py} -m pip"
        temp_dir = f"{kernel}/temp"
        code_dir = f"{temp_dir}/pipeline"
        cd_code = f"cd {code_dir}"

        # Test connection
        run(f"ping {datalab_host} -c 1")
        run(f"{ssh} whoami")
        run(f"{ssh} {py} --version")
        run(f"{ssh} {pip} --version")

        # Clean local
        clean()
        meh("rm -rf antares/frontend/node_modules")  # No need to rsync this over

        # Clean destination
        meh(f"{ssh} rm -rf {temp_dir}")

        # Install antares
        run(f"{ssh} mkdir -p {code_dir}")
        run(
            f"rsync -vr antares setup.py requirements.txt requirements-filters.txt {connection}:{code_dir}"
        )
        run(f"{ssh} {pip} install --upgrade pip")
        run(f'{ssh} "{cd_code} && {py} setup.py install"')
        run(
            f"{ssh} {pip} install -r {code_dir}/requirements.txt -r {code_dir}/requirements-filters.txt"
        )

        # Install noaodatalab package
        run(f"{ssh} {pip} install --upgrade noaodatalab")


def set_version():
    """
    Update the antares package version.

    Interactive. Takes no arguments.
    """
    print()
    print(f"Current version is: {ANTARES_VERSION}")
    v = input("Enter new version:  ").strip()
    if input(f'Confirm version:   "{v}" [y/N] ').lower().strip() != "y":
        print("Cancelled")
        return
    with open(VERSION_FILE, "w") as f:
        f.write(v)


def ensure_cass_user(cass_admin_user="", cass_admin_pass=""):
    """
    Ensure that the C* user exists for the current pipeline's config.
    """
    clean()
    app = "pipeline"
    config = load_config_file(app)
    if not config.get("CASSANDRA_USER"):
        print(f"App {app} CASSANDRA_USERis not set.")
        print("Nothing to do.")
        return

    # Create user
    print(f'App {app} CASSANDRA_USER = {config["CASSANDRA_USER"]}')
    assert config["CASSANDRA_USER"].endswith("_staging")
    assert config["CASSANDRA_ALERT_KEYSPACE"].endswith("_staging")
    print(f"Using cass_admin_user = {cass_admin_user}")
    assert cass_admin_user
    assert cass_admin_pass
    from antares.rtdb.admin import CassAdmin

    a = CassAdmin(
        hosts=config["CASSANDRA_HOSTS"],
        admin_user=cass_admin_user,
        admin_pass=cass_admin_pass,
    )
    a.grant(
        username=config["CASSANDRA_USER"],
        password=config["CASSANDRA_PASS"],
        keyspaces=[config["CASSANDRA_ALERT_KEYSPACE"]],
        readonly=False,  # READ + WRITE
    )
    a.grant(
        username=config["CASSANDRA_USER"],
        password=config["CASSANDRA_PASS"],
        keyspaces=[config["CASSANDRA_CATALOG_KEYSPACE"]],
        readonly=True,  # READ ONLY
    )
    del a

    # Create DevKit user
    a = CassAdmin(
        hosts=config["CASSANDRA_HOSTS"],
        admin_user=cass_admin_user,
        admin_pass=cass_admin_pass,
    )
    a.grant(
        username="antares_datalab",
        password="pro_Moonrise_epi",
        keyspaces=[
            config["CASSANDRA_ALERT_KEYSPACE"],
            config["CASSANDRA_CATALOG_KEYSPACE"],
        ],
        readonly=True,  # READ ONLY
    )
    del a

    # Test credentials user
    from antares.rtdb.admin import test_login

    test_login(
        hosts=config["CASSANDRA_HOSTS"],
        user=config["CASSANDRA_USER"],
        password=config["CASSANDRA_PASS"],
    )


def deploy_kube(preserve_redis=False, no_build=False):
    """
    Create kube namespace and deploy all apps with existing config files.

    Optional flags:

    --preserve_redis  # if given, do not touch Redis
    """
    deployed = []
    skipped = []

    require_yaml()  # Will be needed by deploy_app() below.

    # Abort of no config files exist for current `ENV`
    if not any(config_file_exists(app) for app in KUBE_APPS):
        print(f"No config files found for env={ENV}")
        print("Abort")
        sys.exit(1)

    # Build & push docker images
    if not no_build:
        push()

    # Create kube namespace, if it does not exist
    meh(f"kubectl create namespace {NAMESPACE}")

    # Delete all config and deployments
    delete_docker_secret()
    for app in sorted(KUBE_APPS):
        delete_config(app)
        delete_app(app)

    # Re-deploy Redis and wait for it to boot
    if not preserve_redis:
        delete_redis()
        deploy_redis()
        with quiet():
            while not list_running_pods("redis"):
                print("Waiting for redis...")
                time.sleep(1)
        deployed.append("redis")
    else:
        skipped.append("redis")

    # Deploy config and apps
    deploy_docker_secret()
    for app in sorted(KUBE_APPS):
        if config_file_exists(app):
            deploy_config(app)
            deploy_app(app)
            deployed.append(app)
        else:
            skipped.append(app)

    # Print report of what was done
    print("Done\n")
    for app in deployed:
        print(f"App {app} was DEPLOYED.")
    print()
    for app in skipped:
        print(f"App {app} was skipped.")


def delete_kube():
    """
    Tear down a kubernetes environment, but don't delete the namespace.
    """
    delete_docker_secret()
    delete_redis()
    for app in KUBE_APPS:
        delete_config(app)
        delete_app(app)


def delete_namespace(y=False):
    """
    Delete a kubernetes namespace.

    Optional flags:
    -y  # Do not prompt user for confirmation
    """
    y = y in ("t", "true", True)
    print()
    assert NAMESPACE.endswith("-staging")
    if y or ask(f"Delete namespace {NAMESPACE}?", default=False):
        run(f"kubectl delete namespace {NAMESPACE}")


def list_pods(pattern="/*", status="/*"):
    """
    List pods in K8s which match a grep pattern and a status.

    eg:
        ./build.py env=staging list_pods --status=Running
        ./build.py env=production list_pods --pattern=pipe --status=Running
    """
    pattern = pattern.replace("_", "-")
    pods = out(
        f"""
        kubectl get pod --namespace={NAMESPACE} \\
            | tail -n +2 \\
            | grep '{status}' \\
            | awk '{{print $1}}' \\
            | grep '{pattern}' \\
            | sort
    """
    )
    lines = (line.strip() for line in pods.split("\n"))
    return list(filter(bool, lines))


def list_running_pods(pattern="/*"):
    """
    List running pods in K8s which match a grep pattern.

    eg:
        ./build.py env=staging list_running_pods
        ./build.py env=staging list_running_pods pipeline
        ./build.py env=staging list_running_pods rq_worker
        ./build.py env=staging list_running_pods rq_
    """
    return list_pods(pattern, "Running")


def kp(
    cmd, pattern, i=0, loop=True, require_running=True, sleep=0, raise_failures=False
):
    """
    Loop and run `cmd` on a pod running in Kubernetes.

    :param cmd: a shell command requiring template variables `NAMESPACE` and `pod`.
    :param pattern: pattern to grep pods by
    :param i: index of pod to chose from grep results
    :param loop: if False, exit after first successful execution of `cmd`
    :param require_running: only choose pod which is Running
    :param sleep: seconds to sleep between loops, if `loop` is True
    :param raise_failures: raise exception if command fails in remote container
    """
    pattern = pattern.replace("_", "-")
    i = int(i)
    while True:
        with quiet():
            if require_running:
                pods = list_running_pods(pattern)
            else:
                pods = list_pods(pattern)
        if len(pods) >= i + 1:
            pod = pods[i]
            print()
            print()
            if raise_failures:
                run(cmd.format(NAMESPACE=NAMESPACE, pod=pod))
            else:
                meh(cmd.format(NAMESPACE=NAMESPACE, pod=pod))
            if loop:
                continue
            else:
                break
        else:
            print(".", end="", flush=True)
            time.sleep(sleep)


def dc(cmd, pattern, loop=True, sleep=0, raise_failures=False):
    """
    Loop and run `cmd` with `service` running in local docker-compose.

    :param cmd: a shell command requiring template variables `service`.
    :param pattern: pattern to grep docker-compose services
    :param loop: if True, call repeatedly
    :param sleep: time to sleep between loops
    :param raise_failures: raise exception if command fails
    """

    def get_services(p):
        lines = out(f"docker-compose ps --services | grep {p}").strip().split()
        return [l.strip() for l in lines if l.strip()]

    while True:
        with quiet():
            services = get_services(pattern)
        if services:
            service = services[0]
            print()
            print()
            if raise_failures:
                run(cmd.format(service=service))
            else:
                meh(cmd.format(service=service))
            if loop:
                continue
            else:
                break
        else:
            print(".", end="", flush=True)
            time.sleep(sleep)


def kpl(pattern, i=0):
    """
    "Kubernetes Pod Log": tail logs from a pod

    eg:
        ./build.py env=production kpl pipeline
    """
    kp("kubectl logs -f --namespace={NAMESPACE} {pod}", pattern, i)


def kplc(pattern, delay=5):
    """
    "Kubernetes Pod Log Cycle": Print logs from multiple pods in a loop

    eg:
        ./build.py env=production kplc pipeline
    """
    delay = int(delay)
    while True:
        for pod in list_pods(pattern):
            meh(f"kubectl logs --namespace={NAMESPACE} {pod}")
            time.sleep(delay)


def kps(pattern, i=0):
    """
    "Kubernetes Pod Shell": open a shell pod

    eg:
        ./build.py env=production kps pipeline
    """
    kp("kubectl exec --namespace={NAMESPACE} -it {pod} -- bash", pattern, i, loop=False)


def kpp(pattern, i=0):
    """
    "Kubernetes Pod Python": get a python shell in a pod

    eg:
        ./build.py env=production kpp pipeline
    """
    kp(
        "kubectl exec --namespace={NAMESPACE} -it {pod} -- python -m antares.shell",
        pattern,
        i,
        sleep=1,
    )


def kpd(pattern, i=0):
    """
    "Kubernetes Pod Describe": describe k8s pod

    eg:
        ./build.py env=production kpd pipeline
    """
    kp(
        "kubectl describe --namespace={NAMESPACE} pod/{pod}",
        pattern,
        i,
        loop=False,
        require_running=False,
    )


def kpe(pattern, *cmd):
    """
    "Kubernetes Pod Execute": execute a command on a k8s pod

    eg:
        ./build.py env=production kpe pipeline "ls -al"
    """
    cmd = " ".join(cmd)
    kp(
        "kubectl exec --namespace={NAMESPACE} {pod} -- " + cmd,
        pattern,
        loop=False,
        require_running=True,
        raise_failures=True,
    )


def dcl(pattern):
    """
    "docker-compose logs": view logs from a docker-compose service

    eg:
        ./build.py dcl index_worker
    """
    dc("docker-compose logs -f {service}", pattern)


def dce(pattern, cmd):
    """
    "docker-compose exec": exec a command in a docker-compose service

    eg:
        ./build.py dce pipeline "ls -al"
    """
    dc("docker-compose exec {service} " + cmd, pattern, loop=False)


def dcp(pattern):
    """
    "docker-compose Python": open python shell in a docker-compose service

    eg:
        ./build.py dcp pipeline
    """
    dc("docker-compose exec {service} python -m antares.shell", pattern)


def make_docs(i=None, u=None):
    """
    Build the Sphinx docs.

    :param i: If `-i` is given, pip install antares before building docs
    :param u: If `-u` is given, pip uninstall antares after building docs
    """
    if i:
        run("pip install .")
    run("cd docs/ && make html")
    if u:
        run("pip uninstall antares -y")


def set_go_signal():
    """
    Set the 'go' flag in Redis, telling pipeline to start pulling alerts.
    """
    from antares.redis import redis

    redis.set("go", 1)


if __name__ == "__main__":
    main()
